package com.forms.library.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.forms.library.baseUtil.net.Action;
import com.forms.library.baseUtil.net.Controller;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.net.UiObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 14-9-11.
 */
public class BaseFragment extends Fragment implements UiObject {
    protected BaseActivity baseActivity;
    protected Controller controller;
    private List<Action> actionList = new ArrayList<>();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        baseActivity = (BaseActivity) activity;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    public BaseActivity getBaseActivity() {
        return baseActivity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
//        resetUiOject();
        super.onDestroy();
    }

    @Override
    public void requestSuccess(RetCode retCode, Object response) {

    }

    @Override
    public void requestFalied(RetCode retCode) {

    }

    @Override
    public void addAction(Action action) {
        actionList.add(action);
    }

    @Override
    public void resetUiOject() {
        for (Action action : actionList) {
            action.clear();
        }
    }
}
