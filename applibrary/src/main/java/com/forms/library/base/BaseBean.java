package com.forms.library.base;

import  com.forms.library.baseUtil.db.annotation.Id;

import java.io.Serializable;

/**
 * Created by user on 14-9-12.
 */
public class BaseBean implements Serializable {
    @Id
    public Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
