package com.forms.library.base;

import android.app.Application;

import com.forms.library.baseUtil.cache.CacheBean;
import com.forms.library.baseUtil.exception.AppException;
import com.forms.library.tools.FileUtils;
import com.forms.library.tools.FormsUtil;

/**
 * Created by hwt on 14-9-11.
 * <p/>
 * 应用Bean ,添加数据缓存管理，登录用户管理，activity管理功能。
 */
public class BaseApplication extends Application {
    protected CacheBean cacheBean;//缓存bean
    protected ActivityTrans activityTrans = new ActivityTrans();

    @Override
    public void onCreate() {
        super.onCreate();
        /*应用初始化*/
        AppException appException = AppException.getAppExceptionHandler(getApplicationContext());//全局异常捕获
        Thread.setDefaultUncaughtExceptionHandler(appException);
        cacheBean = new CacheBean();
        FormsUtil.getDisplayMetrics(this);
    }

    /**
     * 获得缓存工具
     *
     * @return CacheBean
     */
    public CacheBean getCacheBean() {
        return cacheBean;
    }

    /**
     * 退出应用
     * 退出应用
     */
    public void exit() {
        exit(null);
    }

    /**
     * 退出应用
     *
     * @param exitAppListener 应用退出时监听，在应用完全退出前可进行额外操作。
     */
    public void exit(OnExitAppListener exitAppListener) {
        //应用退出监听
        if (exitAppListener != null) exitAppListener.onExit();
        //清除应用内缓存
        FileUtils.deleteFilesByDirectory(getCacheDir());
//        FileUtils.deleteFilesByDirectory(getFilesDir());
//        FileUtils.deleteFilesByDirectory(getExternalCacheDir());
        //清除图片缓存
//        imageLoader.clearMemoryCache();
//        imageLoader.clearDiskCache();
        //清除临时缓存，结束所有已打开activity
        cacheBean.clear();
        //杀进程，关闭应用
        android.os.Process
                .killProcess(android.os.Process.myPid());
        android.app.ActivityManager activityMgr = (android.app.ActivityManager) getSystemService(ACTIVITY_SERVICE);
        activityMgr.restartPackage(getPackageName());
        System.exit(0);
        System.gc();
    }

    /**
     * 应用退出时监听
     */
    public interface OnExitAppListener {
        public void onExit();
    }

    public ActivityTrans getActivityTrans() {
        return activityTrans;
    }
}
