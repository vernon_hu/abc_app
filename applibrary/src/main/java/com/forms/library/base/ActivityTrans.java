package com.forms.library.base;

import java.util.LinkedList;

/**
 * activity交易流程管理。
 * <br/>
 * 在一次交易中涉及到多个 activity 时用来管理 activity 队列的生命周期。
 * Created by hwt on 4/1/15.
 */
public class ActivityTrans {
    private LinkedList<Entry> activityTrans = new LinkedList<Entry>();//activity 队列
    private Entry tempEntry = new Entry();

    /**
     * 添加activity到队尾
     *
     * @param activity
     */
    public void addActivity(BaseActivity activity) {
        if (activity != null) {
            Entry entry = new Entry(activity.getID(), activity);
            activityTrans.add(entry);
        } else {
            throw new NullPointerException("Params BaseActivity is NUll.");
        }
    }

    /**
     * 取出指定activity
     *
     * @param ID
     * @return
     */
    public BaseActivity getByID(String ID) {
        Entry entry = activityTrans.get(activityTrans.indexOf(tempEntry.setKey(ID)));
        return entry != null ? entry.getActivity() : null;
    }

    /**
     * 移除指定activity
     *
     * @param ID
     * @return
     */
    public BaseActivity remove(String ID) {
        Entry entry = activityTrans.remove(activityTrans.indexOf(tempEntry.setKey(ID)));
        return entry != null ? entry.getActivity() : null;
    }

    /**
     * 移除指定activity
     *
     * @param activity
     * @return
     */
    public BaseActivity remove(BaseActivity activity) {
        return remove(activity.getID());
    }

    /**
     * 结束交易，清除所有 activity
     */
    public void completed() {
        for (Entry entry : activityTrans) {
            activityTrans.remove(entry);
            entry.getActivity().finish();
        }
    }

    /**
     * 结束掉最后一个 activity
     */
    public void finish() {
        activityTrans.getLast().getActivity().finish();
    }

    /**
     * 结束指定activity
     *
     * @param ID
     */
    public void finish(String ID) {
        int position = activityTrans.indexOf(tempEntry.setKey(ID));
        finish(activityTrans.get(position).getActivity());
    }


    /**
     * 结束指定activity
     *
     * @param baseActivity
     */
    public void finish(BaseActivity baseActivity) {
        if (baseActivity != null)
            baseActivity.finish();
    }

    /**
     * @param startNum 开始结束的 Activity位置
     * @param endNum   取后结束的 activity 位置
     */
    public void finishFormStartNum(int startNum, int endNum) {
        if (startNum < activityTrans.size()) {
            while (startNum > 0 && endNum > 0) {
                Entry entry = activityTrans.get(--startNum);
                endNum--;
                entry.getActivity().finish();
            }
        }
    }

    /**
     * @param ID        当前activity ID
     * @param finishNum 指定要被结束的activity数目
     */
    public void finishFormPosition(String ID, int finishNum) {
        int position = activityTrans.indexOf(tempEntry.setKey(ID));
        while (finishNum > 0 && position > 0) {
            Entry entry = activityTrans.get(position--);
            finishNum--;
            entry.getActivity().finish();
        }
    }

    /**
     * @param ID        当前activity ID
     * @param finishNum 指定要被结束的activity数目
     */
    public void finishWithOutPosition(String ID, int finishNum) {
        int position = activityTrans.indexOf(tempEntry.setKey(ID));
        while (finishNum > 0 && position > 0) {
            Entry entry = activityTrans.get(--position);
            finishNum--;
            entry.getActivity().finish();
        }
    }

    /**
     * 结束队列中指定位置的 activity
     *
     * @param position
     */
    public void finishPosition(int position) {
        if (position < activityTrans.size())
            activityTrans.get(position).getActivity().finish();
    }


    /**
     * 结束指定activity并返回指定结果
     *
     * @param ID
     * @param result
     */
    public void finishForResult(String ID, int result) {
        finishForResult(remove(ID), result);
    }

    /**
     * 查找指定 activity 在队列中的位置
     *
     * @param ID
     * @return
     */
    public int getPosition(String ID) {
        return activityTrans.indexOf(tempEntry.setKey(ID));
    }

    /**
     * 结束指定activity并返回指定结果
     *
     * @param baseActivity
     * @param result
     */
    public void finishForResult(BaseActivity baseActivity, int result) {
        if (baseActivity != null) {
            baseActivity.finishForBack(result);
        }
    }

    /**
     *
     */
    public class Entry {
        String key;
        BaseActivity activity;

        public Entry() {
        }

        public Entry(String key, BaseActivity activity) {
            this.key = key;
            this.activity = activity;
        }

        public String getKey() {
            return key;
        }

        public Entry setKey(String key) {
            this.key = key;
            return this;
        }

        public BaseActivity getActivity() {
            return activity;
        }

        public Entry setActivity(BaseActivity activity) {
            this.activity = activity;
            return this;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Entry) {
                return ((Entry) o).getKey().equals(key);
            }
            return false;
        }
    }
}
