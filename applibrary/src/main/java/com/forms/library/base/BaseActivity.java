package com.forms.library.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import com.forms.library.baseUtil.cache.CacheBean;
import com.forms.library.baseUtil.exception.AppException;
import com.forms.library.baseUtil.net.Action;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.net.UiObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by user on 14-9-11.
 * <p>activity基类，</p>
 */
public class BaseActivity extends AppCompatActivity implements UiObject {
    protected BaseApplication baseApp;
    protected CacheBean cacheBean;
    protected String ID;

    private int state = 0;

    public final static int CREATED = 0;
    public final static int ON_PAUSED = 1;
    public final static int ON_STARTED = 2;
    public final static int ON_STOPED = 3;
    public final static int ON_DISTORY = 4;
    public final static int ON_RESULT_BACK = 5;
    public final static String START_TRANS = "startTrans";
    private List<Action> actionList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ID = UUID.randomUUID().toString();
        Thread.setDefaultUncaughtExceptionHandler(AppException.getAppExceptionHandler(this));
        baseApp = (BaseApplication) getApplication();
        cacheBean = baseApp.getCacheBean();
        if (getIntent().getBooleanExtra(START_TRANS, false)) {
            baseApp.getActivityTrans().addActivity(this);
        }
    }

    public CacheBean getCacheBean() {
        return cacheBean;
    }

    /**
     * 跳转到指定activity
     */
    public void callMe(Class<? extends BaseActivity> meClass) {
        /*通用打开ACTIVITY方法*/
        goTo(meClass);
    }

    /**
     * 跳转到指定activity
     */
    public void callMe(Class<? extends BaseActivity> meClass, Bundle bundle) {
        /*通用打开ACTIVITY方法*/
        goTo(meClass, bundle);
    }

    public void goTo(Class<?> meClass) {
        goTo(meClass, null);
    }

    public void goTo(Class<?> meClass, Bundle bundle) {
        Intent intent = new Intent(this, meClass);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }


    public void startTrans(Class<? extends BaseActivity> meClass) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(START_TRANS, true);
        callMe(meClass);
    }

    /**
     * 跳转到指定activity
     */
    public void callMeForBack(Class<? extends BaseActivity> meClass, int requestCode) {
        /*通用打开ACTIVITY方法*/
        callMeForBack(meClass, null, requestCode);
    }

    /**
     * @param backResult
     */
    public void finishForBack(int backResult) {
        setResult(backResult);
        finish();
    }

    /**
     * 跳转到指定activity
     */
    public void callMeForBack(Class<? extends BaseActivity> meClass, Bundle bundle, int requestCode) {
        Intent intent = new Intent(this, meClass);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, requestCode);
    }

    /**
     * <p>传递onActivityResult方法到fragment</p>
     * <ul>如继承activity中重写了此方法，如还需要传递此回调到activity所有的fragment中则需再调一次：</ul><br/>
     * super.onActivityResult(requestCode, resultCode, data);
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        state = ON_RESULT_BACK;
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null && fragments.size() > 0) {
            for (Fragment fragment : fragments) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        state = ON_PAUSED;
    }

    @Override
    protected void onResume() {
        super.onResume();
        state = CREATED;
        for (Action action : actionList) {
            action.reset(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        state = ON_STARTED;
    }

    @Override
    protected void onStop() {
        super.onStop();
        state = ON_STOPED;
    }

    @Override
    protected void onDestroy() {
        state = ON_DISTORY;
        super.onDestroy();
    }

    public int getState() {
        return state;
    }

    public String getID() {
        return ID;
    }


    @Override
    public void requestSuccess(RetCode retCode, Object response) {

    }

    @Override
    public void requestFalied(RetCode retCode) {

    }

    @Override
    public void addAction(Action action) {
        actionList.add(action);
    }

    @Override
    public void resetUiOject() {
        for (Action action : actionList) {
            action.clear();
        }
    }


}
