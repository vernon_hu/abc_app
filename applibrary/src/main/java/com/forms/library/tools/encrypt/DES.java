package com.forms.library.tools.encrypt;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * DES加解密
 * 
 * @author kejia
 * 
 */
public class DES {
	
//	 private static byte[] iv = { 1, 2, 3, 4, 5, 6, 7, 8 };
//	 public  static byte[] iv ="10000000".;

	/**
	 * 加密
	 * <p>
	 * 以String明文输入，String密文输出
	 * </p>
	 * 
	 * @param str
	 * @param key
	 * @return String
	 */
	public static String encrypt(String str, String key) {
		byte[] byteMi = null;
		byte[] byteMing = null;
		String strMi = "";
		String URLEncodeStr = null;

		try {
			byteMing = str.getBytes("UTF8");
			byteMi = getEncCode(byteMing, key);
			strMi = Base64.encode(byteMi);
			URLEncodeStr = URLEncoder.encode(strMi, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			byteMing = null;
			byteMi = null;
		}

		return URLEncodeStr;
	}

	/**
	 * 解密
	 * <p>
	 * 以String密文输入，String明文输出
	 * </p>
	 * 
	 * @param str
	 * @param key
	 * @return String
	 */
	public static String decrypt(String str, String key,String iv) {
		byte[] byteMing = null;
		byte[] byteMi = null;
		String strMing = "";
		String URLDecodeStr;
		try {
			URLDecodeStr = URLDecoder.decode(str, "UTF-8");
			byteMi = Base64.decode(URLDecodeStr);
//			byteMi = Base64.decode(str);
			byteMing = getDesCode(byteMi, key,iv);
			strMing = new String(byteMing, "UTF8");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			byteMing = null;
			byteMi = null;
		}

		return strMing;
	}

	/**
	 * 加密
	 * <p>
	 * 以byte[]明文输入，byte[]密文输出
	 * </p>
	 * 
	 * @param byteS
	 * @param key
	 * @return byte[]
	 */
	private static byte[] getEncCode(byte[] byteS, String key) {
		byte[] byteFina = null;
		Cipher cipher;

		try {
			cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
			SecretKeySpec KeySpc = new SecretKeySpec(key.getBytes(), "DES");
			cipher.init(Cipher.ENCRYPT_MODE, KeySpc);
			// IvParameterSpec zeroIv = new IvParameterSpec(iv);
			// cipher.init(Cipher.ENCRYPT_MODE, KeySpc, zeroIv);
			byteFina = cipher.doFinal(byteS);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cipher = null;
		}

		return byteFina;
	}

	/**
	 * 解密
	 * <p>
	 * 以byte[]密文输入，以byte[]明文输出
	 * </p>
	 * 
	 * @param byteD
	 * @param key
	 * @return byte[]
	 */
	private static byte[] getDesCode(byte[] byteD, String key,String iv) {
		Cipher cipher;
		byte[] byteFina = null;

		try {
//			cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
			cipher = Cipher.getInstance("DES/ECB/NoPadding");
			SecretKeySpec KeySpc = new SecretKeySpec(key.getBytes("UTF8"), "DES");
//			cipher.init(Cipher.DECRYPT_MODE, KeySpc);
			 IvParameterSpec zeroIv = new IvParameterSpec(iv.getBytes("UTF8"));
			 cipher.init(Cipher.DECRYPT_MODE, KeySpc, zeroIv);
			byteFina = cipher.doFinal(byteD);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cipher = null;
		}

		return byteFina;
	}

}
