package com.forms.library.tools.encrypt;


import com.forms.library.baseUtil.logger.Logger;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by korey on 14-12-23.
 */
public class DESUtils {
    /**
     * 小达人DES KEY 解密
     *
     * @param key
     * @param iv
     * @param content
     * @return
     * @throws Exception
     */
    public final static String decryptBase64(byte[] key, byte[] iv, String content) throws Exception {
        Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
        SecretKeySpec skeySpec = new SecretKeySpec(key, "DES");
        Logger.d("block size:%d", cipher.getBlockSize());
        Logger.d("iv size:%d", ivParameterSpec.getIV().length);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivParameterSpec);
        byte[] contentByte = Base64.decode(content);
        contentByte = cipher.doFinal(contentByte);
        content = new String(contentByte, "utf-8");
        return content;
    }
}
