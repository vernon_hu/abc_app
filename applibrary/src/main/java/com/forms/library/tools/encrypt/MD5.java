package com.forms.library.tools.encrypt;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5加密
 * 
 * @author kejia
 *
 */
public class MD5 {

	/**
	 * 加密
	 * 
	 * @param str
	 * @return 加密后字符串（默认小写）
	 */
	public static String encrypt(String str) {
		if (str == null || str.length() < 1) {
			return "";
		}
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			byte[] m = md5.digest(str.getBytes());
			StringBuffer sb = new StringBuffer("");
			for (int i = 0; i < m.length; i++) {
				int b = (m[i]) & 0xff;
				if (b < 16) {
					sb.append("0");
				}
				sb.append(Integer.toHexString(b));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "";
		}
	}
	
}
