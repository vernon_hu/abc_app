package com.forms.library.tools.encrypt;

import android.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.net.URLEncoder;
import java.security.*;

/**
 * RSA加解密
 * 
 * @author kejia
 * 
 */
public class RSA {
	
	public static final String ALGORITHM = "RSA";
	private static final int keySize = 1024;

//	public static final String ALGORITHM = "RSA/ECB/PKCS1Padding";
//	private static final int keySize = 1024;
	
	private PublicKey publicKey = null;
	private PrivateKey privateKey = null;

	/**
	 * Init java security to add BouncyCastle as an RSA provider
	 */
	public boolean init() {
		KeyPairGenerator kpg = null;
		try {
			kpg = KeyPairGenerator.getInstance(ALGORITHM);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return false;
		}

		kpg.initialize(keySize);
		KeyPair kp = kpg.genKeyPair();
		publicKey = kp.getPublic();
		privateKey = kp.getPrivate();

		return true;
	}

	public PublicKey getPubKey() {
		return publicKey;
	}

	public PrivateKey getPrvtKey() {
		return privateKey;
	}

	/**
	 * 加密
	 * <p>
	 * String明文输入，String密文输出
	 * </p>
	 * 
	 * @param data
	 * @param peerPubKey
	 * @return String
	 */
	public String encrypt(String data, java.security.Key peerPubKey) {
		byte[] byteMi = null;
		byte[] byteMing = null;
		String encStr = "";
		String URLEncodeStr = null;

		try {
			byteMing = data.getBytes("UTF8");
			byteMi = encode(byteMing, peerPubKey);
			encStr = Base64.encodeToString(byteMi, Base64.DEFAULT);
			URLEncodeStr = URLEncoder.encode(encStr, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return URLEncodeStr;
	}

	/**
	 * 解密
	 * <p>
	 * 以String密文输入，String明文输出
	 * </p>
	 * 
	 * @param str
	 * @param localPrvtKey
	 * @return String
	 */
	public String decrypt(String str, java.security.Key localPrvtKey) {
		byte[] byteMing = null;
		byte[] byteMi = null;
		String decStr = "";
		try {
			byteMi = Base64.decode(str, Base64.DEFAULT);
			byteMing = decode(byteMi, localPrvtKey);
			decStr = new String(byteMing, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			byteMing = null;
			byteMi = null;
		}

		return decStr;
	}

	/**
	 * 加密
	 * <p>
	 * 以byte[]明文输入，byte[]密文输出
	 * </p>
	 * 
	 * @param byteS
	 * @param peerPubKey
	 * @return byte[]
	 */
	private byte[] encode(byte[] data, java.security.Key peerPubKey) {
		Cipher cipher = null;
		byte[] cipherData = null;

		try {
			cipher = Cipher.getInstance(ALGORITHM);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
			return null;
		}

		try {
			cipher.init(Cipher.ENCRYPT_MODE, peerPubKey);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			return null;
		}

		try {
			cipherData = cipher.doFinal(data);
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
			return null;
		} catch (BadPaddingException e) {
			e.printStackTrace();
			return null;
		}

		return cipherData;
	}

	/**
	 * 解密
	 * <p>
	 * 以byte[]密文输入，以byte[]明文输出
	 * </p>
	 * 
	 * @param byteD
	 * @param localPrvtKey
	 * @return byte[]
	 */
	private byte[] decode(byte[] byteD, java.security.Key localPrvtKey) {
		Cipher cipher;
		byte[] byteFina = null;

		try {
			cipher = Cipher.getInstance(ALGORITHM);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
			return null;
		}

		try {
			cipher.init(Cipher.DECRYPT_MODE, localPrvtKey);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			return null;
		}

		try {
			byteFina = cipher.doFinal(byteD);
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
			return null;
		} catch (BadPaddingException e) {
			e.printStackTrace();
			return null;
		}

		return byteFina;
	}
	
}
