package com.forms.library.widget.lockpattern.external;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by korey on 2015/5/13.
 */
public class PatternUtil {
    public static final String PATTERN_POINT_SHAREPREF = "PATTERN_POINT_SHAREPREF";
    public static final String PATTERN_POINT = "PATTERN_POINT";
    public static final String PATTERN_COUNTTIME = "PATTERN_COUNTTIME";

    public static boolean isPattern(String patternStr) {
        if (!TextUtils.isEmpty(patternStr)) {
            String tempStr = patternStr.replaceAll("-", "");
            char[] tempChars = tempStr.toCharArray();
            for (char temp : tempChars) {
                if (!Character.isDigit(temp)) {
                    return false;
                }
            }
            if (tempChars.length % 2 == 0) return true;
        }
        return false;
    }

    /**
     * @param patterns
     * @return
     */
    public static String patternToStr(List<Point> patterns) {
        StringBuilder sb = new StringBuilder();
        if (patterns != null) {
            for (Point point : patterns) {
                sb.append(point.toString()).append("-");
            }
        }
        return sb.toString();
    }

    /**
     * @param patternStr
     * @return
     */
    public static List<Point> parsePatterns(String patternStr) {
        List<Point> points = new ArrayList<>();
        if (!TextUtils.isEmpty(patternStr)) {
            String[] patterns = patternStr.split("-");
            List<String> tempList = Arrays.asList(patterns);
            String x = null;
            String y = null;
            Point point = null;
            for (int i = 0; i < tempList.size(); i++) {
                if (i % 2 == 0) {
                    point = new Point();
                    x = tempList.get(i);
                }
                if (i % 2 == 1) {
                    y = tempList.get(i);
                }
                if (point != null && x != null && y != null) {
                    point.set(Integer.valueOf(x), Integer.valueOf(y));
                    points.add(point);
                    point = null;
                    x = null;
                    y = null;
                }
            }
        }
        return points;
    }

    public static String getPattern(Context context, String phone) {
        SharedPreferences sharedPref = context.getSharedPreferences(PATTERN_POINT_SHAREPREF, Context.MODE_PRIVATE);
        return sharedPref.getString(PATTERN_POINT + "_" + phone, null);
    }

    public static void savePattern(Context context, String phone, String pattern) {
        SharedPreferences sharedPref = context.getSharedPreferences(PATTERN_POINT_SHAREPREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PATTERN_POINT + "_" + phone, pattern);
        editor.apply();
    }

    public static long getMillisUntilFinished(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(PATTERN_POINT_SHAREPREF, Context.MODE_PRIVATE);
        return sharedPref.getLong(PATTERN_COUNTTIME, 0l);
    }

    public static void saveMillisUntilFinished(Context context, long mMillisUntilFinished) {
        SharedPreferences sharedPref = context.getSharedPreferences(PATTERN_POINT_SHAREPREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(PATTERN_COUNTTIME, mMillisUntilFinished);
        editor.apply();
    }

//┏┓　　　┏┓
//┏┛┻━━━┛┻┓
//┃　　　　　　　┃ 　
//┃　　　━　　　┃
//┃　┳┛　┗┳　┃
//┃　　　　　　　┃
//┃　　　┻　　　┃
//┃　　　　　　　┃
//┗━┓　　　┏━┛
//┃　　　┃  神兽保佑　　　　　　　　
//┃　　　┃  代码无BUG！
//┃　　　┗━━━┓
//┃　　　　　　　┣┓
//┃　　　　　　　┏┛
//┗┓┓┏━┳┓┏┛
// ┃┫┫　┃┫┫
//
}
