package com.forms.library.widget.other;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.ImageView;

import com.abc.ABC_SZ_APP_library.R;

public class XdrDialog extends AlertDialog {

    private ImageView iv;

    public XdrDialog(Context context) {
        super(context, R.style.transparent_Dialog);
    }

    protected XdrDialog(Context context, boolean cancelable,
                        OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_xdr);
        iv = (ImageView) findViewById(R.id.iv_loading);
        iv.setImageResource(R.drawable.xdr_run_animation);
    }

    @Override
    public void show() {
        super.show();
        AnimationDrawable drawable = (AnimationDrawable) iv.getDrawable();
        drawable.start();
    }
}
