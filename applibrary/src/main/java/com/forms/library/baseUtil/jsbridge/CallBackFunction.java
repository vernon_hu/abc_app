package com.forms.library.baseUtil.jsbridge;

public interface CallBackFunction {

    void onCallBack(String data);

}
