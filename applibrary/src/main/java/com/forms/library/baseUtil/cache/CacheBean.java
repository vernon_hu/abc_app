package com.forms.library.baseUtil.cache;


import java.util.LinkedHashMap;
import java.util.List;

import com.forms.library.baseUtil.logger.Logger;

/**
 * 公共缓存对象
 */
public class CacheBean {
    private static CacheMap<String, Object> cacheMap;

    /**
     * 创建缓存容器，无上限
     */
    public CacheBean() {
        cacheMap = new CacheMap<String, Object>(-1);
    }

    /**
     * 创建缓存容器，如给定maxSize小于0则不限定缓存数量，如给定值大于0则当添加缓存数据超出时删除最先添加的数据。
     *
     * @param maxSize
     */
    public CacheBean(int maxSize) {
        cacheMap = new CacheMap<String, Object>(maxSize);
    }

    /**
     * 取出缓存数据
     *
     * @param key 缓存数据对应key值
     * @return String类型数据
     */
    public String getStringCache(String key) {
        return getCache(key, String.class);
    }

    /**
     * 取出List类型的缓存数据。
     *
     * @param key 缓存数据对应key值
     * @return Long类型数据
     */
    public Long getLongCache(String key) {
        return getCache(key, Long.class);
    }

    /**
     * 取出List类型的缓存数据。
     *
     * @param key 缓存数据对应key值
     * @return Double类型数据
     */
    public Double getDoubleCache(String key) {
        return getCache(key, Double.class);
    }

    /**
     * 取出List类型的缓存数据。
     *
     * @param tClass 缓存数据类型
     * @param <T>    缓存数据类型
     * @return 缓存数据
     */
    public <T> List<T> getCacheArray(Class<T> tClass) {
        return getCacheArray(tClass.getName(), tClass);
    }

    /**
     * 取出List类型的缓存数据并从缓存容器中清除。
     *
     * @param tClass 缓存数据类型
     * @param <T>    缓存数据类型
     * @return 缓存数据
     */
    public <T> List<T> getCacheArrayClear(Class<T> tClass) {
        return getCacheArrayClear(tClass.getName(), tClass);
    }

    /**
     * 取出List类型的缓存数据。
     *
     * @param key 缓存对应key
     * @param <T> 缓存数据类型
     * @return 缓存数据
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> getCacheArray(String key, Class<T> tClass) {
        return (List<T>) get(key);
    }

    /**
     * 取出List类型的缓存数据并从缓存容器中清除。
     *
     * @param key 缓存对应key
     * @param <T> 缓存数据类型
     * @return 缓存数据
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> getCacheArrayClear(String key, Class<T> tClass) {
        return (List<T>) get(key);
    }

    /**
     * 添加缓存对象。
     *
     * @param obj 缓存数据
     */
    public void putCache(Object obj) {
        if (obj != null) {
            put(obj.getClass().getName(), obj);
        } else {
            Logger.d("错误！缓存对象不能为NULL obj=NULL");
        }
    }

    /**
     * 取出缓存数据。
     *
     * @param tClass 缓存数据类型
     * @param <T>    缓存数据类型
     * @return 缓存数据
     */
    public <T> T getCache(Class<T> tClass) {
        return getCache(tClass.getName(), tClass);
    }

    /**
     * 取出缓存数据并从缓存容器中清除。
     *
     * @param tClass 缓存数据类型
     * @param <T>    缓存数据类型
     * @return 缓存数据
     */
    public <T> T getCacheClear(Class<T> tClass) {
        return getCacheClear(tClass.getName(), tClass);
    }

    /**
     * 取出缓存数据，如未找到或数据为null则创建一个对象使非空。
     *
     * @param tClass 缓存数据类型
     * @param <T>    缓存数据类型
     * @return 缓存数据
     */
    public <T> T getNotNullCache(Class<T> tClass) {
        T t = getCache(tClass);
        if (t == null) {
            try {
                t = tClass.newInstance();
                putCache(t);
                return t;
            } catch (InstantiationException e) {
                Logger.d(e.getMessage(), e);
            } catch (IllegalAccessException e) {
                Logger.d(e.getMessage(), e);
            }
        }
        return t;
    }

    /**
     * 取出缓存数据并从缓存容器中清除，如未找到或数据为null则创建一个对象使非空。
     *
     * @param tClass 缓存数据类型
     * @param <T>    缓存数据类型
     * @return 缓存数据
     */
    public <T> T getNotNullCacheClear(Class<T> tClass) {
        T t = getCacheClear(tClass);
        if (t == null) {
            try {
                t = tClass.newInstance();
                putCache(t);
                return t;
            } catch (InstantiationException e) {
                Logger.d(e.getMessage(), e);
            } catch (IllegalAccessException e) {
                Logger.d(e.getMessage(), e);
            }
        }
        return t;
    }

    /**
     * 取出缓数据。
     *
     * @param key    缓存对应key
     * @param tClass 缓存数据类型
     * @param <T>    缓存数据类型
     * @return 缓存数据
     */
    public <T> T getCache(String key, Class<T> tClass) {
        Object obj = get(key);
        if (obj != null) {
            if (obj.getClass().isAssignableFrom(tClass)) {
                return tClass.cast(obj);
            } else {
                Logger.d("tClass 缓存数据类型不匹配 obj=" + obj.getClass() + " \ttClass=" + tClass);
            }
        } else {
            Logger.d("所给key值不正确或不存在此key所指向的数据");
        }
        return null;
    }

    /**
     * 取出缓数据，并从缓存容器中清除。
     *
     * @param key    缓存对应key
     * @param tClass 缓存数据类型
     * @param <T>    缓存数据类型
     * @return 缓存数据
     */
    public <T> T getCacheClear(String key, Class<T> tClass) {
        Object obj = clear(key);
        if (obj != null) {
            if (tClass.isAssignableFrom(obj.getClass())) {
                return tClass.cast(obj);
            } else {
                Logger.d("tClass 缓存数据类型不匹配 obj=" + obj.getClass() + " \ttClass=" + tClass);
            }
        } else {
            Logger.d("所给key值不正确或不存在此key所指向的数据");
        }
        return null;
    }

    /**
     * 添加缓存数据。
     *
     * @param key   缓存key
     * @param value 缓存值
     */
    public void put(String key, Object value) {
        if (value != null) {
            cacheMap.put(key, value);
        } else {
            Logger.d("错误！缓存值不能为NULL key==" + key + "   value==" + value);
        }
    }

    /**
     * 返回指定缓存数据。
     *
     * @param key 缓存数据对应的key
     * @return 缓存数据
     */
    public Object get(String key) {
        return cacheMap.get(key);
    }

    /**
     * 返回所有缓存数据。
     *
     * @return 返回缓存容器
     */
    public LinkedHashMap<String, Object> getCacheMap() {
        return cacheMap;
    }

    /**
     * 清除所有已缓存数据。
     */
    public void clear() {
        cacheMap.clear();
    }

    /**
     * 清除指定的缓存数据。
     *
     * @param key 缓存数据对应key
     * @return 返回被清除的数据
     */
    public Object clear(String key) {
        return cacheMap.remove(key);
    }

}
