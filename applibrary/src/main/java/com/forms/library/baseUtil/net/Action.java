package com.forms.library.baseUtil.net;

import android.content.Context;
import com.forms.library.base.BaseActivity;
import com.forms.library.base.BaseApplication;
import com.forms.library.base.BaseFragment;
import com.forms.library.baseUtil.asynchttpclient.RequestParams;
import com.forms.library.baseUtil.logger.Logger;

/**
 * Created by hwt on 14/11/4.
 * 请求响应的结果数据处理
 */
public abstract class Action {
    protected UiObject uiObject;
    protected BaseApplication application;
    private boolean isReset = false;
    private Request mRquest;

    public Action() {
    }

    public Action init(UiObject uiObject, BaseApplication baseApplication) {
        this.uiObject = uiObject;
        this.application = baseApplication;
        return this;
    }

    public Action(UiObject uiObject, BaseApplication baseApplication) {
        this.uiObject = uiObject;
        this.application = baseApplication;
    }

    public abstract Http excute(String requestUrl, RequestParams params);

    public Object getUiObject() {
        return uiObject;
    }

    /**
     * 判断当前操作的 UI 对象是否可用
     *
     * @return
     */
    public boolean isNative() {
        if (uiObject != null) {
            if (uiObject instanceof BaseActivity) {
                BaseActivity activity = ((BaseActivity) uiObject);
                return !activity.isFinishing();
            } else if (uiObject instanceof BaseFragment) {
                BaseFragment fragment = ((BaseFragment) uiObject);
                return !fragment.isRemoving();
            }
            return true;
        }
        return false;
    }

    public void clear() {
        setIsReset(true);
        if (uiObject != null) {
            Logger.d("重置UIObject <=> %s ; Action 重置 <> %s",
                    uiObject != null ? uiObject.getClass().getName() : "当前UIObject=NULL",
                    this.getClass().getName() + "被重置");
            uiObject = null;
            application = null;
        }

    }

    public boolean isReset() {
        return isReset;
    }

    public void setIsReset(boolean isReset) {
        this.isReset = isReset;
    }

    public void setRequest(Request request) {
        mRquest = request;
    }

    public void reset(Context context) {
        Request.getHttpRequest().resetContext(context);
    }
}
