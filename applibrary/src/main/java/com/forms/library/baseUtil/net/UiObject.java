package com.forms.library.baseUtil.net;

import java.util.UUID;

/**
 * Created by hwt on 4/8/15.
 */
public interface UiObject {

    String TAG = UUID.randomUUID().toString();

    void requestSuccess(RetCode retCode, Object response);

    void requestFalied(RetCode retCode);

    void addAction(Action action);

    void resetUiOject();

}
