package com.forms.library.baseUtil.net;

/**
 * Created by hwt on 2/9/15.
 * 网络请求加载过程控件描述。
 */
public interface ViewLoading {
    /**
     * 加载中处理
     * @param progress
     */
    void loading(int progress);

    /**
     * 载入完成但无数据处理
     * @param noDataMsg
     */
    void noData(String noDataMsg);

    /**
     * 载入完成但数据加载失败处理
     * @param failedMsg
     */
    void failed(String failedMsg);

    /**
     * 载入完成
     */
    void loaded();

    /**
     *
     */
    void canceled();

    /**
     * 判断是否下在载入处理
     * @return
     */
    boolean isLoading();

    /**
     * 设置重试操作
     * @param onRetryListener
     */
    void retry(OnRetryListener onRetryListener);

    /**
     * 判断是否已有重试设置
     * @return
     */
    boolean isHasRetry();

    /**
     * 重试监听
     */
    interface OnRetryListener {
        /**
         * 实现在请求重试时的操作
         */
        void retry();
    }
}
