package com.forms.library.baseUtil.db.listener;

import com.forms.library.baseUtil.db.DbUtils;
import com.forms.library.baseUtil.exception.DbException;
import com.forms.library.baseUtil.log.LogUtils;

/**
 * Created by hwt on 14-9-15.
 *
 * 数据库表升级监听。
 */
public abstract class BaseDbUpgradeListener implements DbUtils.DbUpgradeListener{

    @Override
    public void onUpgrade(DbUtils db, int oldVersion, int newVersion) {
        LogUtils.i("----------------------数据库升级----------------------");
        LogUtils.i("oldVersion:" + oldVersion + "\t  newVersion:" + newVersion);
        if (newVersion > oldVersion) {
            clearTableUpdate(db);
            justOnlyUpdateTableColumn(db);
        }
    }

    /**
     * 只做表字段更新
     *
     * @param db 数据库操作工具
     */
    public abstract void justOnlyUpdateTableColumn(DbUtils db);

    /**
     * 清理数据重新建表
     *
     * @param db 数据库表升级监听
     */
    public abstract void clearTableUpdate(DbUtils db);
}
