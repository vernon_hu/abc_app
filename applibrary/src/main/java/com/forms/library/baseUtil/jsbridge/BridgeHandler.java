package com.forms.library.baseUtil.jsbridge;

public interface BridgeHandler {

    void handler(String data, CallBackFunction function);

}
