package com.forms.library.baseUtil.cache;

import java.util.LinkedList;

/**
 * Created by hwt on 3/31/15.
 */
public class CacheList<k, v> extends LinkedList<Cache<k, v>> {

    public static <k, v> Cache<k, v> getCache(k tag, v baseActivity) {
        return new Cache<k, v>(tag, baseActivity);
    }

    public void put(k key, v value) {
        add(getCache(key, value));
    }

    public Cache<k, v> getCache(k key) {
        for (int i = 0; i < size(); i++) {
            Cache<k, v> kvCache = get(i);
            if (kvCache.getTag().equals(key)) {
                return kvCache;
            }
        }
        return null;
    }

    public Cache<k, v> getCache(int position) {
        return get(position);
    }

    public k getCacheTag(k key) {
        Cache<k, v> cache = getCache(key);
        if (cache != null) {
            return cache.getTag();
        }
        return null;
    }

    public k getCacheTag(int position) {
        Cache<k, v> cache = getCache(position);
        if (cache != null) {
            return cache.getTag();
        }
        return null;
    }

    public v getCacheValue(k key) {
        Cache<k, v> cache = getCache(key);
        if (cache != null) {
            return cache.getValue();
        }
        return null;
    }

    public v getCacheValue(int position) {
        Cache<k, v> cache = getCache(position);
        if (cache != null) {
            return cache.getValue();
        }
        return null;
    }

    public void clearCache() {
        clear();
    }

    public boolean removeCache(k tag) {
        return remove(tag);
    }
}

class Cache<k, v> {
    private k tag;
    private v value;

    public Cache(k tag, v value) {
        this.tag = tag;
        this.value = value;
    }

    public k getTag() {
        return tag;
    }

    public void setTag(k tag) {
        this.tag = tag;
    }

    public v getValue() {
        return value;
    }

    public void setValue(v value) {
        this.value = value;
    }

    public boolean isNull() {
        if (tag == null && value != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof Cache) {
            Cache cache = (Cache) o;
            if (getTag().equals(cache.getTag())
                    && getValue().getClass().getName().equals(cache.getValue().getClass().getName())) {
                return true;
            }
        }
        return false;
    }
}
