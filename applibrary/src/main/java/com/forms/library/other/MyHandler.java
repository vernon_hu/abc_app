package com.forms.library.other;

import android.os.Handler;
import android.os.Message;

import java.lang.ref.WeakReference;

/**
 * Created by hwt on 4/15/15.
 */
public abstract class MyHandler<T> {
    private StaticHandler<T> handle = null;

    public static <T> Handler getHandler(T t) {
        Handler handle = new StaticHandler<T>(t) {
            @Override
            public void handleMessage(Message msg) {
            }
        };
        return handle;
    }

    static class StaticHandler<T> extends Handler {
        private WeakReference<T> tWeakReference = null;

        public StaticHandler(T t) {
            this.tWeakReference = new WeakReference<T>(t);
        }
    }

    public abstract void myhandler(Message msg);
}
