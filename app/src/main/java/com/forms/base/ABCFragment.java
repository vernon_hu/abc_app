package com.forms.base;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.abc.sz.app.listener.OnFragmentShowListener;
import com.forms.library.base.BaseFragment;
import com.forms.library.baseUtil.net.Controller;
import com.forms.library.baseUtil.view.ViewUtils;

/**
 * Created by user on 14-9-11.
 */
public class ABCFragment extends BaseFragment implements OnFragmentShowListener {
    protected ABCActivity baseActivity;
    protected ABCApplication application;
    //布局
    protected View view;
    //是否访问
    protected boolean isVisite = false;
    protected Controller controller;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        baseActivity = (ABCActivity) activity;
        application = baseActivity.getBaseApp();
        controller = baseActivity.getController();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewUtils.inject(this, view);
        if (!isVisite) {
            initData();
            initView();
            initListener();
        }
    }

    public ABCActivity getABCActivity() {
        return baseActivity;
    }

    protected void initData() {

    }

    protected void initView() {

    }

    protected void initListener() {

    }
//
//    protected View initView(int id) {
//        if (view == null) {
//            view = View.inflate(baseActivity, id, null);
//        } else {
//            ((ViewGroup) (view.getRootView())).removeAllViews();
//            isVisite = true;
//        }
//        return view;
//    }


    @Override
    public void showFragment(int position) {
        // TODO Auto-generated method stub
    }

    public Controller getController() {
        return baseActivity.getController();
    }
}
