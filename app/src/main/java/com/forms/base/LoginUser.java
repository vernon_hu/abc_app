package com.forms.base;

import com.abc.sz.app.http.bean.personal.RLogin;
import com.abc.sz.app.http.bean.personal.RUser;
import com.forms.library.base.BaseBean;

/**
 * Created by hwt on 14-9-15.
 */
public class LoginUser extends BaseBean {
    private RLogin loginInfo;

    public LoginUser(RLogin RLoginInfo) {
        this.loginInfo = RLoginInfo;
    }

    public String getUserId() {
        if (loginInfo != null) {
            return loginInfo.getUser() != null ? loginInfo.getUser().getUserId() : null;
        }
        return null;
    }

    public String getNickName() {
        if (loginInfo != null) {
            return loginInfo.getUser() != null ? loginInfo.getUser().getNickname() : null;
        }
        return null;
    }

    public String getAccessToken() {
        if (loginInfo != null) {
            return loginInfo.getAccessToken();
        }
        return null;
    }

    public String getHeadUrl() {
        if (loginInfo != null) {
            return loginInfo.getUser() != null ? loginInfo.getUser().getHeadUrl() : null;
        }
        return null;
    }

    public String getGender() {
        if (loginInfo != null) {
            return loginInfo.getUser() != null ? loginInfo.getUser().getGender() : null;
        }
        return null;
    }

    public Long getPoints() {
        if (loginInfo != null) {
            return loginInfo.getUser() != null ? loginInfo.getUser().getPoints() : 0;
        }
        return null;
    }
    
    public void setPoints(Long points){
    	if (loginInfo != null) {
    		loginInfo.getUser().setPoints(points);
    	}
    }

    public String getPhone() {
        if (loginInfo != null) {
            return loginInfo.getUser() != null ? loginInfo.getUser().getPhone() : null;
        }
        return null;
    }

    public RLogin getLoginInfo() {
        return loginInfo;
    }

    public RUser getLoginUser() {
        if (loginInfo != null) {
            return loginInfo.getUser();
        }
        return null;
    }

    public String getUserName() {
        if (loginInfo != null) {
            return loginInfo.getUser() != null ? loginInfo.getUser().getUserName() : null;
        }
        return null;
    }

}
