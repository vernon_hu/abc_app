package com.forms.base;

/**
 * Created by monkey on 2015/8/26.
 */
public interface IWorker {

    public void doWork(WorkerData workerData);
}
