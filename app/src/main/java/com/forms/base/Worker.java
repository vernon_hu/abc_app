package com.forms.base;

/**
 * Created by monkey on 2015/8/26.
 */
public abstract class Worker implements IWorker {

    private Worker mNextWorker = null;
    private boolean isStop = false;

    @Override
    public void doWork(WorkerData workerData) {
        WorkerData goNext = handle(workerData);
        if (!isStop) {
            if (goNext.goNext) {
                mNextWorker.doWork(goNext);
            } else {
                mNextWorker.failedHandle(goNext);
            }
        }
    }

    public abstract WorkerData handle(WorkerData workerData);

    public abstract WorkerData failedHandle(WorkerData workerData);

    public Worker getmNextWorker() {
        return mNextWorker;
    }

    public void setmNextWorker(Worker mNextWorker) {
        this.mNextWorker = mNextWorker;
    }

    public boolean isStop() {
        return isStop;
    }

    public void setIsStop(boolean isStop) {
        this.isStop = isStop;
    }
}

