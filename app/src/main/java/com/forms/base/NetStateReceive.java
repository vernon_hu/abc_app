package com.forms.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.forms.view.toast.MyToast;

/**
 * Created by hwt on 14/11/5.
 * 网络状态监听
 */
public class NetStateReceive extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (!netIsAvailable(context)) {
            MyToast.showTEXT(context, "无可用网络");
        } else {
            MyToast.showTEXT(context, "网络已连接");
        }
    }

    /**
     * 判断是否有可用网络
     *
     * @return
     */
    public boolean netIsAvailable(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getApplicationContext().getSystemService(
                Context.CONNECTIVITY_SERVICE);
        if (manager == null) {
            return false;
        }
        NetworkInfo networkinfo = manager.getActiveNetworkInfo();
        if (networkinfo == null || !networkinfo.isAvailable()) {
            return false;
        }
        return true;
    }
}
