package com.forms.base;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.SparseArray;

import com.abc.sz.app.http.bean.personal.RLogin;
import com.abc.sz.app.http.bean.personal.RUser;
import com.abc.sz.app.listener.SimpleDBUpgradeListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.forms.library.base.BaseApplication;
import com.forms.library.baseUtil.db.DbUtils;
import com.forms.library.baseUtil.db.sqlite.Selector;
import com.forms.library.baseUtil.exception.DbException;
import com.forms.library.baseUtil.log.LogUtils;
import com.forms.library.baseUtil.net.HttpConfig;
import com.forms.library.tools.FileUtils;
import com.forms.library.tools.FormsUtil;
import com.nostra13.universalimageloader.cache.disc.impl.LimitedAgeDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hwt on 14-9-11.
 * <p/>
 * 应用Bean ,添加数据缓存管理，登录用户管理，activity管理功能。
 */
public class ABCApplication extends BaseApplication {
    private DbUtils dbUtils;
    private ImageLoader imageLoader;
    private SparseArray<ABCFragment> fragmentList;
    private LoginUser loginUser;
    private HttpConfig httpConfig = new HttpConfig();
    private boolean isTest = true;
    public final static String testIP = "TESRT_SERVER";
    public final static String testIPString = "TESRT_SERVER_IP";
    private List<Activity> activityList = new ArrayList<>();
    private LocationClient mLocationClient;

    @Override
    public void onCreate() {
        super.onCreate();
//        setTest();
        /*应用初始化*/
        imageLoader = initImageLoader(getApplicationContext());
        initDBUtils(new SimpleDBUpgradeListener());
        fragmentList = new SparseArray<ABCFragment>();
        FormsUtil.getDisplayMetrics(this);

        httpConfig.setCharset("UTF-8");
        httpConfig.setHttpRetry(2);
        httpConfig.setHttpTimeout(15000);
        httpConfig.setMaxConnects(5);
        HttpConfig.initRetCode(BaseConfig.RETMAP);

        //初始化百度地图SDK
        SDKInitializer.initialize(this);
        initLoginedUser();

        mLocationClient = new LocationClient(getApplicationContext());     //声明LocationClient类
        initLocation();
    }

    private void initLocation() {
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy
        );//可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
//        option.setCoorType("bd09ll");//可选，默认gcj02，设置返回的定位结果坐标系
        option.setCoorType("gcj02");//可选，默认gcj02，设置返回的定位结果坐标系
        int span = 1000;
        option.setScanSpan(span);//可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setIsNeedAddress(true);//可选，设置是否需要地址信息，默认不需要
        option.setOpenGps(true);//可选，默认false,设置是否使用gps
        option.setLocationNotify(true);//可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        option.setIsNeedLocationDescribe(true);//可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        option.setIsNeedLocationPoiList(true);//可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        option.setIgnoreKillProcess(false);//可选，默认false，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认杀死
        option.SetIgnoreCacheException(false);//可选，默认false，设置是否收集CRASH信息，默认收集
        option.setEnableSimulateGps(false);//可选，默认false，设置是否需要过滤gps仿真结果，默认需要
        mLocationClient.setLocOption(option);
    }

    private void setTest() {
        if (isTest) {
            SharedPreferences sharedPref = getSharedPreferences(testIP, Context.MODE_PRIVATE);
            String serverIp = sharedPref.getString(testIPString, "");
            if (!TextUtils.isEmpty(serverIp)) {
                BaseConfig.SERVER_URL = "http://" + serverIp + "/MobileApp/";
                BaseConfig.PICTURE_URL = "http://" + serverIp + "/ABCMM/netBank/zh_CN/";
            }
        }
    }

    public boolean isTest() {
        return isTest;
    }

    /**
     * 初始化ImageLoader工具
     *
     * @param context 应用上下文
     */
    public ImageLoader initImageLoader(Context context) {
        ImageLoaderConfiguration config = null;
        File cacheFile = FileUtils.getStorageDerectory(BaseConfig.IMAGE_CACHE_PATH);
        if (cacheFile == null) {
            config = new ImageLoaderConfiguration.Builder(context)
                    .threadPriority(Thread.NORM_PRIORITY - 2)
                    .denyCacheImageMultipleSizesInMemory()
                    .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                    .tasksProcessingOrder(QueueProcessingType.LIFO)
                    .writeDebugLogs() // Remove for release app
                    .build();
        } else {
            config = new ImageLoaderConfiguration.Builder(context)
                    .threadPriority(Thread.NORM_PRIORITY - 2)
                    .denyCacheImageMultipleSizesInMemory()
                    .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                    .tasksProcessingOrder(QueueProcessingType.LIFO)
                    .writeDebugLogs() // Remove for release app
//                    .memoryCache(new WeakMemoryCache())
                    .diskCache(new LimitedAgeDiscCache(cacheFile, 30 * 60))
                    .diskCacheFileCount(100)
                    .build();
        }
        ImageLoader loader = ImageLoader.getInstance();
        loader.init(config);
        return loader;
    }

    /**
     * 初始化数据库操作工具
     *
     * @return
     */
    private void initDBUtils(DbUtils.DbUpgradeListener upgradeListener) {
        DbUtils.DaoConfig daoConfig = new DbUtils.DaoConfig(this);
        daoConfig.setDbName(BaseConfig.DB_NAME);
        daoConfig.setDbVersion(BaseConfig.DB_VERSION);
        if (!TextUtils.isEmpty(BaseConfig.DB_DERECTORY)) {
            daoConfig.setDbDir(FileUtils.getStorageDerectory(BaseConfig.DB_DERECTORY).getAbsolutePath());
        }
        if (upgradeListener != null)
            daoConfig.setDbUpgradeListener(upgradeListener);
        dbUtils = DbUtils.create(daoConfig);
        dbUtils.configAllowTransaction(true);
    }

    /**
     * 得到数据库操作工具
     *
     * @return DbUtils
     */
    public DbUtils getDbUtils() {
        if (dbUtils == null) {
            initDBUtils(new SimpleDBUpgradeListener());
        }
        return dbUtils;
    }

    public LoginUser getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(LoginUser loginUser) {
        this.loginUser = loginUser;
    }

    /**
     * 登录初始
     *
     * @param loginUser
     */
    public void login(LoginUser loginUser) {
        this.loginUser = loginUser;
        if (loginUser != null) {
            SharedPreferences sharePref = getSharedPreferences(BaseConfig.LOGIN_INFO, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharePref.edit();
            editor.putString(BaseConfig.LOGIN_TOKEN, loginUser.getAccessToken());
            editor.putString(BaseConfig.LOGIN_USER, loginUser.getUserId());
            editor.putString(BaseConfig.LOGIN_USER_PHONE, loginUser.getPhone());
            editor.apply();
            RUser rUser = loginUser.getLoginUser();
            try {
                rUser.setId(null);
                getDbUtils().save(rUser);
            } catch (DbException e) {
                LogUtils.d(e.getMessage(), e);
            }
        }

    }

    /**
     * 登出
     */
    public void logout() {
        if (loginUser != null) {
            SharedPreferences sharePref = getSharedPreferences(BaseConfig.LOGIN_INFO, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharePref.edit();
            editor.remove(BaseConfig.LOGIN_TOKEN);
            editor.remove(BaseConfig.LOGIN_USER);
            editor.apply();
            loginUser = null;
        }
    }

    /**
     * 加载 已登录用户信息
     */
    public void initLoginedUser() {
        SharedPreferences sharePref = getSharedPreferences(BaseConfig.LOGIN_INFO, Context.MODE_PRIVATE);
        String userId = sharePref.getString(BaseConfig.LOGIN_USER, null);
        String token = sharePref.getString(BaseConfig.LOGIN_TOKEN, null);
//        String phone = sharePref.getString(BaseConfig.LOGIN_USER_PHONE, null);
        try {
            if (userId != null) {
                RUser rUser = getDbUtils().findFirst(Selector.from(RUser.class).where("userId", "=", userId));
                RLogin rLogin = new RLogin();
//              RUser rUser = new RUser();
//                rUser.setPhone(phone);
//                rUser.setPoints(0L);
                rLogin.setUser(rUser);
                rLogin.setAccessToken(token);
                loginUser = new LoginUser(rLogin);
            }
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }

    /**
     * 获得图片加载工具
     *
     * @return ImageLoader
     */
    public XDRImageLoader getImageLoader() {
        return new XDRImageLoader(imageLoader);
    }

    /**
     * 应用退出时监听
     */
    public interface OnExitAppListener {
        void onExit();
    }

    public SparseArray<ABCFragment> getFragmentList() {
        return fragmentList;
    }

    public void setFragmentList(SparseArray<ABCFragment> fragmentList) {
        this.fragmentList = fragmentList;
    }

    public HttpConfig getHttpConfig() {
        return httpConfig;
    }

    public List<Activity> getActivityList() {
        return activityList;
    }

    public LocationClient getLocationClient() {
        return mLocationClient;
    }
}
