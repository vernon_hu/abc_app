package com.forms.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;

import com.abc.sz.app.activity.personal.LoginActivity;
import com.abc.sz.app.activity.personal.MyInfoActivity;
import com.abc.sz.app.listener.OnFormsDbUpgradeListener;
import com.abc.sz.app.util.FormsDBUtils;
import com.abc.sz.app.view.XdrDialog;
import com.forms.library.base.BaseActivity;
import com.forms.library.baseUtil.cache.CacheBean;
import com.forms.library.baseUtil.exception.AppException;
import com.forms.library.baseUtil.log.LogUtils;
import com.forms.library.baseUtil.net.Controller;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.HttpConfig;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.ViewUtils;
import com.forms.view.toast.MyToast;

import java.util.List;


/**
 * Created by user on 14-9-11.
 * <p>activity基类，</p>
 */
public class ABCActivity extends BaseActivity {
    public static final int LOGIN_RESULTCODE = 0x00888;
    public static final int LOGINOUT_RESULTCODE = 0x00889;
    private ABCApplication baseApp;
    private String Tag;

    protected CacheBean cacheBean;
    protected FormsDBUtils dbUtils;
    protected Controller controller;
    protected XDRImageLoader imageLoader;
    private OnFinishingListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtils.inject(this);
        Thread.setDefaultUncaughtExceptionHandler(AppException.getAppExceptionHandler(this));
        baseApp = (ABCApplication) getApplication();

        imageLoader = baseApp.getImageLoader();
        cacheBean = baseApp.getCacheBean();
        initDBUtils();

        HttpConfig httpConfig = baseApp.getHttpConfig();
        httpConfig.setProgressDialog(new XdrDialog(this));
        httpConfig.setHttpTimeout(7000);
        httpConfig.setIllegalListener(new Http.LoginIllegalListener() {
            @Override
            public void onLoginIllegal() {
                MyToast.showTEXT(getBaseContext(), RetCode.loginIllegal.getRetMsg());
                Bundle bundle = new Bundle();
                bundle.putBoolean("loginTimeOut", true);
                callMe(LoginActivity.class, bundle);
                if (ABCActivity.this instanceof MyInfoActivity) {
                    finish();
                }
            }
        });
        controller = Controller.getInstance(httpConfig);
        getActivityList().add(this);
    }

    public boolean isLogin() {
        if (baseApp.getLoginUser() == null) {
            //未登录
            Intent intent = new Intent(this, LoginActivity.class);
            startActivityForResult(intent, LOGIN_RESULTCODE);
            return false;
        }
        return true;
    }

    /**
     * 加载数据库操作类
     */
    private void initDBUtils() {
        OnFormsDbUpgradeListener dbUpgradeListener = new OnFormsDbUpgradeListener();
        dbUtils = FormsDBUtils.getInstance(this, dbUpgradeListener);
        dbUtils.configAllowTransaction(true);
    }

    /**
     * 得到数据库操作工具
     *
     * @return
     */
    public FormsDBUtils getDBUtils() {
        if (dbUtils == null) {
            initDBUtils();
            dbUtils = getDBUtils();
        }
        return dbUtils;
    }

    @Override
    protected void onResume() {
        imageLoader.getImageLoader().resume();
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.onResume();
            }
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        imageLoader.getImageLoader().pause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        imageLoader.getImageLoader().stop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        for (int i = 0; i < getActivityList().size(); i++) {
            if (this == getActivityList().get(i)) {
                getActivityList().remove(i);
                break;
            }
        }
        super.onDestroy();
    }


    /**
     * <p>传递onActivityResult方法到fragment</p>
     * <ul>如继承activity中重写了此方法，如还需要传递此回调到activity所有的fragment中则需再调一次：</ul><br/>
     * super.onActivityResult(requestCode, resultCode, data);
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null && fragments.size() > 0) {
            for (Fragment fragment : fragments) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    /**
     * @return BaseApplication
     */
    public ABCApplication getBaseApp() {
        return baseApp = (ABCApplication) getApplication();
    }

    /**
     * @return CacheBean
     */
    public CacheBean getCacheBean() {
        return getBaseApp().getCacheBean();
    }


    public ABCFragment replaceFragment(int containerId, ABCFragment fragment, boolean flag) {
        /*在指定容器中切换fragment*/
        if (fragment != null) {
            FragmentManager manager = getSupportFragmentManager();
            ABCFragment fragmentTag = null;
            if (flag) {
                fragmentTag = (ABCFragment) manager.findFragmentByTag(fragment.getClass().getName());
            }
            fragment = fragmentTag == null ? fragment : fragmentTag;
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.addToBackStack(fragment.getClass().getName());
            transaction.replace(containerId, fragment, fragment.getClass().getName());
            transaction.commit();
            return fragment;
        } else {
            LogUtils.i("添加fragment失败,fragment=" + fragment);
        }
        return null;
    }

    public ABCFragment replaceFragment(int containerId, ABCFragment fragment) {
        /*在指定容器中切换fragment*/
        return replaceFragment(containerId, fragment, true);
    }

    public ABCFragment replaceFragment(int containerId, Class<? extends ABCFragment> fragmentClass) {
        return replaceFragment(containerId, fragmentClass, true);
    }

    public ABCFragment replaceFragment(int containerId, Class<? extends ABCFragment> fragmentClass, boolean flag) {
        /*在指定容器中切换fragment*/
        try {
            ABCFragment fragment = null;
            FragmentManager manager = getSupportFragmentManager();
            if (flag) {
                fragment = (ABCFragment) manager.findFragmentByTag(fragmentClass.getName());
            }
            fragment = fragment == null ? fragmentClass.newInstance() : fragment;
            return replaceFragment(containerId, fragment, flag);
        } catch (InstantiationException e) {
            LogUtils.i(e.getMessage(), e);
        } catch (IllegalAccessException e) {
            LogUtils.i(e.getMessage(), e);
        }
        return null;
    }


    public ABCFragment loadFragment(int containerId, ABCFragment fragment) {
        /*在指定容器中添加一个fragment*/
        if (fragment != null) {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(containerId, fragment, fragment.getClass().getName());
            transaction.commit();
            return fragment;
        } else {
            LogUtils.i("添加fragment失败,fragment=" + fragment);
        }
        return null;
    }

    public ABCFragment loadFragment(int containerId, Class<? extends ABCFragment> fragmentClass) {
        /*在指定容器中添加一个fragment*/
        try {
            ABCFragment ABCFragment = fragmentClass.newInstance();
            return loadFragment(containerId, ABCFragment);
        } catch (InstantiationException e) {
            LogUtils.i(e.getMessage(), e);
        } catch (IllegalAccessException e) {
            LogUtils.i(e.getMessage(), e);
        }
        return null;
    }

    public String getTag() {
        return Tag;
    }

    public void setTag(String tag) {
        Tag = tag;
    }

    public XDRImageLoader getImageLoader() {
        return imageLoader;
    }

    /**
     * 结束前监听
     */
    public void setOnFinishingListener(OnFinishingListener listener) {
        this.listener = listener;
    }

    public OnFinishingListener getOnFinishingListener() {
        return listener;
    }

    public interface OnFinishingListener {
        boolean onFinishing();
    }

    public Controller getController() {
        return controller;
    }

    public List<Activity> getActivityList() {
        return baseApp.getActivityList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
