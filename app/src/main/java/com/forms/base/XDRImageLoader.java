package com.forms.base;

import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.forms.library.tools.FormsUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

public class XDRImageLoader {

    private ImageLoader imageLoader;

    public XDRImageLoader(ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

    public void displayImage(String uri, ImageView imageView, DisplayImageOptions options) {
//        if(!TextUtils.isEmpty(uri)) {
              imageLoader.displayImage(BaseConfig.PICTURE_URL + uri, imageView, options);
//              FadeInBitmapDisplayer.animate(imageView, 700);
//            imageLoader.displayImage(uri, imageView, options);
//        }
    }
    
    public void displayImage(String uri, ImageView imageView, DisplayImageOptions options, int type) {
    	
    	imageLoader.displayImage(BaseConfig.PICTURE_URL + uri, imageView, options, loadingListener);
    }
    
    public ImageLoader getImageLoader(){
    	return imageLoader;
    }
    
    /**
     * 适配屏幕加载消息图片
     */
    private SimpleImageLoadingListener loadingListener = new SimpleImageLoadingListener(){

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                if(imageView != null){
	                float oldWidth = loadedImage.getWidth();
	                float oldHeight = loadedImage.getHeight();
	                float width = FormsUtil.SCREEN_WIDTH;
	                float height = width / oldWidth * oldHeight;
	                ViewGroup.LayoutParams params = imageView.getLayoutParams();
	                if(params != null){
	                	params.width = (int) width;
		                params.height = (int) height;
		                imageView.setLayoutParams(params);
		                FadeInBitmapDisplayer.animate(imageView, 700);
	                }
                }
            }
        }
    };
}
