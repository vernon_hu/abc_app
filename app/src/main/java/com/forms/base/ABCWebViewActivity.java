package com.forms.base;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.abc.ABC_SZ_APP.R;
import com.forms.library.baseUtil.log.LogUtils;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * WebView显示页
 * <p/>
 * 调用说明</br>
 * 需要传入参数:"txtTitle"-当前页面标题；"targetUrl"-将要打开的目标页面地址</br>
 * 例：</br>
 * Bundle bundle = new Bundle();</br>
 * bundle.putString("txtTitle","绑卡");</br>
 * bundle.putString("targetUrl","https://......");</br>
 * callMe(CommonWebViewActivity.class,bundle);</br>
 */
@ContentView(R.layout.activity_base_webview)
public class ABCWebViewActivity extends ABCActivity {
    public final static String TITLE_TEXT = "txtTitle";
    public final static String TARGET_URL = "targetUrl";
    public final static String LEFT_ARROW_RESID = "left_arrow_resId";

    @ViewInject(R.id.appBar) 
    private Toolbar toolbar;
    @ViewInject(R.id.wvCommonWebView)
    private WebView wvCommonWebView;
    @ViewInject(R.id.loadingBar)
    private ProgressBar loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String title = getIntent().getStringExtra(TITLE_TEXT);
        if(title == null)
        	title = "";
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        
        initData();
    }

    @SuppressLint("SetJavaScriptEnabled")
	public void initData() {
        String targetUrl = getIntent().getStringExtra(TARGET_URL);
        loadingBar.setMax(100);
        wvCommonWebView.getSettings().setJavaScriptEnabled(true);//可以运行javaScript
        wvCommonWebView.getSettings().setBuiltInZoomControls(true);
        wvCommonWebView.loadUrl(targetUrl);

        wvCommonWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                wvCommonWebView.loadUrl(url);
                loadingBar.setVisibility(View.VISIBLE);// 再次请求网页，显示进度条
                return true;
            }

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                // 打开错误，隐藏进度条，并置零
                loadingBar.setVisibility(View.GONE);
                loadingBar.setProgress(0);
            }
        });

        wvCommonWebView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                loadingBar.setProgress(newProgress);
                if (100 == newProgress) {// 加载完成页面
                    loadingBar.setVisibility(View.GONE);
                    loadingBar.setProgress(0);
                }
            }

            //添加的
            @Override
            public boolean onJsAlert(WebView view, String url, String message,
                                     JsResult result) {
                LogUtils.d("url=" + url);
                LogUtils.d("message=" + message);
                LogUtils.d("JsResult=" + result);
                return super.onJsAlert(view, url, message, result);
            }
        });

        wvCommonWebView.addJavascriptInterface(new JSObject(), "order");
    }

    @Override
    // 默认点回退键，会退出Activity，需监听按键操作，使回退在WebView内发生
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (wvCommonWebView.canGoBack()) {
                wvCommonWebView.goBack();
            } else {
                finishForBack(RESULT_OK);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    class JSObject {
        @JavascriptInterface
        public void createOrder(String orderJson) {

        }
    }
}
