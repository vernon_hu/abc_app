package com.forms.base;


import com.forms.library.baseUtil.net.RetCode;

import java.util.HashMap;

/**
 * Created by user on 14-9-12.
 * <p>
 * 项目配置类，网络配置，常量配置，接口路径配置。
 */
public class BaseConfig {
    public static final HashMap<String, RetCode> RETMAP = new HashMap<String, RetCode>();
    //    public static String SERVER_URL = "http://10.191.3.113:9999/MobileApp/";
    public static String SERVER_URL = "http://newclound.wicp.net:9999/MobileApp/";
    //    public static String PICTURE_URL = "http://10.191.3.113:8070/ABCMM/netBank/zh_CN/";
    public static String PICTURE_URL = "http://newclound.wicp.net:9999/ABCMM/netBank/zh_CN/";
    public static final String IMAGE_CACHE_PATH = "/小达人/imagecache/";
    public static final String ERROR_LOG_PATH = "";
    public static final String DB_NAME = "";
    public static final int DB_VERSION = 1;
    //    public static final String DB_DERECTORY = "/小达人/test.db";
    public static final String DB_DERECTORY = null;
    public static final String LOGIN_INFO = "login_info";
    public static final String LOGIN_USER = "login_user";
    public static final String LOGIN_TOKEN = "login_token";
    public static final String LOGIN_USER_PHONE = "login_user_phone";
    public static final String OPEN_MESSAGE_NOTIFY = "open_message_notify";
    //应用下载地址
    public static final String APP_DOWNLOAD_URL = "http://www.baidu.com";
    //订单状态修改通知
    public static final String ORDER_NOTIFY_URL = "http://10.191.3.113:9999/MobileApp/UpdateOrderPayStatusAct.ebf";
    //我的YOU特权路径
    public static final String MY_PREROGATIVE_URL = SERVER_URL + "YouPrivilegeInitAct.ebf?hide=1";
    //优惠商户
    public static final String CARD_PREROGATIVE_URL = SERVER_URL + "CreditCardPrivilegeInitAct.ebf?hide=1";
    //分期计算器
    public static final String CALCULATOR_URL = SERVER_URL + "StagesCounterAct.ebf?hide=1";
    //图片默认缓存文件
    public static final String DEFAULT_CACHE_DIR = "img";
    //图片缓存个数
    public static final int DEFAULT_CACHE_COUNT = 20;
    //图片最大缓存空间
    public static final long DEFAULT_DISK_CACHE_SIZE = 1024 * 1024 * 50;//50m
    //内存图片缓存空间
    public final static int DEFAULT_MEMORY_CACHE_SIZE = 1024 * 1024 * 4; // 4M
    //免费wifi名称
    public final static String FREE_WIFI_SSID = "abchina.com";
    //信用卡预约申请地址
    public final static String CARD_APPLY_URL = "http://abc.weazm.com/youhui-card-lite/public/slider?ref=552c913e07b18";
    //理财试算地址
    public final static String MANAGE_MONEY_URL = "http://m.abchina.com/touch/mobilecalc/calc";
    //行情数据地址
    public final static String PRICE_DATA_URL = "http://m.abchina.com/touch/mobilecalc/utility/";
    //交通罚款
    public final static String TRAFFIC_FINE_URL = "http://jfxdr.berbon.com/merchant/trafficabc/index.htm?userAgent=";
//    public final static String TRAFFIC_FINE_URL = "http://abctest.berbon.com/merchant/trafficabc/index.htm?userAgent=";
    //宽带缴费
    public final static String BROADBAND_PAY_URL = "http://jfxdr.berbon.com/merchant/broadband/index.htm?userAgent=";
//    public final static String BROADBAND_PAY_URL = "http://abctest.berbon.com/merchant/broadband/index.htm?userAgent=";
    //流量
    public final static String PHONE_FLOW_URL = "http://112.95.172.89:81/wap_new/abcTalent/indexFlow.php?userAgent=";
    //话费
    public final static String PHONE_PAY_URL = "http://112.95.172.89:81/wap_new/abcTalent/index.php?userAgent=";

    public final static String CITYID = "56";
    public final static String QUERY_CITYAREA_URL = "http://newclound.wicp.net:9999/BranchService/District/Any/{0}";
    public final static String QUERY_BRANCH_URL = "http://newclound.wicp.net:9999/BranchService/Branch?Province={0}&City={1}&Area={2}&keyword={3}&type={4}&PageIndex={5}";
    public final static String QUERY_NEARBRANCH_URL = "http://newclound.wicp.net:9999/BranchService/Branch/Near?CurrentPointText=POINT({0}%20{1})&Distance={2}&type={3}&PageIndex={4}";


    static {
        RETMAP.put("10000", RetCode.success);
        RETMAP.put("10001", RetCode.returnDescription);
        RETMAP.put("00002", RetCode.noData);
        RETMAP.put("F00001", RetCode.netError);
        RETMAP.put("F00002", RetCode.timeOut);
        RETMAP.put("F00003", RetCode.unKnow);
        RETMAP.put("60001", RetCode.oldPswdError);
        RETMAP.put("60011", RetCode.exist);
        RETMAP.put("60014", RetCode.authCodeError);
        RETMAP.put("60017", RetCode.loginFailed);
        RETMAP.put("60020", RetCode.checkinError);
        RETMAP.put("60021", RetCode.loginIllegal);
        RETMAP.put("60013", RetCode.exitsBindCard);

    }


}
