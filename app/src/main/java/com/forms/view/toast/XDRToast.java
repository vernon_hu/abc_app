package com.forms.view.toast;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by korey on 14-12-31.
 */
public class XDRToast {
    private static XDRToast xdrToast = null;
    private Toast toast;
    private Context mContext;
    private int duration = Toast.LENGTH_SHORT;

    private XDRToast() {

    }

    public static XDRToast getXDRToast(Context context, int duration) {
        if (xdrToast == null) {
            xdrToast = new XDRToast();
            xdrToast.mContext = context;
            xdrToast.duration = duration;
        }
        return xdrToast;
    }

    public static XDRToast getXDRToast(Context context) {
        if (xdrToast == null) {
            xdrToast = new XDRToast();
            xdrToast.mContext = context;
        }
        return xdrToast;
    }

    public void show(String text) {
        if (toast != null) {
            toast.cancel();
        }

        toast = Toast.makeText(mContext, text, Toast.LENGTH_SHORT);
        toast.show();
    }
}
