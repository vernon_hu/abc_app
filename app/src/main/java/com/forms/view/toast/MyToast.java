package com.forms.view.toast;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.abc.ABC_SZ_APP.R;

/**
 * 自定义吐司
 * <p>
 * 1.显示纯文本<br/>
 * MyToast.show(this, "Hello World", MyToast.TEXT, Gravity.CENTER, Toast.LENGTH_SHORT);<br/>
 * 2.显示带默认图标的文本<br/>
 * MyToast.show(this, "Hello World", MyToast.ICON_DEFAULT, Gravity.CENTER, Toast.LENGTH_LONG);<br/>
 * 3.显示带自定义图标的文本<br/>
 * MyToast.show(this, "Hello World", R.drawable.ic_launcher, Gravity.CENTER, Toast.LENGTH_SHORT);<br/>
 * </p>
 *
 * @author kejia
 */
public class MyToast {

    private static Toast toast;

    public final static int TEXT = 0;
    public final static int ICON_DEFAULT = -1;
    public final static int ICON_FAILED = -2;

    /**
     * 显示自定义吐司
     *
     * @param context  上下文
     * @param message  文本
     * @param drawable 图标
     * @param duration 时长
     */
    public static void show(Context context, String message, int drawable, int gravity, int duration) {
        View view = LayoutInflater.from(context).inflate(R.layout.my_toast, null);
        TextView tv = (TextView) view.findViewById(R.id.tv_toast);
        ImageView iv = (ImageView) view.findViewById(R.id.iv_toast);
        switch (drawable) {
            case TEXT:
                iv.setVisibility(View.GONE);
                break;
            case ICON_DEFAULT:
                iv.setBackgroundResource(R.drawable.icon_info);
                break;
            case ICON_FAILED:
                iv.setBackgroundResource(R.drawable.icon_failed);
                break;
            default:
                iv.setBackgroundResource(drawable);
                break;
        }
        tv.setText(message);

        if (toast == null) {
            toast = new Toast(context);
        }
        toast.setView(view);
        toast.setDuration(duration);
        toast.setGravity(gravity, 0, 0);
        toast.show();
    }

    public static void showTEXT(Context context, String message) {
        show(context, message, TEXT, Gravity.CENTER, Toast.LENGTH_SHORT);
    }

    public static void showDEFAULT(Context context, String message) {
        show(context, message, ICON_DEFAULT, Gravity.BOTTOM, Toast.LENGTH_SHORT);
    }

    public static void showFAILED(Context context, String message) {
        show(context, message, ICON_FAILED, Gravity.CENTER, Toast.LENGTH_SHORT);
    }
}