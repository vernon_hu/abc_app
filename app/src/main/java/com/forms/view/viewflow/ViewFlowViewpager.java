package com.forms.view.viewflow;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by korey on 15-1-5.
 */
public class ViewFlowViewpager extends ViewPager {
    public ViewFlowViewpager(Context context) {
        super(context);
    }

    public ViewFlowViewpager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
        if (v != this && v instanceof ViewFlow) {
            return true;
        }
        return super.canScroll(v, checkV, dx, x, y);
    }
}
