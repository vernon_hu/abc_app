/*
 * Copyright 2009 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.forms.view.zxing.util;

import android.graphics.Bitmap;
import com.google.zxing.common.BitMatrix;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Writes a {@link com.google.zxing.common.BitMatrix} to {@link Bitmap},
 * file or stream. Provided here instead of core since it depends on
 * Java SE libraries.
 *
 * @author Sean Owen
 */
public final class MatrixToImageWriter {

    private static final MatrixToImageConfig DEFAULT_CONFIG = new MatrixToImageConfig();

    private MatrixToImageWriter() {
    }

    /**
     * Renders a {@link com.google.zxing.common.BitMatrix} as an image, where "false" bits are rendered
     * as white, and "true" bits are rendered as black. Uses default configuration.
     *
     * @param matrix {@link com.google.zxing.common.BitMatrix} to write
     * @return {@link Bitmap} representation of the input
     */
    public static Bitmap toBufferedImage(BitMatrix matrix) {
        return toBufferedImage(matrix, DEFAULT_CONFIG);
    }

    /**
     * As {@link #toBufferedImage(com.google.zxing.common.BitMatrix)}, but allows customization of the output.
     *
     * @param matrix {@link com.google.zxing.common.BitMatrix} to write
     * @param config output configuration
     * @return {@link Bitmap} representation of the input
     */
    public static Bitmap toBufferedImage(BitMatrix matrix, MatrixToImageConfig config) {
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        Bitmap bitmap = Bitmap.createBitmap(Bitmap.createBitmap(width, height, config.getBufferedImageColorModel()));
        int onColor = config.getPixelOnColor();
        int offColor = config.getPixelOffColor();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                bitmap.setPixel(x, y, matrix.get(x, y) ? onColor : offColor);
            }
        }
        return bitmap;
    }

    /**
     * @param matrix {@link com.google.zxing.common.BitMatrix} to write
     * @param format image format
     * @param file   file {@link File} to write image to
     * @throws IOException if writes to the file fail
     */
    public static void writeToFile(BitMatrix matrix, Bitmap.CompressFormat format, File file) throws IOException {
        writeToFile(matrix, format, file, DEFAULT_CONFIG);
    }

    /**
     * @param matrix {@link com.google.zxing.common.BitMatrix} to write
     * @param format image format
     * @param file   file {@link File} to write image to
     * @param config output configuration
     * @throws IOException if writes to the file fail
     */
    public static void writeToFile(BitMatrix matrix, Bitmap.CompressFormat format, File file, MatrixToImageConfig config)
            throws IOException {
        if (file.exists()) {
            OutputStream stream = new FileOutputStream(file);
            writeToStream(matrix, format, stream, config);
        }
    }

    /**
     * Writes a {@link com.google.zxing.common.BitMatrix} to a stream with default configuration.
     *
     * @param matrix {@link com.google.zxing.common.BitMatrix} to write
     * @param format image format
     * @param stream {@link OutputStream} to write image to
     * @throws IOException if writes to the stream fail
     * @see #toBufferedImage(com.google.zxing.common.BitMatrix)
     */
    public static void writeToStream(BitMatrix matrix, Bitmap.CompressFormat format, OutputStream stream) throws IOException {
        writeToStream(matrix, format, stream, DEFAULT_CONFIG);
    }

    /**
     * @param matrix {@link com.google.zxing.common.BitMatrix} to write
     * @param format image format
     * @param stream {@link OutputStream} to write image to
     * @param config output configuration
     * @throws IOException if writes to the stream fail
     */
    public static void writeToStream(BitMatrix matrix, Bitmap.CompressFormat format, OutputStream stream, MatrixToImageConfig config)
            throws IOException {
        Bitmap image = toBufferedImage(matrix, config);
        image.compress(format, 100, stream);
    }

}
