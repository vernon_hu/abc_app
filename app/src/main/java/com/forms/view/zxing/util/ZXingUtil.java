package com.forms.view.zxing.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.google.zxing.*;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hwt on 14/11/6.
 */
public class ZXingUtil {
    // 图片宽度的一般
    private static final String CHARSET = "utf-8";
    private static final Map<DecodeHintType, Object> decodeHints = new HashMap<DecodeHintType, Object>();
    private static final Map<EncodeHintType, Object> encodeHints = new HashMap<EncodeHintType, Object>();

    static {
        // 指定编码格式
        decodeHints.put(DecodeHintType.CHARACTER_SET, CHARSET);
        // 指定纠错等级
        encodeHints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.Q);
        encodeHints.put(EncodeHintType.CHARACTER_SET, CHARSET);
        encodeHints.put(EncodeHintType.MARGIN, 0);
    }

    /**
     * 生成普通二维码
     *
     * @param contents
     * @param width
     * @param height
     * @return
     */
    public static Bitmap createQRCodeBitmap(String contents, int width, int height) {
        try {
            BitMatrix bitMatrix = new MultiFormatWriter().encode(contents,
                    BarcodeFormat.QR_CODE, width, height, encodeHints);
            return MatrixToImageWriter.toBufferedImage(bitMatrix);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 生成普通二维码
     *
     * @param contents
     * @param width
     * @param height
     * @param imgPath
     */
    public static Bitmap createQRCode(String contents, int width, int height, String imgPath) {
        Bitmap image = null;
        try {
            BitMatrix bitMatrix = new MultiFormatWriter().encode(contents,
                    BarcodeFormat.QR_CODE, width, height, encodeHints);
            image = MatrixToImageWriter.toBufferedImage(bitMatrix);
            image.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(imgPath));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }

    /**
     * 生成带图片的二维码
     *
     * @param content
     * @param width
     * @param height
     * @param srcImagePath
     * @param destImagePath
     */
    public static void createQRCodeWidthLogo(String content, int width, int height,
                                             String srcImagePath, String destImagePath) {
        try {
            genBarcode(content, width, height, srcImagePath).
                    compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(destImagePath));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    /**
     * 针对二维码进行解析
     *
     * @param imgPath
     * @return
     */
    public static String decodeQRCode(String imgPath) {
        Bitmap image = null;
        Result result = null;
        try {
            image = BitmapFactory.decodeFile(imgPath);
            if (image == null) {
                System.out.println("the decode image may be not exists.");
                return null;
            }
            LuminanceSource source = new RGBLuminanceSource(image.getWidth(), image.getHeight(), getBitmapPixels(image));
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
            result = new MultiFormatReader().decode(bitmap, decodeHints);
            return result.getText();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 创建条形码
     *
     * @param contents
     * @param width
     * @param height
     * @param imgPath
     */
    public static void encodeBarCode(String contents, int width, int height, String imgPath) {
        // 条形码的最小宽度
        int codeWidth = 98;
        codeWidth = Math.max(codeWidth, width);
        try {
            BitMatrix bitMatrix = new MultiFormatWriter().encode(contents,
                    BarcodeFormat.EAN_13, codeWidth, height, encodeHints);

            MatrixToImageWriter.writeToStream(bitMatrix, Bitmap.CompressFormat.PNG,
                    new FileOutputStream(imgPath));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 针对条形码进行解析
     *
     * @param imgPath
     * @return
     */
    public static String decodeBarCode(String imgPath) {

        Bitmap image = null;
        Result result = null;
        try {
            image = BitmapFactory.decodeFile(imgPath);
            if (image == null) {
                System.out.println("the decode image may be not exit.");
                return null;
            }
            LuminanceSource source = new RGBLuminanceSource(image.getWidth(), image.getHeight(), getBitmapPixels(image));
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

            result = new MultiFormatReader().decode(bitmap, null);
            return result.getText();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 把传入的原始图像按高度和宽度进行缩放，生成符合要求的图标
     *
     * @param srcImageFile 源文件地址
     * @param height       目标高度
     * @param width        目标宽度
     * @param hasFiller    比例不对时是否需要补白：true为补白; false为不补白;
     * @throws IOException
     */
    private static Bitmap scale(String srcImageFile, int width, int height,
                                boolean hasFiller) throws IOException {
        FileInputStream inputStream = new FileInputStream(srcImageFile);
        Bitmap srcImage = BitmapFactory.decodeStream(inputStream);
        return Bitmap.createScaledBitmap(srcImage, width, height, hasFiller);
    }

    /**
     * 产生带有图片的二维码缓冲图像
     *
     * @param content
     * @param width
     * @param height
     * @param srcImagePath
     * @return
     * @throws com.google.zxing.WriterException
     * @throws IOException
     */
    private static Bitmap genBarcode(String content, int width, int height,
                                     String srcImagePath) throws WriterException, IOException {
        // 读取源图像
        Bitmap scaleImage = scale(srcImagePath, width / 5, height / 5, true);
        int[][] srcPixels = new int[scaleImage.getWidth()][scaleImage.getHeight()];
        for (int i = 0; i < scaleImage.getWidth(); i++) {
            for (int j = 0; j < scaleImage.getHeight(); j++) {
                srcPixels[i][j] = scaleImage.getPixel(i, j);
            }
        }
        // 生成二维码
        MultiFormatWriter mutiWriter = new MultiFormatWriter();
        BitMatrix matrix = mutiWriter.encode(content, BarcodeFormat.QR_CODE,
                width, height, encodeHints);

        int logoLeft = (width - scaleImage.getWidth()) / 2;
        int logoTop = (height - scaleImage.getHeight()) / 2;
        int logoRight = (width - scaleImage.getWidth()) / 2 + scaleImage.getWidth();
        int logoBottom = (height - scaleImage.getHeight()) / 2 + scaleImage.getHeight();
        int[] pixels = new int[width * height];

        for (int y = 0; y < matrix.getHeight(); y++) {
            for (int x = 0; x < matrix.getWidth(); x++) {
                // 读取图片
                if (x > logoLeft
                        && x < logoRight
                        && y > logoTop
                        && y < logoBottom) {
                    pixels[y * width + x] = srcPixels[x - logoLeft][y - logoTop];
                } else {
                    // 此处可以修改二维码的颜色，可以分别制定二维码和背景的颜色；
                    pixels[y * width + x] = matrix.get(x, y) ? 0xff000000
                            : 0xffFFFFFF;
                }
            }
        }
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        image.setPixels(pixels, 0, width, 0, 0, width, height);
        return image;
    }

    public static int[] getBitmapPixels(Bitmap bitmap) {
        // 首先，要取得该图片的像素数组内容
        int[] data = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(data, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        return data;
    }

}
