package com.forms.view.zxing;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.ClipboardManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.OrderAction;
import com.abc.sz.app.activity.order.InputOrderActivity;
import com.abc.sz.app.activity.order.OrderAffirmPayActivity;
import com.abc.sz.app.bean.Order;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.util.DialogUtil;
import com.alibaba.fastjson.JSON;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.log.LogUtils;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.encrypt.Base64;
import com.forms.view.toast.MyToast;
import com.forms.view.zxing.camera.CameraManager;
import com.forms.view.zxing.decode.CaptureActivityHandler;
import com.forms.view.zxing.decode.FinishListener;
import com.forms.view.zxing.decode.InactivityTimer;
import com.forms.view.zxing.view.ViewfinderView;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;

/**
 * 扫描二维码
 *
 * @author 火蚁（http://my.oschina/LittleDY）
 */
@SuppressWarnings("deprecation")
@ContentView(R.layout.activity_capture)
public class CaptureActivity extends ABCActivity implements SurfaceHolder.Callback, View.OnClickListener {
    @ViewInject(R.id.viewfinder_view) ViewfinderView viewfinderView;
    @ViewInject(R.id.appBar) Toolbar toolBar;

    private boolean hasSurface;
    private String characterSet;
    /**
     * 活动监控器，用于省电，如果手机没有连接电源线，那么当相机开启后如果一直处于不被使用状态则该服务会将当前activity关闭。
     * 活动监控器全程监控扫描活跃状态，与CaptureActivity生命周期相同.每一次扫描过后都会重置该监控，即重新倒计时。
     */
    private InactivityTimer inactivityTimer;
    private CameraManager cameraManager;
    private Vector<BarcodeFormat> decodeFormats;// 编码格式
    private CaptureActivityHandler mHandler;// 解码线程

    private static final Collection<ResultMetadataType> DISPLAYABLE_METADATA_TYPES = EnumSet
            .of(ResultMetadataType.ISSUE_NUMBER,
                    ResultMetadataType.SUGGESTED_PRICE,
                    ResultMetadataType.ERROR_CORRECTION_LEVEL,
                    ResultMetadataType.POSSIBLE_COUNTRY);
    private MenuItem flashOpen;//闪光灯打开
    private MenuItem flash;//闪光灯关闭
    private AlertDialog mProgress;
    private OrderAction orderAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolBar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); // 保持屏幕处于点亮状态
        mProgress = DialogUtil.showDxrDialog(this,
                null,
                "正在打开摄像头，请稍候...",
                true, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        finish();
                    }
                });
        orderAction = (OrderAction) controller.getAction(this, OrderAction.class);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_capture_menu, menu);
        flashOpen = menu.findItem(R.id.flashOpen);
        flash = menu.findItem(R.id.flash);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.flash) {
            if (cameraManager != null) {
                flash.setVisible(false);
                flashOpen.setVisible(true);
                cameraManager.setTorch(true);
            } else {
                Toast.makeText(this, "没有闪光灯", Toast.LENGTH_SHORT);
            }
        }
        if (id == R.id.flashOpen) {
            if (cameraManager != null) {
                flash.setVisible(true);
                flashOpen.setVisible(false);
                cameraManager.setTorch(false);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * 初始化视图
     */
    private void initView() {
        hasSurface = false;
        inactivityTimer = new InactivityTimer(this);
        cameraManager = new CameraManager(getApplication());
        viewfinderView.setCameraManager(cameraManager);
    }

    /**
     * 主要对相机进行初始化工作
     */
    @Override
    protected void onResume() {

        inactivityTimer.onActivity();
        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        if (hasSurface) {
            new InitCameraTask().execute(surfaceHolder);
        } else {
            // 如果SurfaceView已经渲染完毕，会回调surfaceCreated，在surfaceCreated中调用initCamera()
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }
        // 恢复活动监控器
        inactivityTimer.onResume();
        super.onResume();
    }

    public void drawViewfinder() {
        viewfinderView.drawViewfinder();
    }

    /**
     * 初始化摄像头。打开摄像头，检查摄像头是否被开启及是否被占用
     *
     * @param surfaceHolder
     */
    private void initCamera(SurfaceHolder surfaceHolder) throws IOException {
        if (surfaceHolder == null) {
            throw new IllegalStateException("No SurfaceHolder provided");
        }
        if (cameraManager.isOpen()) {
            return;
        }
        cameraManager.openDriver(surfaceHolder);
        // Creating the mHandler starts the preview, which can also throw a
        // RuntimeException.
    }

    class InitCameraTask extends AsyncTask<SurfaceHolder, Integer, Exception> {
        @Override
        protected Exception doInBackground(SurfaceHolder... params) {
            Exception exception = null;
            try {
                initCamera(params[0]);
            } catch (Exception e) {
                exception = e;
            }
            return exception;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onPostExecute(Exception e) {
            if (e != null) {
                if (e instanceof IOException) {
                    displayFrameworkBugMessageAndExit();
                } else if (e instanceof RuntimeException) {
                    // Barcode Scanner has seen crashes in the wild of this variety:
                    // java.?lang.?RuntimeException: Fail to connect to camera service
                    displayFrameworkBugMessageAndExit();
                }
                return;
            }

            if (mHandler == null) {
                mHandler = new CaptureActivityHandler(CaptureActivity.this, decodeFormats,
                        characterSet, cameraManager);
            }
            if (mProgress != null && mProgress.isShowing()) {
                mProgress.dismiss();
            }
        }
    }

    /**
     * 初始化照相机失败显示窗口
     */
    private void displayFrameworkBugMessageAndExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.msg_camera_framework_bug));
        builder.setPositiveButton("确定", new FinishListener(this));
        builder.setOnCancelListener(new FinishListener(this));
        builder.show();
    }

    /**
     * 暂停活动监控器,关闭摄像头
     */
    @Override
    protected void onPause() {
        if (mHandler != null) {
            mHandler.quitSynchronously();
            mHandler = null;
        }
        // 暂停活动监控器
        inactivityTimer.onPause();
        // 关闭摄像头
        cameraManager.closeDriver();
        if (!hasSurface) {
            SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
            SurfaceHolder surfaceHolder = surfaceView.getHolder();
            surfaceHolder.removeCallback(this);
        }

        if (mProgress != null && mProgress.isShowing()) {
            mProgress.dismiss();
        }
        super.onPause();
    }

    /**
     * 停止活动监控器,保存最后选中的扫描类型
     */
    @Override
    protected void onDestroy() {
        // 停止活动监控器
        inactivityTimer.shutdown();
//        if (mProgress != null) {
//            mProgress.dismiss();
//            mProgress = null;
//        }
        super.onDestroy();
    }

    /**
     * 获取扫描结果
     *
     * @param rawResult
     * @param barcode
     * @param scaleFactor
     */
    public void handleDecode(Result rawResult, Bitmap barcode, float scaleFactor) {
        //inactivityTimer.onActivity();
        boolean fromLiveScan = barcode != null;
        if (fromLiveScan) {

            // Then not from history, so beep/vibrate and we have an image to
            // draw on
        }
        DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.SHORT,
                DateFormat.SHORT);
        Map<ResultMetadataType, Object> metadata = rawResult
                .getResultMetadata();
        StringBuilder metadataText = new StringBuilder(20);
        if (metadata != null) {
            for (Map.Entry<ResultMetadataType, Object> entry : metadata
                    .entrySet()) {
                if (DISPLAYABLE_METADATA_TYPES.contains(entry.getKey())) {
                    metadataText.append(entry.getValue()).append('\n');
                }
            }
            if (metadataText.length() > 0) {
                metadataText.setLength(metadataText.length() - 1);
            }
        }
        parseBarCode(rawResult.getText());
    }

    // 解析二维码
    private void parseBarCode(String msg) {
        // 手机震动
        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(100);
       /* mProgress = ProgressDialog.show(CaptureActivity.this, null,
                "已扫描，正在处理···", true, true);
        mProgress.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialog) {
                restartPreviewAfterDelay(1l);
            }
        });*/
        LogUtils.d("二维码数据:" + msg);
        Order order = null;
        try {
            msg = new String(Base64.decode(msg), "utf-8");
            LogUtils.d("\n二维码数据==>" + msg);
            if (msg.startsWith(getString(R.string.barcode_header))) {
                msg = msg.substring(getString(R.string.barcode_header).length());
                order = JSON.parseObject(msg, Order.class);
                if (order != null) {
                	List<Product> list = new ArrayList<>();
                	Product product = new Product();
                	product.setProductId("SH012345678910");
                	product.setProductName("商户端扫码商品");
                	product.setProductPrice(String.valueOf(order.getAmount()));
                	product.setNum("1");
                	list.add(product);
                	orderAction.orderImmediatePay(order.getOrderId(), String.valueOf(order.getAmount()), 
                			null, list, "1").start();
                } else {
                    MyToast.showTEXT(this, getString(R.string.error_barcode));
                    restartPreviewAfterDelay(200);
                }
            }
        } catch (Exception e) {
            LogUtils.d(e.getMessage(), e);
            MyToast.showTEXT(this, getString(R.string.error_barcode));
            restartPreviewAfterDelay(200);
        }
        // 判断是否符合基本的json格式
        //showDialog(msg);
    }

    /**
     * 扫描结果对话框
     *
     * @param msg
     */
    private void showDialog(final String msg) {
        new AlertDialog.Builder(CaptureActivity.this)
                .setTitle("扫描结果")
                .setMessage("非oschina提供活动签到二维码\n内容：" + msg)
                .setPositiveButton("复制", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mProgress.dismiss();
                        dialog.dismiss();
                        // 获取剪贴板管理服务
                        ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        // 将文本数据复制到剪贴板
                        cm.setText(msg);
                        DialogUtil.showWithNoTitile(CaptureActivity.this, "复制成功");
                    }
                })
                .setNegativeButton("返回", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mProgress.dismiss();
                        dialog.dismiss();
                        restartPreviewAfterDelay(0L);

                    }
                }).show();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (holder == null) {

        }
        if (!hasSurface) {
            hasSurface = true;
            new InitCameraTask().execute(holder);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        hasSurface = false;
    }

    /**
     * 闪光灯调节器。自动检测环境光线强弱并决定是否开启闪光灯
     */
    public ViewfinderView getViewfinderView() {
        return viewfinderView;
    }

    public Handler getHandler() {
        return mHandler;
    }

    public CameraManager getCameraManager() {
        return cameraManager;
    }

    /**
     * 在经过一段延迟后重置相机以进行下一次扫描。 成功扫描过后可调用此方法立刻准备进行下次扫描
     *
     * @param delayMS
     */
    public void restartPreviewAfterDelay(long delayMS) {
        if (mHandler != null) {
            mHandler.sendEmptyMessageDelayed(R.id.restart_preview, delayMS);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    @OnClick(value = {R.id.input_code})
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.input_code:
                callMe(InputOrderActivity.class);
                break;
            default:
                break;
        }
    }
    
    /**
     * 订单支付成功
     */
    public void paySuccess(String content){
    	Bundle bundle = new Bundle();
    	bundle.putString("paymentyURL", content);
    	callMe(OrderAffirmPayActivity.class, bundle);
    }
}
