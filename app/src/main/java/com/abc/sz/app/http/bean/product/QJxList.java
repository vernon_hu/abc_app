package com.abc.sz.app.http.bean.product;

/**
 * 快报列表查询
 *
 * @author llc
 */
public class QJxList {
    //当前页码，从1开始计数
    private String pageNum;

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

}
