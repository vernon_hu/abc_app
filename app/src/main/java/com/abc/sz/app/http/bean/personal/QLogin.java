package com.abc.sz.app.http.bean.personal;

/**
 * Created by hwt on 14/11/18.
 */
public class QLogin {
    private String custName;
    private String custPswd;
    private String vson;//系统版本号
    private String type;//手机型号
    private String userType;//用户类型

    public QLogin(String custName, String custPswd, String vson, String type, String userType) {
        this.custName = custName;
        this.custPswd = custPswd;
        this.vson = vson;
        this.type = type;
        this.userType = userType;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustPswd() {
        return custPswd;
    }

    public void setCustPswd(String custPswd) {
        this.custPswd = custPswd;
    }

	public String getVson() {
		return vson;
	}

	public void setVson(String vson) {
		this.vson = vson;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
    
}
