package com.abc.sz.app.activity_phase2;


import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.view.XdrDialog;
import com.airnet.wifi.AirnetWIFICheckNetworkObservable;
import com.airnet.wifi.AirnetWIFILoginObservable;
import com.airnet.wifi.AirnetWIFILoginTypeObservable;
import com.airnet.wifi.AirnetWIFISDK;
import com.forms.base.ABCActivity;
import com.forms.base.BaseConfig;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.WifiAdmin;

/**
 * Created by hwt on 4/22/15.
 */
@ContentView(R.layout.activity_freewifi)
public class FreeWIFIActivity extends ABCActivity implements Observer{
	
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.wifiState) TextView wifiState;
    @ViewInject(R.id.connect) Button connect;
    
    private String phone;
    private XdrDialog xdrDialog;
    private WifiManager wifiManager;
    private WifiAdmin wifiAdmin;
    private Timer timer;
    private Handler mHandler = new Handler(){
    	
    	@Override
		public void handleMessage(Message msg) {
			 super.handleMessage(msg);
			 switch(msg.what){
				 case 1:
					 if(wifiAdmin.checkState() == WifiManager.WIFI_STATE_ENABLED){
						 wifiAdmin.startScan();
						 List<ScanResult> list = wifiAdmin.getWifiList();
						 if(list.size() == 0){
							 mHandler.sendEmptyMessageDelayed(1, 500);
						 }else{
							 ScanResult scanResult = null;
							 for (int i = 0; i < list.size(); i++) {
								if(list.get(i).SSID.equals(BaseConfig.FREE_WIFI_SSID)){
									scanResult = list.get(i);
									break;
								}
							 }
							 if(scanResult != null){
								 if (!wifiAdmin.isConnect(scanResult)) {
									 wifiAdmin.connectSpecificAP(scanResult);
									 mHandler.sendEmptyMessage(2);
								 }
							 }
						 }
					 }else{
						 mHandler.sendEmptyMessageDelayed(1, 500);
					 }
					 break;
				 case 2:
					 WifiInfo wifiInfo = wifiManager.getConnectionInfo();
					 //已经连上指定wifi
					 if(wifiInfo.getSSID().equals("\"" + BaseConfig.FREE_WIFI_SSID + "\"")){
						 initWifi();
						 countTime();
					 }else{
						 mHandler.sendEmptyMessageDelayed(2, 500);
					 }
					 break;
				 case 3:
					 AirnetWIFISDK.deleteObserver(FreeWIFIActivity.this);
					 Toast.makeText(FreeWIFIActivity.this, "WIFI已连接，请验证当前网络", Toast.LENGTH_SHORT).show();
					 wifiState.setText(getString(R.string.wifiStateYes));
					 connect.setVisibility(View.VISIBLE);
					 break;
			 }
		}
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.lightPay);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        
        init();
    }
    
    public void init(){
    	Bundle bundle = getIntent().getExtras();
    	if(bundle != null){
    		phone = bundle.getString("phone");
    	}
    	
    	 xdrDialog = new XdrDialog(FreeWIFIActivity.this);
		 xdrDialog.setCancelable(true);
		 xdrDialog.show();
		 
		 wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		 wifiAdmin = new WifiAdmin(FreeWIFIActivity.this);
		 WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		 //已经连上指定wifi
		 if(wifiInfo.getSSID().equals("\"" + BaseConfig.FREE_WIFI_SSID + "\"")){
			 initWifi();
			 countTime();
		 }else{
			 if(wifiManager.isWifiEnabled()){
				 mHandler.sendEmptyMessage(1);
			 }else{
				 wifiAdmin.openWifi();
			     mHandler.sendEmptyMessageDelayed(1, 1000);
			 }
		 }
    }

    /**
     * wifi认证初始化
     */
    public void initWifi() {
    	AirnetWIFISDK.init(this);
    	AirnetWIFISDK.addObserver(this);
    	AirnetWIFISDK.setIconResID(R.drawable.ic_launcher);
    }
    
    @OnClick(value = {R.id.agreementCheck, R.id.connect})
    public void onClick(View view) {
        switch (view.getId()) {
	        case R.id.agreementCheck:
	        	 AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    		 String message = getString(R.string.wifiStatement);
	    		 builder.setTitle("免责声明");
	    	     builder.setMessage(message);
	    	     builder.setPositiveButton("确定", new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				 });
	    	     builder.setCancelable(false);
	    	     builder.show();
	        	 break;
	        case R.id.connect:
	        	connect();
	        	break;
        }
    }
    
    public void countTime(){
    	 timer = new Timer();
         TimerTask tt = new TimerTask() {
              @Override
              public void run() {
            	  if(xdrDialog != null){
            		  xdrDialog.dismiss();
            	  }
            	  timer.cancel();
                  timer.purge();
                  mHandler.sendEmptyMessage(3);
              }
         };
         timer.schedule(tt, 3000, 1);
    }
    
    public void connect(){
    	xdrDialog = new XdrDialog(FreeWIFIActivity.this);
		xdrDialog.setCancelable(true);
		xdrDialog.show();
    	initWifi();
    	countTime();
    }
    
	@Override
	public void update(Observable observable, Object data) {
		if (observable instanceof AirnetWIFICheckNetworkObservable) {
			if (xdrDialog != null) {
      		    xdrDialog.dismiss();
      	  	}
			if (timer != null) {
                timer.cancel();
                timer.purge();
            }
			// 检测网络接口的返回
			// status 值：
			// 1 表示当前网络需要认证（errno = 0 并且 获取到token和logintype）
			// 0 表示当前网络畅通 （访问url成功并且返回 <HTML><HEAD><TITLE>Success</TITLE></HEAD><BODY>Success</BODY></HTML>）
			// 否则返回-1
			String status = (String)data;
			String info = "";
			if (status.equals("-1")) {
				info = "检测网络失败";
			} else if (status.equals("0")) {
				info = "网络畅通";
				wifiState.setText("网络畅通");
				connect.setVisibility(View.INVISIBLE);
			} else if (status.equals("1")) {
				info = "当前网络需要认证";
			} 
			Toast.makeText(FreeWIFIActivity.this, info, Toast.LENGTH_SHORT).show();
		} else if (observable instanceof AirnetWIFILoginTypeObservable) {
			// 处理网络检测接口返回的logintype
			String logintype = (String)data;
			if (logintype.equalsIgnoreCase("0")) {
				AirnetWIFISDK.nflogin(this, phone);
			}
		} else if (observable instanceof AirnetWIFILoginObservable) {
			// 处理登陆认证的返回结果
			String status = (String)data;
			if (status.equalsIgnoreCase("0")) {
				Toast.makeText(FreeWIFIActivity.this, "登录成功 ", Toast.LENGTH_SHORT).show();
				wifiState.setText("网络畅通");
				connect.setVisibility(View.INVISIBLE);
			} else {
				Toast.makeText(FreeWIFIActivity.this, "登录失败 " + status, Toast.LENGTH_SHORT).show();
			}
		}
	}
	
    @Override
    public void onStop() {
    	super.onStop();
    	AirnetWIFISDK.deleteObserver(this);
    	if(xdrDialog != null){
    		xdrDialog.dismiss();
        	xdrDialog = null;
    	}
    	if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }
    }
}
