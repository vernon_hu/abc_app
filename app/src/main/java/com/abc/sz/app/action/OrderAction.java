package com.abc.sz.app.action;

import android.app.Activity;
import android.content.Context;

import com.abc.sz.app.activity.order.ChoiceCardPayActivity;
import com.abc.sz.app.activity.order.EntityOrderActivity;
import com.abc.sz.app.activity.order.InputOrderActivity;
import com.abc.sz.app.activity.order.OrderAffirmActivity;
import com.abc.sz.app.activity.order.OrderDetailsActivity;
import com.abc.sz.app.activity.order.OrderPayListActivity;
import com.abc.sz.app.activity.order.ProductEvaluateActivity;
import com.abc.sz.app.bean.Discount;
import com.abc.sz.app.bean.Evaluate;
import com.abc.sz.app.bean.Order;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.fragment.order.EntityOrderFragment;
import com.abc.sz.app.fragment.order.MovieOrderFragment;
import com.abc.sz.app.fragment.order.PrepaidOrderFragment;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.order.QEvaluateList;
import com.abc.sz.app.http.bean.order.QOrderAccount;
import com.abc.sz.app.http.bean.order.QOrderAdvancePay;
import com.abc.sz.app.http.bean.order.QOrderList;
import com.abc.sz.app.http.bean.order.QOrderOperate;
import com.abc.sz.app.http.bean.order.QOrderPay;
import com.abc.sz.app.http.bean.order.QOrderUpdate;
import com.abc.sz.app.http.bean.order.QQueryOrder;
import com.abc.sz.app.http.bean.pay.QPay;
import com.abc.sz.app.http.bean.pay.QProduct;
import com.abc.sz.app.http.bean.pay.RPay;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.abc.sz.app.util.DialogUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.view.toast.MyToast;
import com.forms.view.zxing.CaptureActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 订单操作
 *
 * @author ftl
 */
public class OrderAction extends BaseAction {

    /**
     * 订单结算
     *
     * @param orderType
     * @param sumPrice
     * @param list
     * @return
     */
    public Http orderAccount(List<Product> list) {
        StringBuffer sb = new StringBuffer();
        QOrderAccount qOrderAccount = new QOrderAccount();
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i).getProductId() + ",");
        }
        String proList = sb.toString();
        qOrderAccount.setProList(proList.substring(0, proList.length() - 1));
        RequestBean<QOrderAccount> params = new RequestBean<QOrderAccount>(getAccessToken(), qOrderAccount);
        return APPRequestPhase2.orderAccount(params, new ResponseNotify<List<Product>>(new TypeReference<RespBean<List<Product>>>() {
        }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<List<Product>> response) {
                ((OrderAffirmActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                ((OrderAffirmActivity) uiObject).queryFailed(retCode);
            }
        });
    }

    /**
     * 生成订单
     *
     * @param addressId
     * @param list
     * @return
     */
    public Http createOrder(String addressId, List<Product> list) {
        QOrderUpdate qOrderUpdate = new QOrderUpdate();
        qOrderUpdate.setAddressId(addressId);
        qOrderUpdate.setOrderType("1");
        List<com.abc.sz.app.http.bean.order.QProduct> productList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            Product product = list.get(i);
            com.abc.sz.app.http.bean.order.QProduct qProduct = new com.abc.sz.app.http.bean.order.QProduct();
            qProduct.setProductId(product.getProductId());
            qProduct.setNum(product.getNum());
            if (product.getList() != null && product.getList().size() > 0) {
                Discount discount = product.getList().get(0);
                if (discount != null) {
                    qProduct.setDiscountId(discount.getDiscountId());
                }
            }
            qProduct.setPostCode(product.getPostCode());
            qProduct.setCommodityType(product.getCommodityType());
            productList.add(qProduct);
        }
        qOrderUpdate.setProductList(productList);
        RequestBean<QOrderUpdate> params = new RequestBean<QOrderUpdate>(getAccessToken(), qOrderUpdate);
        return APPRequestPhase2.createOrder(params, new ResponseNotify<List<Order>>(new TypeReference<RespBean<List<Order>>>() {
        }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<List<Order>> response) {
                ((OrderAffirmActivity) uiObject).createOrderSuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                DialogUtil.showWithNoTitile((ABCActivity) uiObject, retCode.getRetMsg());
            }
        });
    }

    /**
     * 订单支付
     *
     * @param orderId
     * @param orderNo
     * @param valideCode
     * @return
     */
    public Http orderPay(String orderId, String orderNo, String valideCode) {
        QOrderPay qOrderPay = new QOrderPay(orderId, orderNo, valideCode);
        RequestBean<QOrderPay> params = new RequestBean<QOrderPay>(getAccessToken(), qOrderPay);
        return APPRequestPhase2.orderPay(params, new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
        }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {

            }

            @Override
            public void onFailed(RetCode retCode) {

            }
        });
    }

    /**
     * 订单直接支付
     *
     * @param orderNo
     * @param orderAmount
     * @param address
     * @param detailList
     * @return
     */
    public Http orderImmediatePay(String orderNo, String orderAmount, String address,
                                  List<Product> list, String isSelect) {
        QPay qPay = new QPay();
        qPay.setPayTypeID("ImmediatePay");
        qPay.setOrderDate("");
        qPay.setOrderTime("");
        qPay.setExpiredDate("");
        qPay.setCurrencyCode("156");
        qPay.setOrderNo(orderNo);
        qPay.setOrderAmount(orderAmount);
        qPay.setFee("");
        qPay.setOrderURL("");
        qPay.setReceiverAddress(address);
        qPay.setInstallmentMark("0");
//        qPay.setInstallmentCode("");
//        qPay.setInstallmentNum("");
//        qPay.setCommodityType("0202");
        qPay.setBuyIP("");
        qPay.setOrderDesc("");
        qPay.setOrderTimeoutDate("");
        List<QProduct> detailList = new ArrayList<>();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                QProduct qProduct = new QProduct();
                Product product = list.get(i);
                qProduct.setSubMerId(product.getSellerId());
                qProduct.setSubMerName(product.getSellerName());
                qProduct.setSubMerMCC("");
                qProduct.setSubMerchantRemarks("");
                qProduct.setProductId(product.getProductId());
                qProduct.setProductName(product.getProductName());
                qProduct.setUnitPrice(product.getProductPrice());
                qProduct.setQty(product.getNum());
                qProduct.setProductRemarks("");
                qProduct.setProductType("");
                qProduct.setProductDiscount("");
                qProduct.setProductExpiredDate("");
                detailList.add(qProduct);
            }
        }
        qPay.setDetailList(detailList);
        qPay.setPaymentType("1");
        qPay.setPaymentLinkType("2");
//        qPay.setUnionPayLinkType("");
        qPay.setReceiveAccount("");
        qPay.setReceiveAccName("");
        qPay.setNotifyType("0");
        qPay.setResultNotifyURL("");
        qPay.setMerchantRemarks("");
        qPay.setIsBreakAccount("0");
        qPay.setSplitAccTemplate("");
        qPay.setIsSelect(isSelect);
        RequestBean<QPay> params = new RequestBean<QPay>(getAccessToken(), qPay);
        return APPRequestPhase2.orderImmediatePay(params, new ResponseNotify<RPay>(new TypeReference<RespBean<RPay>>() {
        }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<RPay> response) {
                if (response.getContent() != null) {
                    String content = response.getContent().getPaymentyURL();
                    String returnCode = response.getContent().getReturnCode();
                    String errorMessage = response.getContent().getErrorMessage();
                    if (returnCode != null && "0000".equals(returnCode)) {
                        if (uiObject instanceof OrderPayListActivity) {
                            ((OrderPayListActivity) uiObject).paySuccess(content);
                        } else if (uiObject instanceof EntityOrderFragment) {
                            ((EntityOrderActivity) ((EntityOrderFragment) uiObject).getABCActivity()).paySuccess(content);
                        } else if (uiObject instanceof OrderDetailsActivity) {
                            ((OrderDetailsActivity) uiObject).paySuccess(content);
                        } else if (uiObject instanceof CaptureActivity) {
                            ((CaptureActivity) uiObject).paySuccess(content);
                        } else if (uiObject instanceof InputOrderActivity) {
                            ((InputOrderActivity) uiObject).paySuccess(content);
                        }
                    } else {
                        if (errorMessage == null || "".equals(errorMessage)) {
                            errorMessage = retCode.getRetMsg();
                        }
                        if (uiObject instanceof OrderPayListActivity) {
                            ((OrderPayListActivity) uiObject).payFailed(errorMessage);
                        } else if (uiObject instanceof EntityOrderFragment) {
                            DialogUtil.showWithNoTitile(((EntityOrderFragment) uiObject).getABCActivity(), errorMessage);
                        } else if (uiObject instanceof OrderDetailsActivity || uiObject instanceof CaptureActivity ||
                                uiObject instanceof InputOrderActivity) {
                            DialogUtil.showWithNoTitile((ABCActivity) uiObject, errorMessage);
                        }
                    }
                }

            }

            @Override
            public void onFailed(RetCode retCode) {
                DialogUtil.showWithNoTitile((ABCActivity) uiObject, retCode.getRetMsg());
            }
        });
    }

    /**
     * 订单预支付
     *
     * @param phone
     * @param orderId
     * @param orderNo
     * @param valideCode
     * @return
     */
    public Http orderAdvancePay(String phone, String orderId, String orderNo, String valideCode) {
        QOrderAdvancePay qOrderAdvancePay = new QOrderAdvancePay(phone, orderId, orderNo, valideCode);
        RequestBean<QOrderAdvancePay> params = new RequestBean<QOrderAdvancePay>(getAccessToken(), qOrderAdvancePay);
        return APPRequestPhase2.orderAdvancePay(params, new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
        }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {

            }

            @Override
            public void onFailed(RetCode retCode) {

            }
        });
    }

    /**
     * 订单取消
     *
     * @param orderId
     * @return
     */
    public Http orderCancel(String orderId) {
        QOrderOperate qOrderOperate = new QOrderOperate(orderId);
        RequestBean<QOrderOperate> params = new RequestBean<QOrderOperate>(getAccessToken(), qOrderOperate);
        return APPRequestPhase2.orderCancel(params, new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
        }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
                ((OrderDetailsActivity) uiObject).cancelSuccess();
            }

            @Override
            public void onFailed(RetCode retCode) {
                MyToast.showFAILED((OrderDetailsActivity) uiObject, retCode.getRetMsg());
            }
        });
    }

    /**
     * 订单列表查询
     *
     * @param pageNum
     * @param type
     * @param state
     * @return
     */
    public Http queryOrderList(int pageNum, String type, String state) {
        QOrderList QOrderQuery = new QOrderList(pageNum + "", type, state, "", "");
        RequestBean<QOrderList> params = new RequestBean<QOrderList>(QOrderQuery);
        params.setToken(getAccessToken());
        return APPRequestPhase2.queryOrderList(params, new ResponseNotify<List<Order>>(new TypeReference<RespBean<List<Order>>>() {
        }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<List<Order>> response) {
                if (uiObject instanceof EntityOrderFragment) {
                    ((EntityOrderFragment) uiObject).querySuccess(retCode, response.getContent());
                } else if (uiObject instanceof PrepaidOrderFragment) {
                    ((PrepaidOrderFragment) uiObject).querySuccess(retCode, response.getContent());
                } else if (uiObject instanceof MovieOrderFragment) {
                    ((MovieOrderFragment) uiObject).querySuccess(retCode, response.getContent());
                }
            }

            @Override
            public void onFailed(RetCode retCode) {
                if (uiObject instanceof EntityOrderFragment) {
                    ((EntityOrderFragment) uiObject).queryFailed(retCode);
                } else if (uiObject instanceof PrepaidOrderFragment) {
                    ((PrepaidOrderFragment) uiObject).queryFailed(retCode);
                } else if (uiObject instanceof MovieOrderFragment) {
                    ((MovieOrderFragment) uiObject).queryFailed(retCode);
                }
            }
        });
    }

    /**
     * 查询订单详情
     *
     * @param orderId
     * @return
     */
    public Http queryOrderDetails(String orderId) {
        QOrderOperate qOrderOperate = new QOrderOperate(orderId);
        RequestBean<QOrderOperate> params = new RequestBean<QOrderOperate>(getAccessToken(), qOrderOperate);
        return APPRequestPhase2.queryOrderDetails(params, new ResponseNotify<Order>(new TypeReference<RespBean<Order>>() {
        }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<Order> response) {
                if (uiObject instanceof OrderDetailsActivity) {
                    ((OrderDetailsActivity) uiObject).querySuccess(response.getContent());
                } else if (uiObject instanceof InputOrderActivity) {
                    ((InputOrderActivity) uiObject).querySuccess(response.getContent());
                }else if (uiObject instanceof EntityOrderActivity) {
                    ((EntityOrderActivity) uiObject).queryOrderDetail(response.getContent());
                }

            }

            @Override
            public void onFailed(RetCode retCode) {
                if (uiObject instanceof OrderDetailsActivity) {
                    ((OrderDetailsActivity) uiObject).queryFailed(retCode);
                } else if (uiObject instanceof InputOrderActivity) {
                    DialogUtil.showWithNoTitile((InputOrderActivity) uiObject, retCode.getRetMsg());
                }
            }
        });
    }


    /**
     * 订单评价
     *
     * @param orderId
     * @param list
     * @return
     */
    public Http evaluateOrder(String orderId, List<Evaluate> list) {
        QEvaluateList qEvaluateList = new QEvaluateList(orderId, list);
        RequestBean<QEvaluateList> params = new RequestBean<QEvaluateList>(getAccessToken(), qEvaluateList);
        return APPRequestPhase2.evaluateOrder(params, new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
        }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
                ((ProductEvaluateActivity) uiObject).evaluateSuccess();
            }

            @Override
            public void onFailed(RetCode retCode) {
                ((ProductEvaluateActivity) uiObject).evaluateFailed(retCode);
            }
        });
    }

    /**
     * 确认收货
     *
     * @param orderId
     * @return
     */
    public Http conFirmReceiveGood(String orderId) {
        return APPRequestPhase2.conFirmReceiveGood(getAccessToken(), orderId,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        if (uiObject instanceof EntityOrderFragment) {
                            ABCActivity activity = ((EntityOrderFragment) uiObject).getABCActivity();
                            ((EntityOrderActivity) activity).onRefresh();
                        } else if (uiObject instanceof OrderDetailsActivity) {
                            ((OrderDetailsActivity) uiObject).finishForBack(Activity.RESULT_OK);
                        }
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        MyToast.showFAILED((OrderDetailsActivity) uiObject, retCode.getRetMsg());
                    }
                });
    }

    /**
     * 供应商通知APP生成订单
     *
     * @param params
     * @return
     */
    public Http addOrder(String params) {
        return APPRequestPhase2.OrderAddAct(getAccessToken(), params,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {

                    }

                    @Override
                    public void onFailed(RetCode retCode) {

                    }
                });
    }

    /**
     * 订单状态查询
     *
     * @param orderId
     * @return
     */
    public Http queryOrderRequest(String orderId) {
        QQueryOrder qQueryOrder = new QQueryOrder("ImmediatePay", orderId, "1");
        RequestBean<QQueryOrder> params = new RequestBean<>(getAccessToken(), qQueryOrder);
        return APPRequestPhase2.queryOrderRequest(
                params,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {}) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        if (uiObject instanceof EntityOrderFragment) {
//                            ((EntityOrderFragment) uiObject).queryOrderSuccess(response.getContent());
                        } else if (uiObject instanceof OrderDetailsActivity) {
                            ((OrderDetailsActivity) uiObject).queryOrderSuccess(response.getContent());
                        }
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
                    }
                });
    }

    /**
     * 同步APP与总行的订单支付状态
     *
     * @param orderId
     * @return
     */
    public Http syncOrderStatusRequest(final String orderId) {
        return APPRequestPhase2.syncOrderStatus(getAccessToken(),orderId,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {}){

            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
                String content = response.getContent();
                JSONObject jsonObject = JSON.parseObject(content);
                String status = jsonObject.getString("Status");
                if(uiObject instanceof EntityOrderActivity){
                    ((EntityOrderActivity)uiObject).syncOrderStatus(orderId,status);
                }
            }

            @Override
            public void onFailed(RetCode retCode) {

            }
        } );
    }
}
