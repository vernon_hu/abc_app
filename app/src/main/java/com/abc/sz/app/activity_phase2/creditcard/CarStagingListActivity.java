package com.abc.sz.app.activity_phase2.creditcard;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.CreditcardAction;
import com.abc.sz.app.adapter_phase2.ImageAdapter;
import com.abc.sz.app.bean.cridetcard.CarShop;
import com.abc.sz.app.bean.cridetcard.CarShopPreferentialContent;
import com.abc.sz.app.bean.cridetcard.DistrictAndBrand;
import com.abc.sz.app.bean_phase2.ImageAD;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.base.BaseAdapter;
import com.forms.library.baseUtil.net.HttpTrans;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;
import com.forms.view.viewflow.CircleFlowIndicator;
import com.forms.view.viewflow.ViewFlow;

/**
 * 汽车分期列表页面
 * 
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_carstaging_list)
public class CarStagingListActivity extends ABCActivity{

	@ViewInject(R.id.appBar) Toolbar toolbar;
	@ViewInject(R.id.relativeLayout) RelativeLayout relativeLayout;
	@ViewInject(R.id.viewFlow) ViewFlow viewFlow;
	@ViewInject(R.id.viewflowindic) CircleFlowIndicator viewflowindic;
	@ViewInject(R.id.lvStaging) ListView lvStaging;
	@ViewInject(R.id.loadingView) LoadingView loadingView;
	@ViewInject(R.id.llCarBrand) LinearLayout llCarBrand;
	@ViewInject(R.id.llArea) LinearLayout llArea;
	@ViewInject(R.id.tvCarBrand) TextView tvCarBrand;
	@ViewInject(R.id.tvArea) TextView tvArea;

	private BaseAdapter<CarShop> adapter;
	private List<CarShop> carShopList = new ArrayList<CarShop>();
	private List<String> areaKeyList = new ArrayList<String>();
	private List<String> areaValueList = new ArrayList<String>();
	private List<String> brandKeyList = new ArrayList<String>();
	private List<String> brandValueList = new ArrayList<String>();
	private CreditcardAction creditcardAction;
	private int pageNum = 0; 
	private int index = 1;
	private List<ImageAD> mImageADs = new ArrayList<>();
	private ImageAdapter imageAdapter;
	private final int NEXTIMAGE = 1;
	private int imageADPosition = 0;
	
	private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == NEXTIMAGE) {
                if (imageADPosition == mImageADs.size() - 1) {
                    imageADPosition = 0;
                } else {
                    imageADPosition += 1;
                    viewFlow.setSelection(imageADPosition);
                    handler.sendEmptyMessageDelayed(NEXTIMAGE, 3000);
                }
            }
        }
    };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		creditcardAction = (CreditcardAction) controller.getAction(this, CreditcardAction.class);
		creditcardAction.loadingAdv("3").start(false);
		HttpTrans httpTrans = getController().getTrans(this);
		httpTrans.addRequest(creditcardAction.queryDistrictAndBrandList());
		httpTrans.addRequest(creditcardAction.queryCarShopList(String.valueOf(pageNum), "", "", ""));
		httpTrans.setLoadingView(loadingView).start();
		
		ViewGroup.LayoutParams layoutParams = relativeLayout.getLayoutParams();
		layoutParams.height = FormsUtil.SCREEN_WIDTH / 4;
		relativeLayout.setLayoutParams(layoutParams);
		ViewGroup.LayoutParams params = viewFlow.getLayoutParams();
        params.height = FormsUtil.SCREEN_WIDTH / 4;
        viewFlow.setLayoutParams(params);
        viewFlow.setFlowIndicator(viewflowindic);
        mImageADs.add(new ImageAD("3", "", "", ""));
        imageAdapter = new ImageAdapter(this, getImageLoader(), mImageADs);
        viewFlow.setAdapter(imageAdapter, 0);
        viewFlow.setOnViewSwitchListener(new ViewFlow.ViewSwitchListener() {
             @Override
             public void onSwitched(View view, int position) {
                 imageADPosition = viewFlow.getSelectedItemPosition();
             }
        });
		adapter = new BaseAdapter<CarShop>(this, carShopList, R.layout.layout_phase2_carstaging_item) {

			@Override
			public void viewHandler(final int position, final CarShop t, View convertView) {
				LinearLayout lltItem = ViewHolder.get(convertView, R.id.lltItem);
				ImageView ivImgUrl = ViewHolder.get(convertView, R.id.ivImgUrl);
				TextView tvShopName = ViewHolder.get(convertView, R.id.tvShopName);
				LinearLayout llFavorable = ViewHolder.get(convertView, R.id.llFavorable);
				List<CarShopPreferentialContent> list = t.getCarShopPreferentialContent();
				if(list != null && list.size() > 0){
					llFavorable.removeAllViews();
					for (int i = 0; i < list.size(); i++) {
						CarShopPreferentialContent cs = list.get(i);
						View view = LayoutInflater.from(CarStagingListActivity.this).inflate(R.layout.layout_phase2_carstaging_favorable, null);
						TextView tvFavorableTitle = (TextView)view.findViewById(R.id.tvFavorableTitle);
						TextView tvFavorableContent = (TextView)view.findViewById(R.id.tvFavorableContent);
						if (cs != null) {
							if(cs.getTitle() != null && !"".equals(cs.getTitle())){
								tvFavorableTitle.setText(cs.getTitle() + "：");
							}
							FormsUtil.setTextViewTxt(tvFavorableContent, cs.getContent());
						}
						llFavorable.addView(view);
					}
				}
				
				if (t != null) {
					imageLoader.displayImage(t.getCarShopIconUrl(), ivImgUrl, ImageViewUtil.getOption());
					FormsUtil.setTextViewTxt(tvShopName, t.getCarShopName());
					lltItem.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							cacheBean.put("carShopID", t.getCarShopID());
							callMe(CarShopInfoActivity.class);
						}
					});
				}
			}
		};
		lvStaging.setAdapter(adapter);
	}

	@OnClick(value={R.id.llCarBrand, R.id.llArea})
	public void next(View v) {
		switch(v.getId()){
			case R.id.llCarBrand:
				index = 1;
				break;
			case R.id.llArea:
				index = 2;
				break;
		}
		showPopupWindow(index);
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_applyforlimit_menu, menu);
        return true;
    }

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.limitApply:
			callMe(CarStagingActivity.class);
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
     * 广告列表查询成功回调
     */
    public void advSuccess(List<ImageAD> list){
    	if(list != null && list.size() > 0){
    		mImageADs.clear();
    		mImageADs.addAll(list);
    		imageAdapter.notifyDataSetChanged();
    		if(mImageADs.size() > 1) {
    	        handler.sendEmptyMessageDelayed(NEXTIMAGE, 3000);
    	    }
    	}
    }
	
	/**
     * 信用卡优惠商户列表查询成功回调
     */
    public void querySuccess(List<CarShop> list) {
    	carShopList.clear();
        if (list != null && list.size() > 0) {
        	carShopList.addAll(list);
        }
        adapter.notifyDataSetChanged();
    }
    
    /**
     * 车商区域和品牌查询成功回调
     */
    public void districtAndBrandSuccess(DistrictAndBrand districtAndBrand){
    	if(districtAndBrand != null){
    		Map<String, String> districtMap = districtAndBrand.getDistrictList();
    		Map<String, String> brandMap = districtAndBrand.getBrandList();
    		if(districtMap != null && districtMap.size() > 0){
    			Iterator<Entry<String,String>> iterator = districtMap.entrySet().iterator();
    			while(iterator.hasNext()){
    				Entry<String,String> entry = iterator.next();
    				areaKeyList.add(entry.getKey());
    				areaValueList.add(entry.getValue());
    			}
    		}
    		if(brandMap != null && brandMap.size() > 0){
    			Iterator<Entry<String,String>> iterator = brandMap.entrySet().iterator();
    			while(iterator.hasNext()){
    				Entry<String,String> entry = iterator.next();
    				brandKeyList.add(entry.getKey());
    				brandValueList.add(entry.getValue());
    			}
    		}
    	}
    }
    
    public void showPopupWindow(final int index){
    	final List<String> keyList = index == 1 ? brandKeyList : areaKeyList;
    	final List<String> valueList = index == 1 ? brandValueList : areaValueList;
    	final View dropView = index == 1 ? llCarBrand : llArea;
    	final TextView textView = index == 1 ? tvCarBrand : tvArea;
    	View layout = LayoutInflater.from(this).inflate(R.layout.layout_phase2_district_brand_list, null);
    	final PopupWindow pop = new PopupWindow(layout, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
    	ListView listView = (ListView)layout.findViewById(R.id.listView);
    	listView.setAdapter(new BaseAdapter<String>(this, valueList, R.layout.layout_phase2_district_brand_item) {

			@Override
			public void viewHandler(int position, String t, View convertView) {
				TextView tvContent = ViewHolder.get(convertView, R.id.tvContent);
				FormsUtil.setTextViewTxt(tvContent, t);
			}
		});
    	listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				pop.dismiss();
				FormsUtil.setTextViewTxt(textView, valueList.get(position));
				if(index == 1){
					creditcardAction.queryCarShopList(String.valueOf(pageNum), "", keyList.get(position), "").start();
				}else{
					creditcardAction.queryCarShopList(String.valueOf(pageNum), keyList.get(position), "", "").start();
				}
			}
		});
    	
    	pop.setOutsideTouchable(true);
    	pop.setBackgroundDrawable(new PaintDrawable());
    	pop.setFocusable(true);// 注意必须设置可获得焦点
    	pop.showAsDropDown(dropView);
    	
    }
    
}
