package com.abc.sz.app.adapter.product;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.product.EvalutionBean;
import com.forms.library.tools.ViewHolder;

import java.util.List;

public class ProductEvaluationAdapter extends BaseAdapter {
    //评论内容
    private List<EvalutionBean> mList;

    public ProductEvaluationAdapter(List<EvalutionBean> list) {
        // TODO Auto-generated constructor stub
        mList = list;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        EvalutionBean eBean = mList.get(position);
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_evaluation_item, null);
        }
        RatingBar rb_ratingbar = ViewHolder.get(convertView, R.id.rb_ratingbar);
        TextView tv_date = ViewHolder.get(convertView, R.id.tv_date);
        TextView tv_name = ViewHolder.get(convertView, R.id.tv_name);
        TextView tv_content = ViewHolder.get(convertView, R.id.tv_content);

        tv_date.setText(eBean.getDateTime());
        tv_name.setText(eBean.getUserName());
        tv_content.setText(eBean.getContent());
        float score = Float.parseFloat(eBean.getScore());
        rb_ratingbar.setRating(score);

        return convertView;
    }

}
