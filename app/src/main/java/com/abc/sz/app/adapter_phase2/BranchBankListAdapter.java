package com.abc.sz.app.adapter_phase2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity_phase2.wdmap.WdMapActivity;
import com.abc.sz.app.bean.branch.BranchBank;
import com.abc.sz.app.bean.branch.BranchSearchRests;
import com.forms.library.baseUtil.view.ViewUtils;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by monkey on 2015/8/13.
 */
public class BranchBankListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int SEARCHTYPE = 1;
    public static final int BRANCHBANK_LIST = 2;
    public static final int LASTITEM = 3;
    public final static String BRANCH_DATA = "BRANCH_DATA";

    private List<BranchSearchRests> branchBanks = new ArrayList<>();

    public BranchBankListAdapter(List<BranchSearchRests> branchBanks) {
        this.branchBanks = branchBanks;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*if (viewType == SEARCHTYPE) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_phase2_wdlist_item_searchview, parent, false);
            return new SearchView(view);
        } else */
        if (viewType == LASTITEM) {
            TextView textView = new TextView(parent.getContext());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            textView.setGravity(Gravity.CENTER);
            int paddingBase = FormsUtil.dip2px(8);
            textView.setPadding(paddingBase * 2, paddingBase, paddingBase * 2, paddingBase * 2);
            textView.setText("没有更多数据");

            textView.setLayoutParams(layoutParams);
            return new LastItemView(textView);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_phase2_wdlist_item, parent, false);
            return new BranchBankView(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (BRANCHBANK_LIST == getItemViewType(position)) {
            final BranchBankView branchBankView = (BranchBankView) holder;
            final BranchSearchRests branchSearchRests = branchBanks.get(position);
            //支行
            BranchBank branchBank = branchSearchRests.getBranchBank();
            BranchBank selfServiceBank = branchSearchRests.getSelfServiceBank();
            BranchBank selfServiceEquip = branchSearchRests.getSelfServiceEquip();
            if (branchBank != null) {
                setBranchBanks(branchSearchRests.getTypeId(), branchBankView, branchBank);
            } else if (selfServiceBank != null) {
                //自助银行
                setBranchBanks(branchSearchRests.getTypeId(), branchBankView, selfServiceBank);
            } else if (selfServiceEquip != null) {
                //自助设备
                setBranchBanks(branchSearchRests.getTypeId(), branchBankView, selfServiceEquip);
            }
            branchBankView.containerLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = branchBankView.containerLayout.getContext();
                    Intent intent = new Intent(context, WdMapActivity.class);
                    intent.putExtra("BRANCH_DATA", branchSearchRests);
                    context.startActivity(intent);
                }
            });
        }
    }


    /**
     * 设置页面数据
     *
     * @param type           设备类型
     * @param branchBankView 页面视图对象
     * @param branchBanks    网点数据
     */
    private void setBranchBanks(String type, BranchBankView branchBankView, BranchBank branchBanks) {
        if ("2".equals(type)) {
            //自助银行
            branchBankView.tvBranchName.setText("自助银行");
        } else if ("3".equals(type)) {
            //自助设备
            branchBankView.tvBranchName.setText("自助设备");
        } else {
            FormsUtil.setTextViewTxt(branchBankView.tvBranchName, branchBanks.getName());
        }
        if (!TextUtils.isEmpty(branchBanks.getPhoneNumber())) {
            branchBankView.tvPhone.setVisibility(View.VISIBLE);
            FormsUtil.setTextViewTxts(branchBankView.tvPhone, "电话：{0}", branchBanks.getPhoneNumber());
        }
        FormsUtil.setTextViewTxts(branchBankView.tvAddress, "地址：{0}", branchBanks.getFullAddress());
    }

    @Override
    public int getItemCount() {
        if (branchBanks.size() > 0) {
            return branchBanks.size() + 1;
        } else {
            return 0;
        }
    }

    @Override
    public int getItemViewType(int position) {
        /*if (position == 0) {
            return SEARCHTYPE;
        } else */
        if (position == getItemCount() - 1) {
            return LASTITEM;
        } else {
            return BRANCHBANK_LIST;
        }
    }

   /* public static class SearchView extends RecyclerView.ViewHolder {
        @ViewInject(R.id.containerLayout) RelativeLayout containerLayout;

        public SearchView(View itemView) {
            super(itemView);
            containerLayout = (RelativeLayout) itemView.findViewById(R.id.containerLayout);
            containerLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = containerLayout.getContext();
                    Intent intent = new Intent(context, WdSearchActivity.class);
                    context.startActivity(intent);
                }
            });
        }
    }*/

    public static class BranchBankView extends RecyclerView.ViewHolder {
        @ViewInject(R.id.containerLayout) RelativeLayout containerLayout;
        @ViewInject(R.id.tvBranchName) TextView tvBranchName;
        @ViewInject(R.id.tvAddress) TextView tvAddress;
        @ViewInject(R.id.tvPhone) TextView tvPhone;

        public BranchBankView(View itemView) {
            super(itemView);
            ViewUtils.inject(this, itemView);
        }
    }

    public static class LastItemView extends RecyclerView.ViewHolder {

        public LastItemView(View itemView) {
            super(itemView);
        }
    }

}
