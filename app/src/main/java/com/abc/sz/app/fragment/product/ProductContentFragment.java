package com.abc.sz.app.fragment.product;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCFragment;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;

import java.util.ArrayList;
import java.util.List;


/**
 * 商品详情下半页
 *
 * @author llc
 */
public class ProductContentFragment extends ABCFragment {

    //Fragment页
    @ViewInject(R.id.fl_content)
    private FrameLayout fl_content;
    //商品介绍
    @ViewInject(R.id.tv_spjs)
    private TextView tv_spjs;
    //商品评价
    @ViewInject(R.id.tv_sppj)
    private TextView tv_sppj;
    //包装收后
    @ViewInject(R.id.tv_bzsh)
    private TextView tv_bzsh;

    FragmentManager fragmentManager;
    private List<ABCFragment> fragmentList = new ArrayList<>();
    private FragmentTransaction transaction;
    //判断评论List是否滑动到顶部
    public static boolean isListScrolledTop;
    public static boolean isTop = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_product_content, container, false);
        return view;
    }

    @Override
    protected void initData() {
        // TODO Auto-generated method stub
        super.initData();
        fragmentManager = baseActivity.getSupportFragmentManager();
        addFragment(new ProductContentInfoFragment());
        addFragment(new ProductContentEvaluationFragment());
        addFragment(new ProductContentBZSHFragment());
        showFragment(0);
    }

    @Override
    protected void initView() {
        // TODO Auto-generated method stub
        super.initView();
        tv_spjs.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
    }


    @OnClick(value = {R.id.tv_spjs, R.id.tv_sppj, R.id.tv_bzsh})
    public void btnOnClick(View view) {
        int index = 0;
        switch (view.getId()) {
            case R.id.tv_spjs:
                //商品详情
                index = 0;
                tv_spjs.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                tv_sppj.setTextColor(getResources().getColor(R.color.black));
                tv_bzsh.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.tv_sppj:
                //商品评论
                index = 1;
                tv_spjs.setTextColor(getResources().getColor(R.color.black));
                tv_sppj.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                tv_bzsh.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.tv_bzsh:
                //包装售后
                index = 2;
                tv_spjs.setTextColor(getResources().getColor(R.color.black));
                tv_sppj.setTextColor(getResources().getColor(R.color.black));
                tv_bzsh.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                break;
        }
        showFragment(index);
    }

//    /**
//     * 切换Fragment
//     */
//    private void replaceFragment(ABCFragment fragment) {
//        // TODO  切换Fragment
//        transaction = fragmentManager.beginTransaction();
//        transaction.replace(R.id.fl_content, fragment);
//        transaction.commit();
//    }
    
    private void addFragment(ABCFragment fragment){
   	 	transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.fl_content, fragment);
        transaction.commit();
        fragmentList.add(fragment);
    }
   
   /**
    * 切换Fragment
    */
   public void showFragment(int indexSelect) {
   		for (int i = 0; i < fragmentList.size(); i++) {
           ABCFragment fragment = fragmentList.get(i);
           if (!fragment.isHidden()) {
               fragmentManager.beginTransaction().hide(fragment).commit();
           }
       }
       fragmentManager.beginTransaction().show(fragmentList.get(indexSelect)).commit();
   }

    /**
     * 获得详情图片列表
     *
     * @return
     */
    public ProductContentInfoFragment getPicFragment() {
        return (ProductContentInfoFragment) fragmentList.get(0);
    }

    /**
     * 获得商品评论
     *
     * @return
     */
    public ProductContentEvaluationFragment getEvaluationFragment() {
        return (ProductContentEvaluationFragment) fragmentList.get(1);
    }

    /**
     * 获得包装售后
     *
     * @return
     */
    public ProductContentBZSHFragment getBZSHFragment() {
        return (ProductContentBZSHFragment) fragmentList.get(2);
    }

}
