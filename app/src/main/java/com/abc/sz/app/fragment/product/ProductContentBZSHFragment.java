package com.abc.sz.app.fragment.product;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.ProductInfoAction;
import com.abc.sz.app.activity.product.ProductInfoActivity;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.product.QBzshInfo;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCFragment;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * 包装售后
 *
 * @author llc
 */
public class ProductContentBZSHFragment extends ABCFragment {

    @ViewInject(R.id.loadingView) LoadingView loadingView;
    @ViewInject(R.id.tv_content) TextView tv_content;

    private ProductInfoAction action;

    private String productId;
    private RequestBean<QBzshInfo> params;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_product_bzsh, container, false);
        return view;
    }

    @Override
    protected void initData() {
        // TODO Auto-generated method stub
        super.initData();

        action = controller.getTargetAction(getABCActivity(), this, ProductInfoAction.class);
        productId = ((ProductInfoActivity) baseActivity).getProductId();


        QBzshInfo info = new QBzshInfo();
        info.setProductId(productId);
        params = new RequestBean<QBzshInfo>(info);
        action.loadingBZSH(params).setLoadingView(loadingView).start();
    }


    /**
     * 评论请求回调
     *
     * @param list
     */
    public void onSuccess(String bean) {
        tv_content.setText(bean);
    }

    /**
     * 请求失败回调
     *
     * @param retCode
     */
    public void failed(RetCode retCode, String requestFrom) {
        if ("loadingEvaluation".equals(requestFrom)) {

        }
    }

}
