package com.abc.sz.app.activity.set;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.Entrance;
import com.abc.sz.app.util.DialogUtil;
import com.forms.base.ABCActivity;
import com.forms.library.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;
import com.forms.view.toast.MyToast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 常用生活助手
 *
 * @author ftl
 */
@ContentView(R.layout.activity_entrance)
public class EntranceActivity extends ABCActivity {
    @ViewInject(R.id.appBar) android.support.v7.widget.Toolbar toolbar;
    @ViewInject(R.id.lv_entrance)
    private ListView lv_entrance;

    private BaseAdapter<Entrance> adapter;
    private List<Entrance> list = new ArrayList<Entrance>();
    private Map<Integer, Boolean> map = new HashMap<Integer, Boolean>();
    private int selectCount = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initData();
        initListener();
    }

    public void initData() {
        List<Entrance> queryList = getDBUtils().findAll(Entrance.class);
        if (queryList != null && queryList.size() != 0) {
            for (int i = 0; i < queryList.size(); i++) {
                map.put(queryList.get(i).getEntranceId(), true);
            }
        }

        String[] entranceText = getResources().getStringArray(R.array.entrance_text);
        String[] entranceIcon = getResources().getStringArray(R.array.entrance_icon);
        for (int i = 0; i < entranceIcon.length; i++) {
            list.add(new Entrance(i, entranceText[i], entranceIcon[i]));
            if (!map.containsKey(i)) {
                map.put(i, false);
            }
        }

        lv_entrance.setAdapter(adapter = new BaseAdapter<Entrance>(this, list, R.layout.layout_entrance_item) {

            @Override
            public void viewHandler(final int position, Entrance entrance, View convertView) {
                ImageView iv_select = ViewHolder.get(convertView, R.id.iv_select);
                ImageView iv_entrance_icon = ViewHolder.get(convertView, R.id.iv_entrance_icon);
                TextView tv_entrance_text = ViewHolder.get(convertView, R.id.tv_entrance_text);

                if (map.get(position)) {
                    iv_select.setVisibility(View.VISIBLE);
                } else {
                    iv_select.setVisibility(View.INVISIBLE);
                }
                int resource = getResources().getIdentifier(entrance.getEntranceIcon(),
                        "drawable", "com.abc.ABC_SZ_APP");
                iv_entrance_icon.setImageResource(resource);
                FormsUtil.setTextViewTxt(tv_entrance_text, entrance.getEntranceText());
            }

        });
    }

    public void initListener() {
        lv_entrance.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                if (map.get(position)) {
                    map.put(position, false);
                } else {
                    map.put(position, true);
                }
                int checkCount = 0;
                for (int i = 0; i < list.size(); i++) {
                    if (map.get(i)) {
                        checkCount++;
                    }
                }
                selectCount = checkCount;
                if (selectCount > 5) {
                    MyToast.show(EntranceActivity.this, "最多选择5个常用生活助手", MyToast.TEXT,
                            Gravity.CENTER, Toast.LENGTH_SHORT);
                    map.put(position, false);
                    selectCount = 5;
                } else {
                    adapter.notifyDataSetChanged();
                }
            }
        });

        setOnFinishingListener(new OnFinishingListener() {

            @Override
            public boolean onFinishing() {
                if (selectCount == 5) {
                    getDBUtils().deleteAll(Entrance.class);
                    for (int i = 0; i < list.size(); i++) {
                        if (map.get(i)) {
                            getDBUtils().save(list.get(i));
                        }
                    }
                    return true;
                } else {
                    DialogUtil.showWithTwoBtnAndTitle(EntranceActivity.this, "提示",
                            "您的选择不足5个常用生活助手，是否放弃此处编辑?",
                            "放弃",
                            "继续",
                            new OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    EntranceActivity.this.finish();
                                }
                            }, new OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    return false;
                }
            }
        });
    }
}
