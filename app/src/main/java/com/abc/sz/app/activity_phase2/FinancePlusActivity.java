package com.abc.sz.app.activity_phase2;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.adapter_phase2.FinancePlusAdapter;
import com.abc.sz.app.view.plusitemview.PlusItemBean;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by hwt on 4/27/15.
 */
@ContentView(R.layout.activity_financeplus)
public class FinancePlusActivity extends ABCActivity {
    @ViewInject(R.id.recyclerView) RecyclerView recyclerView;
    @ViewInject(R.id.appBar) Toolbar toolbar;

    private List<PlusItemBean> plusItemBeans = new ArrayList<>();
    private FinancePlusAdapter plusAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.finance);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        plusItemBeans.addAll(getDBUtils().findAll(PlusItemBean.class));
        Collections.sort(plusItemBeans);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(plusAdapter = new FinancePlusAdapter(plusItemBeans));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.activity_lightpay_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            for (PlusItemBean plusItemBean : plusItemBeans) {
                plusItemBean.setPriority(plusItemBean.isChecked() ? 10 : 0);
            }
            getDBUtils().updateAll(plusItemBeans, "priority", "isChecked");
            setResult(RESULT_OK);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
