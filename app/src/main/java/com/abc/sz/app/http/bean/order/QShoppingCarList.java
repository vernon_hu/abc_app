package com.abc.sz.app.http.bean.order;

import com.abc.sz.app.bean.ShoppingCar;

import java.util.List;


/**
 * 购物车
 *
 * @author ftl
 */
public class QShoppingCarList {

    private List<ShoppingCar> list;

    public QShoppingCarList(List<ShoppingCar> list) {
        super();
        this.list = list;
    }

    public List<ShoppingCar> getList() {
        return list;
    }

    public void setList(List<ShoppingCar> list) {
        this.list = list;
    }

}
