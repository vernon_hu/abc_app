package com.abc.sz.app.activity.kb;

import java.text.MessageFormat;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.KbAction;
import com.abc.sz.app.activity.product.ProductInfoActivity;
import com.abc.sz.app.bean_phase2.Omnibus;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.util.ShoppingCarCount;
import com.abc.sz.app.view.LoadingView;
import com.abc.sz.app.view.ShareUtil;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;

/**
 * 快报详情
 *
 * @author llc
 */
@ContentView(R.layout.activity_kbinfo)
public class KbInfoActivity extends ABCActivity {
    @ViewInject(R.id.loadingView) LoadingView loadingView;
    // 正文布局
    @ViewInject(R.id.main) View main;
    @ViewInject(R.id.tv_product_name) TextView productName;
    @ViewInject(R.id.tv_product_old_price) TextView oldPrice;
    @ViewInject(R.id.tv_product_price) TextView price;
    @ViewInject(R.id.tv_product_preferential) TextView perferential;
    @ViewInject(R.id.tv_seller) TextView sellerName;
    @ViewInject(R.id.tv_context) TextView content;
    @ViewInject(R.id.iv_product_picture) ImageView productImage;
    @ViewInject(R.id.appBar) Toolbar toolbar;

    private Omnibus jxBean;
    private String productId;
    // 请求
    private KbAction kbAction;
    private ShareUtil shareUtil;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initData();
    }

    private void initData() {

        oldPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        jxBean = getCacheBean().getCache(Omnibus.class);

        kbAction = (KbAction) controller.getAction(this, KbAction.class);
        if (jxBean != null) {
            kbAction.loadingBulletinInfo(jxBean.getDailyId()).start(false);
        }
    }

    // 分享图片
    @OnClick(value = {R.id.bt_info})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_info:
                Bundle bundle = new Bundle();
                bundle.putString(ProductInfoActivity.PRODUCT_ID, productId);
                callMe(ProductInfoActivity.class, bundle);
                break;
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_productinfo_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.share && jxBean != null) {
        	if(shareUtil == null){
        		shareUtil = new ShareUtil(this, main, jxBean.getProductName(),
	                     jxBean.getHisPrice(), jxBean.getProductPrice(),
	                     jxBean.getReasonContent(), jxBean.getProductPic(),
	                     jxBean.getUrl(), "2", jxBean.getProductId(),
	                     imageLoader);
        	}
        	shareUtil.showShareWindow();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * 请求成功回调
     *
     * @param bean
     */
    public void success(Omnibus bean) {
        loadingView.loaded();
        jxBean = bean;
        productId = jxBean.getProductId();

        FormsUtil.setTextViewTxt(productName, jxBean.getProductName());
        FormsUtil.setTextViewTxt(perferential, jxBean.getCondition());
        FormsUtil.setTextViewTxt(content, jxBean.getReasonContent());
        FormsUtil.setTextViewTxt(sellerName, MessageFormat.format(
                sellerName.getText().toString(),
                TextUtils.isEmpty(jxBean.getSellerName()) ? "-" : jxBean
                        .getSellerName()));
        oldPrice.setText(MessageFormat.format(oldPrice.getText().toString(),
                TextUtils.isEmpty(jxBean.getHisPrice()) ? "-"
                        : ShoppingCarCount.formatMoney(jxBean.getHisPrice())));
        price.setText(MessageFormat
                .format(price.getText().toString(),
                        TextUtils.isEmpty(jxBean.getProductPrice()) ? "-"
                                : ShoppingCarCount.formatMoney(jxBean
                                .getProductPrice())));
        imageLoader.displayImage(jxBean.getProductPic(), productImage,
                ImageViewUtil.getOption());
    }

    /**
     * TODO:请求失败回调
     *
     * @param retCode
     */
    public void failed(RetCode retCode, String requestFrom) {
        loadingView.loaded();
        loadingView.failed(retCode.getRetMsg());
    }

}
