package com.abc.sz.app.activity.order;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.ShoppingCarAction;
import com.abc.sz.app.adapter.order.ShoppingCarEditAdapter;
import com.abc.sz.app.bean.ShoppingCar;
import com.abc.sz.app.http.bean.order.ShoppingCarDelete;
import com.abc.sz.app.util.DialogUtil;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.view.toast.MyToast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 购物车
 *
 * @author ftl
 */
@ContentView(R.layout.activity_shopping_car_edit)
public class ShoppingCarEditActivity extends ABCActivity {
	
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.lv_product) ListView lv_product;
    @ViewInject(R.id.cb_countPrice) CheckBox cb_countPrice;

    private ShoppingCarEditAdapter adapter;
    private List<ShoppingCar> list = new ArrayList<ShoppingCar>();
    private ShoppingCarAction shoppingCarAction;
    private boolean isUpdate = false;//购物车是否更新
    private Map<Integer, Boolean> map;
    private MenuItem editMenuItem;

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (adapter.getMap().containsValue(false)) {
                cb_countPrice.setChecked(false);
            } else {
                cb_countPrice.setChecked(true);
            }
        };
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        shoppingCarAction = (ShoppingCarAction) controller.getAction(this, ShoppingCarAction.class);
        initData();
        initListener();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_edit_menu,menu);
        editMenuItem = menu.findItem(R.id.edit);
        editMenuItem.setTitle(R.string.shopping_finish);
        editMenuItem.setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.edit){
            shoppingCarAction.updateShoppingCar(list).start();
        }else if(item.getItemId() == android.R.id.home){
        	if(isUpdate){
        		finishForBack(RESULT_OK);
        	}else{
        		finish();
        	}
        }
        return super.onOptionsItemSelected(item);
    }
    /**
     * 初始化数据
     */
    @SuppressWarnings("unchecked")
    private void initData() {
    	Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
        	list = (List<ShoppingCar>) bundle.get("shoppingCarList");
        }
        adapter = new ShoppingCarEditAdapter(list, handler, imageLoader);
        lv_product.setAdapter(adapter);
    }

    private void initListener() {
        cb_countPrice.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    for (int i = 0; i < list.size(); i++) {
                        adapter.getMap().put(i, true);
                    }
                } else {
                    if (!adapter.getMap().containsValue(false)) {
                        for (int i = 0; i < list.size(); i++) {
                            adapter.getMap().put(i, false);
                        }
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });

    }

    @OnClick(value = {R.id.btn_delete})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_delete:
                map = adapter.getMap();
                if (map.containsValue(true)) {
                    final List<ShoppingCar> shoppingCarList = new ArrayList<ShoppingCar>();
                    final List<ShoppingCarDelete> stringList = new ArrayList<ShoppingCarDelete>();
                    for (int i = 0; i < list.size(); i++) {
                        if (!map.get(i)) {
                            shoppingCarList.add(list.get(i));
                        } else {
                            stringList.add(new ShoppingCarDelete(list.get(i).getProductId()));
                        }
                    }
                    DialogUtil.showWithTwoBtnAndTitle(this, "提示", "是否确定删除选中的商品？", "确定", "取消", new OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            shoppingCarAction.deleteShoppingCar(stringList).start();
                        }
                    }, new OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                } else {
                    MyToast.show(ShoppingCarEditActivity.this, "至少选择一个商品", MyToast.TEXT,
                            Gravity.CENTER, Toast.LENGTH_SHORT);
                }
                break;
        }
    }

    /**
     * 购物车更新成功
     */
    public void updateSuccess() {
    	finishForBack(RESULT_OK);
    }
    
    /**
     * 购物车商品删除成功
     */
    public void deleteSuccess(){
    	isUpdate = true;
    	//不是删除所有
        if (map.containsValue(false)) {
            List<ShoppingCar> shoppingCarList = new ArrayList<ShoppingCar>();
            for (int i = 0; i < list.size(); i++) {
                if (!map.get(i)) {
                    shoppingCarList.add(list.get(i));
                }
            }
            list.clear();
            map.clear();
            for (int i = 0; i < shoppingCarList.size(); i++) {
                list.add(shoppingCarList.get(i));
                map.put(i, false);
            }
            adapter.notifyDataSetChanged();
        } else {
        	finishForBack(RESULT_OK);
        }
    }

    /**
     * 购物车更新失败
     *
     * @param retCode
     */
    public void updateFailed(RetCode retCode) {
        DialogUtil.showWithNoTitile(this, retCode.getRetMsg());
    }
    
    @Override
    public void onBackPressed() {
    	if(isUpdate){
    		finishForBack(RESULT_OK);
    	}else{
    		finish();
    	}
    }

}
