package com.abc.sz.app.activity.set;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCActivity;
import com.forms.base.ABCApplication;
import com.forms.base.BaseConfig;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.view.toast.MyToast;

/**
 * 设置
 *
 * @author ftl
 */
@ContentView(R.layout.activity_set)
public class SetActivity extends ABCActivity {
    //    @ViewInject(R.id.isTest) CheckBox isTest;
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.testServerIp) EditText testServerIp;
    @ViewInject(R.id.testServerIpLayout) LinearLayout testServerIpLayout;
    Dialog testTypesDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
//        String[] testDataTypes = getResources().getStringArray(R.array.selected_testType);


//        isTest.setChecked(Request.isTest);
        if (getBaseApp().isTest()) {
            testServerIpLayout.setVisibility(View.VISIBLE);
            Uri uri = Uri.parse(BaseConfig.SERVER_URL);
            testServerIp.setText(uri.getHost());
        }
    }

    @OnClick(value = {R.id.ll_about,
            R.id.ll_check_update,
            R.id.ll_suggestion,
            R.id.commit
    })
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_about:
                callMe(AboutActivity.class);
                break;
            case R.id.ll_check_update:
                MyToast.show(SetActivity.this, "当前已是最新版本!", MyToast.TEXT,
                        Gravity.CENTER, Toast.LENGTH_SHORT);
                break;
            case R.id.ll_suggestion:
                callMe(SuggestionActivity.class);
                break;
//            case R.id.ll_test:
//                testTypesDialog.show();
//                break;
//            case R.id.ll_entrance:
//                callMe(EntranceActivity.class);
//                break;
            case R.id.commit:
                String serverIp = testServerIp.getText().toString();
                SharedPreferences sharedPref = getSharedPreferences(ABCApplication.testIP, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(ABCApplication.testIPString, serverIp);
                editor.apply();
                Toast.makeText(this, "请退出应用重新进入", Toast.LENGTH_SHORT).show();
                finish();
                break;
        }
    }

//    @OnCompoundButtonCheckedChange(R.id.isTest)
//    public void isTEstChcekChange(CompoundButton buttonView, boolean isChecked) {
//        if (isChecked) {
//            Request.isTest = true;
//        } else {
//            Request.isTest = false;
//        }
//    }
}
