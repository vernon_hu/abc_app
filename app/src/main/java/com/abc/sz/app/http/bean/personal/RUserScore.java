package com.abc.sz.app.http.bean.personal;


/**
 * 用户积分
 *
 * @author ftl
 */
public class RUserScore {

    private String score;

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }


}
