package com.abc.sz.app.bean;

import com.forms.library.base.BaseBean;

/**
 * 支付码
 *
 * @author ftl
 */
public class PayCode extends BaseBean {

    /* 商品 ID*/
    private String orderId;
    /* 支付码 ID*/
    private String paymentId;
    /* 商品 名称*/
    private String productName;
    /* 有效期*/
    private String period;
    /* 商品类别图标*/
    private String pictureId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPictureId() {
        return pictureId;
    }

    public void setPictureId(String pictureId) {
        this.pictureId = pictureId;
    }


}
