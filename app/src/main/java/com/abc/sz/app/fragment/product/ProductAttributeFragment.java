package com.abc.sz.app.fragment.product;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.ProductInfoAction;
import com.abc.sz.app.adapter.product.AttributeItemAdapter;
import com.abc.sz.app.bean.product.AttributeValueBean;
import com.abc.sz.app.bean.product.TypeAttributeBean;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.product.QProductInfo;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCFragment;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.widget.slidingmenu.SlidingMenu;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品属性
 *
 * @author llc
 */
public class ProductAttributeFragment extends ABCFragment {

    private SlidingMenu menu;
    // 属性列表
    @ViewInject(R.id.lv_attribute) ListView lv_attribute;
    @ViewInject(R.id.loadingView) LoadingView loadingView;

    private List<TypeAttributeBean> attributeList = new ArrayList<TypeAttributeBean>();
    private List<AttributeValueBean> list = new ArrayList<AttributeValueBean>();
    private AttributeItemAdapter itemAdapter;
    private ProductInfoAction action;
    private String groupId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_product_attribute_menu, container, false);
        return view;
    }

    @Override
    protected void initData() {
        // TODO 初始化数据
        super.initData();
//        ProductInfoFragment productInfoFragment;
//        if ((productInfoFragment = (ProductInfoFragment) baseActivity
//                .getSupportFragmentManager().findFragmentByTag(
//                        "android:switcher:" + R.id.vp_content + ":0")) != null) {
//            menu = ((ProductInfoActivity)productInfoFragment.getABCActivity()).getMenu();
//        }
        action = controller.getTargetAction(baseActivity, baseActivity, ProductInfoAction.class);
//        menu = ((ProductInfoActivity)baseActivity).getMenu();
    }

    @Override
    protected void initView() {
        // TODO 初始化视图
        super.initView();

        itemAdapter = new AttributeItemAdapter(list, attributeList);
        lv_attribute.setAdapter(itemAdapter);
    }

    @Override
    protected void initListener() {
        // TODO Auto-generated method stub
        super.initListener();
    }

    /**
     * 点击事件
     *
     * @param view
     */
    @OnClick(value = {R.id.bt_cancle, R.id.bt_sure})
    public void btnOnClick(View view) {
        switch (view.getId()) {
            case R.id.bt_sure:
                QProductInfo productInfo = new QProductInfo();
                productInfo.setGroupId(groupId);
                productInfo.setAttribute(itemAdapter.getAttributeList());
                RequestBean<QProductInfo> params = new RequestBean<QProductInfo>(productInfo);
                action.loadingProductInfo(params, false).start();
                break;
            case R.id.bt_cancle:
                break;
        }
        menu.showContent();
    }

    public ListView getLv_attribute() {
        return lv_attribute;
    }

    public List<TypeAttributeBean> getAttributeList() {
        return attributeList;
    }

    /**
     * 属性回调
     *
     * @param bean
     */
    public void onAttributeResponse(List<TypeAttributeBean> bean) {
        attributeList.addAll(bean);
        itemAdapter.notifyDataSetChanged();
    }

    /**
     * 属性值
     *
     * @param list
     */
    public void setProductAttribute(List<AttributeValueBean> list) {
        // TODO Auto-generated method stub
        this.list.addAll(list);
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }


}
