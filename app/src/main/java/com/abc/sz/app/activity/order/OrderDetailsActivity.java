package com.abc.sz.app.activity.order;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.OrderAction;
import com.abc.sz.app.activity.product.ProductInfoActivity;
import com.abc.sz.app.adapter.order.EntityOrderProductAdapter;
import com.abc.sz.app.bean.Order;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.util.ShoppingCarCount;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.net.HttpQueue;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.net.ViewLoading.OnRetryListener;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 订单详情
 *
 * @author ftl
 */
@ContentView(R.layout.activity_order_details)
public class OrderDetailsActivity extends ABCActivity {
    @ViewInject(R.id.appBar) android.support.v7.widget.Toolbar toolbar;

    @ViewInject(R.id.tv_sellerName)
    private TextView tv_sellerName;
    @ViewInject(R.id.tv_sumPrice)
    private TextView tv_sumPrice;
    @ViewInject(R.id.tv_sellerOrderId)
    private TextView tv_sellerOrderId;
    @ViewInject(R.id.tv_postTime)
    private TextView tv_postTime;
    @ViewInject(R.id.ll_payTime)
    private LinearLayout ll_payTime;
    @ViewInject(R.id.tv_payTime)
    private TextView tv_payTime;
    @ViewInject(R.id.lv_product)
    private ListView lv_product;
    @ViewInject(R.id.btn_operate)
    private Button btn_operate;
    @ViewInject(R.id.btn_cancel)
    private Button btn_cancel;
    @ViewInject(R.id.loadingView)
    private LoadingView loadingView;

    private EntityOrderProductAdapter adapter;
    private List<Product> list = new ArrayList<Product>();
    private Order order;
    private OrderAction orderAction;
    private HttpQueue httpQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        orderAction = (OrderAction) controller.getAction(this, OrderAction.class);
        initData();
        initListener();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        adapter = new EntityOrderProductAdapter(list, imageLoader);
        lv_product.setAdapter(adapter);
        order = getCacheBean().getCache(Order.class);
        if (order != null) {
        	String state = order.getState();
            if (OrderState.success.getCode().equals(state) || OrderState.noUse.getCode().equals(state) 
            		|| OrderState.valuation.getCode().equals(state)) {
                ll_payTime.setVisibility(View.VISIBLE);
            }
            loadingView.loading(0);
            orderAction.queryOrderDetails(order.getOrderId()).start(false);
            loadingView.retry(new OnRetryListener() {
    			
    			@Override
    			public void retry() {
    				orderAction.queryOrderDetails(order.getOrderId()).start(false);
    			}
    		});
        }
    }

    /**
     * 初始化监听
     */
    private void initListener() {
        lv_product.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                Bundle bundle = new Bundle();
                bundle.putString(ProductInfoActivity.PRODUCT_ID, list.get(position).getProductId());
                callMe(ProductInfoActivity.class, bundle);
            }
        });
    }

    @OnClick(value = {R.id.btn_operate, R.id.btn_cancel})
    public void onClick(View view) {
    	String state = order.getState();
    	if (state != null) {
	        switch (view.getId()) {
	            case R.id.btn_operate:
	                if (OrderState.success.getCode().equals(state)) {
	                    callMeForBack(ProductEvaluateActivity.class, 1);
	                } else if (OrderState.noGet.getCode().equals(state)) {
	                    new AlertDialog.Builder(this).setMessage("是否确认收货?")
	                            .setPositiveButton("确定", new DialogInterface.OnClickListener() {
	                                @Override
	                                public void onClick(DialogInterface dialog, int which) {
	                                    orderAction.conFirmReceiveGood(order.getOrderId()).start();
	                                }
	                            })
	                            .setNegativeButton("取消", new DialogInterface.OnClickListener() {
	                                @Override
	                                public void onClick(DialogInterface dialog, int which) {
	                                    dialog.dismiss();
	                                }
	                            }).create().show();
	                } else if (OrderState.noPay.getCode().equals(state) || OrderState.failed.getCode().equals(state)) {
	                	orderAction.orderImmediatePay(order.getOrderId(), String.valueOf(order.getAmount()), 
	                			"", order.getProductList(), "1").start();
	                } 
	                break;
	            case R.id.btn_cancel:
	            	 new AlertDialog.Builder(this).setMessage("是否确认取消订单?")
		                 .setPositiveButton("确定", new DialogInterface.OnClickListener() {
		                     @Override
		                     public void onClick(DialogInterface dialog, int which) {
		                    	 orderAction.orderCancel(order.getOrderId()).start();
		                     }
		                 })
		                 .setNegativeButton("取消", new DialogInterface.OnClickListener() {
		                     @Override
		                     public void onClick(DialogInterface dialog, int which) {
		                         dialog.dismiss();
		                     }
	                 }).create().show();
	                break;
	         }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1 || requestCode == 2) {
                finishForBack(RESULT_OK);
            }
        }
    }

    /**
     * 查询订单详情成功
     *
     * @param response
     */
    public void querySuccess(Order response) {
        loadingView.loaded();
        if (response != null) {
            Order order = response;
            FormsUtil.setTextViewTxt(tv_sellerName, order.getSellerName(), null);
            FormsUtil.setTextViewTxt(tv_sellerOrderId, order.getOrderId(), null);
            FormsUtil.setTextViewTxt(tv_postTime, order.getPostTime(), null);
            FormsUtil.setTextViewTxt(tv_payTime, order.getPayTime(), null);
            if (order.getAmount() != null && !"".equals(order.getAmount()))
                tv_sumPrice.setText(ShoppingCarCount.formatMoney(order.getAmount()));
            String state = order.getState();
            if (state != null) {
                if (OrderState.success.getCode().equals(state)) {
                    //评价
                    btn_operate.setText(getResources().getString(R.string.product_evaluate));
                } else if (OrderState.noGet.getCode().equals(state)) {
                    //确认收货
                    btn_operate.setText(getResources().getString(R.string.product_affirm_harvest));
                } else if (OrderState.noPay.getCode().equals(state) || OrderState.failed.getCode().equals(state)) {
                    //支付
                    btn_cancel.setVisibility(View.VISIBLE);
                    btn_operate.setText(getResources().getString(R.string.product_pay));
                } else if (OrderState.error.getCode().equals(state) || OrderState.valuation.getCode().equals(state)) {
                	btn_operate.setVisibility(View.GONE);
                } else {
                	btn_cancel.setVisibility(View.GONE);
                }
            }
            list = order.getProductList();
            adapter.setList(list);
            adapter.notifyDataSetChanged();
        } else {
            loadingView.noData(getString(R.string.loadingNoData));
        }
    }

    /**
     * 查询订单详情失败
     *
     * @param retCode
     */
    public void queryFailed(RetCode retCode) {
        loadingView.loaded();
        loadingView.failed(retCode.getRetMsg());
    }

    /**
     * 取消订单成功
     */
    public void cancelSuccess() {
        finishForBack(RESULT_OK);
    }

    /**
     * 订单支付成功
     */
    public void paySuccess(String content){
    	Bundle bundle = new Bundle();
    	bundle.putString("paymentyURL", content);
    	callMeForBack(OrderAffirmPayActivity.class, bundle, 2);
    }
    
    /**
     * 订单状态查询成功
     */
    public void queryOrderSuccess(String content){
    	if(!OrderState.noPay.getCode().equals(content)){
    		httpQueue.interupt();
    	}
    }
}
