package com.abc.sz.app.activity_phase2.depositloan;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;

/**
 * 存贷通收益计算器页面
 * 
 * @author hkj
 */
@ContentView(R.layout.activity_phase2_depositloancalculator)
public class DepositLoanCalculatorActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	@ViewInject(R.id.tvIncome)
	private TextView tvIncome;
	@ViewInject(R.id.tvRealRate)
	private TextView tvRealRate;
	@ViewInject(R.id.etDepositSum)
	private EditText etDepositSum;
	@ViewInject(R.id.etLoanSum)
	private EditText etLoanSum;
	@ViewInject(R.id.etLoanRate)
	private EditText etLoanRate;
	@ViewInject(R.id.etDepositMonth)
	private EditText etDepositMonth;
	@ViewInject(R.id.cbContain)
	private CheckBox cbContain;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		
	}

	@OnClick({ R.id.btnCalculate, R.id.tvRule })
	public void calculate(View v) {
		switch (v.getId()) {
		case R.id.btnCalculate:
			break;
		case R.id.tvRule:
			break;
		}
	}
	
}
