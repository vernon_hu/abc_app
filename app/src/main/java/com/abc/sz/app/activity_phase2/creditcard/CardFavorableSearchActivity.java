package com.abc.sz.app.activity_phase2.creditcard;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.CreditcardAction;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * 信用卡优惠搜索页面
 * 
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_cardfavorablesearch)
public class CardFavorableSearchActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	
	private CreditcardAction creditcardAction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		//隐藏Label标签
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		init();
	}
	
	private void init() {
		creditcardAction = (CreditcardAction) controller.getAction(this, CreditcardAction.class);
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_text_search_menu, menu);
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        searchView.setOnQueryTextListener(new OnQueryTextListener() {
			
			@Override
			public boolean onQueryTextSubmit(String arg0) {
				
				return false;
			}
			
			@Override
			public boolean onQueryTextChange(String newText) {
				if ( newText != null && newText . length ( ) > 0 ) {
                     
                 }
				return false;
			}
		});
        return true;
    }

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		
		}
		return super.onOptionsItemSelected(item);
	}
	
}
