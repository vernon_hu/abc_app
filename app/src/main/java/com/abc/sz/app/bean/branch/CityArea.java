package com.abc.sz.app.bean.branch;

/**
 * Created by monkey on 2015/8/12.
 * 附近网点 城市区域BEAN
 */
public class CityArea {
    private String Id;
    private String Name;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
