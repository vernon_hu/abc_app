package com.abc.sz.app.activity_phase2.wdmap;

import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.CreditcardAction;
import com.abc.sz.app.action_phase2.WdMapAction;
import com.abc.sz.app.adapter_phase2.WdListAdapter;
import com.abc.sz.app.bean.WdMap;
import com.abc.sz.app.bean.cridetcard.DistrictAndBrand;
import com.abc.sz.app.view.XdrDialog;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.forms.base.ABCActivity;
import com.forms.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 网点地图
 * @author ftl
 *
 */
@ContentView(R.layout.activity_phase2_wdlist_copy)
public class WdListActivityCopy extends ABCActivity {
	
	@ViewInject(R.id.appBar) Toolbar toolbar;
	@ViewInject(R.id.tvArea) TextView tvArea;
	@ViewInject(R.id.tvRange) TextView tvRange;
	@ViewInject(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
	@ViewInject(R.id.recyclerView) RecyclerView recyclerView;
	
	//定位相关
	private	LocationClient mLocClient;
	//定位监听
	private	MyLocationListenner myListener = new MyLocationListenner();
	//是否首次定位
	boolean isFirstLoc = true;
	//所在位置纬度
	double latitude = 0;
	//所在位置经度
	double longitude = 0;
	private EditText etSearchText;
	private CreditcardAction creditcardAction;
	private WdMapAction wdMapAction;
	private List<WdMap> wdMapList = new ArrayList<>();
	private List<String> areaKeyList = new ArrayList<String>();
	private List<String> areaValueList = new ArrayList<String>();
	private List<String> rangeKeyList = new ArrayList<String>();
	private List<String> rangeValueList = new ArrayList<String>();
	private WdListAdapter wdListAdapter;
	private int pageNum = 0; 
	private int index = 1;
	private XdrDialog xdrDialog;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		if (getSupportActionBar() != null) {
             ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.MATCH_PARENT,
                    Gravity.CENTER
             );
             View view = LayoutInflater.from(this).inflate(R.layout.layout_phase2_wdlist_searchview, null);
             etSearchText = (EditText) view.findViewById(R.id.etSearchText);
             getSupportActionBar().setCustomView(view, params);
             getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
             getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	         getSupportActionBar().setHomeButtonEnabled(true);
	    }
        
        init();
	}
	
	 private void init() {
		xdrDialog = new XdrDialog(this);
		xdrDialog.setCancelable(false);
		xdrDialog.show();
		
		// 定位初始化
		mLocClient = new LocationClient(this);
		mLocClient.registerLocationListener(myListener);
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);// 打开gps
		option.setCoorType("bd09ll"); // 设置坐标类型
		option.setScanSpan(1000);
		mLocClient.setLocOption(option);
		mLocClient.start();
			
		 String[] rangeKeyArray = getResources().getStringArray(R.array.range_key);
		 String[] rangeValueArray = getResources().getStringArray(R.array.range_value);
		 for (int i = 0; i < rangeKeyArray.length; i++) {
			 rangeKeyList.add(rangeKeyArray[i]);
			 rangeValueList.add(rangeValueArray[i]);
		 }
		 creditcardAction = (CreditcardAction) controller.getAction(this, CreditcardAction.class);
		 wdMapAction = (WdMapAction) controller.getAction(this, WdMapAction.class);
		 LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
	     recyclerView.setHasFixedSize(true);
	     recyclerView.setLayoutManager(linearLayoutManager);
	     
	     wdListAdapter = new WdListAdapter(this, wdMapList, latitude, longitude);
	     recyclerView.setAdapter(wdListAdapter);
	     swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {

	            @Override
	            public void onRefresh() {
	                swipeRefreshLayout.setRefreshing(true);
	            }
	     });
	 }
	 
	 
	@OnClick(value = {R.id.tvArea, R.id.tvRange})
	public void onClick(View view){
		switch(view.getId()){
	    	case R.id.tvArea:
	    		index = 1;
	    		if(areaKeyList.size() == 0){
					creditcardAction.queryDistrictAndBrandList().start();
				}else{
					showPopupWindow(index);
				}
	    		break;
	    	case R.id.tvRange:
	    		index = 2;
	    		showPopupWindow(index);
	    		break;
	   }
	}
	 
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	      getMenuInflater().inflate(R.menu.activity_wdlist_menu, menu);
	      return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == R.id.search){
			String custname = etSearchText.getText().toString();
			if(custname != null && !"".equals(custname)){
				wdMapAction.queryNetPointList(custname, "0", "0", String.valueOf(longitude), 
						String.valueOf(latitude), String.valueOf(pageNum)).start();
			}
		}
		return super.onOptionsItemSelected(item);
	}
	
	 /**
     * 车商区域和品牌查询成功回调
     */
    public void districtAndBrandSuccess(DistrictAndBrand districtAndBrand){
    	if(districtAndBrand != null){
    		Map<String, String> districtMap = districtAndBrand.getDistrictList();
    		if(districtMap != null && districtMap.size() > 0){
    			Iterator<Entry<String,String>> iterator = districtMap.entrySet().iterator();
    			while(iterator.hasNext()){
    				Entry<String,String> entry = iterator.next();
    				areaKeyList.add(entry.getKey());
    				areaValueList.add(entry.getValue());
    			}
    		}
    		showPopupWindow(index);
    	}
    }
	
	public void showPopupWindow(final int index){
		final List<String> keyList = index == 1 ? areaKeyList : rangeKeyList;
    	final List<String> valueList = index == 1 ? areaValueList : rangeValueList;
    	final TextView dropView = index == 1 ? tvArea : tvRange;
    	View layout = LayoutInflater.from(this).inflate(R.layout.layout_phase2_district_brand_list, null);
    	final PopupWindow pop = new PopupWindow(layout, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
    	ListView listView = (ListView)layout.findViewById(R.id.listView);
    	listView.setAdapter(new BaseAdapter<String>(this, valueList, R.layout.layout_phase2_district_brand_item) {

			@Override
			public void viewHandler(int position, final String t, View convertView) {
				TextView tvContent = ViewHolder.get(convertView, R.id.tvContent);
				FormsUtil.setTextViewTxt(tvContent, t);
			}
		});
    	listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				pop.dismiss();
				FormsUtil.setTextViewTxt(dropView, valueList.get(position));
				if(index == 1){
					//根据区域查询
					wdMapAction.queryNetPointList("", keyList.get(position), "0", String.valueOf(longitude), 
							String.valueOf(latitude), String.valueOf(pageNum)).start();
				}else{
					//根据距离查询
					wdMapAction.queryNetPointList("", "0", keyList.get(position), String.valueOf(longitude), 
							String.valueOf(latitude), String.valueOf(pageNum)).start();
				}
			}
		});
    	
    	pop.setOutsideTouchable(true);
    	pop.setBackgroundDrawable(new PaintDrawable());
    	pop.setFocusable(true);// 注意必须设置可获得焦点
    	pop.showAsDropDown(dropView);
    }
	
   /**
	* 定位SDK监听函数
	*/
	public class MyLocationListenner implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			if(isFirstLoc){
				xdrDialog.dismiss();
				isFirstLoc = false;
				longitude = location.getLongitude();
				latitude = location.getLatitude();
				wdMapAction.queryNetPointList("", "0", "0", String.valueOf(longitude), 
						String.valueOf(latitude), String.valueOf(pageNum)).start();
			}
		}

		public void onReceivePoi(BDLocation poiLocation) {
			System.out.println("onReceivePoi......");
		}
	}
	
	/**
	 * 网点查询成功
	 * @param list
	 */
	public void querySuccess(List<WdMap> list){
		if(list != null && list.size() > 0){
			wdMapList.clear();
			wdMapList.addAll(list);
			wdListAdapter.notifyDataSetChanged();
		}
	}
	
	@Override
	protected void onDestroy() {
		// 退出时销毁定位
		mLocClient.stop();
		super.onDestroy();
	}
	
}
