package com.abc.sz.app.listener;

import com.abc.sz.app.view.plusitemview.PlusItemBean;
import com.forms.base.LoginUser;
import com.forms.library.baseUtil.db.DbUtils;
import com.forms.library.baseUtil.db.listener.BaseDbUpgradeListener;
import com.forms.library.baseUtil.exception.DbException;
import com.forms.library.baseUtil.log.LogUtils;

/**
 * Created by hwt on 14-9-16.
 */
public class SimpleDBUpgradeListener extends BaseDbUpgradeListener {
    /**
     * 只做表字段更新
     *
     * @param db 数据库操作工具
     */
    @Override
    public void justOnlyUpdateTableColumn(DbUtils db) {

    }

    @Override
    public void clearTableUpdate(DbUtils db) {
        try {
                /*清理老的数据库表*/
            db.dropTable(LoginUser.class);

                /*重新创建新表*/
            db.createTableIfNotExist(LoginUser.class);

                /*清理老的数据库表*/
            db.dropTable(PlusItemBean.class);

                /*重新创建新表*/
            db.createTableIfNotExist(PlusItemBean.class);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }
}
