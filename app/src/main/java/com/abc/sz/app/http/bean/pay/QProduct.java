package com.abc.sz.app.http.bean.pay;

public class QProduct {
	
	/* 商户代码 */
    private String SubMerId;
    /* 商户名称*/
    private String SubMerName;
    /* 商户MCC码 */
    private String SubMerMCC;
    /* 商户备注项 */
    private String SubMerchantRemarks;
	/* 商品代码 */
    private String ProductId;
	/* 商品名称 */
    private String ProductName;
    /* 商品价格*/
    private String UnitPrice;
    /* 商品数量*/
    private String Qty;
    /* 商品备注项*/
    private String ProductRemarks;
    /* 商品类型*/
    private String ProductType;
    /* 商品折扣*/
    private String ProductDiscount;
    /* 商品有效期*/
    private String ProductExpiredDate;
    
	public String getSubMerId() {
		return SubMerId;
	}
	public void setSubMerId(String subMerId) {
		SubMerId = subMerId;
	}
	public String getSubMerName() {
		return SubMerName;
	}
	public void setSubMerName(String subMerName) {
		SubMerName = subMerName;
	}
	public String getProductId() {
		return ProductId;
	}
	public void setProductId(String productId) {
		ProductId = productId;
	}
	public String getProductName() {
		return ProductName;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public String getUnitPrice() {
		return UnitPrice;
	}
	public void setUnitPrice(String unitPrice) {
		UnitPrice = unitPrice;
	}
	public String getQty() {
		return Qty;
	}
	public void setQty(String qty) {
		Qty = qty;
	}
	public String getSubMerMCC() {
		return SubMerMCC;
	}
	public void setSubMerMCC(String subMerMCC) {
		SubMerMCC = subMerMCC;
	}
	public String getSubMerchantRemarks() {
		return SubMerchantRemarks;
	}
	public void setSubMerchantRemarks(String subMerchantRemarks) {
		SubMerchantRemarks = subMerchantRemarks;
	}
	public String getProductRemarks() {
		return ProductRemarks;
	}
	public void setProductRemarks(String productRemarks) {
		ProductRemarks = productRemarks;
	}
	public String getProductType() {
		return ProductType;
	}
	public void setProductType(String productType) {
		ProductType = productType;
	}
	public String getProductDiscount() {
		return ProductDiscount;
	}
	public void setProductDiscount(String productDiscount) {
		ProductDiscount = productDiscount;
	}
	public String getProductExpiredDate() {
		return ProductExpiredDate;
	}
	public void setProductExpiredDate(String productExpiredDate) {
		ProductExpiredDate = productExpiredDate;
	}

}
