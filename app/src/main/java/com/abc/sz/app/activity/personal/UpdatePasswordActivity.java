package com.abc.sz.app.activity.personal;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.UserAction;
import com.abc.sz.app.util.InputVerifyUtil;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.encrypt.MD5;

/**
 * 修改密码
 *
 * @author ftl
 */
@ContentView(R.layout.activity_update_password)
public class UpdatePasswordActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.et_oldPassword) EditText oldPassword;
    @ViewInject(R.id.et_newPassword) EditText newPassword;
    @ViewInject(R.id.et_rePassword) EditText rePassword;

    private UserAction userAction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        userAction = (UserAction) controller.getAction(this, UserAction.class);
    }

    @OnClick(R.id.btn_confirm)
    public void onClick(View view) {
        if (check()) {
            userAction.modifyPassword(MD5.encrypt(MD5.encrypt(oldPassword.getText().toString())),
            		MD5.encrypt(MD5.encrypt(newPassword.getText().toString()))).start();
        }

    }

    private boolean check() {
        String pswd = newPassword.getText().toString();
        if (TextUtils.isEmpty(oldPassword.getText())) {
            FormsUtil.setErrorHtmlTxt(this, oldPassword, R.string.input_oldPassword_isempty);
            return false;
        } else if (pswd.length() < 6) {
            FormsUtil.setErrorHtmlTxt(this, newPassword, R.string.input_six_password);
            return false;
        } else if (!InputVerifyUtil.checkSecurity(pswd)) {
            FormsUtil.setErrorHtmlTxt(this, newPassword, R.string.input_password_easy);
            return false;
        } else if (TextUtils.isEmpty(rePassword.getText())) {
            FormsUtil.setErrorHtmlTxt(this, rePassword, R.string.input_rePassword_isempty);
            return false;
        } else if (pswd.equals(oldPassword.getText().toString())) {
            FormsUtil.setErrorHtmlTxt(this, newPassword, R.string.input_new_equal_oldpasswd);
            return false;
        } else if (!pswd.equals(rePassword.getText().toString())) {
            FormsUtil.setErrorHtmlTxt(this, rePassword, R.string.input_passwdnoEqual_isempty);
            return false;
        }
        return true;
    }

}
