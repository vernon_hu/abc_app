package com.abc.sz.app.bean.branch;

import android.database.Cursor;

import com.abc.sz.app.util.AutoCompleteFilterObject;
import com.forms.library.base.BaseBean;
import com.forms.library.baseUtil.db.annotation.Column;
import com.forms.library.baseUtil.db.annotation.Table;
import com.forms.library.baseUtil.db.annotation.Unique;

/**
 * Created by monkey on 2015/8/14.
 */
@Table(name = "tb_branchserach_history")
public class HistoryTag extends BaseBean implements AutoCompleteFilterObject {
    @Column(column = "tag_name")
    @Unique
    private String tagName;
    @Column(column = "create_time")
    private String createTime;

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Override
    public String getSearchWord() {
        return tagName;
    }

    @Override
    public AutoCompleteFilterObject createObject(Cursor cursor) {
        if (!cursor.isClosed()) {
            HistoryTag historyTag = new HistoryTag();
            historyTag.tagName = cursor.getString(cursor.getColumnIndex("tag_name"));
            historyTag.createTime = cursor.getString(cursor.getColumnIndex("create_time"));
            historyTag.id = cursor.getLong(cursor.getColumnIndex("id"));
            return historyTag;
        }
        return null;
    }
}
