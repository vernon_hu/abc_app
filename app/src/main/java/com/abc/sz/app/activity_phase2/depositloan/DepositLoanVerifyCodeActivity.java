package com.abc.sz.app.activity_phase2.depositloan;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.DepositloanAction;
import com.abc.sz.app.view.CountDownButton;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;

/**
 * 还款账号验证码校验
 * 
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_depositloan_verifycode)
public class DepositLoanVerifyCodeActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	@ViewInject(R.id.etAuthCode)
	private EditText etAuthCode;
	@ViewInject(R.id.btnAuthCode)
	private CountDownButton btnAuthCode;

	private DepositloanAction depositloanAction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		depositloanAction = (DepositloanAction) controller.getAction(this, DepositloanAction.class);
	}

	@OnClick({ R.id.btnAuthCode, R.id.btnConfirm })
	public void calculate(View v) {
		switch (v.getId()) {
		case R.id.btnAuthCode:
			//获取验证码
			
			break;
		case R.id.btnNext:
			//提交验证
			depositloanAction.checkVerifyCode("").start();
			break;
		}
	}
	
	/**
	 * 还款账号验证码校验成功回调
	 */
	public void checkSuccess(){
		callMe(DepositRelateLoanActivity.class);
	}

}
