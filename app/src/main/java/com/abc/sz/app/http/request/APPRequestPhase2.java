package com.abc.sz.app.http.request;

import android.text.TextUtils;

import com.abc.sz.app.bean.Address;
import com.abc.sz.app.bean.Order;
import com.abc.sz.app.bean.PayCode;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.bean.ShoppingCar;
import com.abc.sz.app.bean.WdMap;
import com.abc.sz.app.bean.branch.City;
import com.abc.sz.app.bean.cridetcard.BillInstallments;
import com.abc.sz.app.bean.cridetcard.CarShop;
import com.abc.sz.app.bean.cridetcard.CardFavorable;
import com.abc.sz.app.bean.cridetcard.CardSchedule;
import com.abc.sz.app.bean.cridetcard.CardShop;
import com.abc.sz.app.bean.cridetcard.ConsumerDetail;
import com.abc.sz.app.bean.cridetcard.ConsumerList;
import com.abc.sz.app.bean.cridetcard.DistrictAndBrand;
import com.abc.sz.app.bean.lightpay.InitResponse;
import com.abc.sz.app.bean.lightpay.RBindCardInfo;
import com.abc.sz.app.bean.loan.Account;
import com.abc.sz.app.bean.loan.DepositLoanInfo;
import com.abc.sz.app.bean.personal.Favorites;
import com.abc.sz.app.bean.product.ProductEvalutionBean;
import com.abc.sz.app.bean.product.ProductInfoBean;
import com.abc.sz.app.bean.product.ProductListBean;
import com.abc.sz.app.bean.product.TypeAttributeBean;
import com.abc.sz.app.bean_phase2.DLIncome;
import com.abc.sz.app.bean_phase2.ImageAD;
import com.abc.sz.app.bean_phase2.MarketItem;
import com.abc.sz.app.bean_phase2.Omnibus;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.address.QAddress;
import com.abc.sz.app.http.bean.address.QCity;
import com.abc.sz.app.http.bean.address.QDeleteAddress;
import com.abc.sz.app.http.bean.card.QApplyConsumer;
import com.abc.sz.app.http.bean.card.QCarLoanCommitment;
import com.abc.sz.app.http.bean.card.QCarShopList;
import com.abc.sz.app.http.bean.card.QCardFavorable;
import com.abc.sz.app.http.bean.card.QCardSchedule;
import com.abc.sz.app.http.bean.card.QHouseLoanCommitment;
import com.abc.sz.app.http.bean.jf.JfBean;
import com.abc.sz.app.http.bean.lightpay.QBindCard;
import com.abc.sz.app.http.bean.lightpay.QGZBindCard;
import com.abc.sz.app.http.bean.lightpay.QRemoveBindCard;
import com.abc.sz.app.http.bean.loan.QAccountDetail;
import com.abc.sz.app.http.bean.loan.QDepositLoanInfo;
import com.abc.sz.app.http.bean.order.QEvaluateList;
import com.abc.sz.app.http.bean.order.QOrderAccount;
import com.abc.sz.app.http.bean.order.QOrderAdvancePay;
import com.abc.sz.app.http.bean.order.QOrderList;
import com.abc.sz.app.http.bean.order.QOrderOperate;
import com.abc.sz.app.http.bean.order.QOrderPay;
import com.abc.sz.app.http.bean.order.QOrderUpdate;
import com.abc.sz.app.http.bean.order.QQueryOrder;
import com.abc.sz.app.http.bean.order.QShoppingCar;
import com.abc.sz.app.http.bean.order.QShoppingCarDelete;
import com.abc.sz.app.http.bean.order.QShoppingCarList;
import com.abc.sz.app.http.bean.other.QSuggestion;
import com.abc.sz.app.http.bean.pay.AESBean;
import com.abc.sz.app.http.bean.pay.QPay;
import com.abc.sz.app.http.bean.pay.RPay;
import com.abc.sz.app.http.bean.personal.QConsumes;
import com.abc.sz.app.http.bean.personal.QLogin;
import com.abc.sz.app.http.bean.personal.QPerfectUserInfo;
import com.abc.sz.app.http.bean.personal.QRegister;
import com.abc.sz.app.http.bean.personal.QResetPassword;
import com.abc.sz.app.http.bean.personal.QUpdatePhone;
import com.abc.sz.app.http.bean.personal.QUser;
import com.abc.sz.app.http.bean.personal.QUserCard;
import com.abc.sz.app.http.bean.personal.RConsume;
import com.abc.sz.app.http.bean.personal.RLogin;
import com.abc.sz.app.http.bean.personal.RUserCard;
import com.abc.sz.app.http.bean.personal.RUserScore;
import com.abc.sz.app.http.bean.product.QBzshInfo;
import com.abc.sz.app.http.bean.product.QEvaluationList;
import com.abc.sz.app.http.bean.product.QJxInfo;
import com.abc.sz.app.http.bean.product.QJxList;
import com.abc.sz.app.http.bean.product.QProductAttribute;
import com.abc.sz.app.http.bean.product.QProductInfo;
import com.abc.sz.app.http.bean.product.QProductList;
import com.abc.sz.app.http.bean.product.QProductType;
import com.abc.sz.app.http.bean.product.QShare;
import com.abc.sz.app.http.bean.wdmap.QWdMap;
import com.alibaba.fastjson.JSON;
import com.forms.base.BaseConfig;
import com.forms.library.baseUtil.asynchttpclient.PersistentCookieStore;
import com.forms.library.baseUtil.asynchttpclient.RequestParams;
import com.forms.library.baseUtil.logger.Logger;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.Request;
import com.forms.library.baseUtil.net.ResponseNotify;

import org.apache.http.Header;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicHeader;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hwt on 14/11/3.
 * 网络接口调用
 */
public class APPRequestPhase2 extends Request {
    private static String ex = ".ebf";

    /**
     * 获取加密因子
     */

    public static Http aesKeyIvQuery(String token, ResponseNotify<AESBean> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "AseKeyIvAct" + ex;
        return postJson(requestUrl, token, null, responseNotify);

    }


    /**
     * 广告请求
     *
     * @param type
     * @param responseNotify
     * @return
     */
    public static Http advQuery(String type, ResponseNotify<List<ImageAD>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "SysAdvertLoopAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"Type\":\"" + type + "\"}");
        return postJson(requestUrl, null, requestParams, responseNotify);
    }

    /**
     * 查询收货地址
     *
     * @param params
     * @param responseNotify;
     * @return
     */
    public static Http queryAddress(RequestBean<QAddress> params, ResponseNotify<List<Address>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "ShippingAddressQueryAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 添加新的收货地址
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http addAddress(RequestBean<QAddress> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "ShippingAddressAddAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 删除收货地址
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http deleteAddress(RequestBean<QDeleteAddress> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "ShippingAddressDeleteAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 修改收货地址
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http updateAddress(RequestBean<QAddress> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "ShippingAddressModifyAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 设置默认收货地址
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http setDefaultAddress(RequestBean<QDeleteAddress> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "ShippingAddressSetDefaultAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 查询城市列表
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http queryCity(RequestBean<QCity> params, ResponseNotify<List<City>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "SysQueryCityAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /*******************************商品相关******************************/

    /**
     * 商品分类查询 007
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http productTypeQuery(RequestBean<QProductType> params, ResponseNotify<List<ProductListBean>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "GoodsQueryPartListAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 商城商品列表查询
     *
     * @param accessToken
     * @param pageNum
     * @param responseNotify
     * @return
     */
    public static Http productListQuery(String accessToken, String pageNum, ResponseNotify<List<MarketItem>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "IndexGoodsQueryListAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"pageNum\":\"" + pageNum + "\"}");
        return postJson(requestUrl, accessToken, requestParams, responseNotify);
    }

    /**
     * 商品列表查询 008
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http productListQuery(RequestBean<QProductList> params, ResponseNotify<List<ProductListBean>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "GoodsQueryListAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 商品详情查询 009
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http productInfoQuery(RequestBean<QProductInfo> params, ResponseNotify<ProductInfoBean> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "GoodsQueryDetailAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 商品属性查询 043
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http productAttributeQuery(RequestBean<QProductAttribute> params, ResponseNotify<List<TypeAttributeBean>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "GoodsPropretyQueryAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 商品详情图片列表查询
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http productInfoPictureQuery(RequestBean<QProductInfo> params, ResponseNotify<List<String>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "ProductDetailInfoQueryAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 快报列表查询  004
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http jxListQuery(RequestBean<QJxList> params, ResponseNotify<List<Omnibus>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "BulletinQueryListAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 快报详情查询  005
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http jxInfoQuery(RequestBean<QJxInfo> params, ResponseNotify<Omnibus> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "BulletinQueryDetailAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 分享 006
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http shareProductInfo(RequestBean<QShare> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "GoodsShareAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }


    /**
     * 评价列表查询 012
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http evaluationQuery(RequestBean<QEvaluationList> params, ResponseNotify<ProductEvalutionBean> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "OrderAppraiseListAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }


    /**
     * 商品包装售后
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http bzshQuery(RequestBean<QBzshInfo> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "ProductCustServerQueryAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }


    /*******************************购物相关******************************/
    /**
     * 添加购物车
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http addShoppingCar(RequestBean<QShoppingCar> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "ShopCarAddGoodsAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 购物车列表查询[013]
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http queryShoppingCar(RequestBean params, ResponseNotify<List<ShoppingCar>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "ShopCarGoodsListAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 购物车商品编辑[014]
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http updateShoppingCar(RequestBean<QShoppingCarList> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "ShopCarGoodsUpdateAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 购物车商品删除[049]
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http deleteShoppingCar(RequestBean<QShoppingCarDelete> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "ShopCarGoodsDeleteAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 订单结算
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http orderAccount(RequestBean<QOrderAccount> params, ResponseNotify<List<Product>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "BalanceAccountsAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 生成订单
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http createOrder(RequestBean<QOrderUpdate> params, ResponseNotify<List<Order>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "SettleAccountsAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 订单信息修改
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http orderUpdate(RequestBean<QOrderUpdate> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "OrderUpdateAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 订单预支付[028]
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http orderAdvancePay(RequestBean<QOrderAdvancePay> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "OrderWillPayAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 订单支付[029]
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http orderPay(RequestBean<QOrderPay> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "OrderPayAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 订单直接支付
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http orderImmediatePay(RequestBean<QPay> params, ResponseNotify<RPay> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "PaymentRequestAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 订单取消[030]
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http orderCancel(RequestBean<QOrderOperate> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "OrderCancelAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 订单列表查询
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http queryOrderList(RequestBean<QOrderList> params, ResponseNotify<List<Order>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "OrderQueryListAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 订单详情查询
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http queryOrderDetails(RequestBean<QOrderOperate> params, ResponseNotify<Order> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "OrderQueryDetialAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 订单评价
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http evaluateOrder(RequestBean<QEvaluateList> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "OrderAddAppraiseAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 确认收货
     *
     * @param accessToken
     * @param orderId
     * @param responseNotify
     * @return
     */
    public static Http conFirmReceiveGood(String accessToken, String orderId, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "ConFirmReceiveGoodsAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"orderId\":\"" + orderId + "\"}");
        return postJson(requestUrl, accessToken, requestParams, responseNotify);
    }

    /**
     * 订单状态查询
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http queryOrderRequest(RequestBean<QQueryOrder> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "QueryOrderRequestAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /*******************************个人中心******************************/
    /**
     * 用户登录
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http userLogin(RequestBean<QLogin> params, ResponseNotify<RLogin> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "MobilestartUpSessionAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 获取注册短信验证码
     *
     * @param phone
     * @param responseNotify
     * @return
     */
    public static Http getRegisterCode(String phone, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "RegisterSmsCheckCodeAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"phone\":\"" + phone + "\"}");
        return postJson(requestUrl, null, requestParams, responseNotify);
    }

    /**
     * 用户注册
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http userRegister(RequestBean<QRegister> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "MobileRegisterAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 注销
     *
     * @param accessToken
     * @param responseNotify
     * @return
     */
    public static Http userLogout(String accessToken, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "UserLogoOutAct" + ex;
        return postJson(requestUrl, accessToken, null, responseNotify);
    }

    /**
     * 收藏
     *
     * @param accessToken
     * @param productId
     * @param responseNotify
     * @return
     */
    public static Http favourite(String accessToken, String productId, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CollectAddGoodsAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"productId\":\"" + productId + "\"}");
        return postJson(requestUrl, accessToken, requestParams, responseNotify);
    }

    /**
     * 查询收藏列表
     *
     * @param accessToken
     * @param responseNotify
     * @return
     */
    public static Http queryFavourites(String accessToken, Integer PageNum, ResponseNotify<List<Favorites>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CollectGoodsListAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"pageNum\":\"" + String.valueOf(PageNum) + "\"}");
        return postJson(requestUrl, accessToken, requestParams, responseNotify);
    }

    /**
     * 删除收藏
     *
     * @param accessToken
     * @param favouriteId
     * @param responseNotify
     * @return
     */
    public static Http delFavourite(String accessToken, String favouriteId, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CollectGoodsDeleteAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"favouriteId\":\"" + favouriteId + "\"}");
        return postJson(requestUrl, accessToken, requestParams, responseNotify);
    }

    /**
     * 查询用户卡列表
     *
     * @param accessToken
     * @param responseNotify
     * @return
     */
    public static Http queryBindCardList(String accessToken, ResponseNotify<List<RUserCard>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CardBindingQueryAct" + ex;
        return postJson(requestUrl, accessToken, null, responseNotify);
    }

    /**
     * 解除卡绑定
     *
     * @param accessToken
     * @param cardId
     * @param phone
     * @param responseNotify
     * @return
     */
    public static Http unBindCard(String accessToken, String cardId, String cardNum, String phone, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CardBindingRemoveAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"cardId\":\"" + cardId + "\",\"phone\":\"" + phone + "\",\"cardNo\":\"" + cardNum + "\"}");
        return postJson(requestUrl, accessToken, requestParams, responseNotify);
    }

    /**
     * 新增绑卡
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http bindNewCard(RequestBean<QUserCard> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CardBindingAddAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 设置默认支付卡
     *
     * @param accessToken
     * @param cardId
     * @param responseNotify
     * @return
     */
    public static Http setDefaultCard(String accessToken, String cardId, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CardBindingSetDefaultAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"cardId\":\"" + cardId + "\"}");
        return postJson(requestUrl, accessToken, requestParams, responseNotify);
    }

    /**
     * 取得短信验证码
     *
     * @param accessToken
     * @param responseNotify
     * @return
     */
    public static Http getMessageCode(String accessToken, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "";
        return postJson(requestUrl, accessToken, null, responseNotify);
    }

    /**
     * 验证短信验证码
     *
     * @param accessToken
     * @param authCode
     * @param responseNotify
     * @return
     */
    public static Http verifyMessageCode(String accessToken, String authCode, ResponseNotify<List<RUserCard>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "";
        RequestParams requestParams = new RequestParams();
        requestParams.put("authCode", authCode);
        return postJson(requestUrl, accessToken, requestParams, responseNotify);
    }

    /**
     * 修改密码
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http modifyPasswd(RequestBean<QUser> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "MobileModifyPasswordAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 获取重置密码短信验证码
     *
     * @param requestParams
     * @param responseNotify
     * @return
     */
    public static Http getResetPasswordCode(RequestParams requestParams,
                                            ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "PassWordFindSmsAct" + ex;
        return postJson(requestUrl, null, requestParams, responseNotify);
    }

    /**
     * 重置用户密码
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http resetPasswd(RequestBean<QResetPassword> params,
                                   ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "PassWordFindAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 完善用戶信息
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http perfectUserInfo(RequestBean<QPerfectUserInfo> params,
                                       ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "UserMessagePerfectAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 获取修改手机号码短信验证码
     *
     * @param accessToken
     * @param responseNotify
     * @return
     */
    public static Http getUpdatePhoneCode(String accessToken, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "GetUpdatePhoneCheckCodeAct" + ex;
        return postJson(requestUrl, accessToken, null, responseNotify);
    }

    /**
     * 修改手机号码
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http updatePhone(RequestBean<QUpdatePhone> params,
                                   ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "UpdateMobileNumAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 查询消费列表
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http queryConsumes(RequestBean<QConsumes> params,
                                     ResponseNotify<List<RConsume>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "SysQueryExpenseAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 签到
     *
     * @param accessToken
     * @param responseNotify
     * @return
     */
    public static Http checkin(String accessToken, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "SignInAct" + ex;
        return postJson(requestUrl, accessToken, null, responseNotify);
    }

    /**
     * 意见反馈
     *
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http sendSuggestion(RequestBean<QSuggestion> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "SuggestionAddAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 查询支付码列表
     *
     * @param accessToken
     * @param responseNotify
     * @return
     */
    public static Http queryPayCodeList(String accessToken, String pageNum, ResponseNotify<List<PayCode>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "PayCodeQueryListAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"pageNum\":\"" + pageNum + "\"}");
        return postJson(requestUrl, accessToken, requestParams, responseNotify);
    }

    /**
     * 查询个人总积分
     *
     * @param accessToken
     * @param responseNotify
     * @return
     */
    public static Http queryUserScore(String accessToken, ResponseNotify<RUserScore> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "SumScoreQueryAct" + ex;
        return postJson(requestUrl, accessToken, null, responseNotify);
    }


    /**
     * 缴费查询
     *
     * @param responseNotify
     * @return
     */
    public static Http jfQuery(RequestBean params, ResponseNotify<JfBean> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "RatePaymentQueryListAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 光子支付初始化
     *
     * @param responseNotify
     * @return
     */
    public static Http GZRPayInitAct(String token, String aesSeed, ResponseNotify<InitResponse> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "GZRPayInitAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"operateType\":\"" + "seed" + "\",\"aesSeed\":\"" + aesSeed + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 光子支付绑卡
     *
     * @param responseNotify
     * @return
     */
    public static Http GZBindCard(RequestBean<QBindCard> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "GZBindCardAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 光子支付绑卡
     *
     * @param responseNotify
     * @return
     */
    public static Http CreditCardCheckSMSAct(RequestBean<QGZBindCard> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CreditCardCheckSMSAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 光子支付获取已绑定卡的列表
     *
     * @param responseNotify
     * @return
     */
    public static Http GZGetBindCardList(String token, String userId, ResponseNotify<RBindCardInfo> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "GZGetBindCardListAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"operateType\":\"" + "card_info" + "\",\"userId\":\"" + userId + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 光子解除卡绑定
     *
     * @param responseNotify
     * @return
     */
    public static Http GZCardRemoveBindAct(RequestBean<QRemoveBindCard> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "GZCardRemoveBindAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 光子解除卡绑定
     *
     * @param responseNotify
     * @return
     */
    public static Http GZDJKRemoveBindAct(RequestBean<QRemoveBindCard> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "GZDJKRemoveBindAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 冻结光ID
     *
     * @param responseNotify
     * @return
     */
    public static Http GZFreezeCardAct(String token, String userId, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "GZFreezeCardAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"operateType\":\"" + "freeze" + "\",\"userId\":\"" + userId + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 解冻光ID
     *
     * @param responseNotify
     * @return
     */
    public static Http GZReleaseCardAct(String token, String userId, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "GZReleaseCardAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"operateType\":\"" + "thawing" + "\",\"userId\":\"" + userId + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 光子绑卡身份验证
     *
     * @param responseNotify
     * @return
     */
    public static Http CheckCodeAct(RequestBean<QBindCard> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CheckCodeAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 光子绑卡身份验证
     *
     * @param responseNotify
     * @return
     */
    public static Http GZDJKBindCardAct(RequestBean<QGZBindCard> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "GZDJKBindCardAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 查询是否符合存贷通开立条件
     *
     * @param token
     * @param responseNotify
     * @return
     */
    public static Http CDTCheckUserCondition(String token, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CDTCheckUserConditionAct" + ex;
        return postJson(requestUrl, token, null, responseNotify);
    }

    /**
     * 存贷通：贷款账号列表
     *
     * @param responseNotify
     * @return
     */
    public static Http CDTLoanAccountList(String token, String customerNum, ResponseNotify<List<Account>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CDTLoanAccountListAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"customerNum\":\"" + customerNum + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 存贷通：选择贷款账号开立存贷通
     *
     * @param responseNotify
     * @return
     */
    public static Http CDTCreateAccount(String token, String account, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CDTCreateAccountAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"account\":\"" + account + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 存贷通：还款账号交易密码校验
     *
     * @param responseNotify
     * @return
     */
    public static Http CDTVerifyPasswd(String token, String customer, String passwd, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CDTVerifyPasswdAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"customer\":\"" + customer + "\",\"passwd\":\"\"+passwd+\"\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 存贷通：还款账号验证码校验
     *
     * @param responseNotify
     * @return
     */
    public static Http CDTCheckVerifyCode(String token, String verifyCode, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CDTCheckVerifyCodeAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"verifyCode\":\"" + verifyCode + "\"");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 存贷通：还款账号列表
     *
     * @param responseNotify
     * @return
     */
    public static Http CDTDepositAccountList(String token, String customerNum, ResponseNotify<List<Account>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CDTDepositAccountListAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"customerNum\":\"" + customerNum + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 存贷通：查询其他贷款账号
     *
     * @param responseNotify
     * @return
     */
    public static Http CDTOtherLoanAccountList(String token, String customerNum, ResponseNotify<List<Account>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CDTOtherLoanAccountListAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"customerNum\":\"" + customerNum + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 存贷通：账号关联信息提交
     *
     * @param responseNotify
     * @return
     */
    public static Http CDTPostCreateInfo(RequestBean<QDepositLoanInfo> requestBean, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CDTPostCreateInfoAct" + ex;
        return postJson(requestUrl, requestBean, responseNotify);
    }

    /**
     * 存贷通：开立信息反查
     *
     * @param responseNotify
     * @return
     */
    public static Http CDTQueryCreateInfo(String token, String customerNum, ResponseNotify<DepositLoanInfo> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CDTQueryCreateInfoAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"customerNum\":\"" + customerNum + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 存贷通：开立信息确认
     *
     * @param responseNotify
     * @return
     */
    public static Http CDTCreateComplete(String token, String customerNum, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CDTCreateCompleteAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"customerNum\":\"" + customerNum + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 存贷通：账户信息查询
     *
     * @param responseNotify
     * @return
     */
    public static Http CDTQueryAccount(String token, String customerNum, ResponseNotify<DepositLoanInfo> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CDTQueryAccountAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"customerNum\":\"" + customerNum + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 存贷通：存贷通收益返还明细查询
     *
     * @param responseNotify
     * @return
     */
    public static Http CDTQueryAccountDetail(RequestBean<QAccountDetail> requestBean, ResponseNotify<DLIncome> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CDTQueryAccountDetailAct" + ex;
        return postJson(requestUrl, requestBean, responseNotify);
    }

    /**
     * 存贷通：贷通计算器利率查询
     *
     * @param responseNotify
     * @return
     */
    public static Http CDTCalculator(String token, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CDTCalculatorAct" + ex;
        return postJson(requestUrl, token, null, responseNotify);
    }

    /**
     * 信用卡进度查询
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKQuerySchedule(RequestBean<QCardSchedule> params, ResponseNotify<List<CardSchedule>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "XYKQueryScheduleAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 信用卡特惠权查询
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKQueryPreferentialRight(RequestBean<QCardFavorable> params, ResponseNotify<List<CardFavorable>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "XYKQueryPreferentialRightAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 信用卡特惠权详情
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKPreferentialRightDetail(String token, String preferentialID, ResponseNotify<CardFavorable> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "XYKPreferentialRightDetailAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"preferentialID\":\"" + preferentialID + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 信用卡优惠商户列表
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKYOUShopList(String token, String gps, String searchText,
                                      ResponseNotify<List<CardShop>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "XYKYOUShopListAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"gps\":\"" + gps + "\"}");
        if (!"".equals(searchText)) {
            requestParams.put("param", "{\"searchText\":\"" + searchText + "\"}");
        }
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 信用卡优惠商户详情
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKYOUShopDetail(String token, String shopID, ResponseNotify<CardShop> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "XYKYOUShopDetailAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"shopID\":\"" + shopID + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 车商查询
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKCarShopList(RequestBean<QCarShopList> params, ResponseNotify<List<CarShop>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "XYKCarShopListAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 车商详情
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKCarShopDetail(String token, String carShopID, ResponseNotify<CarShop> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "XYKCarShopDetailAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"carShopID\":\"" + carShopID + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 汽车分期获取短信验证
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKCarSmsCheck(String phone, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "CarSmsCheckAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"phone\":\"" + phone + "\"}");
        return postJson(requestUrl, null, requestParams, responseNotify);
    }

    /**
     * 家装分期获取短信验证
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKHomeSmsCheck(String phone, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "HomeSmsCheckAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"phone\":\"" + phone + "\"}");
        return postJson(requestUrl, null, requestParams, responseNotify);
    }

    /**
     * 汽车分期贷款额度查询
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKCarLoanCommitment(RequestBean<QCarLoanCommitment> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "XYKCarLoanCommitmentAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 家装分期贷款额度查询
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKHouseDecorationLoanCommitment(RequestBean<QHouseLoanCommitment> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "XYKHouseDecorationLoanCommitmentAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 查询是否已进行账单分期
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKQueryBillInstallments(String token, String cardNumber, ResponseNotify<BillInstallments> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "XYKQueryBillInstallmentsAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"cardNumber\":\"" + cardNumber + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 账单分期摇一摇抽奖
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKBillInstallmentsLottery(String token, String cardNumber, ResponseNotify<BillInstallments> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "XYKBillInstallmentsLotteryAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"cardNumber\":\"" + cardNumber + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 消费分期
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKConsumerInstallment(String token, ResponseNotify<List<ConsumerList>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "XYKConsumerInstallmentAct" + ex;
        return postJson(requestUrl, token, null, responseNotify);
    }

    /**
     * 消费分期详情
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKConsumerDetail(String token, String consumerID, ResponseNotify<ConsumerDetail> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "XYKConsumerDetailAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"consumerID\":\"" + consumerID + "\"}");
        return postJson(requestUrl, token, null, responseNotify);
    }

    /**
     * 申请消费分期
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKApplyConsumerInstallment(RequestBean<QApplyConsumer> params, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "XYKApplyConsumerInstallmentAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 现金分期
     *
     * @param responseNotify
     * @return
     */
    public static Http XYKCashInstallment(String token, String cardNumber, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "XYKCashInstallmentAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"cardNumber\":\"" + cardNumber + "\"}");
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 车商区域和品牌
     *
     * @param responseNotify
     * @return
     */
    public static Http DistrictAndBrandListAct(String token, ResponseNotify<DistrictAndBrand> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "DistrictAndBrandListAct" + ex;
        return postJson(requestUrl, token, null, responseNotify);
    }

    /**
     * 网点查询
     *
     * @param responseNotify
     * @return
     */
    public static Http NetPointQueryListAct(RequestBean<QWdMap> params, ResponseNotify<List<WdMap>> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "NetPointQueryListAct" + ex;
        return postJson(requestUrl, params, responseNotify);
    }

    /**
     * 供应商通知APP生成订单
     *
     * @param responseNotify
     * @return
     */
    public static Http OrderAddAct(String token, String param, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "OrderAddAct" + ex;
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", param);
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * 同步APP与总行支付方的订单状态
     *
     * @param orderId
     * @return
     */
    public static Http syncOrderStatus(String token, String orderId, ResponseNotify<String> responseNotify) {
        String requestUrl = BaseConfig.SERVER_URL + "QueryOrderRequestAct" + ex;

        Map<String, String> params = new HashMap<>();
        params.put("PayTypeID", "ImmediatePay");
        params.put("OrderNo", orderId);
        params.put("QueryDetail", "0");//0：状态查询； 1：详细查询
        params.put("IsUpdate", "0");//是否更新订单状态  0：更新   1：不更新
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", JSON.toJSONString(params));
        return postJson(requestUrl, token, requestParams, responseNotify);
    }

    /**
     * @param postUrl
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http postJson(String postUrl, RequestBean params, ResponseNotify<?> responseNotify) {
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", params.getParam());
        return postJson(postUrl, params.getToken(), requestParams, responseNotify);
    }

    /**
     * @param postUrl
     * @param token
     * @param requestParams
     * @param responseNotify
     * @return
     */
    public static Http postJson(String postUrl, String token, RequestParams requestParams, ResponseNotify<?> responseNotify) {
        String paramStr = null;
        if (requestParams != null) {
            paramStr = requestParams.toString().replace("param=", "");
            Logger.json(paramStr);
        }
        Logger.d(
                "token=%s   params.length||%s"
                , token
                , TextUtils.isEmpty(paramStr) ? "没有参数" : paramStr.length()
        );

        BasicHeader header = new BasicHeader("Cookie", "ASP.NET_SessionId=" + token);
        Http http = getHttp().createRequest(Http.POST, postUrl, new Header[]{header}, requestParams, null, getResponseHandler(responseNotify));
        PersistentCookieStore cookieStore = http.getCookieStore();
        cookieStore.clear();
        BasicClientCookie clientCookie = new BasicClientCookie("ASP.NET_SessionId", token);
        clientCookie.setDomain("");
        cookieStore.addCookie(clientCookie);

        http.getHttpClient().setCookieStore(cookieStore);
        return http;
    }

    /**
     * @param postUrl
     * @param params
     * @param responseNotify
     * @return
     */
    public static Http getJson(String postUrl, RequestBean params, ResponseNotify<?> responseNotify) {
        Header header = new BasicHeader("Cookie", MessageFormat.format("ASP.NET_SessionId={0}", params.getToken()));
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", params.getParam());
        return getHttp().createRequest(Http.GET, postUrl, new Header[]{header}, requestParams, null, getResponseHandler(responseNotify));
    }

    /**
     * @param postUrl
     * @param requestParams
     * @param responseNotify
     * @return
     */
    public static Http getJson(String postUrl, String token, RequestParams requestParams, ResponseNotify<?> responseNotify) {
        Header header = new BasicHeader("Cookie", MessageFormat.format("ASP.NET_SessionId={0}", token));
        return getHttp().createRequest(Http.GET, postUrl, new Header[]{header}, requestParams, null, getResponseHandler(responseNotify));
    }

}


