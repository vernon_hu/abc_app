package com.abc.sz.app.activity_phase2.lightpay;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.UserAction;
import com.abc.sz.app.util.DialogUtil;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.encrypt.MD5;

/**
 * 验证密码
 * @author ftl
 *
 */
@ContentView(R.layout.activity_phase2_checkpassword)
public class CheckPasswrodActivity extends ABCActivity {
    @ViewInject(R.id.username) EditText username;
    @ViewInject(R.id.password) EditText password;
    @ViewInject(R.id.appBar) Toolbar toolbar;

    private UserAction userAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        userAction = (UserAction) controller.getAction(this, UserAction.class);

        initData();
    }

    public void initData() {
    	username.setText(getBaseApp().getLoginUser().getUserName());
    }

    @OnClick(value = {R.id.check})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.check:
                if (checkInfo()) {
                        /*登录*/
                    userAction.login(username.getText().toString(),
                    		MD5.encrypt(MD5.encrypt(password.getText().toString()))).start();
                }
                break;
        }
    }

    /**
     * 检查输入信息
     *
     * @return
     */
    private boolean checkInfo() {
        if (TextUtils.isEmpty(password.getText())) {
            FormsUtil.setErrorHtmlTxt(this, password, R.string.input_password_isempty);
            return false;
        }
        return true;
    }

    /**
     * 验证成功
     */
    public void checkSuccess() {
        setResult(RESULT_OK);
        finish();
    }

    /**
     * 验证失败
     *
     * @param retCode 失败返回错误信息
     */
    public void checkFailed(RetCode retCode) {
        DialogUtil.showWithNoTitile(this, retCode.getRetMsg());
    }

}
