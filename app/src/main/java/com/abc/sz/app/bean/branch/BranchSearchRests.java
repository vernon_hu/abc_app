package com.abc.sz.app.bean.branch;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by monkey on 2015/8/13.
 * 支行网点列表
 */
public class BranchSearchRests implements Parcelable, Comparable<BranchSearchRests> {
    private String RestIndex;
    private BranchBank SelfServiceBank;
    private BranchBank SelfServiceEquip;
    private String TypeId;
    private BranchBank BranchBank;

    public String getRestIndex() {
        return RestIndex;
    }

    public void setRestIndex(String restIndex) {
        RestIndex = restIndex;
    }

    public com.abc.sz.app.bean.branch.BranchBank getSelfServiceEquip() {
        return SelfServiceEquip;
    }

    public void setSelfServiceEquip(com.abc.sz.app.bean.branch.BranchBank selfServiceEquip) {
        SelfServiceEquip = selfServiceEquip;
    }

    public com.abc.sz.app.bean.branch.BranchBank getSelfServiceBank() {
        return SelfServiceBank;
    }

    public void setSelfServiceBank(com.abc.sz.app.bean.branch.BranchBank selfServiceBank) {
        SelfServiceBank = selfServiceBank;
    }

    public String getTypeId() {
        return TypeId;
    }

    public void setTypeId(String typeId) {
        TypeId = typeId;
    }

    public com.abc.sz.app.bean.branch.BranchBank getBranchBank() {
        return BranchBank;
    }

    public void setBranchBank(com.abc.sz.app.bean.branch.BranchBank branchBank) {
        BranchBank = branchBank;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.RestIndex);
        dest.writeParcelable(this.SelfServiceBank, 0);
        dest.writeParcelable(this.SelfServiceEquip, 0);
        dest.writeString(this.TypeId);
        dest.writeParcelable(this.BranchBank, 0);
    }

    public BranchSearchRests() {
    }

    protected BranchSearchRests(Parcel in) {
        this.RestIndex = in.readString();
        this.SelfServiceBank = in.readParcelable(com.abc.sz.app.bean.branch.BranchBank.class.getClassLoader());
        this.SelfServiceEquip = in.readParcelable(com.abc.sz.app.bean.branch.BranchBank.class.getClassLoader());
        this.TypeId = in.readString();
        this.BranchBank = in.readParcelable(com.abc.sz.app.bean.branch.BranchBank.class.getClassLoader());
    }

    public static final Parcelable.Creator<BranchSearchRests> CREATOR = new Parcelable.Creator<BranchSearchRests>() {
        public BranchSearchRests createFromParcel(Parcel source) {
            return new BranchSearchRests(source);
        }

        public BranchSearchRests[] newArray(int size) {
            return new BranchSearchRests[size];
        }
    };

    /**
     * 比较两个支行信息是否为同一类型
     * @param another
     * @return
     */
    @Override
    public int compareTo(BranchSearchRests another) {
        return another != null && another.getTypeId().equals(TypeId) ? 0 : -1;
    }
}
