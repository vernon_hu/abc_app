package com.abc.sz.app.activity_phase2.wdmap;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.branch.HistoryTag;
import com.abc.sz.app.util.AutoCompleteFilterFormDbAdapter;
import com.abc.sz.app.util.AutoCompleteFilterObject;
import com.abc.sz.app.view.flowlayout.FlowLayout;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.db.sqlite.Selector;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

import java.util.List;
import java.util.TimeZone;

import hirondelle.date4j.DateTime;

@ContentView(R.layout.activity_wd_search)
public class WdSearchActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.flowLayout) FlowLayout flowLayout;
    @ViewInject(R.id.autoCompleteTextView) AutoCompleteTextView autoCompleteTextView;
    private List<HistoryTag> historyTagList;
    public final static String QUERYTEXT_TAG = "QUERYTEXT_TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        autoCompleteTextView.setAdapter(new AutoCompleteFilterFormDbAdapter(
                this,
                new HistoryTag(),
                HistoryTag.class,
                "tag_name",
                "create_time",
                R.layout.layout_product_search_auto
        ) {

            @Override
            public void viewHandler(int position, AutoCompleteFilterObject filterObject, View convertView) {
                TextView textView = ViewHolder.get(convertView, R.id.text1);
                FormsUtil.setTextViewTxts(textView, filterObject.getSearchWord());
            }
        });
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                searchFromWord();
            }
        });
        autoCompleteTextView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                    searchFromWord();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //////////////初始化最近搜索数据///////////////
        initViewData();
    }

    /**
     * 初始化最近搜索数据
     */
    private void initViewData() {
        historyTagList = getDBUtils().findAll(Selector.from(HistoryTag.class).orderBy("create_time").limit(30));
        if (historyTagList != null) {
            flowLayout.removeAllViews();
            for (int i = 0; i < historyTagList.size(); i++) {
                final TextView TextView = (TextView) LayoutInflater.from(this).inflate(R.layout.layout_history_tagview, null);
                ViewGroup.MarginLayoutParams layoutParams = new ViewGroup.MarginLayoutParams(
                        ViewGroup.MarginLayoutParams.WRAP_CONTENT,
                        ViewGroup.MarginLayoutParams.WRAP_CONTENT
                );
                layoutParams.topMargin = FormsUtil.dip2px(8);
                layoutParams.leftMargin = FormsUtil.dip2px(8);
                TextView.setLayoutParams(layoutParams);
                TextView.setText(historyTagList.get(i).getTagName());
                TextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextView textView = (android.widget.TextView) v;
                        if (!TextUtils.isEmpty(textView.getText())) {
                            Bundle bundle = new Bundle();
                            bundle.putString(QUERYTEXT_TAG, textView.getText().toString());
                            callMe(WdSearchResultActivity.class, bundle);
                        } else {
                            Toast.makeText(WdSearchActivity.this, "请输入查询条件", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                flowLayout.addView(TextView);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_wd_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.search) {
            searchFromWord();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * 搜索输入的字符串
     */
    private void searchFromWord() {
        String tagName = autoCompleteTextView.getText().toString();
        if (!TextUtils.isEmpty(tagName)) {
            HistoryTag historyTag = new HistoryTag();
            Bundle bundle = new Bundle();
            bundle.putString(QUERYTEXT_TAG, tagName);
            callMe(WdSearchResultActivity.class, bundle);

            historyTag.setCreateTime(DateTime.now(TimeZone.getDefault()).format("YYYY-MM-DD hh:mm:ss"));
            historyTag.setTagName(tagName);
            getDBUtils().save(historyTag);
        } else {
            Toast.makeText(this, "请输入查询条件", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick({R.id.iv_search})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_search:
                searchFromWord();
                break;
        }
    }

}
