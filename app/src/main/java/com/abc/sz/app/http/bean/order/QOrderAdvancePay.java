package com.abc.sz.app.http.bean.order;

/**
 * 订单预支付
 *
 * @author ftl
 */
public class QOrderAdvancePay {

    /* 银行预留电话号码 */
    private String phone;
    /* 订单 ID*/
    private String orderId;
    /* 支付卡号*/
    private String orderNo;
    /* 短信验证码*/
    private String valideCode;

    public QOrderAdvancePay(String phone, String orderId, String orderNo,
                            String valideCode) {
        super();
        this.phone = phone;
        this.orderId = orderId;
        this.orderNo = orderNo;
        this.valideCode = valideCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getValideCode() {
        return valideCode;
    }

    public void setValideCode(String valideCode) {
        this.valideCode = valideCode;
    }

}
