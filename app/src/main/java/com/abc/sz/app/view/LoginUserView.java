package com.abc.sz.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCApplication;
import com.forms.base.LoginUser;
import com.forms.library.baseUtil.view.ViewUtils;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;

/**
 * Created by hwt on 14/11/17.
 */
public class LoginUserView extends LinearLayout {
    @ViewInject(R.id.userPic) ImageView userPic;
    @ViewInject(R.id.userName) TextView userName;
    @ViewInject(R.id.defaultCard) TextView defaultCard;
    @ViewInject(R.id.rightImage) ImageView rightImage;
    @ViewInject(R.id.userLayout) LinearLayout userLayout;
    @ViewInject(R.id.loginLayout) LinearLayout loginLayout;

    OnClickListener loginClick;
    OnClickListener registClick;
    ABCApplication application;

    public LoginUserView(Context context) {
        super(context);
    }

    public LoginUserView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isInEditMode()) return;
        View view = LayoutInflater.from(context).inflate(R.layout.layout_loginuser, this);
        ViewUtils.inject(this, view);
    }

    /**
     * @param loginClick  点击登录按钮
     * @param registClick 点击注册按钮
     */
    public void init(ABCApplication application, OnClickListener loginClick, OnClickListener registClick) {
        this.loginClick = loginClick;
        this.registClick = registClick;
        this.application = application;
    }

    /**
     * 置为登录
     *
     * @param loginUser
     */
    public void login(LoginUser loginUser) {
        if (loginUser != null) {
            rightImage.setVisibility(VISIBLE);
            userLayout.setVisibility(VISIBLE);
            userLayout.setVisibility(VISIBLE);
            loginLayout.setVisibility(GONE);
            FormsUtil.setTextViewTxt(userName, loginUser.getUserName());

            application.login(loginUser);
        } else {
            loginout();
        }
    }

    /**
     * 置为登出
     */
    public void loginout() {
        rightImage.setVisibility(GONE);
        userLayout.setVisibility(GONE);
        rightImage.setVisibility(GONE);
        loginLayout.setVisibility(VISIBLE);
        application.logout();
    }

    @OnClick(value = {R.id.loginBtn, R.id.registBtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginBtn:
                loginClick.onClick(view);
                break;
            case R.id.registBtn:
                registClick.onClick(view);
                break;
        }
    }
}
