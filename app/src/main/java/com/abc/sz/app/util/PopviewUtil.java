package com.abc.sz.app.util;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

/**
 * Created by korey on 2015/5/7.
 */
public class PopviewUtil {

    public static void showPopView(Context context,int menuLayoutResId){
        View view = LayoutInflater.from(context).inflate(menuLayoutResId,null);
        PopupWindow pop = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                false);
        pop.setBackgroundDrawable(new BitmapDrawable(context.getResources(), ""));
        pop.setOutsideTouchable(true);
        pop.setFocusable(true);
    }
}
