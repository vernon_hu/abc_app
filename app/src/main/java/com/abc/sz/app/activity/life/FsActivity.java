package com.abc.sz.app.activity.life;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.JfAction;
import com.abc.sz.app.http.bean.jf.JfBean;
import com.abc.sz.app.http.bean.jf.QJfBean;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;


/**
 * 非税缴费
 *
 * @author llc
 */
@ContentView(R.layout.activity_fs)
public class FsActivity extends ABCActivity {

    //单位编号
    @ViewInject(R.id.et_companyNum) private EditText et_companyNum;

    //缴款通知单编号
    @ViewInject(R.id.et_noticeNum) private EditText et_noticeNum;
    @ViewInject(R.id.appBar) private Toolbar toolbar;


    private JfAction jfAction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initData();
        initView();
    }

    private void initView() {
        // TODO Auto-generated method stub
        et_companyNum.setText("903001");
        et_noticeNum.setText("0721400000100003");
    }

    private void initData() {
        // TODO Auto-generated method stub
        jfAction = (JfAction) controller.getAction(this, JfAction.class);
    }


    @OnClick(value = {R.id.btn_sure})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sure:
                if (checkInfo()) {
                    QJfBean bean = new QJfBean();
                    bean.setCompanyNum(et_companyNum.getText().toString());
                    bean.setNoticeNum(et_noticeNum.getText().toString());
                    jfAction.startJf(bean).start(true);
                    break;
                }
        }
    }


    /**
     * 检查输入信息
     *
     * @return
     */
    private boolean checkInfo() {
        if (TextUtils.isEmpty(et_companyNum.getText())) {
            FormsUtil.setErrorHtmlTxt(this, et_companyNum, R.string.input_companyNum_isempty);
            return false;
        } else if (TextUtils.isEmpty(et_noticeNum.getText())) {
            FormsUtil.setErrorHtmlTxt(this, et_noticeNum, R.string.input_noticeNum_isempty);
            return false;
        }
        return true;
    }


    /**
     * 请求失败回调
     *
     * @param retCode
     */
    public void failed(RetCode retCode, String requestFrom) {

    }


    /**
     * 请求成功回调
     */
    public void success(JfBean bean) {
        getCacheBean().put(FsInfoActivity.JF_INFO, bean);
        callMe(FsInfoActivity.class);
    }

}
