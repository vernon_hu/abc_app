package com.abc.sz.app.http.bean.other;


/**
 * 意见反馈
 *
 * @author ftl
 */
public class QSuggestion {

    /* 反馈内容*/
    private String content;
    /* 反馈版本*/
    private String version;
    /* 反馈平台*/
    private String platform;

    public QSuggestion(String content, String version, String platform) {
        super();
        this.content = content;
        this.version = version;
        this.platform = platform;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }


}
