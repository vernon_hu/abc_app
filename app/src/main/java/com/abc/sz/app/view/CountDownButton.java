package com.abc.sz.app.view;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * 计时按钮
 *
 * @author ftl
 */
public class CountDownButton extends TextView {
    private TimerListener timerListener;

    public CountTimer timer = new CountTimer(59999, 1000);

    public CountDownButton(Context context) {
        super(context);
    }

    public CountDownButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CountDownButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public class CountTimer extends CountDownTimer {

        public CountTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            if (timerListener != null) {
                timerListener.onFinish();
            }
            CountDownButton.this.setEnabled(true);
            CountDownButton.this.setText("获取验证码");
        }

        @Override
        public void onTick(long millisUntilFinished) {
            CountDownButton.this.setEnabled(false);
            CountDownButton.this.setText((millisUntilFinished / 1000) + "秒后重试");
        }

    }

    public interface TimerListener {
        public void onFinish();
    }

    public TimerListener getTimerListener() {
        return timerListener;
    }

    public void setTimerListener(TimerListener timerListener) {
        this.timerListener = timerListener;
    }
}
