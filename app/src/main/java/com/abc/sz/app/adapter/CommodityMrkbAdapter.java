package com.abc.sz.app.adapter;

import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.drjx.JxBean;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.util.ShoppingCarCount;
import com.forms.base.XDRImageLoader;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

import java.util.List;

/**
 * 每日快报商品列表适配
 *
 * @author llc
 */
public class CommodityMrkbAdapter extends BaseAdapter {

    private List<JxBean> commodityList;

    private LayoutParams params;

    private XDRImageLoader mImageLoader;

    int itemWidth;


    public CommodityMrkbAdapter(XDRImageLoader mImageLoader, List<JxBean> commodityList) {
        this.commodityList = commodityList;

        itemWidth = (FormsUtil.SCREEN_WIDTH - FormsUtil.dip2px(3 * 6)) / 2;

//        params = new LayoutParams(itemWidth,itemWidth/2*3);
        params = new LayoutParams(itemWidth, LayoutParams.WRAP_CONTENT);
        this.mImageLoader = mImageLoader;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return commodityList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return commodityList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final JxBean bean = commodityList.get(position);
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_mrkb_item, null);
            convertView.setLayoutParams(params);
        }

        ImageView ivCommodityImage = (ImageView) convertView.findViewById(R.id.iv_mrkb_commodity_image);

        ivCommodityImage.setLayoutParams(new LinearLayout.LayoutParams(itemWidth - FormsUtil.dip2px(4), itemWidth - FormsUtil.dip2px(4)));
        mImageLoader.displayImage(bean.getProductPic(), ivCommodityImage, ImageViewUtil.getOption());

        //积分折扣
        TextView tv_jfzk = ViewHolder.get(convertView, R.id.tv_jfzk);
        //特价活动
        TextView tv_tjhd = ViewHolder.get(convertView, R.id.tv_tjhd);
        //名称
        TextView tvCommodityName = ViewHolder.get(convertView, R.id.tv_mrkb_commodityName);
        //现价
        TextView tvPrice = ViewHolder.get(convertView, R.id.tv_mrkb_price);
        //历史价格
        TextView tvHisPrice = ViewHolder.get(convertView, R.id.tv_mrkb_hisPrice);
        //收藏数量
        TextView tvStarNum = ViewHolder.get(convertView, R.id.tv_star_num);
        tvHisPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        if ("1".equals(bean.getProductType())) {
            tv_jfzk.setVisibility(View.VISIBLE);
            tv_tjhd.setVisibility(View.GONE);
        } else {
            tv_jfzk.setVisibility(View.GONE);
            tv_tjhd.setVisibility(View.VISIBLE);
        }

        tvCommodityName.setText(bean.getProductName());
        tvHisPrice.setText(ShoppingCarCount.formatMoney(bean.getHisPrice()));
        tvPrice.setText(ShoppingCarCount.formatMoney(bean.getProductPrice()));
        tvStarNum.setText(bean.getFavoriteNum());

        return convertView;
    }

}
