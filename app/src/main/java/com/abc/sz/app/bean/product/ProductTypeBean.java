package com.abc.sz.app.bean.product;

import com.forms.library.base.BaseBean;

import java.util.List;

/**
 * 商品类别
 *
 * @author llc
 */
public class ProductTypeBean extends BaseBean {

    private List<Type> list;

    public List<Type> getList() {
        return list;
    }

    public void setList(List<Type> list) {
        this.list = list;
    }


}
