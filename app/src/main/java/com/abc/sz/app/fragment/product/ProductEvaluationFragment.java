package com.abc.sz.app.fragment.product;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.ProductInfoAction;
import com.abc.sz.app.activity.product.ProductEvaluationActivity;
import com.abc.sz.app.adapter.product.ProductEvaluationAdapter;
import com.abc.sz.app.bean.product.EvalutionBean;
import com.abc.sz.app.bean.product.ProductEvalutionBean;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.product.QEvaluationList;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCFragment;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.view.pullToRefresh.PullToRefreshBase;
import com.forms.view.pullToRefresh.PullToRefreshListView;
import com.forms.view.toast.MyToast;

/**
 * 商品列表
 *
 * @author ftl
 */
public class ProductEvaluationFragment extends ABCFragment implements
		PullToRefreshBase.OnLastItemVisibleListener {

	@ViewInject(R.id.lv_evaluation_list)
	private PullToRefreshListView lv_evaluation_list;
	@ViewInject(R.id.loadingView)
	private LoadingView loadingView;

	private List<EvalutionBean> list = new ArrayList<>();
	private ProductEvaluationAdapter adapter;
	private ProductInfoAction piAction;
	private int pageNum = 0;
	private boolean isRefresh = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_product_evaluation_list, container,
				false);
	}

	@Override
	protected void initData() {
		super.initData();
		adapter = new ProductEvaluationAdapter(list);
		lv_evaluation_list.setAdapter(adapter);
		piAction = (ProductInfoAction) controller.getAction(baseActivity, this, 
				ProductInfoAction.class);
		queryProductList(false);
	}

	@Override
	protected void initView() {
		super.initView();
	}

	@Override
	protected void initListener() {
		super.initListener();
	}

	public void queryProductList(boolean isPullUp) {
		QEvaluationList qEvaluationList = ((ProductEvaluationActivity)baseActivity).getqEvaluationList();
		qEvaluationList.setPageNum(String.valueOf(pageNum));
		RequestBean<QEvaluationList> params = new RequestBean<QEvaluationList>(
				qEvaluationList);
		Http http = piAction.loadingEvaluationAll(params);
		if (isPullUp) {
			http.start(false);
		} else {
			http.setLoadingView(loadingView).start();
		}
	}

	@Override
	public void onLastItemVisible() {
		++pageNum;
		queryProductList(true);
	}

	public void onRefresh() {
		isRefresh = true;
		pageNum = 0;
		queryProductList(false);
	}

	/**
	 * 查询产品列表成功
	 */
	public void querySuccess(RetCode retCode, ProductEvalutionBean content) {
		((ProductEvaluationActivity)baseActivity).initView(content);
		if (isRefresh)
			list.clear();
		if (retCode != RetCode.noData && content != null && content.getOrderAppraiseDetailList().size() != 0) {
			list.addAll(content.getOrderAppraiseDetailList());
			adapter.notifyDataSetChanged();
		} else {
			if (list.size() > 0) {
				MyToast.showTEXT(baseActivity, getString(R.string.noMoreData));
			} else {
				loadingView.noData(getString(R.string.loadingNoData));
			}
		}
		isRefresh = false;
	}

	/**
	 * 查询列表失败
	 *
	 * @param retCode
	 */
	public void queryFailed(RetCode retCode) {
		if (list.size() > 0) {
			MyToast.showTEXT(baseActivity, retCode.getRetMsg());
		} else {
			loadingView.failed(retCode.getRetMsg());
		}
		isRefresh = false;
	}
}
