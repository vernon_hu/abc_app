package com.abc.sz.app.bean.loan;

import java.util.List;

/**
 * Created by hwt on 4/30/15.
 */
public class DepositLoanInfo {
    private String loanAccount;// 存贷通开能账号
    private String depositAccount;// 还款账号
    private List<Account> depositList;// 存款账号列表
    private List<Account> loanList;//贷款账号列表
    private String payBackType;//收益返回方式
    private String managerFee;//账号管理费
    private String cycle;//收益支付周期

    public String getLoanAccount() {
        return loanAccount;
    }

    public void setLoanAccount(String loanAccount) {
        this.loanAccount = loanAccount;
    }

    public String getDepositAccount() {
        return depositAccount;
    }

    public void setDepositAccount(String depositAccount) {
        this.depositAccount = depositAccount;
    }

    public List<Account> getDepositList() {
        return depositList;
    }

    public void setDepositList(List<Account> depositList) {
        this.depositList = depositList;
    }

    public List<Account> getLoanList() {
        return loanList;
    }

    public void setLoanList(List<Account> loanList) {
        this.loanList = loanList;
    }

    public String getPayBackType() {
        return payBackType;
    }

    public void setPayBackType(String payBackType) {
        this.payBackType = payBackType;
    }

    public String getManagerFee() {
        return managerFee;
    }

    public void setManagerFee(String managerFee) {
        this.managerFee = managerFee;
    }

    public String getCycle() {
        return cycle;
    }

    public void setCycle(String cycle) {
        this.cycle = cycle;
    }
}
