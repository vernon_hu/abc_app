package com.abc.sz.app.bean.cridetcard;

import java.util.Map;
/**
 * 车商区域和品牌
 * @author ftl
 *
 */
public class DistrictAndBrand {

	private Map<String, String> DistrictList;
	private Map<String, String> BrandList;
	
	public Map<String, String> getDistrictList() {
		return DistrictList;
	}
	public void setDistrictList(Map<String, String> districtList) {
		DistrictList = districtList;
	}
	public Map<String, String> getBrandList() {
		return BrandList;
	}
	public void setBrandList(Map<String, String> brandList) {
		BrandList = brandList;
	}
	
}
