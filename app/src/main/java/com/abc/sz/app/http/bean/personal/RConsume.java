package com.abc.sz.app.http.bean.personal;


/**
 * Created by hwt on 14/11/18.
 */
public class RConsume {
    private String orderId;//1：商品 2：快报 ID  3：跳转 URL
    private String orderType;//订单类型 1：实物类2：票券类3：电影类4：充值类
    private String productNames;//广告简述
    private String postTime;//下单时间
    private Double amt;//订单总额

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getProductNames() {
        return productNames;
    }

    public void setProductNames(String productNames) {
        this.productNames = productNames;
    }

    public String getPostTime() {
        return postTime;
    }

    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }

    public Double getAmt() {
        return amt;
    }

    public void setAmt(Double amt) {
        this.amt = amt;
    }
}
