package com.abc.sz.app.bean.product;


import java.util.List;

import com.abc.sz.app.bean.Product;
import com.forms.library.base.BaseBean;

/**
 * 商品列表
 *
 * @author llc
 */
public class ProductListBean extends BaseBean {
    //商品类别ID
    private String typeId;
    //商品类别名称
    private String typeName;
    //类别图标URL
    private String PictureUrl;
    //是否为最后属性：0：不是 1：是
    private String IsLast;
    //类别等级
    private String grade;
    //商品类别下的商品列表
    private List<ProductListBean> ChildCategoryList;
    private List<Product> typeList;

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getPictureUrl() {
        return PictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        PictureUrl = pictureUrl;
    }

    public String getIsLast() {
        return IsLast;
    }

    public void setIsLast(String isLast) {
        IsLast = isLast;
    }

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public List<ProductListBean> getChildCategoryList() {
		return ChildCategoryList;
	}

	public void setChildCategoryList(List<ProductListBean> childCategoryList) {
		ChildCategoryList = childCategoryList;
	}

	public List<Product> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<Product> typeList) {
		this.typeList = typeList;
	}

}
