package com.abc.sz.app.fragment.product;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.ProductInfoAction;
import com.abc.sz.app.activity.product.ProductEvaluationActivity;
import com.abc.sz.app.activity.product.ProductInfoActivity;
import com.abc.sz.app.adapter.product.ProductEvaluationAdapter;
import com.abc.sz.app.bean.product.EvalutionBean;
import com.abc.sz.app.bean.product.ProductEvalutionBean;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.product.QEvaluationList;
import com.abc.sz.app.view.LoadingView;
import com.abc.sz.app.view.MyEvaluationListView;
import com.forms.base.ABCFragment;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品评论
 *
 * @author llc
 */
public class ProductContentEvaluationFragment extends ABCFragment {

    @ViewInject(R.id.lv_evaluation)
    private MyEvaluationListView lv_evaluation;
    //进入评论详情
    @ViewInject(R.id.rl_top)
    private RelativeLayout rl_top;
    //总人数
    @ViewInject(R.id.tv_count_evalution)
    private TextView tv_count_evalution;
    //好评率
    @ViewInject(R.id.tv_rate)
    private TextView tv_rate;

    @ViewInject(R.id.loadingView) LoadingView loadingView;

    //评论适配器
    private ProductEvaluationAdapter mAdapter;
    //商品评论列表
    private List<EvalutionBean> list = new ArrayList<EvalutionBean>();
    //请求
    private ProductInfoAction action;
    //商品ID
    private String productId;
    private RequestBean<QEvaluationList> params;
    private QEvaluationList evaluationBean;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_product_evaluation, container, false);
        return view;
    }


    @Override
    protected void initData() {
        // TODO Auto-generated method stub
        super.initData();

        action = controller.getTargetAction(getABCActivity(), this, ProductInfoAction.class);
        productId = ((ProductInfoActivity) baseActivity).getProductId();

        mAdapter = new ProductEvaluationAdapter(list);

        //配置请求参数
        evaluationBean = new QEvaluationList();
        evaluationBean.setProductId(productId);
        evaluationBean.setPageNum("0");
        evaluationBean.setType("0");
        params = new RequestBean<QEvaluationList>(evaluationBean);
        //发送请求
        action.loadingEvaluation(params).setLoadingView(loadingView).start();
    }

    @Override
    protected void initView() {
        // TODO Auto-generated method stub
        super.initView();

        lv_evaluation.setAdapter(mAdapter);
    }

    @Override
    protected void initListener() {
        // TODO Auto-generated method stub
        super.initListener();

        lv_evaluation.setOnScrollListener(new OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                System.out.println("firstVisibleItem:" + firstVisibleItem);
                View firstView = view.getChildAt(0);
                if (firstView == null)
                    return;
                if (firstVisibleItem == 0) {
                    if (firstView.getTop() == 0) {
                        ProductContentFragment.isTop = true;
                    }
                } else {
                    ProductContentFragment.isTop = false;
                }
            }
        });
    }

    @OnClick(value = {R.id.rl_top})
    public void btnOnClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putString(ProductEvaluationActivity.PRODUCT_ID, productId);
        baseActivity.callMe(ProductEvaluationActivity.class, bundle);
    }


    /**
     * 计算比例
     *
     * @return
     */
    private int mathRate(String all, String last) {
        float rate = Float.parseFloat(last) / Float.parseFloat(all);
        rate = rate * 100;
        return (int) rate;
    }


    /**
     * 评论请求回调
     */
    public void onSuccess(ProductEvalutionBean bean) {
    	if(bean != null && bean.getOrderAppraiseDetailList().size() != 0){
    		this.list.addAll(bean.getOrderAppraiseDetailList());
    	    tv_count_evalution.setText(bean.getAllReviews());
    	    tv_rate.setText(mathRate(bean.getAllReviews(), bean.getGood()) + "%");
    	    mAdapter.notifyDataSetChanged();
    	}else{
    		loadingView.noData(getString(R.string.loadingNoData));
    	}
    }

    /**
     * 请求失败回调
     *
     * @param retCode
     */
    public void failed(RetCode retCode) {
    	loadingView.failed(retCode.getRetMsg());
    }
}
