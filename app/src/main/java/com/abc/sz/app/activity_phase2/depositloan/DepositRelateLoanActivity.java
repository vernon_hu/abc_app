package com.abc.sz.app.activity_phase2.depositloan;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.DepositloanAction;
import com.abc.sz.app.bean.loan.Account;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.ViewHolder;

/**
 * 关联贷款页面
 * 
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_depositloanopen)
public class DepositRelateLoanActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	@ViewInject(R.id.tvExplain)
	private TextView tvExplain;
	@ViewInject(R.id.lvAccount)
	private ListView lvAccount;
	@ViewInject(R.id.loadingView)
	private LoadingView loadingView;

	private BaseAdapter<Account> adapter;
	private List<Account> accountList = new ArrayList<Account>();
	private DepositloanAction depositloanAction;
	private int currentPos = -1;
	private List<String> depositAccouts;
	private List<String> loanAccounts;
	private String payBackType;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		tvExplain.setText(getString(R.string.loan_account_explain));
		depositAccouts = cacheBean.getCacheArray("depositAccouts", String.class);
		loanAccounts = cacheBean.getCacheArray("loanAccounts", String.class);
		depositloanAction = (DepositloanAction) controller.getAction(this, DepositloanAction.class);
		depositloanAction.queryOtherLoanAccountList("").setLoadingView(loadingView).start();
		adapter = new BaseAdapter<Account>(this, accountList, R.layout.layout_phase2_dlloanaccount_item) {

			@Override
			public void viewHandler(final int position, Account t, View convertView) {
				LinearLayout lltItem = ViewHolder.get(convertView, R.id.lltItem);
				ImageView ivSelect = ViewHolder.get(convertView, R.id.ivSelect);
				TextView tvAccount = ViewHolder.get(convertView, R.id.tvAccount);
				TextView tvSum = ViewHolder.get(convertView, R.id.tvSum);
				
				if (position == currentPos) {
					ivSelect.setImageResource(R.drawable.shopping_checkbox_select);
				} else {
					ivSelect.setImageResource(R.drawable.shopping_checkbox_default);
				}
				if (t != null) {
					tvAccount.setText(t.getAccount());
					tvSum.setText(t.getAmount());
				}
				lltItem.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (position != currentPos) {
							currentPos = position;
							notifyDataSetChanged();
						}
					}
				});
			}
		};
		lvAccount.setAdapter(adapter);
	}

	@OnClick({ R.id.btnNext })
	public void calculate(View v) {
		switch (v.getId()) {
		case R.id.btnNext:
			if(currentPos == -1){
				Toast.makeText(this, "请选择关联的贷款账号", Toast.LENGTH_SHORT).show();
			}else{
				View view = LayoutInflater.from(this).inflate(R.layout.layout_dialog_relate_loan, null);
				RadioGroup rgSelect = (RadioGroup) view.findViewById(R.id.rgSelect);
				rgSelect.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					
					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						payBackType = String.valueOf(checkedId);
					}
				});
				rgSelect.check(1);
				
				AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
				builder.setTitle("收益返回方式选择");
			    builder.setView(view);
			    builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						depositloanAction.postCreateInfo("", depositAccouts, loanAccounts, payBackType).start();
					}
				});
			}
			break;
		}
	}

	/**
	 * 查询其他贷款账号列表成功回调
	 */
	public void querySuccess(List<Account> list){
		 if (list != null && list.size() > 0) {
			accountList.clear();
			accountList.addAll(list);
            adapter.notifyDataSetChanged();
	     }
	}
	
	/**
	 * 账号关联信息提交成功回调
	 */
	public void postSuccess(){
		callMe(DepositLoanConfirmActivity.class);
	}
}
