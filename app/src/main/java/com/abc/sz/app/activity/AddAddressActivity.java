package com.abc.sz.app.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.AddressAction;
import com.abc.sz.app.bean.Address;
import com.abc.sz.app.bean.branch.City;
import com.forms.base.ABCActivity;
import com.forms.library.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;
import com.forms.view.toast.MyToast;

/**
 * 增加新的地址/修改地址
 *
 * @author ftl
 */
@ContentView(R.layout.activity_add_address)
public class AddAddressActivity extends ABCActivity {
    @ViewInject(R.id.appBar) android.support.v7.widget.Toolbar toolbar;
    @ViewInject(R.id.et_person)
    private EditText et_person;
    @ViewInject(R.id.et_phone)
    private EditText et_phone;
    @ViewInject(R.id.et_address)
    private EditText et_address;
    @ViewInject(R.id.llDefault)
    private LinearLayout llDefault;
    @ViewInject(R.id.setDefault)
    private RadioButton setDefault;
    @ViewInject(R.id.tv_province)
    private TextView tv_province;
    @ViewInject(R.id.btn_operate)
    private Button btn_operate;

    private String addressId;
    private AddressAction addressAction;
    private AlertDialog dialog;
    private ListView lv_address;
    private TextView tv_title;
    private Button btn_cancel;
    private Button btn_confirm;
    private BaseAdapter<City> adapter;
    private List<City> list = new ArrayList<City>();
    private List<City> provinceList = new ArrayList<City>();
    private List<City> cityList = new ArrayList<City>();
    private List<City> areaList = new ArrayList<City>();
    private Map<Integer, Boolean> map = new HashMap<Integer, Boolean>();
    private String[] array = new String[3];
    private String cityId = "";
    //1表示来自地址列表画面
    //2表示来自地址列表编辑画面
    //3表示来自订单确认画面
    private int fromPage = 0;
    private int index = 0;
    private Address mAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        addressAction = (AddressAction) controller.getAction(this, AddressAction.class);
        initData();
        initView();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fromPage = bundle.getInt("fromPage");
            if (fromPage == 2) {
            	llDefault.setVisibility(View.GONE);
                Address address = (Address) bundle.get("address");
                if (address != null) {
                    addressId = address.getAddressId();
                    array[0] = address.getProvince();
                    array[1] = address.getCity();
                    array[2] = address.getArea();
                    FormsUtil.setTextViewTxt(et_person, address.getPerson(), null);
                    FormsUtil.setTextViewTxt(et_phone, address.getPhone(), null);
                    FormsUtil.setTextViewTxt(tv_province, array[0] + " " + array[1] + " " + array[2], null);
                    FormsUtil.setTextViewTxt(et_address, address.getAddress(), null);
                }
            } else if (fromPage == 3) {
                btn_operate.setText(getString(R.string.save_and_use));
            }
        }
    }

    /**
     * 初始化界面
     */
    private void initView() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.layout_dialog_address, null);
        dialog = new AlertDialog.Builder(this).create();
        dialog.setView(dialogView, 0, 0, 0, 0);
        dialog.setInverseBackgroundForced(true);

        lv_address = (ListView) dialogView.findViewById(R.id.lv_address);
        tv_title = (TextView) dialogView.findViewById(R.id.tv_title);
        btn_cancel = (Button) dialogView.findViewById(R.id.btn_cancel);
        btn_confirm = (Button) dialogView.findViewById(R.id.btn_confirm);
        lv_address.setAdapter(adapter = new BaseAdapter<City>(this, list, R.layout.layout_dialog_address_item) {

            @Override
            public void viewHandler(final int position, final City city, View convertView) {
                LinearLayout llAddress = ViewHolder.get(convertView, R.id.ll_address);
                TextView tvCityName = ViewHolder.get(convertView, R.id.tv_cityName);
                ImageView ivSelect = ViewHolder.get(convertView, R.id.iv_select);
                FormsUtil.setTextViewTxt(tvCityName, city.getCityName(), null);
                if (map.get(position)) {
                    ivSelect.setVisibility(View.VISIBLE);
                } else {
                    ivSelect.setVisibility(View.INVISIBLE);
                }
                llAddress.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        for (int i = 0; i < list.size(); i++) {
                            map.put(i, false);
                        }
                        map.put(position, true);
                        array[index - 1] = city.getCityName();
                        cityId = city.getCityId();
                        notifyDataSetChanged();
                    }
                });
            }
        });

        btn_cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (index == 1) {
                    dialog.dismiss();
                } else if (index == 2) {
                    changeListContent(provinceList);
                    tv_title.setText(getString(R.string.spinner_select_province));
                    btn_cancel.setText(getString(R.string.action_cancel));
                } else {
                    changeListContent(cityList);
                    tv_title.setText(getString(R.string.spinner_select_city));
                    btn_cancel.setText(getString(R.string.up_step));
                    btn_confirm.setText(getString(R.string.next_step));
                }
                index--;
            }
        });
        btn_confirm.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!map.containsValue(true)) {
                    String message = "请选择";
                    if (index == 1) {
                        message += getString(R.string.spinner_province) + "!";
                    } else if (index == 2) {
                        message += getString(R.string.spinner_city) + "!";
                    } else {
                        message += getString(R.string.spinner_area) + "!";
                    }
                    MyToast.show(AddAddressActivity.this, message, MyToast.TEXT,
                            Gravity.CENTER, Toast.LENGTH_SHORT);
                } else {
                    if (index == 3) {
                        index = 0;
                        dialog.dismiss();
                        FormsUtil.setTextViewTxt(tv_province, array[0] + " " + array[1] + " " + array[2], null);
                    } else {
                        addressAction.queryCity(cityId, String.valueOf(index + 1)).start();
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (fromPage == 2) {
        	toolbar.setTitle(getString(R.string.grzx_alert_address));
        }
    }


    @OnClick(value = {R.id.tv_province, R.id.btn_operate})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.tv_province:
                if (provinceList.size() == 0) {
                    addressAction.queryCity(null, "1").start();
                } else {
                    index++;
                    if (!dialog.isShowing()) {
                    	changeListContent(provinceList);
                        tv_title.setText(getString(R.string.spinner_select_province));
                        btn_cancel.setText(getString(R.string.action_cancel));
                        btn_confirm.setText(getString(R.string.next_step));
                        dialog.show();
                    }
                }
                break;
            case R.id.btn_operate:
                String person = et_person.getText().toString();
                String phone = et_phone.getText().toString();
                String province = array[0];
                String city = array[1];
                String area = array[2];
                String address = et_address.getText().toString();
                if ("".equals(person.trim())) {
                    MyToast.show(this, "请输入收货人!", MyToast.TEXT, Gravity.CENTER, Toast.LENGTH_SHORT);
                    return;
                } else if ("".equals(phone.trim())) {
                    MyToast.show(this, "请输入手机号码!", MyToast.TEXT, Gravity.CENTER, Toast.LENGTH_SHORT);
                    return;
                } else if ("".equals(tv_province.getText().toString().trim())) {
                    MyToast.show(this, "请选择所在区域!", MyToast.TEXT, Gravity.CENTER, Toast.LENGTH_SHORT);
                    return;
                } else if ("".equals(address.trim())) {
                    MyToast.show(this, "请输入详细地址!", MyToast.TEXT, Gravity.CENTER, Toast.LENGTH_SHORT);
                    return;
                }
                if (fromPage == 2) {
                    addressAction.updateAddress(addressId, person, phone, province, city, area, address).start();
                } else {
                	String isDefault = setDefault.isChecked() ? "1" : "0";
                    if (fromPage == 3) {
                        mAddress = new Address(person, phone, province, city, area, address);
                    }
                    addressAction.addAddress(person, phone, province, city, area, address, isDefault, fromPage).start();
                }
                break;
        }
    }

    /**
     * 查询城市列表成功
     */
    public void querySuccess(List<City> list) {
        index++;
        if(list != null && list.size() > 0){
        	if (!dialog.isShowing()) {
                dialog.show();
            }
	        if (index == 1) {
	            provinceList = list;
	            changeListContent(provinceList);
	            tv_title.setText(getString(R.string.spinner_select_province));
	            btn_cancel.setText(getString(R.string.action_cancel));
	            btn_confirm.setText(getString(R.string.next_step));
	        } else if (index == 2) {
	            cityList = list;
	            changeListContent(list);
	            tv_title.setText(getString(R.string.spinner_select_city));
	            btn_cancel.setText(getString(R.string.up_step));
	        } else {
	            areaList = list;
	            changeListContent(areaList);
	            tv_title.setText(getString(R.string.spinner_select_area));
	            btn_confirm.setText(getString(R.string.action_sure));
	        }
        }else{
        	if(index == 2){
        		FormsUtil.setTextViewTxt(tv_province, array[0], null);
        	}else if(index == 3){
        		FormsUtil.setTextViewTxt(tv_province, array[0] + " " + array[1], null);
        	}
        	index = 0;
        	dialog.dismiss();
        }
    }

    /**
     * 改变listView中的显示内容
     *
     * @param cityList
     */
    public void changeListContent(List<City> cityList) {
        if (cityList != null && cityList.size() != 0) {
            list.clear();
            map.clear();
            for (int i = 0; i < cityList.size(); i++) {
                list.add(cityList.get(i));
                map.put(i, false);
            }
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * 添加并使用地址成功
     */
    public void addAndUseSuccess() {
        getCacheBean().putCache(mAddress);
        finishForBack(RESULT_OK);
    }

}
