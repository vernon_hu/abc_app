package com.abc.sz.app.http.bean.card;

/**
 * 信用卡进度
 * 
 * @author ftl
 */
public class QCardSchedule {

    private String certtyp;	//证件类型
    private String certno;	//证件号码
    private String custname;
    private String reqSrc;
    
    public QCardSchedule(){}
    
	public QCardSchedule(String certtyp, String certno, String custname,
			String reqSrc) {
		super();
		this.certtyp = certtyp;
		this.certno = certno;
		this.custname = custname;
		this.reqSrc = reqSrc;
	}

	public String getCerttyp() {
		return certtyp;
	}

	public void setCerttyp(String certtyp) {
		this.certtyp = certtyp;
	}

	public String getCertno() {
		return certno;
	}

	public void setCertno(String certno) {
		this.certno = certno;
	}

	public String getCustname() {
		return custname;
	}

	public void setCustname(String custname) {
		this.custname = custname;
	}

	public String getReqSrc() {
		return reqSrc;
	}

	public void setReqSrc(String reqSrc) {
		this.reqSrc = reqSrc;
	}
    
}
