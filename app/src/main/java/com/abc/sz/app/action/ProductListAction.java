package com.abc.sz.app.action;

import java.util.List;

import com.abc.sz.app.bean.product.ProductListBean;
import com.abc.sz.app.bean.product.TypeAttributeBean;
import com.abc.sz.app.fragment.product.ProductListFragment;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.product.QProductAttribute;
import com.abc.sz.app.http.bean.product.QProductList;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.alibaba.fastjson.TypeReference;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;


/**
 * 商品列表请求
 *
 * @author llc
 */
public class ProductListAction extends BaseAction {


    /**
     * 请求产品列表
     *
     * @param params
     * @return
     */
    public Http loadingProductList(RequestBean<QProductList> params) {
        return APPRequestPhase2.productListQuery(params,
                new ResponseNotify<List<ProductListBean>>(new TypeReference<RespBean<List<ProductListBean>>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<List<ProductListBean>> response) {
                        ((ProductListFragment) uiObject).querySuccess(retCode, response.getContent());
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        ((ProductListFragment) uiObject).queryFailed(retCode);
                    }
                });
    }

    /**
     * 请求商品属性
     *
     * @param params
     * @return
     */
    public Http loadingAttribute(RequestBean<QProductAttribute> params) {
        return APPRequestPhase2.productAttributeQuery(params,
                new ResponseNotify<List<TypeAttributeBean>>(new TypeReference<RespBean<List<TypeAttributeBean>>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<List<TypeAttributeBean>> response) {
                        ((ProductListFragment) uiObject).queryAttributeSuccess(response.getContent());
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        ((ProductListFragment) uiObject).queryFailed(retCode);
                    }
                });
    }
}
