package com.abc.sz.app.activity.personal;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.UserAction;
import com.abc.sz.app.bean.PayCode;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.library.base.BaseAdapter;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.net.ViewLoading.OnRetryListener;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;
import com.forms.view.pullToRefresh.PullToRefreshBase;
import com.forms.view.pullToRefresh.PullToRefreshListView;
import com.forms.view.toast.MyToast;

import java.util.ArrayList;
import java.util.List;

/**
 * 支付码
 *
 * @author ftl
 */
@ContentView(R.layout.activity_pay_code)
public class PayCodeActivity extends ABCActivity implements PullToRefreshBase.OnLastItemVisibleListener {

    @ViewInject(R.id.lv_payCode) PullToRefreshListView lv_payCode;
    @ViewInject(R.id.loadingView) LoadingView loadingView;
    @ViewInject(R.id.appBar) Toolbar toolbar;

    private List<PayCode> list = new ArrayList<PayCode>();
    private BaseAdapter<PayCode> adapter;
    private UserAction userAction;
    private int pageNum = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        initData();
        initListener();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        loadingView.loading(0);
        userAction = (UserAction) controller.getAction(this, UserAction.class);
        queryPayCodeList();
        loadingView.retry(new OnRetryListener() {
			
			@Override
			public void retry() {
				 queryPayCodeList();
			}
		});
        lv_payCode.setOnLastItemVisibleListener(this);
        lv_payCode.setAdapter(adapter = new BaseAdapter<PayCode>(this, list, R.layout.layout_pay_code_item) {

            @Override
            public void viewHandler(int position, PayCode payCode, View convertView) {
                ImageView ivPictureId = ViewHolder.get(convertView, R.id.iv_pictureId);
                TextView tvProductName = ViewHolder.get(convertView, R.id.tv_productName);
                TextView tvPeriod = ViewHolder.get(convertView, R.id.tv_period);
                FormsUtil.setTextViewTxt(tvProductName, payCode.getProductName(), null);
                FormsUtil.setTextViewTxt(tvPeriod, payCode.getPeriod(), null);
                imageLoader.displayImage(payCode.getPictureId(), ivPictureId, ImageViewUtil.getOption());
            }
        });
    }

    public void initListener() {
        lv_payCode.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                cacheBean.putCache(list.get(position - 1));
                callMe(PayCodeDetailsActivity.class);
            }
        });
    }

    public void queryPayCodeList() {
        userAction.queryPayCodeList(String.valueOf(pageNum)).start(false);
    }

    @Override
    public void onLastItemVisible() {
        ++pageNum;
        queryPayCodeList();
    }

    /**
     * 查询支付码列表成功
     *
     * @param retCode
     * @param content
     */
    public void querySuccess(RetCode retCode, List<PayCode> content) {
        loadingView.loaded();
        if (retCode != RetCode.noData && content != null && content.size() != 0) {
            list.addAll(content);
            adapter.notifyDataSetChanged();
        } else {
            if (list.size() > 0) {
                MyToast.showTEXT(this, getString(R.string.noMoreData));
            } else {
                loadingView.noData(getString(R.string.loadingNoData));
            }
        }
    }

    /**
     * 查询支付码列表失败
     *
     * @param retCode
     */
    public void queryFailed(RetCode retCode) {
        loadingView.loaded();
        if (list.size() > 0) {
            MyToast.showTEXT(this, retCode.getRetMsg());
        } else {
            loadingView.failed(retCode.getRetMsg());
        }
    }
}
