package com.abc.sz.app.view;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.drawable.PaintDrawable;
import android.media.ThumbnailUtils;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.ProductInfoAction;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.product.QShare;
import com.abc.sz.app.util.ImageViewUtil;
import com.forms.base.ABCActivity;
import com.forms.base.BaseConfig;
import com.forms.base.XDRImageLoader;
import com.forms.library.baseUtil.net.Controller;
import com.forms.library.tools.FormsUtil;
import com.forms.view.toast.MyToast;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXWebpageObject;

public class ShareUtil {

    private ABCActivity activity;
    private View atView;
    private PopupWindow pop;
    private Platform platform = null;
    private String productName;
    private String hisPrice;
    private String price;
    private String reson;
    private String imgUrl;
    private String url;
    //分享类型：1、商品  2、快报
    private String type;
    //分享内容：商品ID或快报ID
    private String attribute;
    //分享平台：1、朋友圈，2、微博
    private String category;
    private XDRImageLoader imageLoader;
    private ProductInfoAction action;
    //APP_ID 替换为你的应用从官方网站申请到的合法appId
    public static final String APP_ID = "wx1dcd29951ff3bf31";
    //IWXAPI 是第三方app和微信通信的openapi接口
    private IWXAPI api;
    //微博分享对话框
    private XdrDialog xdrDialog;
    //分享类型：1、产品，2、App
    private int shareType;

    /**
     * 构造函数
     *
     * @param atView
     * @param productName 商品名称
     * @param hisPrice    原价
     * @param price       现价
     * @param reson       推荐原因
     * @param imgUrl      商品小图url
     * @param url         商品链接url
     * @param type
     * @param attribute
     * @param imageLoader
     */
    public ShareUtil(ABCActivity activity, View atView, String productName, String hisPrice, String price,
                     String reson, String imgUrl, String url, String type, String attribute, XDRImageLoader imageLoader) {
        this.activity = activity;
        this.atView = atView;
        this.productName = productName;
        this.hisPrice = hisPrice;
        this.price = price;
        this.reson = reson;
        this.imgUrl = imgUrl;
        this.url = url;
        this.type = type;
        this.attribute = attribute;
        this.imageLoader = imageLoader;
        shareType = 1;
        initData();
    }
    
    public ShareUtil(ABCActivity activity, View atView, String productName, String url){
    	this.activity = activity;
        this.atView = atView;
        this.productName = productName;
        this.url = url;
        shareType = 2;
        initData();
    }
    
    public void initData(){
    	View view = View.inflate(activity, R.layout.layout_share_popwindow, null);
        action = (ProductInfoAction) Controller.getInstance().getAction(activity, ProductInfoAction.class);

        pop = new PopupWindow(view, FormsUtil.SCREEN_WIDTH, FormsUtil.SCREEN_HEIGHT / 6, true);
        pop.setAnimationStyle(R.style.popAnimation);
        pop.setBackgroundDrawable(new PaintDrawable());
        pop.setOutsideTouchable(true);

        //通过WXAPIFactory工厂，获取IWXAPI的实例
        api = WXAPIFactory.createWXAPI(activity, APP_ID, false);
        api.registerApp(APP_ID);
        ShareSDK.initSDK(activity);

        ImageView ivWechat = (ImageView) view.findViewById(R.id.iv_select_1);
        ImageView ivWechatmoments = (ImageView) view.findViewById(R.id.iv_select_2);
        ImageView ivSinaweibo = (ImageView) view.findViewById(R.id.iv_select_3);
        ivWechat.setOnClickListener(new MyOnClickListener());
        ivWechatmoments.setOnClickListener(new MyOnClickListener());
        ivSinaweibo.setOnClickListener(new MyOnClickListener());
    }

    public PopupWindow showShareWindow() {
        pop.showAtLocation(atView, Gravity.BOTTOM, 0, 0);
        return pop;
    }

    public void closeWindow() {
        if (pop.isShowing())
            pop.dismiss();
    }

    public class MyOnClickListener implements OnClickListener {

        @SuppressWarnings("unused")
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.iv_select_1:
                    category = "1";
                    sendWebpage(false);
                    break;
                case R.id.iv_select_2:
                    category = "1";
                    sendWebpage(true);
                    break;
                case R.id.iv_select_3:
                    category = "2";
                    if(shareType == 1){
	                    View dialogView = LayoutInflater.from(activity).inflate(R.layout.layout_dialog_share, null);
	                    final AlertDialog dialog = new AlertDialog.Builder(activity).create();
	                    dialog.setView(dialogView, 0, 0, 0, 0);
	                    dialog.setInverseBackgroundForced(true);
	                    dialog.show();
	                    final TextView tvProductName = (TextView) dialogView.findViewById(R.id.tv_productName);
	                    final TextView tvHisPrice = (TextView) dialogView.findViewById(R.id.tv_hisPrice);
	                    final TextView tvPrice = (TextView) dialogView.findViewById(R.id.tv_price);
	                    final LinearLayout llReason = (LinearLayout) dialogView.findViewById(R.id.ll_reason);
	                    final TextView tvReason = (TextView) dialogView.findViewById(R.id.tv_reason);
	                    final ImageView ivImgUrl = (ImageView) dialogView.findViewById(R.id.iv_imgUrl);
	                    final EditText etEvaluate = (EditText) dialogView.findViewById(R.id.et_evaluate);
	                    Button btnShare = (Button) dialogView.findViewById(R.id.btn_share);
	                    Button btnCancel = (Button) dialogView.findViewById(R.id.btn_cancel);
	                    tvHisPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
	                    imageLoader.displayImage(imgUrl, ivImgUrl, ImageViewUtil.getOption());
	                    btnShare.setOnClickListener(new OnClickListener() {
	
	                        @Override
	                        public void onClick(View v) {
	                            shareSinaWeibo(etEvaluate.getText().toString());
	                            dialog.dismiss();
	                        }
	                    });
	                    btnCancel.setOnClickListener(new OnClickListener() {
	
	                        @Override
	                        public void onClick(View v) {
	                            dialog.dismiss();
	                        }
	                    });
                    }else if(shareType == 2){
                    	shareSinaWeibo(null);
                    }
                    break;
            }
        }
    }

    /**
     * 微信分享
     *
     * @param flag
     */
    private void sendWebpage(boolean flag) {
        WXWebpageObject webpage = new WXWebpageObject();
        webpage.webpageUrl = url;
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.title = productName;
        
        Bitmap thumb = null;
        if(shareType == 1){
	        StringBuffer sb = new StringBuffer();
	        sb.append(productName + ",原价：" + hisPrice + ",现价：" + price);
	        msg.description = sb.toString();
	        if(imgUrl != null && !"".equals(imgUrl)){
	        	thumb = imageLoader.getImageLoader().loadImageSync(BaseConfig.PICTURE_URL + imgUrl, 
	            		ImageViewUtil.getOption());
	        	if(thumb != null){
	        		thumb = ThumbnailUtils.extractThumbnail(thumb, 72, 72);
	        	}else{
	        		thumb = BitmapFactory.decodeResource(activity.getResources(), R.drawable.ic_launcher);
	        	}
	        }else{
	        	thumb = BitmapFactory.decodeResource(activity.getResources(), R.drawable.ic_launcher);
	        }
        }else if(shareType == 2){
        	thumb = BitmapFactory.decodeResource(activity.getResources(), R.drawable.ic_launcher);
        }
        msg.thumbData = bmpToByteArray(thumb, true);

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = msg;
        req.scene = flag ? SendMessageToWX.Req.WXSceneTimeline : SendMessageToWX.Req.WXSceneSession;
        api.sendReq(req);
    }

    private String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
    }

    private byte[] bmpToByteArray(final Bitmap bmp, final boolean needRecycle) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        bmp.compress(CompressFormat.PNG, 100, output);
        if (needRecycle) {
            bmp.recycle();
        }

        byte[] result = output.toByteArray();
        try {
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 新浪微博分享
     *
     * @param evaluate
     */
    public void shareSinaWeibo(String evaluate) {
        platform = ShareSDK.getPlatform("SinaWeibo");
        Platform.ShareParams sp = new Platform.ShareParams();
        if(shareType == 1){
	        StringBuffer sb = new StringBuffer();
	        sb.append(productName + ",原价：" + hisPrice + ",现价：" + price);
	        if (evaluate != null) {
	            sb.append(evaluate);
	        }
	        sb.append("商品链接:" + url);
	        sp.setText(sb.toString());
	        if (imgUrl != null && !"".equals(imgUrl)) {
	            sp.setImageUrl(BaseConfig.PICTURE_URL + imgUrl);
	        }
        }else if(shareType == 2){
        	sp.setText("App链接:" + url);
        }
        // 设置分享事件回调
        platform.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(Platform platform, int action,
                                   HashMap<String, Object> res) {
                Message msg = new Message();
                msg.arg1 = 1;
                msg.arg2 = action;
                msg.obj = platform;
                handler_share.sendMessage(msg);
            }

            @Override
            public void onError(Platform platform, int action, Throwable t) {
                Message msg = new Message();
                msg.arg1 = 2;
                msg.arg2 = action;
                msg.obj = platform;
                handler_share.sendMessage(msg);
            }

            @Override
            public void onCancel(Platform platform, int action) {
                Message msg = new Message();
                msg.arg1 = 3;
                msg.arg2 = action;
                msg.obj = platform;
                handler_share.sendMessage(msg);
            }
        });
        platform.share(sp);
        
        xdrDialog = new XdrDialog(activity);
		xdrDialog.setCancelable(true);
		xdrDialog.show();
    }

    /*
     *
     * 分享事件回调
     */
    final Handler handler_share = new Handler() {
        public void handleMessage(Message msg) {
        	xdrDialog.dismiss();
            switch (msg.arg1) {
                case 1:
                    MyToast.show(activity, "分享成功!", MyToast.TEXT, Gravity.CENTER, Toast.LENGTH_SHORT);
                    if (activity.getBaseApp().getLoginUser() != null) {
                        shareRequest();
                    }
                    break;
                case 2:
                    MyToast.show(activity, "分享失败!", MyToast.TEXT, Gravity.CENTER, Toast.LENGTH_SHORT);
                    break;
                case 3:
                    MyToast.show(activity, "分享取消!", MyToast.TEXT, Gravity.CENTER, Toast.LENGTH_SHORT);
                    break;
            }
        }
    };

    public void shareRequest() {
        QShare qShare = new QShare(type, attribute, category);
        RequestBean<QShare> params = new RequestBean<QShare>(action.getAccessToken(), qShare);
        action.loadingShare(params).start(false);
    }
    
    public void closeInputMethod(View view){
		InputMethodManager imm = (InputMethodManager)activity
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

}
