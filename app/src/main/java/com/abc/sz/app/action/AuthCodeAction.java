package com.abc.sz.app.action;

import com.abc.sz.app.activity.personal.BindNewCardActivity;
import com.abc.sz.app.http.bean.personal.RUserCard;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.abc.sz.app.util.DialogUtil;
import com.alibaba.fastjson.TypeReference;
import com.forms.library.base.BaseActivity;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;

import java.util.List;

/**
 * Created by hwt on 14/11/25.
 */
public class AuthCodeAction extends BaseAction {

    public Http getMessageCode() {
        return APPRequestPhase2.getMessageCode(getAccessToken(),
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        DialogUtil.show(((BaseActivity) uiObject), retCode.getRetMsg());
                    }

                    @Override
                    public void onFailed(RetCode retCode) {

                    }
                });
    }

    public Http checkCode(String authCode) {
        return APPRequestPhase2.verifyMessageCode(getAccessToken(),
                authCode,
                new ResponseNotify<List<RUserCard>>(new TypeReference<RespBean<List<RUserCard>>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<List<RUserCard>> response) {
                        if (uiObject instanceof BindNewCardActivity) {
//                                ((BindNewCardActivity)uiObject).checkAucthCode(retCode);
                        }
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        DialogUtil.show(((BaseActivity) uiObject), retCode.getRetMsg());
                    }
                });
    }
}
