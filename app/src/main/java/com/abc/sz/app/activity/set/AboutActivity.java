package com.abc.sz.app.activity.set;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * 关于
 *
 * @author ftl
 */
@ContentView(R.layout.activity_about)
public class AboutActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.tv_appVersion)
    private TextView tv_appVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initView();
    }

    /**
     * 初始化界面
     */
    private void initView() {
        PackageInfo packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = packageInfo.versionName;
            tv_appVersion.setText(getResources().getString(R.string.grzx_app_version) + version);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
    }

}
