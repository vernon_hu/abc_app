package com.abc.sz.app.bean_phase2;

import java.util.List;

/**
 * 存贷通收益
 * 
 * @author ftl
 */
public class DLIncome {
	
	private String loanAccount; //贷款账号
	private String queryDate; //查询期间
	private String queryDateTotalRevenue; //查询期间合计返还总金额
	private List<DLIncomeDetail> returnIncomeList;//每期返回收益列表
	
	public String getLoanAccount() {
		return loanAccount;
	}
	public void setLoanAccount(String loanAccount) {
		this.loanAccount = loanAccount;
	}
	public String getQueryDate() {
		return queryDate;
	}
	public void setQueryDate(String queryDate) {
		this.queryDate = queryDate;
	}
	public String getQueryDateTotalRevenue() {
		return queryDateTotalRevenue;
	}
	public void setQueryDateTotalRevenue(String queryDateTotalRevenue) {
		this.queryDateTotalRevenue = queryDateTotalRevenue;
	}
	public List<DLIncomeDetail> getReturnIncomeList() {
		return returnIncomeList;
	}
	public void setReturnIncomeList(List<DLIncomeDetail> returnIncomeList) {
		this.returnIncomeList = returnIncomeList;
	}
	
}
