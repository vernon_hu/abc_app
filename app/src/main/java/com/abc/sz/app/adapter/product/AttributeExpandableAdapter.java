package com.abc.sz.app.adapter.product;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.product.AttributeValueBean;
import com.abc.sz.app.bean.product.TypeAttributeBean;
import com.forms.library.tools.ViewHolder;

import java.util.List;

/**
 * 属性列表
 *
 * @author llc
 */
public class AttributeExpandableAdapter extends BaseExpandableListAdapter {

    private List<TypeAttributeBean> mList;

    public AttributeExpandableAdapter(List<TypeAttributeBean> list) {
        mList = list;
    }

    @Override
    public int getGroupCount() {
        // TODO Auto-generated method stub
        return mList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        // TODO Auto-generated method stub
        return mList.get(groupPosition).getList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        // TODO Auto-generated method stub
        return mList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return mList.get(groupPosition).getList().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        // TODO Auto-generated method stub
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        TypeAttributeBean bean = mList.get(groupPosition);
        // TODO Auto-generated method stub
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_attribute, null);
        }
        TextView tv_name = ViewHolder.get(convertView, R.id.tv_name);
//		TextView tv_value = ViewHolder.get(convertView, R.id.tv_value);
        tv_name.setText(bean.getAttributeName());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        AttributeValueBean bean = mList.get(groupPosition).getList().get(childPosition);
        // TODO Auto-generated method stub
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_attribute_value, null);
        }
        TextView tv_name = ViewHolder.get(convertView, R.id.tv_name);
        tv_name.setText(bean.getValue());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return true;
    }


    public void setmList(List<TypeAttributeBean> mList) {
        this.mList = mList;
    }


}
