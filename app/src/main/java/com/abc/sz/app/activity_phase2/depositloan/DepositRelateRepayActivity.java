package com.abc.sz.app.activity_phase2.depositloan;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.DepositloanAction;
import com.abc.sz.app.bean.loan.Account;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

/**
 * 关联还款页面
 * 
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_depositloanrepay)
public class DepositRelateRepayActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	@ViewInject(R.id.tvRepayAccount)
	private TextView tvRepayAccount;
	@ViewInject(R.id.lvAccount)
	private ListView lvAccount;
	@ViewInject(R.id.loadingView)
	private LoadingView loadingView;

	private BaseAdapter<Account> adapter;
	private List<Account> accountList = new ArrayList<Account>();
	private DepositloanAction depositloanAction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		FormsUtil.setTextViewTxt(tvRepayAccount, cacheBean.getStringCache("repayAccount"));
		depositloanAction = (DepositloanAction) controller.getAction(this, DepositloanAction.class);
		depositloanAction.queryDepositAccountList("").setLoadingView(loadingView).start();
		adapter = new BaseAdapter<Account>(this, accountList, R.layout.layout_phase2_dlrepayaccount_item) {

			@Override
			public void viewHandler(final int position, final Account t, View convertView) {
				LinearLayout lltItem = ViewHolder.get(convertView, R.id.lltItem);
				ImageView ivSelect = ViewHolder.get(convertView, R.id.ivSelect);
				TextView tvAccount = ViewHolder.get(convertView, R.id.tvAccount);			
				
				if (t != null) {
					if (t.isSelect()) {
						ivSelect.setImageResource(R.drawable.shopping_checkbox_select);
					} else {
						ivSelect.setImageResource(R.drawable.shopping_checkbox_default);
					}
					tvAccount.setText(t.getAccount());
					lltItem.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							if (t.isSelect()) {
								t.setSelect(false);
							}else{
								t.setSelect(true);
							}
							notifyDataSetChanged();
						}
					});
				}
			}
		};
		lvAccount.setAdapter(adapter);
	}

	@OnClick({ R.id.btnNext })
	public void calculate(View v) {
		switch (v.getId()) {
		case R.id.btnNext:
			List<Account> repayList = new ArrayList<Account>();
			for (int i = 0; i < accountList.size(); i++) {
				if(accountList.get(i).isSelect()){
					repayList.add(accountList.get(i));
				}
			}
			if(repayList.size() == 0){
				Toast.makeText(this, "请选择关联的存款账号", Toast.LENGTH_SHORT).show();
			}else{
				View view = LayoutInflater.from(this).inflate(R.layout.layout_dialog_relate_repay, null);
				TextView tvRepayAccount = (TextView) view.findViewById(R.id.tvRepayAccount);
				final EditText etPassword = (EditText) view.findViewById(R.id.etPassword);
				FormsUtil.setTextViewTxt(tvRepayAccount, cacheBean.getStringCache("repayAccount"));
				AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT);
				builder.setTitle("提示");
			    builder.setView(view);
			    builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						String passwd = etPassword.getText().toString();
						if("".equals(passwd)){
							return;
						}
						depositloanAction.verifyPasswd("", passwd);
					}
				});
		        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
					}
				});
		        builder.show();
			}
			break;
		}
	}
	
	/**
	 * 查询存款账号列表成功回调
	 */
	public void querySuccess(List<Account> list){
		 if (list != null && list.size() > 0) {
			accountList.clear();
			accountList.addAll(list);
            adapter.notifyDataSetChanged();
	     }
	}
	
	/**
	 * 还款账号交易密码校验成功回调
	 */
	public void verifyPasswdSuccess(){
		//进入还款账号验证码校验页面
		callMe(DepositLoanVerifyCodeActivity.class);
	}
}
