package com.abc.sz.app.http.bean.personal;

/**
 * 修改手机号码
 *
 * @author ftl
 */
public class QUpdatePhone {
    private String newphone;
    private String checkCode;

    public QUpdatePhone(String newphone, String checkCode) {
        super();
        this.newphone = newphone;
        this.checkCode = checkCode;
    }

    public String getNewphone() {
        return newphone;
    }

    public void setNewphone(String newphone) {
        this.newphone = newphone;
    }

    public String getCheckCode() {
        return checkCode;
    }

    public void setCheckCode(String checkCode) {
        this.checkCode = checkCode;
    }

}
