package com.abc.sz.app.http.bean.pay;

public class RPay {
	
    private String ReturnCode;//总行交易返回码
    private String ErrorMessage;//总行交易返回描述
    private String PaymentyURL;//支付页面地址
    
	public String getReturnCode() {
		return ReturnCode;
	}
	public void setReturnCode(String returnCode) {
		ReturnCode = returnCode;
	}
	public String getErrorMessage() {
		return ErrorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}
	public String getPaymentyURL() {
		return PaymentyURL;
	}
	public void setPaymentyURL(String paymentyURL) {
		PaymentyURL = paymentyURL;
	}

}
