package com.abc.sz.app.activity_phase2.depositloan;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.DepositloanAction;
import com.abc.sz.app.bean_phase2.DLIncome;
import com.abc.sz.app.bean_phase2.DLIncomeDetail;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;
import com.forms.view.noscoller.MyListView;

/**
 * 存贷通收益返还明细页面
 * 
 * @author hkj
 */
@ContentView(R.layout.activity_phase2_depositloanincomedetail)
public class DepositLoanIncomeDetailActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	@ViewInject(R.id.svMain)
	private ScrollView svMain;
	@ViewInject(R.id.tvLoanAccount)
	private TextView tvLoanAccount;
	@ViewInject(R.id.tvQueryDate)
	private TextView tvQueryDate;
	@ViewInject(R.id.tvRefundSum)
	private TextView tvRefundSum;
	@ViewInject(R.id.mlvDetail)
	private MyListView mlvDetail;
	@ViewInject(R.id.loadingView)
	private LoadingView loadingView;
	
	private BaseAdapter<DLIncomeDetail> adapter;
	private List<DLIncomeDetail> list = new ArrayList<DLIncomeDetail>();
	private DepositloanAction depositloanAction;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		depositloanAction = (DepositloanAction) controller.getAction(this, DepositloanAction.class);
		String accountNum = cacheBean.getStringCache("account");
		String startTime = cacheBean.getStringCache("startTime");
		String endTime = cacheBean.getStringCache("endTime");
		depositloanAction.queryAccountDetail(accountNum, startTime, endTime).setLoadingView(loadingView).start();
		adapter = new BaseAdapter<DLIncomeDetail>(this, list, R.layout.layout_phase2_dlincomedetail_item) {

			@Override
			public void viewHandler(final int position, DLIncomeDetail t, View convertView) {
				TextView tvDate = ViewHolder.get(convertView, R.id.tvDate);
				TextView tvSum = ViewHolder.get(convertView, R.id.tvSum);
				TextView tvDetail = ViewHolder.get(convertView, R.id.tvDetail);
				
				if (t != null) {
					tvDate.setText(t.getReturnDate());
					tvSum.setText(t.getReturnMoney());
					tvDetail.setText(t.getReturnDepositAccountDetail());
				}
			}
		};
		mlvDetail.setAdapter(adapter);
		svMain.smoothScrollTo(0, 0);
	}
	
	/**
	 * 存贷通收益返还明细查询成功回调
	 */
	public void querySuccess(DLIncome result){
		 if (result != null) {
			FormsUtil.setTextViewTxt(tvLoanAccount, result.getLoanAccount());
			FormsUtil.setTextViewTxt(tvQueryDate, result.getQueryDate());
			FormsUtil.setTextViewTxt(tvRefundSum, result.getQueryDateTotalRevenue());
			list = result.getReturnIncomeList();
            adapter.notifyDataSetChanged();
	     }
	}

}
