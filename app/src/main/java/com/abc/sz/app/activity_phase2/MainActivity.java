package com.abc.sz.app.activity_phase2;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity.order.ShoppingCarActivity;
import com.abc.sz.app.activity.product.ProductSearchActivity;
import com.abc.sz.app.activity.product.ProductTypeActivity;
import com.abc.sz.app.activity_phase2.lightpay.LockActivity;
import com.abc.sz.app.fragment_phase2.CreditCardFragment;
import com.abc.sz.app.fragment_phase2.FinanceFragment;
import com.abc.sz.app.fragment_phase2.MainDrawerFragment;
import com.abc.sz.app.fragment_phase2.MarketFragment;
import com.abc.sz.app.util.DialogUtil;
import com.abc.sz.app.view.TabButton;
import com.abc.sz.app.view.TabLayout;
import com.forms.base.ABCActivity;
import com.forms.base.ABCFragment;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.view.zxing.CaptureActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hwt on 4/13/15.
 */
@ContentView(R.layout.activity_phase2_main)
public class MainActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.drawerLayout) DrawerLayout drawerLayout;
    @ViewInject(R.id.containerLayout) FrameLayout containerLayout;
    @ViewInject(R.id.tabFinance) TabButton tabFinance;
    @ViewInject(R.id.tabMarket) TabButton tabMarket;
    @ViewInject(R.id.tabSuggest) TabButton tabSuggest;
    @ViewInject(R.id.tabLifeAssistant) TabButton tabLifeAssistant;

    // 选项卡id
    private int[] buttonId = {
            R.id.tabFinance,
            R.id.tabMarket,
            R.id.tabSuggest,
            R.id.tabLifeAssistant};

    private TabLayout tabLayout;
    private int currentIndex;

    private FragmentManager fragmentManager;
    private List<ABCFragment> fragmentList = new ArrayList<>();
    private MenuItem moreMenu;
    private View searchView;
    private MenuItem shopcarMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.MATCH_PARENT,
                    Gravity.CENTER
            );
            View view = LayoutInflater.from(this).inflate(R.layout.layout_main_searchview, null);
            ImageView ivProductSort = (ImageView) view.findViewById(R.id.ivProductSort);
            LinearLayout llSearch = (LinearLayout) view.findViewById(R.id.llSearch);
            getSupportActionBar().setCustomView(view, params);
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

            searchView = getSupportActionBar().getCustomView();
            ivProductSort.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    callMe(ProductTypeActivity.class);
                }
            });
            llSearch.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    callMe(ProductSearchActivity.class);
                }
            });
            searchView.setVisibility(View.GONE);
        }
        
        /* 初始tab项 */
        currentIndex = 0;
        tabLayout = new TabLayout(R.color.bottom_bg, R.color.bottom_bg)
                .addBtn(tabFinance, tabMarket, tabSuggest, tabLifeAssistant);
        tabFinance.init(R.drawable.icon_finance_default, R.drawable.icon_finance_select, getString(R.string.finance), false, this);
        tabMarket.init(R.drawable.icon_market_default, R.drawable.icon_market_select, getString(R.string.market), true, this);
        tabSuggest.init(R.drawable.icon_creditcard_default, R.drawable.icon_creditcard_select, getString(R.string.creditAbout), false, this);
        tabLifeAssistant.init(R.drawable.icon_my_default, R.drawable.icon_my_select, getString(R.string.myInfo), false, this);
        tabLayout.selectBtn(buttonId[currentIndex]);
        toolbar.setTitle(getFragmentTitle());

        fragmentList.add(new FinanceFragment());
        fragmentList.add(new MarketFragment());
        fragmentList.add(new CreditCardFragment());
        fragmentList.add(new MainDrawerFragment());

        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        for (int i = 0; i < fragmentList.size(); i++) {
            transaction.add(R.id.containerLayout,
                    fragmentList.get(i),
                    fragmentList.get(i).getClass().getSimpleName());
        }
        transaction.commit();
        showFragment(currentIndex);
    }

    @OnClick(value = {R.id.tabFinance, R.id.tabMarket, R.id.tabSuggest, R.id.tabLifeAssistant})
    public void btnOnClick(View view) {
        switch (view.getId()) {
            case R.id.tabFinance:
                currentIndex = 0;
                toolbar.setTitle(R.string.finance);
                break;
            case R.id.tabMarket:
                currentIndex = 1;
                toolbar.setTitle("");
                break;
            case R.id.tabSuggest:
                currentIndex = 2;
                toolbar.setTitle(R.string.creditAbout);
                break;
            case R.id.tabLifeAssistant:
                currentIndex = 3;
                toolbar.setTitle("我的");
                break;
        }
        tabLayout.selectBtn(view.getId());
        if (currentIndex == 1) {
            shopcarMenu.setVisible(true);
            moreMenu.setVisible(false);
            searchView.setVisibility(View.VISIBLE);
        } else if (currentIndex == 3) {
            shopcarMenu.setVisible(false);
            moreMenu.setVisible(false);
            searchView.setVisibility(View.GONE);
        } else {
            shopcarMenu.setVisible(false);
            moreMenu.setVisible(true);
            searchView.setVisibility(View.GONE);
        }
        // 展示对应页面
        showFragment(currentIndex);
    }

    private void showFragment(int currentIndex) {
        for (int i = 0; i < fragmentList.size(); i++) {
            ABCFragment fragment = fragmentList.get(i);
            if (!fragment.isHidden()) {
                fragmentManager.beginTransaction().hide(fragment).commit();
            }
        }
        ABCFragment fragment = fragmentList.get(currentIndex);
        fragmentManager.beginTransaction().show(fragment).commit();
        if (fragment.getClass().isAssignableFrom(MainDrawerFragment.class)) {
            toolbar.setVisibility(View.GONE);
        } else {
            toolbar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        DialogUtil.showWithTwoBtnAndTitle(this, "提示", "确定要退出应用吗？", "确定", "取消",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getBaseApp().exit();
                    }
                }, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }

    public String getFragmentTitle() {
        return tabLayout.getSelectedButton().getTabName();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Only show items in the action bar relevant to this screen
        // if the drawer is not showing. Otherwise, let the drawer
        // decide what to show in the action bar.
        getMenuInflater().inflate(R.menu.activity_main_menu, menu);
        shopcarMenu = menu.findItem(R.id.shopCar);
        moreMenu = menu.findItem(R.id.more);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.lightPay) {
            callMe(LockActivity.class);
        } else if (id == R.id.qrCode) {
            callMe(CaptureActivity.class);
        } else if (id == R.id.shopCar) {
            if (isLogin()) {
                callMe(ShoppingCarActivity.class);
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
