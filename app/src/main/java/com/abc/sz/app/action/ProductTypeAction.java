package com.abc.sz.app.action;

import java.util.List;

import com.abc.sz.app.activity.product.ProductTypeActivity;
import com.abc.sz.app.bean.product.ProductListBean;
import com.abc.sz.app.fragment_phase2.MarketFragment;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.product.QProductType;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.alibaba.fastjson.TypeReference;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;

/**
 * 商品分类查询
 *
 * @author llc
 */
public class ProductTypeAction extends BaseAction {

    //商品分类列表请求
    public Http loadingProductType(RequestBean<QProductType> params) {
        return APPRequestPhase2.productTypeQuery(params,
                new ResponseNotify<List<ProductListBean>>(new TypeReference<RespBean<List<ProductListBean>>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<List<ProductListBean>> response) {
                        List<ProductListBean> list = response.getContent();
                        if(uiObject instanceof MarketFragment){
                        	((MarketFragment) uiObject).typeListSuccess(list);
                        }else if(uiObject instanceof ProductTypeActivity){
                        	((ProductTypeActivity) uiObject).typeListSuccess(list);
                        }
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                    	if(uiObject instanceof ProductTypeActivity){
                    		((ProductTypeActivity) uiObject).failed(retCode);
                    	}
                    }
                });
    }

}
