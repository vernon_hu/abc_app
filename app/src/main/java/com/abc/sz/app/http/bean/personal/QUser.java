package com.abc.sz.app.http.bean.personal;

/**
 * Created by hwt on 14/11/18.
 */
public class QUser {
    private String oldPassword;
    private String newPassword;

    public QUser(String oldPassword, String newPassword) {
        super();
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

}
