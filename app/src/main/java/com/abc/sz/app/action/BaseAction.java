package com.abc.sz.app.action;

import com.forms.base.ABCApplication;
import com.forms.base.LoginUser;
import com.forms.library.base.BaseApplication;
import com.forms.library.baseUtil.asynchttpclient.RequestParams;
import com.forms.library.baseUtil.logger.Logger;
import com.forms.library.baseUtil.net.Action;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.UiObject;

/**
 * Created by hwt on 14/11/5.
 */
public class BaseAction extends Action {

    private String accessToken;

    @Override
    public Action init(UiObject uiObject, BaseApplication baseApplication) {
//    	ABCApplication abcApplication = (ABCApplication) baseApplication;
//    	LoginUser loginUser = abcApplication.getLoginUser();
//      if (loginUser != null) {
//          accessToken = loginUser.getAccessToken();
//      }
//      Logger.d("login access token vlaue:"+accessToken);
    	return super.init(uiObject, baseApplication);
    }
    
    public String getAccessToken() {  
    	LoginUser loginUser = getApplication().getLoginUser();
    	if (loginUser != null) {
            accessToken = loginUser.getAccessToken();
        }
    	Logger.d("login access token vlaue:"+accessToken);
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public ABCApplication getApplication() {
        return (ABCApplication) application;
    }

    @Override
    public Http excute(String requestUrl, RequestParams params) {
        return null;
    }
}
