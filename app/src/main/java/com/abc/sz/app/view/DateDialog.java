package com.abc.sz.app.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import com.abc.ABC_SZ_APP.R;
import com.forms.library.baseUtil.log.LogUtils;
import hirondelle.date4j.DateTime;

public class DateDialog extends Dialog implements View.OnClickListener {

    private DatePicker payDateDp;
    private boolean isBegin;
    private DateTime dateTime;

    private Button surePayDateBtn;
    private Button cancelPayDateBtn;
    private View.OnClickListener sureClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_selectdate_dialog);

        surePayDateBtn = (Button) findViewById(R.id.bt_dialogdate_sure);
        cancelPayDateBtn = (Button) findViewById(R.id.bt_dialogdate_cancel);
        payDateDp = (DatePicker) findViewById(R.id.dp_dialogdate_paydate);

        cancelPayDateBtn.setOnClickListener(this);
        surePayDateBtn.setOnClickListener(sureClickListener);
        if (dateTime != null) {
            payDateDp.init(dateTime.getYear(),
                    dateTime.getMonth() - 1,
                    dateTime.getDay(),
                    new DatePicker.OnDateChangedListener() {
                        @Override
                        public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            dateTime = new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0, 0, 0);
                            LogUtils.d("selected date :" + dateTime.format("YYYY-MM-DD"));
                        }
                    });
        }
    }

    public DateDialog(Context context, DateTime dateTime, View.OnClickListener sureClickListener) {
        super(context, R.style.MyDialog);
        this.dateTime = dateTime;
        this.sureClickListener = sureClickListener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_dialogdate_cancel:
                dismiss();
                break;
        }
    }

    /**
     * @return
     */
    public String getDateStr() {
        if (dateTime != null) {
            return dateTime.format("YYYY-MM-DD");
        }
        return null;
    }

    /**
     * @return
     */
    public DateTime getDateTime() {
        return dateTime;
    }
}
