package com.abc.sz.app.action;

import com.abc.sz.app.activity.life.FsActivity;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.jf.JfBean;
import com.abc.sz.app.http.bean.jf.QJfBean;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.alibaba.fastjson.TypeReference;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;


/**
 * 缴费
 *
 * @author llc
 */
public class JfAction extends BaseAction {

    //缴费
    public Http startJf(QJfBean bean) {
        RequestBean<QJfBean> params = new RequestBean<QJfBean>(bean);

        return APPRequestPhase2.jfQuery(params, new ResponseNotify<JfBean>(new TypeReference<RespBean<JfBean>>() {
        }) {

            @Override
            public void onResponse(RetCode retCode, RespBean<JfBean> response) {
                JfBean bean = response.getContent();
                ((FsActivity) uiObject).success(bean);
            }

            @Override
            public void onFailed(RetCode retCode) {
                ((FsActivity) uiObject).failed(retCode, "startJf");
            }

        });
    }
}
