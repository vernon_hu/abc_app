package com.abc.sz.app.http.bean.card;

/**
 * 家装分期贷款额度
 * 
 * @author ftl
 */
public class QHouseLoanCommitment {

    private String district;//区域
    private String area;//装修面积
    private String phone;//手机号码
    private String checkCode;//验证码
    private String agreedToCall;//是否愿意客户经理联系
    
    public QHouseLoanCommitment(){}
    
	public QHouseLoanCommitment(String district, String area, String phone,
			String checkCode, String agreedToCall) {
		super();
		this.district = district;
		this.area = area;
		this.phone = phone;
		this.checkCode = checkCode;
		this.agreedToCall = agreedToCall;
	}



	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	public String getAgreedToCall() {
		return agreedToCall;
	}

	public void setAgreedToCall(String agreedToCall) {
		this.agreedToCall = agreedToCall;
	}
	
}
