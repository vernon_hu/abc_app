package com.abc.sz.app.activity_phase2.creditcard;

import java.util.ArrayList;
import java.util.List;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.CreditcardAction;
import com.abc.sz.app.bean.cridetcard.Consumer;
import com.abc.sz.app.bean.cridetcard.ConsumerList;
import com.abc.sz.app.util.DialogUtil;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.library.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

/**
 * 消费分期页面
 * 
 * @author hkj
 */
@ContentView(R.layout.activity_phase2_consumestaging)
public class ConsumeStagingActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	@ViewInject(R.id.tvAccount)
	private TextView tvAccount;
	@ViewInject(R.id.lvStaging)
	private ListView lvStaging;
	@ViewInject(R.id.loadingView) 
	private LoadingView loadingView;
	
	private BaseAdapter<Consumer> adapter;
	private List<ConsumerList> consumerlistList = new ArrayList<ConsumerList>();
	private List<Consumer> consumerList = new ArrayList<Consumer>();
	private String[] CARDS = null;
	private CreditcardAction creditcardAction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		creditcardAction = (CreditcardAction) controller.getAction(this, CreditcardAction.class);
		creditcardAction.queryConsumerInstallment().setLoadingView(loadingView).start();
		adapter = new BaseAdapter<Consumer>(this, consumerList, R.layout.layout_phase2_consumestaging_item) {

			@Override
			public void viewHandler(int position, final Consumer t, View convertView) {
				RelativeLayout rltItem = ViewHolder.get(convertView, R.id.rltItem);
				TextView tvMonth = ViewHolder.get(convertView, R.id.tvMonth);
				TextView tvSum = ViewHolder.get(convertView, R.id.tvSum);
				TextView tvLine = ViewHolder.get(convertView, R.id.tvLine);
				if(t != null){
					FormsUtil.setTextViewTxt(tvMonth, t.getConsumerDate());
					FormsUtil.setTextViewTxt(tvSum, t.getConsumerMonery());
					rltItem.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							cacheBean.put("consumerID", t.getConsumerID());
							callMe(ConsumeApplyActivity.class);
						}
					});
				}
				if (position == (consumerList.size() - 1)) {
					tvLine.setVisibility(View.GONE);
				} else {
					tvLine.setVisibility(View.VISIBLE);
				}
			}
			
		};
		lvStaging.setAdapter(adapter);
	}

	@OnClick({ R.id.tvAccount }) 
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tvAccount:
			showListDlg();
			break;
		}
	}
	
	private void showListDlg() {
		DialogUtil.showToSelect(this, "选择", CARDS, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				tvAccount.setText(CARDS[which]);
			}
			
		});
	}
	
	/**
     *消费分期查询成功回调
     */
    public void querySuccess(List<ConsumerList> list) {
    	if(list != null && list.size() > 0){
    		consumerlistList.clear();
    		consumerlistList.addAll(list);
    		consumerList = consumerlistList.get(0).getConsumerList();
    		adapter.notifyDataSetChanged();
    		CARDS = new String[consumerlistList.size()];
    		for (int i = 0; i < consumerlistList.size(); i++) {
    			CARDS[i] = consumerlistList.get(i).getCardNumber();
			}
    	}
    }
	
}
