package com.abc.sz.app.http.bean.order;
/**
 * 查询订单状态
 * @author ftl
 *
 */
public class QQueryOrder {

	private String PayTypeID;
	private String OrderNo;
	private String QueryDetail;
	
	public QQueryOrder(String payTypeID, String orderNo, String queryDetail) {
		super();
		PayTypeID = payTypeID;
		OrderNo = orderNo;
		QueryDetail = queryDetail;
	}
	public String getPayTypeID() {
		return PayTypeID;
	}
	public void setPayTypeID(String payTypeID) {
		PayTypeID = payTypeID;
	}
	public String getOrderNo() {
		return OrderNo;
	}
	public void setOrderNo(String orderNo) {
		OrderNo = orderNo;
	}
	public String getQueryDetail() {
		return QueryDetail;
	}
	public void setQueryDetail(String queryDetail) {
		QueryDetail = queryDetail;
	}
	
	
}
