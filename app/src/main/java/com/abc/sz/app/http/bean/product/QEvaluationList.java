package com.abc.sz.app.http.bean.product;


/**
 * 请求评论列表
 *
 * @author llc
 */
public class QEvaluationList {

    //当前页码
    private String PageNum;
    //商品id
    private String ProductId;
    //评价类别 0-全部 1-好评 2-中评 3-差评
    private String Type;

    public String getPageNum() {
        return PageNum;
    }

    public void setPageNum(String pageNum) {
        PageNum = pageNum;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

}
