package com.abc.sz.app.activity_phase2.creditcard;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * 账单分期页面
 * 
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_billstaging)
public class BillStagingActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	@ViewInject(R.id.webView)
	private WebView webView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		init();
	}
	
	public void init(){
		
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_shark_menu, menu);
        return true;
    }

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.shark:
			callMe(BillStagingShakeActivity.class);
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
}
