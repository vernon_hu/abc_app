package com.abc.sz.app.http.bean.order;

import com.abc.sz.app.bean.Evaluate;

import java.util.List;

/**
 * 评价
 *
 * @author ftl
 */
public class QEvaluateList {

    /* 订单编号 */
    private String orderId;
    /* 评价列表*/
    private List<Evaluate> list;

    public QEvaluateList(String orderId, List<Evaluate> list) {
        super();
        this.orderId = orderId;
        this.list = list;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<Evaluate> getList() {
        return list;
    }

    public void setList(List<Evaluate> list) {
        this.list = list;
    }


}
