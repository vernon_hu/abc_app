package com.abc.sz.app.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 输入校验
 * 
 * @author ftl
 *
 */
public class InputVerifyUtil {
	
	/**
	 * 是否是邮箱
	 * 
	 * @param email
	 * @return
	 */
	public static boolean isEmail(String email) {
		if (email != null && !"".equals(email)) {
			Pattern p = Pattern.compile("^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$");
			Matcher m = p.matcher(email);
			return m.matches();
		} else {
			return false;
		}
	}

	/**
	 * 是否是手机号
	 * 
	 * @param mobile
	 * @return
	 */
	public static boolean isMobileNo(String mobile) {
		if (mobile != null && !"".equals(mobile)) {
			Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
			Matcher m = p.matcher(mobile);
			return m.matches();
		} else {
			return false;
		}
	}

	/**
	 * 是否是身份证号
	 * 
	 * @param identity
	 * @return
	 */
	public static boolean isIdentityNo(String identity) {
		if (identity != null && !"".equals(identity)) {
			Pattern p = Pattern.compile("(\\d{14}[0-9a-zA-Z])|(\\d{17}[0-9a-zA-Z])");
			Matcher m = p.matcher(identity);
			return m.matches();
		} else {
			return false;
		}
	}
	
	/**
	 * 密码是否合法
	 * 
	 * @param pwd
	 * @return
	 */
	public static boolean checkSecurity(String pwd) {
		if (pwd != null && !"".equals(pwd)) {
			//判断是否相同
			if(pwd.matches(pwd.substring(0, 1)+ "{" + pwd.length() + "}")){  
				return false;
			}else if(isOrderNum(pwd)){
				return false;
			}else{
				return true;
			}
		} else {
			return false;
		}
	}
	
	
	/**
	 * 是否连续数字
	 * 
	 * @param pwd
	 * @return
	 */
	public static boolean isOrderNum(String content){
		String aes = "0123456789";
		String des = "9876543210";
		if(aes.contains(content) || des.contains(content)){
			return true;
		}else{
			return false;
		}
	}
	
}