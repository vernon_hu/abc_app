package com.abc.sz.app.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.AddressAction;
import com.abc.sz.app.bean.Address;
import com.abc.sz.app.util.DialogUtil;
import com.abc.sz.app.view.SlideItem;
import com.abc.sz.app.view.SlideListView;
import com.abc.sz.app.view.SlideView;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 收货地址编辑列表
 *
 * @author ftl
 */
@ContentView(R.layout.activity_address_list_edit)
public class AddressListEditActivity extends ABCActivity implements SlideView.OnSlideListener,
        View.OnClickListener {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.lv_address)
    private SlideListView lv_address;

    private List<SlideItem> addressList = new ArrayList<SlideItem>();
    private SlideView mLastSlideViewWithStatusOn;
    private SlideAdapter adapter;
    private Integer deleteItemPosition = null;
    private AddressAction addressAction;
    private boolean isUpdate = false;//是否更新


    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        addressAction = (AddressAction) controller.getAction(this, AddressAction.class);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
	        List<Address> list = (List<Address>) bundle.get("addressList");
	        for (int i = 0; i < list.size(); i++) {
	            SlideItem slideItem = new SlideItem();
	            slideItem.card = list.get(i);
	            addressList.add(slideItem);
	        }
    	}
        adapter = new SlideAdapter();
        lv_address.setAdapter(adapter);

        lv_address.setOnmItemClickListener(new SlideListView.OnmItemClickListener() {

            @Override
            public void onItemClick(SlideView slideView, int position) {
                SlideItem item = addressList.get(position);
                Bundle bundle = new Bundle();
                bundle.putInt("fromPage", 2);
                bundle.putSerializable("address", (Address) item.card);
                callMeForBack(AddAddressActivity.class, bundle, 1);
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.selideDelete: {
                deleteItemPosition = lv_address.getPositionForView(v);
                DialogUtil.showWithTwoBtn(this, "是否确定删除该地址", "确定", "取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (deleteItemPosition != SlideListView.INVALID_POSITION) {
                            SlideItem item = addressList.get(deleteItemPosition);
                            addressAction.deleteAddress(((Address) item.card).getAddressId()).start();
                        }
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                break;
            }
        }

    }

    /**
     * 删除收货地址成功
     */
    public void deleteSuccess() {
    	isUpdate = true;
        if (addressList.size() != 0) {
            if (deleteItemPosition != null) {
                addressList.remove(deleteItemPosition.intValue());
                adapter.notifyDataSetChanged();
                if (addressList.size() == 0) {
                    finishForBack(RESULT_OK);
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
        	finishForBack(RESULT_OK);
        }
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
        	if(isUpdate){
        		finishForBack(RESULT_OK);
        	}else{
        		finish();
        	}
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    public void onBackPressed() {
    	if(isUpdate){
    		finishForBack(RESULT_OK);
    	}else{
    		finish();
    	}
    }

    @Override
    public void onSlide(View view, int status) {
        if (mLastSlideViewWithStatusOn != null && mLastSlideViewWithStatusOn != view) {
            mLastSlideViewWithStatusOn.shrink();
        }
        if (status == SLIDE_STATUS_ON) {
            mLastSlideViewWithStatusOn = (SlideView) view;
        }

    }

    class SlideAdapter extends android.widget.BaseAdapter {
        private LayoutInflater mInflater;

        SlideAdapter() {
            super();
            mInflater = getLayoutInflater();
        }

        @Override
        public int getCount() {
            return addressList.size();
        }

        @Override
        public Object getItem(int position) {
            return addressList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            SlideView slideView = (SlideView) convertView;
            if (slideView == null) {
                View itemView = mInflater.inflate(R.layout.layout_address_edit_item, null);

                slideView = new SlideView(AddressListEditActivity.this);
                slideView.setContentView(itemView);

                holder = new ViewHolder(slideView);
                slideView.setOnSlideLinstener(AddressListEditActivity.this);
                slideView.setTag(holder);
            } else {
                holder = (ViewHolder) slideView.getTag();
            }
            SlideItem item = addressList.get(position);
            if (item != null) {
                item.slideView = slideView;
                item.slideView.shrink();
                item.slideView.setButtonText("删除");
                final Address address = (Address) item.card;
                FormsUtil.setTextViewTxt(holder.tvPerson, address.getPerson(), null);
                FormsUtil.setTextViewTxt(holder.tvPhone, address.getPhone(), null);
                FormsUtil.setTextViewTxt(holder.tvArea, address.getProvince() + address.getCity()
                        + address.getArea(), null);
                FormsUtil.setTextViewTxt(holder.tvAddress, address.getAddress(), null);
                holder.deleteHolder.setOnClickListener(AddressListEditActivity.this);
            }
            return slideView;
        }
    }

    private static class ViewHolder {
        public TextView tvPerson;
        public TextView tvPhone;
        public TextView tvArea;
        public TextView tvAddress;
        public Button deleteHolder;

        ViewHolder(View view) {
            tvPerson = (TextView) view.findViewById(R.id.tv_person);
            tvPhone = (TextView) view.findViewById(R.id.tv_phone);
            tvArea = (TextView) view.findViewById(R.id.tv_area);
            tvAddress = (TextView) view.findViewById(R.id.tv_address);
            deleteHolder = (Button) view.findViewById(R.id.selideDelete);
        }
    }

}
