package com.abc.sz.app.adapter.product;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.product.ProductListBean;
import com.abc.sz.app.util.ImageViewUtil;
import com.forms.base.ABCActivity;
import com.forms.base.ABCApplication;
import com.forms.base.XDRImageLoader;
import com.forms.library.tools.ViewHolder;

import java.util.List;

/**
 * 商品类别适配器
 *
 * @author llc
 */
public class TypeAdapter extends BaseAdapter {

    private List<ProductListBean> mList;
    //	private LayoutParams params;
    //图片加载器
    private XDRImageLoader mImageLoader;

    public TypeAdapter(Context context, List<ProductListBean> list) {
        mList = list;

        mImageLoader = ((ABCApplication) ((ABCActivity) context).getBaseApp()).getImageLoader();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stubProductListBean
        ProductListBean bean = mList.get(position);

        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_type_item, null);
//			convertView.setLayoutParams(params);
        }
        ImageView iv_picture = ViewHolder.get(convertView, R.id.iv_picture);
        mImageLoader.displayImage(bean.getPictureUrl(), iv_picture, ImageViewUtil.getOption());

        TextView tv_name = ViewHolder.get(convertView, R.id.tv_name);

        if (position == mList.size() - 1) {
            //设置最后一条有下划线
            convertView.findViewById(R.id.iv_last).setVisibility(View.VISIBLE);
        } else {
            convertView.findViewById(R.id.iv_last).setVisibility(View.GONE);
        }

        tv_name.setText(bean.getTypeName());

        return convertView;
    }

    public void setmList(List<ProductListBean> mList) {
        this.mList = mList;
    }

}
