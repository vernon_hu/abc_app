package com.abc.sz.app.view;


import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;
import com.abc.sz.app.fragment.product.ProductInfoFragment;

public class MyScrollView extends ScrollView {

    private int currY;
    private int downY;

    public MyScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        System.out.println("sv onInterceptTouchEvent: " + event.getAction());
        downY = (int) event.getY();

        if (getChildAt(0).getHeight() - getHeight() <= 0) {
            ProductInfoFragment.isBelow = true;
//			getParent().getParent().getParent().getParent().requestDisallowInterceptTouchEvent(true);
            return true;
        }

        if (ProductInfoFragment.isBelow && currY > 0) {
            getParent().getParent().getParent().getParent().requestDisallowInterceptTouchEvent(false);
        } else {
            getParent().getParent().getParent().getParent().requestDisallowInterceptTouchEvent(true);
        }
        return super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        System.out.println("sv onTouchEvent: " + event.getAction());
        float scrollY = getScrollY();
        int scroll = getChildAt(0).getHeight() - getHeight();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
//			ProductInfoFragment.isBelow = false; 
                break;
            case MotionEvent.ACTION_MOVE:
                currY = (int) event.getY() - downY;
                System.out.println("currY:" + currY);
                System.out.println("scrollY:" + scrollY);
                if (scroll <= 0) {
                    ProductInfoFragment.isBelow = true;
                    getParent().requestDisallowInterceptTouchEvent(false);
                    break;
                }
                if (scroll != scrollY) {
                    ProductInfoFragment.isBelow = false;
                } else {
                    ProductInfoFragment.isBelow = true;
                    if (currY < 0) {
                        getParent().requestDisallowInterceptTouchEvent(false);
                    } else {
                        getParent().requestDisallowInterceptTouchEvent(true);
                    }
                }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:

                break;
        }
        return super.onTouchEvent(event);
    }

}
