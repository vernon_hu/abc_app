package com.abc.sz.app.bean.cridetcard;

import java.io.Serializable;

/**
 * 信用卡进度
 * 
 * @author ftl
 */
public class CardSchedule implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String sqrq;	//申请日期
    private String sqqd;	//申请渠道
    private String sqlx;    //申请类型
    private String cpmc;	//产品名称
    private String zhwhrq;  //最后维护日期
    private String mailcode;//邮寄件单号
    private String applyStatus;//卡片申领进度
    private String mobile;  //手机号码
    
    private String custname;//客户姓名
    private String cardno;  //卡号
    private String kazhong; //卡种文件号
    private String currtype;//币种
    private String zhjed;   //账户级额度
    private String cardstat;//卡片状态(激活/未激活)
    private String dqjg;    //当前机构
    private String jjyy;    //拒绝原因
    private String cardtyp; //卡片种类
    private String sysrate; //评分额度
    
    
    
	public String getSqrq() {
		return sqrq;
	}
	public void setSqrq(String sqrq) {
		this.sqrq = sqrq;
	}
	public String getSqqd() {
		return sqqd;
	}
	public void setSqqd(String sqqd) {
		this.sqqd = sqqd;
	}
	public String getSqlx() {
		return sqlx;
	}
	public void setSqlx(String sqlx) {
		this.sqlx = sqlx;
	}
	public String getCpmc() {
		return cpmc;
	}
	public void setCpmc(String cpmc) {
		this.cpmc = cpmc;
	}
	public String getZhwhrq() {
		return zhwhrq;
	}
	public void setZhwhrq(String zhwhrq) {
		this.zhwhrq = zhwhrq;
	}
	public String getMailcode() {
		return mailcode;
	}
	public void setMailcode(String mailcode) {
		this.mailcode = mailcode;
	}
	public String getApplyStatus() {
		return applyStatus;
	}
	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getCustname() {
		return custname;
	}
	public void setCustname(String custname) {
		this.custname = custname;
	}
	public String getCardno() {
		return cardno;
	}
	public void setCardno(String cardno) {
		this.cardno = cardno;
	}
	public String getKazhong() {
		return kazhong;
	}
	public void setKazhong(String kazhong) {
		this.kazhong = kazhong;
	}
	public String getCurrtype() {
		return currtype;
	}
	public void setCurrtype(String currtype) {
		this.currtype = currtype;
	}
	public String getZhjed() {
		return zhjed;
	}
	public void setZhjed(String zhjed) {
		this.zhjed = zhjed;
	}
	public String getCardstat() {
		return cardstat;
	}
	public void setCardstat(String cardstat) {
		this.cardstat = cardstat;
	}
	public String getDqjg() {
		return dqjg;
	}
	public void setDqjg(String dqjg) {
		this.dqjg = dqjg;
	}
	public String getJjyy() {
		return jjyy;
	}
	public void setJjyy(String jjyy) {
		this.jjyy = jjyy;
	}
	public String getCardtyp() {
		return cardtyp;
	}
	public void setCardtyp(String cardtyp) {
		this.cardtyp = cardtyp;
	}
	public String getSysrate() {
		return sysrate;
	}
	public void setSysrate(String sysrate) {
		this.sysrate = sysrate;
	}
    
}
