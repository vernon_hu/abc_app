package com.abc.sz.app.activity_phase2.creditcard;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.CreditcardAction;
import com.abc.sz.app.bean.cridetcard.CardFavorable;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;

/**
 * 优特权信息页面
 * 
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_prerogativeinfo)
public class PrerogativeInfoActivity extends ABCActivity {

	@ViewInject(R.id.appBar) Toolbar toolbar;
	@ViewInject(R.id.tvValidDate) TextView tvValidDate;
	@ViewInject(R.id.tvValidDetail) TextView tvValidDetail;
	
	private CreditcardAction creditcardAction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		init();
	}
	
	private void init() {
		creditcardAction = (CreditcardAction) controller.getAction(this, CreditcardAction.class);
		String preferentialID = cacheBean.getStringCache("preferentialID");
		creditcardAction.queryPreferentialRightDetail(preferentialID).start();
	}
	
	/**
     * 信用卡特惠权详情查询成功回调
     */
    public void querySuccess(CardFavorable cardFavorable) {
        if (cardFavorable != null) {
        		FormsUtil.setTextViewTxt(tvValidDate, cardFavorable.getPreferentialDate());
        		FormsUtil.setTextViewTxt(tvValidDetail, cardFavorable.getPreferentialDetail());
        }
    }
}
