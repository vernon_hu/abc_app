package com.abc.sz.app.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.AddressAction;
import com.abc.sz.app.adapter.AddressAdapter;
import com.abc.sz.app.bean.Address;
import com.abc.sz.app.util.DialogUtil;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.net.ViewLoading.OnRetryListener;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.view.toast.MyToast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 收货地址列表
 *
 * @author ftl
 */
@ContentView(R.layout.activity_address_list)
public class AddressListActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.ll_addAddress)
    private LinearLayout ll_addAddress;
    @ViewInject(R.id.lv_address)
    private ListView lv_address;
    @ViewInject(R.id.loadingView)
    private LoadingView loadingView;

    private AddressAdapter adapter;
    private List<Address> list = new ArrayList<Address>();
    //0代表来自我的地址
    //1代表来自订单确认
    private int fromPage = 0;
    private AddressAction addressAction;
    private Address defaultAddress = null;
    private MenuItem editMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        
        initData();
        initListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_edit_menu,menu);
        editMenuItem = menu.findItem(R.id.edit);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.edit){
        	Bundle bundle = new Bundle();
        	bundle.putSerializable("addressList", (Serializable) list); 
            callMeForBack(AddressListEditActivity.class, bundle, 1);
        }
        return super.onOptionsItemSelected(item);
    }
    /**
     * 初始化数据
     */
    private void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fromPage = bundle.getInt("fromPage");
        }
        if(editMenuItem != null) {
            editMenuItem.setVisible(false);
        }
        addressAction = (AddressAction) controller.getAction(this, AddressAction.class);
        queryAddress();
        loadingView.retry(new OnRetryListener() {
			
			@Override
			public void retry() {
				queryAddress();
			}
		});
    }

    private void initListener() {
        lv_address.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                defaultAddress = list.get(arg2);
                if (fromPage == 0) {
                	if("0".equals(defaultAddress.getIsDefault())){
	                    DialogUtil.showWithTwoBtn(AddressListActivity.this, "是否设置为默认收货地址", "确定", "取消", new DialogInterface.OnClickListener() {
	                        @Override
	                        public void onClick(DialogInterface dialog, int which) {
	                            addressAction.setDefaultAddress(defaultAddress.getAddressId()).start();
	                        }
	                    }, new DialogInterface.OnClickListener() {
	                        @Override
	                        public void onClick(DialogInterface dialog, int which) {
	                            dialog.dismiss();
	                        }
	                    });
                	}else{
                		MyToast.showTEXT(AddressListActivity.this, "已经是默认收货地址");
                	}
                } else {
                    getCacheBean().putCache(defaultAddress);
                    finishForBack(RESULT_OK);
                }
            }
        });
    }

    @OnClick(value = {R.id.ll_addAddress})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_addAddress:
                Bundle bundle = new Bundle();
                bundle.putInt("fromPage", 1);
                callMeForBack(AddAddressActivity.class, bundle, 1);
                break;
        }
    }

    /**
     * 查询收货地址成功
     */
    public void querySuccess(RetCode retCode, List<Address> content) {
        if (retCode != RetCode.noData && content != null && content.size() != 0) {
            if (editMenuItem != null) {
                editMenuItem.setVisible(true);
            }
            list = content;
            adapter = new AddressAdapter(list);
            lv_address.setAdapter(adapter);
        } else {
            loadingView.noData(getString(R.string.loadingNoData));
        }
    }

    /**
     * 查询收货地址失败
     */
    public void queryFailed(RetCode retCode) {
        loadingView.failed(retCode.getRetMsg());
    }

    /**
     * 设为默认地址成功
     */
    public void setDefaultSuccess() {
        if (defaultAddress != null) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).equals(defaultAddress)) {
                    defaultAddress.setIsDefault(String.valueOf(1));
                } else {
                    list.get(i).setIsDefault(String.valueOf(0));
                }
            }

        }
        adapter.notifyDataSetChanged();
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	if(resultCode == RESULT_OK){
    		queryAddress();
    	}
    }
    
    public void queryAddress(){
    	addressAction.queryAddress(null).setLoadingView(loadingView).start();
    }
}
