package com.abc.sz.app.view.drawer;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Build;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.UserAction;
import com.abc.sz.app.http.bean.personal.RUserScore;
import com.forms.base.LoginUser;
import com.forms.library.base.BaseActivity;
import com.forms.library.baseUtil.net.Action;
import com.forms.library.baseUtil.net.Controller;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.net.UiObject;
import com.forms.library.baseUtil.view.ViewUtils;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.view.toast.XDRToast;

/**
 * Created by hwt on 4/16/15.
 */
public class UserInfoView extends LinearLayout implements UiObject {
    @ViewInject(R.id.userPic) ImageView mUserPic;
    @ViewInject(R.id.loginId) TextView mLoginId;
    @ViewInject(R.id.gender) TextView mGender;
    @ViewInject(R.id.point) TextView mPoint;
    @ViewInject(R.id.sign) ImageView mSign;
    @ViewInject(R.id.userInfo) LinearLayout mUserInfo;
    @ViewInject(R.id.rlPoint) RelativeLayout rlPoint;
    @ViewInject(R.id.loadingScore) ContentLoadingProgressBar loadingScore;

    private boolean isLogin = false;
    private OnSignListener mOnSignListener;
//    private LoginUser mLoginUser;
    private UserAction mUserAction;
    private List<Action> actionList = new ArrayList<>();

    public UserInfoView(Context context) {
        super(context);
    }

    public UserInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_phase2_userinfoview, this);
        ViewUtils.inject(this, view);
        if (isInEditMode()) return;
        mSign.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnSignListener != null) {
                    long point = !TextUtils.isEmpty(mPoint.getText()) ? Long.valueOf(mPoint.getText().toString()) : 0;
                    mOnSignListener.onSign(v, point);
                }
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mUserInfo.setPadding(mUserInfo.getPaddingLeft(),
                    context.getResources().getDimensionPixelSize(R.dimen.statusBarHeight) + mUserInfo.getPaddingTop(),
                    mUserInfo.getPaddingRight(),
                    mUserInfo.getPaddingBottom());
        }
        mUserAction = (UserAction) Controller.getInstance().getAction(
                (BaseActivity) context, this, UserAction.class);
    }

    public UserInfoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setOnSignListener(OnSignListener onSignListener) {
        mOnSignListener = onSignListener;
    }

    /**
     * 初始化用户信息
     *
     * @param loginUser
     */
    public void initUserInfo(LoginUser loginUser) {
        if (loginUser != null) {
//            if (mLoginUser != null && mLoginUser.getUserId().equals(loginUser.getUserId())) {
//                mLoginUser = loginUser;
//            }
//            setIsLogin(true);
            String phone = loginUser.getPhone();
            if(phone != null && !"".equals(phone)){
            	phone = phone.substring(0, 3) + "****" + phone.substring(7);
            }
            mLoginId.setText(phone);
            if(loginUser.getPoints() != null && !"".equals(loginUser.getPoints())){
            	mPoint.setText("" + loginUser.getPoints());
            }
            rlPoint.setVisibility(View.VISIBLE);
        } else {
            setIsLogin(false);
            mLoginId.setText("登录");
            mPoint.setText("0");
            rlPoint.setVisibility(View.GONE);
        }
    }

    @Override
    public void requestSuccess(RetCode retCode, Object response) {
//        loadingScore.setVisibility(GONE);
//        mPoint.setVisibility(VISIBLE);
        if (response != null && response instanceof RUserScore) {
            RUserScore rUserScore = (RUserScore) response;
            mPoint.setText(rUserScore.getScore());
        } else {
            mPoint.setText("0");
        }
    }

    @Override
    public void requestFalied(RetCode retCode) {
        mPoint.setText("0");
        XDRToast.getXDRToast(getContext()).show("积分查询失败");
    }

    @Override
    public void addAction(Action action) {
        actionList.add(action);
    }

    @Override
    public void resetUiOject() {
        for (Action action : actionList) {
            action.clear();
        }
    }

    public interface OnSignListener {
        /**
         * 签名监听
         *
         * @param v
         * @param point 现有签名积分
         */
        void onSign(View v, long point);
    }

    /**
     * 点击用户信息区域事件
     *
     * @param onClickListener
     * @return
     */
    public UserInfoView ToUserInfo(OnClickListener onClickListener) {
        if (onClickListener != null) {
            mUserInfo.setOnClickListener(onClickListener);
        }
        return this;
    }

    public boolean isLogin() {
        return isLogin;
    }

    public void setIsLogin(boolean isLogin) {
        this.isLogin = isLogin;
    }

    public void loadUserScore() {
//        if (isLogin()) {
//            loadingScore.setVisibility(VISIBLE);
//            mPoint.setVisibility(GONE);
            mUserAction.queryUserScore().start(false);
//        }
    }

    @Override
    protected void onDetachedFromWindow() {
        resetUiOject();
        super.onDetachedFromWindow();
    }
}
