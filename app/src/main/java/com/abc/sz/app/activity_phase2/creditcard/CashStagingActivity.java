package com.abc.sz.app.activity_phase2.creditcard;

import java.util.ArrayList;
import java.util.List;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.util.DialogUtil;
import com.forms.base.ABCActivity;
import com.forms.library.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.ViewHolder;
import com.forms.view.noscoller.MyListView;

/**
 * 现金分期页面
 * 
 * @author hkj
 */
@ContentView(R.layout.activity_phase2_cashstaging)
public class CashStagingActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	@ViewInject(R.id.tvAccount)
	private TextView tvAccount;
	@ViewInject(R.id.tvPeriods)
	private TextView tvPeriods;
	@ViewInject(R.id.mlvStaging)
	private MyListView mlvStaging;
	
	private BaseAdapter<String> adapter;
	private List<String> list = new ArrayList<String>() {
		{
			add("100.00"); add("200.00"); add("300.00");
		}
	};
	
	private String[] PERIODS_TYPES = { "有期", "无期", "6期" };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		adapter = new BaseAdapter<String>(this, list, R.layout.layout_phase2_cashstaging_item) {

			@Override
			public void viewHandler(int position, String t, View convertView) {
				TextView tvPrincipal = ViewHolder.get(convertView, R.id.tvPrincipal);
				TextView tvPoundage = ViewHolder.get(convertView, R.id.tvPoundage);
				TextView tvLine = ViewHolder.get(convertView, R.id.tvLine);
				
				tvPoundage.setText(t);
				if (position == (list.size() - 1)) {
					tvLine.setVisibility(View.GONE);
				} else {
					tvLine.setVisibility(View.VISIBLE);
				}
			}
			
		};
		mlvStaging.setAdapter(adapter);
	}

	@OnClick({ R.id.tvPeriods, R.id.btnApply }) 
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tvPeriods:
			showListDlg();
			break;
		case R.id.btnApply:
			
			break;
		}
	}
	
	private void showListDlg() {
		DialogUtil.showToSelect(this, "选择", PERIODS_TYPES, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				tvPeriods.setText(PERIODS_TYPES[which]);
			}
			
		});
	}
	
}
