package com.abc.sz.app.activity_phase2.depositloan;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.DepositloanAction;
import com.abc.sz.app.bean.loan.Account;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.CalendarUtils;
import com.forms.library.tools.ViewHolder;
import com.forms.view.noscoller.MyListView;

/**
 * 存贷通收益返还明细查询页面
 * 
 * @author hkj
 */
@ContentView(R.layout.activity_phase2_depositloanincomequery)
public class DepositLoanIncomeQueryActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	@ViewInject(R.id.lvAccount)
	private MyListView lvAccount;
	@ViewInject(R.id.tvStartTime)
	private TextView tvStartTime;
	@ViewInject(R.id.tvEndTime)
	private TextView tvEndTime;
	@ViewInject(R.id.loadingView)
	private LoadingView loadingView;
	
	private BaseAdapter<Account> adapter;
	private List<Account> accountList = new ArrayList<Account>();
	private DepositloanAction depositloanAction;
	private int currentPos = 0;
	private String account;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		depositloanAction = (DepositloanAction) controller.getAction(this, DepositloanAction.class);
		depositloanAction.queryLoanAccountList("").setLoadingView(loadingView).start();
		adapter = new BaseAdapter<Account>(this, accountList, R.layout.layout_phase2_dlaccount_item) {

			@Override
			public void viewHandler(final int position, final Account t, View convertView) {
				LinearLayout lltItem = ViewHolder.get(convertView, R.id.lltItem);
				ImageView ivSelect = ViewHolder.get(convertView, R.id.ivSelect);
				TextView tvAccount = ViewHolder.get(convertView, R.id.tvAccount);
				
				if (position == currentPos) {
					ivSelect.setImageResource(R.drawable.shopping_checkbox_select);
				} else {
					ivSelect.setImageResource(R.drawable.shopping_checkbox_default);
				}
				if (t != null) {
					tvAccount.setText(t.getAccount());
					lltItem.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							if (position != currentPos) {
								currentPos = position;
								account = t.getAccount();
								notifyDataSetChanged();
							}
						}
					});
				}
			}
		};
		lvAccount.setAdapter(adapter);
	}

	@OnClick({ R.id.tvStartTime, R.id.tvEndTime, R.id.btnQuery })
	public void calculate(View v) {
		switch (v.getId()) {
		case R.id.tvStartTime:
			showDatePickerDlg(0);
			break;
		case R.id.tvEndTime:
			showDatePickerDlg(1);
			break;
		case R.id.btnQuery:
			if(currentPos == -1){
				Toast.makeText(this, "请选择贷款账号", Toast.LENGTH_SHORT).show();
			}else{
				if("".equals(tvStartTime.getText().toString()) || "".equals(tvEndTime.getText().toString())){
					Toast.makeText(this, "请选择查询日期", Toast.LENGTH_SHORT).show();
				}else{
					cacheBean.put("account", account);
					cacheBean.put("startTime", tvStartTime.getText().toString());
					cacheBean.put("endTime", tvEndTime.getText().toString());
					callMe(DepositLoanIncomeDetailActivity.class);
				}
			}
			break;
		}
	}

	private void showDatePickerDlg(final int pos) {
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int monthOfYear = c.get(Calendar.MONTH);
		int dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
		DatePickerDialog dialog = new DatePickerDialog(this, new OnDateSetListener() {
			
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				if (pos == 0) {
					tvStartTime.setText(CalendarUtils.getDateWithSeparatorStr(year, monthOfYear + 1, dayOfMonth));
				} else {
					tvEndTime.setText(CalendarUtils.getDateWithSeparatorStr(year, monthOfYear + 1, dayOfMonth));
				}
			}
		}, year, monthOfYear, dayOfMonth);
		dialog.show();
	}
	
	/**
	 * 查询贷款账号列表成功回调
	 */
	public void querySuccess(List<Account> list){
		 if (list != null && list.size() > 0) {
			accountList.clear();
			accountList.addAll(list);
            adapter.notifyDataSetChanged();
	     }
	}

}
