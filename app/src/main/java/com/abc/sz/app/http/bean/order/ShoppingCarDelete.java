package com.abc.sz.app.http.bean.order;

/**
 * 删除购物车 请求参数
 *
 * @author llc
 */
public class ShoppingCarDelete {

    private String id;

    public ShoppingCarDelete(String id) {
        super();
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
