package com.abc.sz.app.action_phase2;

import android.content.Context;

import com.abc.sz.app.action.BaseAction;
import com.abc.sz.app.activity.personal.BindNewCardActivity;
import com.abc.sz.app.activity_phase2.lightpay.LightBindCardActivity;
import com.abc.sz.app.activity_phase2.lightpay.LightPayActivity;
import com.abc.sz.app.activity_phase2.lightpay.LightUnBindActivity;
import com.abc.sz.app.bean.lightpay.InitResponse;
import com.abc.sz.app.bean.lightpay.RBindCardInfo;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.lightpay.QBindCard;
import com.abc.sz.app.http.bean.lightpay.QGZBindCard;
import com.abc.sz.app.http.bean.lightpay.QRemoveBindCard;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.alibaba.fastjson.TypeReference;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.view.toast.MyToast;

/**
 * 光支付Action
 *
 * @author ftl
 */
public class LightpayAction extends BaseAction {
	
	 // 光子支付初始化
    public Http payInit(String aesSeed) {
        return APPRequestPhase2.GZRPayInitAct(getAccessToken(), aesSeed, new ResponseNotify<InitResponse>(
                new TypeReference<RespBean<InitResponse>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<InitResponse> response) {
            	((LightPayActivity)uiObject).initSuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }

    // 光子支付绑卡(借记卡)
    public Http bindCard(String cardNum, String phone, String checkCode,
			String userId, String follow) {
    	QBindCard qBindCard = new QBindCard(cardNum, phone, checkCode, userId, follow, "tie_card");
        RequestBean<QBindCard> params = new RequestBean<QBindCard>(getAccessToken(), qBindCard);
        return APPRequestPhase2.GZBindCard(params, new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	((LightBindCardActivity)uiObject).bindCardSuccess();
            }

            @Override
            public void onFailed(RetCode retCode) {
            	MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }
    
    // 光子支付绑卡(贷记卡)
    public Http CreditCardCheckSMSAct(String accNo, String phoneNum, String checkCode, String userId) {
    	QGZBindCard qBindCard = new QGZBindCard(accNo, phoneNum, checkCode, userId, "tie_card");
        RequestBean<QGZBindCard> params = new RequestBean<QGZBindCard>(getAccessToken(), qBindCard);
        return APPRequestPhase2.CreditCardCheckSMSAct(params, new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	((LightBindCardActivity)uiObject).bindCardSuccess();
            }

            @Override
            public void onFailed(RetCode retCode) {
            	MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }
    
    // 光子支付获取已绑定卡的列表
    public Http queryBindCardList(String userId) {
        return APPRequestPhase2.GZGetBindCardList(getAccessToken(), userId, new ResponseNotify<RBindCardInfo>(
                new TypeReference<RespBean<RBindCardInfo>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<RBindCardInfo> response) {
                 ((LightPayActivity) uiObject).querySuccess(response.getContent().getCardinfo());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 解除绑卡(借记卡)
    public Http removeBindAct(String userId, String cardId, String phone) {
    	QRemoveBindCard qRemoveBindCard = new QRemoveBindCard("remove_card", userId, cardId, phone);
        RequestBean<QRemoveBindCard> params = new RequestBean<QRemoveBindCard>(getAccessToken(), qRemoveBindCard);
        return APPRequestPhase2.GZCardRemoveBindAct(params, new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	((LightUnBindActivity)uiObject).removeBindSuccess();
            }

            @Override
            public void onFailed(RetCode retCode) {
            	MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }
    
    // 解除绑卡(贷记卡)
    public Http jkRemoveBindAct(String userId, String cardId) {
    	QRemoveBindCard qRemoveBindCard = new QRemoveBindCard("remove_card", userId, cardId);
        RequestBean<QRemoveBindCard> params = new RequestBean<QRemoveBindCard>(getAccessToken(), qRemoveBindCard);
        return APPRequestPhase2.GZDJKRemoveBindAct(params, new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	((LightUnBindActivity)uiObject).removeBindSuccess();
            }

            @Override
            public void onFailed(RetCode retCode) {
            	MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }
    
    // 冻结光ID
    public Http freezeCardAct(String userId) {
        return APPRequestPhase2.GZFreezeCardAct(getAccessToken(), userId, new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	MyToast.showTEXT((Context) uiObject, "冻结成功");
            }

            @Override
            public void onFailed(RetCode retCode) {
            	MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }
    
    // 解冻光ID
    public Http releaseCardAct(String userId) {
        return APPRequestPhase2.GZReleaseCardAct(getAccessToken(), userId, new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	MyToast.showTEXT((Context) uiObject, "解冻成功");
            }

            @Override
            public void onFailed(RetCode retCode) {
            	MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }
    
    
    // 光子绑卡身份验证(借记卡)
    public Http checkCode(String custName, String idType, String idNumber,
			String cardNum, String phone) {
    	QBindCard qBindCard = new QBindCard(custName, idType, idNumber, cardNum, phone);
    	RequestBean<QBindCard> params = new RequestBean<QBindCard>(getAccessToken(), qBindCard);
        return APPRequestPhase2.CheckCodeAct(params, new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	if(uiObject instanceof LightBindCardActivity){
            		((LightBindCardActivity)uiObject).requestSuccess(response.getContent());
            	}else if(uiObject instanceof BindNewCardActivity){
            		((BindNewCardActivity)uiObject).requestSuccess(response.getContent());
            	}
            	
            }

            @Override
            public void onFailed(RetCode retCode) {
            	MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }
    
    // 光子绑卡身份验证(贷记卡)
    public Http GZDJKBindCard(String accName, String certNo, String accNo, String phoneNum) {
    	QGZBindCard qBindCard = new QGZBindCard(accName, certNo, accNo, phoneNum);
    	RequestBean<QGZBindCard> params = new RequestBean<QGZBindCard>(getAccessToken(), qBindCard);
        return APPRequestPhase2.GZDJKBindCardAct(params, new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	if(uiObject instanceof LightBindCardActivity){
            		((LightBindCardActivity)uiObject).requestSuccess(response.getContent());
            	}else if(uiObject instanceof BindNewCardActivity){
            		((BindNewCardActivity)uiObject).requestSuccess(response.getContent());
            	}
            	
            }

            @Override
            public void onFailed(RetCode retCode) {
            	MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }

}
