package com.abc.sz.app.http.bean.personal;

import com.forms.library.base.BaseBean;
import com.forms.library.baseUtil.db.annotation.Column;
import com.forms.library.baseUtil.db.annotation.Table;

/**
 * Created by hwt on 14/11/17.
 */
@Table(name = "tb_users")
public class RUser extends BaseBean {
    @Column(column = "userId")
    private String userId;//
    @Column(column = "userName")
    private String userName;//
    @Column(column = "type")
    private String type;//
    @Column(column = "phone")
    private String phone;//
    @Column(column = "email")
    private String email;//
    @Column(column = "nickname")
    private String nickname;//
    @Column(column = "headUrl")
    private String headUrl;//
    @Column(column = "points")
    private Long points;//
    @Column(column = "gender")
    private String gender;//

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl;
    }

    public Long getPoints() {
        return points;
    }

    public void setPoints(Long points) {
        this.points = points;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


}
