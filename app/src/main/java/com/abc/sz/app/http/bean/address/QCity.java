package com.abc.sz.app.http.bean.address;


/**
 * 城市
 *
 * @author ftl
 */
public class QCity {

    private String cityId;
    private String lvl;

    public QCity(String cityId, String lvl) {
        this.cityId = cityId;
        this.lvl = lvl;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getLvl() {
        return lvl;
    }

    public void setLvl(String lvl) {
        this.lvl = lvl;
    }


}
