package com.abc.sz.app.bean.product;

import com.forms.library.base.BaseBean;

import java.util.List;

/**
 * 商品评论对象
 *
 * @author llc
 */
public class ProductEvalutionBean extends BaseBean {
    //总评论条数
    private String allReviews;
    //好评数
    private String good;
    //中评
    private String normal;
    //差评
    private String bad;
    //评价列表
    private List<EvalutionBean> OrderAppraiseDetailList;

    public String getAllReviews() {
        return allReviews;
    }

    public void setAllReviews(String allReviews) {
        this.allReviews = allReviews;
    }

    public String getGood() {
        return good;
    }

    public void setGood(String good) {
        this.good = good;
    }

    public String getNormal() {
        return normal;
    }

    public void setNormal(String normal) {
        this.normal = normal;
    }

    public String getBad() {
        return bad;
    }

    public void setBad(String bad) {
        this.bad = bad;
    }

    public List<EvalutionBean> getOrderAppraiseDetailList() {
        return OrderAppraiseDetailList;
    }

    public void setOrderAppraiseDetailList(
            List<EvalutionBean> orderAppraiseDetailList) {
        OrderAppraiseDetailList = orderAppraiseDetailList;
    }

}
