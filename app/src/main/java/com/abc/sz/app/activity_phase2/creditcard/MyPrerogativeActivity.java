package com.abc.sz.app.activity_phase2.creditcard;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.CreditcardAction;
import com.abc.sz.app.bean.cridetcard.CardFavorable;
import com.forms.base.ABCActivity;
import com.forms.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

/**
 * 我的优特权页面
 * 
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_myprerogative)
public class MyPrerogativeActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	@ViewInject(R.id.lvPrerogative)
	private ListView lvPrerogative;

	private BaseAdapter<CardFavorable> adapter;
	private List<CardFavorable> cardFavorableList = new ArrayList<CardFavorable>();
	private CreditcardAction creditcardAction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		init();
	}
	
	private void init() {
		creditcardAction = (CreditcardAction) controller.getAction(this, CreditcardAction.class);
		String idCardNum = cacheBean.getStringCache("idCardNum");
		String cardNum = cacheBean.getStringCache("cardNum");
		creditcardAction.queryPreferentialRight(idCardNum, cardNum).start();
		adapter = new BaseAdapter<CardFavorable>(this, cardFavorableList, R.layout.layout_phase2_myprerogative_item) {

			@Override
			public void viewHandler(final int position, final CardFavorable t, View convertView) {
				LinearLayout lltItem = ViewHolder.get(convertView, R.id.lltItem);
				TextView tvPrerogative = ViewHolder.get(convertView, R.id.tvPrerogative);
				TextView tvTime = ViewHolder.get(convertView, R.id.tvTime);
				TextView tvValidDate = ViewHolder.get(convertView, R.id.tvValidDate);
				
				if (t != null) {
					FormsUtil.setTextViewTxt(tvPrerogative, t.getPreferentialName());
					FormsUtil.setTextViewTxt(tvTime, t.getPreferentialNumber());
					FormsUtil.setTextViewTxt(tvValidDate, t.getPreferentialDate());
					lltItem.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							cacheBean.put("preferentialID", t.getPreferentialID());
							callMe(PrerogativeInfoActivity.class);
						}
					});
				}
			}
		};
		lvPrerogative.setAdapter(adapter);
	}
	
	/**
     * 信用卡特惠权查询成功回调
     */
    public void querySuccess(List<CardFavorable> list) {
        if (list != null && list.size() > 0) {
        	cardFavorableList.clear();
        	cardFavorableList.addAll(list);
            adapter.notifyDataSetChanged();
        }
    }
}
