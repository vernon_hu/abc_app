package com.abc.sz.app.activity_phase2.lightpay;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.LightpayAction;
import com.abc.sz.app.adapter_phase2.CardListAdapter;
import com.abc.sz.app.bean.lightpay.BindCardInfo;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.view.toast.MyToast;

/**
 * Created by hwt on 4/21/15.
 */
@ContentView(R.layout.activity_light_unbind)
public class LightUnBindActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.recyclerView) RecyclerView recyclerView;
    
    private List<BindCardInfo> bindCardInfoList = new ArrayList<>();
    private LightpayAction lightpayAction;
    private CardListAdapter adapter;
    private String photonID;
    private boolean isUpdate = false;//是否更新

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        
        init();
    }
    
    @SuppressWarnings("unchecked")
    private void init() {
    	lightpayAction = (LightpayAction) controller.getAction(this, LightpayAction.class);
    	Bundle bundle = getIntent().getExtras();
    	if(bundle != null){
    		photonID = bundle.getString("photonID");
    		if(bundle.get("cardList") != null){
    			bindCardInfoList = (List<BindCardInfo>) bundle.get("cardList");
    		}
    	}
		adapter = new CardListAdapter(this, bindCardInfoList, lightpayAction, photonID);
    	recyclerView.setAdapter(adapter);
	}

    public void removeBindSuccess(){
    	isUpdate = true;
    	MyToast.showTEXT(this, "已解除绑定");
    	bindCardInfoList.remove(adapter.getClickIndex());
    	adapter.notifyDataSetChanged();
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
        	if(isUpdate){
        		finishForBack(RESULT_OK);
        	}else{
        		finish();
        	}
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    public void onBackPressed() {
    	if(isUpdate){
    		finishForBack(RESULT_OK);
    	}else{
    		finish();
    	}
    }

}
