package com.abc.sz.app.activity_phase2.wdmap;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.adapter_phase2.BranchBankListAdapter;
import com.abc.sz.app.bean.branch.BranchSearchRests;
import com.abc.sz.app.bean.product.BranchRests;
import com.abc.sz.app.view.LoadingView;
import com.alibaba.fastjson.JSON;
import com.forms.base.ABCActivity;
import com.forms.base.BaseConfig;
import com.forms.library.baseUtil.asynchttpclient.AsyncHttpClient;
import com.forms.library.baseUtil.asynchttpclient.TextHttpResponseHandler;
import com.forms.library.baseUtil.logger.Logger;
import com.forms.library.baseUtil.net.Controller;
import com.forms.library.baseUtil.net.HttpUtil;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

import org.apache.http.Header;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@ContentView(R.layout.activity_wd_search_result)
public class WdSearchResultActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.loadingView) LoadingView loadingView;
    @ViewInject(R.id.recyclerView) RecyclerView recyclerView;

    AsyncHttpClient httpClient = null;
    private List<BranchSearchRests> branchBanks = new ArrayList<>();
    private List<BranchSearchRests> branchBanksBackup = new ArrayList<>();
    private BranchBankListAdapter branchBankListAdapter;
    private TextView mSearchText;
    private String queryText;
    private MenuItem settingItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        queryText = getIntent().getStringExtra(WdSearchActivity.QUERYTEXT_TAG);
        httpClient = Controller.getInstance().getHttpClient();
        if (getSupportActionBar() != null) {
            /*ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.MATCH_PARENT,
                    Gravity.CENTER
            );
            View view = LayoutInflater.from(this).inflate(R.layout.layout_phase2_wdlist_item_searchview, null);
            mSearchText = (TextView) view.findViewById(R.id.searchText);
            RelativeLayout containerLayout = (RelativeLayout) view.findViewById(R.id.containerLayout);
            ImageView searchImage = (ImageView) view.findViewById(R.id.searchImage);
            searchImage.setVisibility(View.GONE);
            containerLayout.setBackgroundColor(Color.TRANSPARENT);
            mSearchText.setTextColor(getResources().getColor(R.color.WhiteSmoke));
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callMe(WdSearchActivity.class);
                }
            });
            getSupportActionBar().setCustomView(view, params);
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);*/
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setTitle(queryText);
        }

        branchBankListAdapter = new BranchBankListAdapter(branchBanks);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(branchBankListAdapter);

        loadingView.loading(0);

        queryBranchs(queryText);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_wd_search_result, menu);
        settingItem = menu.findItem(R.id.action_settings);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else {
            loadingView.loading(0);
            branchBanks.clear();
            for (BranchSearchRests searchRests : branchBanksBackup) {
                if (item.getOrder() == 0) {
                    //选择全部网点
                    branchBanks.addAll(branchBanksBackup);
                } else if (String.valueOf(item.getOrder()).equals(searchRests.getTypeId())) {
                    branchBanks.add(searchRests);
                }
            }
            branchBankListAdapter.notifyDataSetChanged();
            if (branchBanks.size() == 0) {
                loadingView.noData(getString(R.string.loadingNoData));
            } else {
                loadingView.loaded();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void queryBranchs(final String searchText) {
        String requestUrl = MessageFormat.format(BaseConfig.QUERY_BRANCH_URL, new Object[]{
                "5", "56", "-1",
                searchText,
                0,
                0,
        });
        Logger.d("requestUrl=%s", requestUrl);
        httpClient.get(this, requestUrl, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                RetCode retCode = HttpUtil.parseFailedInfo(throwable);
                if (loadingView.isLoading()) {
                    loadingView.loaded();
                    loadingView.failed(retCode.getRetMsg(), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            queryBranchs(searchText);
                        }
                    });
                } else {
                    Toast.makeText(WdSearchResultActivity.this, retCode.getRetMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Logger.json(responseString);
                if (loadingView.isLoading()) {
                    loadingView.loaded();
                }
                BranchRests branchBanksTemp = JSON.parseObject(responseString, BranchRests.class);
                branchBanks.addAll(branchBanksTemp.getBranchSearchRests());
                branchBanksBackup.addAll(branchBanks);
                branchBankListAdapter.notifyDataSetChanged();
            }
        });
    }
}
