package com.abc.sz.app.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.forms.library.baseUtil.log.LogUtils;
import com.forms.library.baseUtil.view.ViewUtils;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * Created by hwt on 4/14/15.
 */
public class CellView extends LinearLayout {
    @ViewInject(R.id.cellIcon) ImageView cellIconView;
    @ViewInject(R.id.cellText) TextView cellTextView;

    private Class<?> mTargetClass;
    private int mIndex = 0;
    private ClickListener clickListener;
    private CellViewBean mCellViewBean;


    public CellView(Context context) {
        super(context);
    }

    public CellView(Context context, AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_cellview, this);
        ViewUtils.inject(this, view);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        );
        view.setLayoutParams(layoutParams);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CellView);
        String cellText = typedArray.getString(R.styleable.CellView_cellTextResId);
        Drawable cellIcon = typedArray.getDrawable(R.styleable.CellView_cellIconResId);
        if (!TextUtils.isEmpty(cellText)) {
            cellTextView.setText(cellText);
        }
        if (cellIcon != null) {
            cellIconView.setImageDrawable(cellIcon);
        }
        mIndex = typedArray.getInteger(R.styleable.CellView_cellIndex, 0);
        String classStr = typedArray.getString(R.styleable.CellView_cellTargetClass);
        if (!TextUtils.isEmpty(classStr)) {
            try {
                mTargetClass = Class.forName(classStr);
            } catch (ClassNotFoundException e) {
                LogUtils.d(e.getMessage(), e);
            }
        }
        typedArray.recycle();

        if (isInEditMode()) return;

        if (mTargetClass != null) {
            setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), mTargetClass);
                    getContext().startActivity(intent);
                }
            });
        }
    }

    public CellView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CellView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setClickListener(final ClickListener clickListener) {
        this.clickListener = clickListener;
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.onClick(v, mCellViewBean);
                }
            }
        });
    }

    public interface ClickListener {
        public void onClick(View view, CellViewBean cellViewBean);
    }

    public CellView(Context context, CellViewBean cellViewBean) {
        super(context);
        reset(cellViewBean, 0);
    }

    public CellViewBean getCellViewBean() {
        return mCellViewBean;
    }

    public void setCellViewBean(CellViewBean cellViewBean) {
        mCellViewBean = cellViewBean;
    }

    public void reset(CellViewBean cellViewBean, int columnWidth) {
        cellTextView.setVisibility(VISIBLE);
        mCellViewBean = cellViewBean;
        if (mCellViewBean != null) {
            if (!TextUtils.isEmpty(mCellViewBean.cellTextStr)) {
                cellTextView.setText(mCellViewBean.cellTextStr);
            } else {
                if(mCellViewBean.cellTextResId>0) {
                    cellTextView.setText(mCellViewBean.cellTextResId);
                }else{
                    cellTextView.setVisibility(GONE);
                }
            }
            if(mCellViewBean.cellIconResId>0) {
                cellIconView.setImageResource(mCellViewBean.cellIconResId);
            }
        }
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
//        layoutParams.height = columnWidth;
        layoutParams.width = columnWidth;
        setLayoutParams(layoutParams);
        invalidate();
    }

    public int getIndex() {
        return mIndex;
    }

    public CellView setIndex(int index) {
        mIndex = index;
        return this;
    }

    public Class<?> getTargetClass() {
        return mTargetClass;
    }

    public CellView setTargetClass(Class<?> targetClass) {
        mTargetClass = targetClass;
        return this;
    }

    public static class CellViewBean {

        int cellIconResId = 0;
        int cellTextResId = 0;
        boolean isChecked;
        String cellTextStr;
        Class<?> mTargetClass;
        int mIndex = 0;

        public CellViewBean(int cellIconResId, int cellTextResId) {
            this.cellIconResId = cellIconResId;
            this.cellTextResId = cellTextResId;
        }

        public CellViewBean(Integer cellIconResId, String cellTextStr) {
            this.cellIconResId = cellIconResId;
            this.cellTextStr = cellTextStr;
        }

        public int getCellIconResId() {
            return cellIconResId;
        }

        public CellViewBean setCellIconResId(int cellIconResId) {
            this.cellIconResId = cellIconResId;
            return this;
        }

        public int getCellTextResId() {
            return cellTextResId;
        }

        public CellViewBean setCellTextResId(int cellTextResId) {
            this.cellTextResId = cellTextResId;
            return this;
        }

        public String getCellTextStr() {
            return cellTextStr;
        }

        public CellViewBean setCellTextStr(String cellTextStr) {
            this.cellTextStr = cellTextStr;
            return this;
        }

        public Class<?> getTargetClass() {
            return mTargetClass;
        }

        public CellViewBean setTargetClass(Class<?> targetClass) {
            mTargetClass = targetClass;
            return this;
        }
        public CellViewBean setTargetClass(String targetClass) {
            if (!TextUtils.isEmpty(targetClass)) {
                try {
                    mTargetClass = Class.forName(targetClass);
                } catch (ClassNotFoundException e) {
                    LogUtils.d(e.getMessage(), e);
                }
            }
            return this;
        }
        public int getIndex() {
            return mIndex;
        }

        public CellViewBean setIndex(int mIndex) {
            this.mIndex = mIndex;
            return this;
        }

        public boolean isChecked() {
            return isChecked;
        }

        public CellViewBean setIsChecked(boolean isChecked) {
            this.isChecked = isChecked;
            return this;
        }
    }
}
