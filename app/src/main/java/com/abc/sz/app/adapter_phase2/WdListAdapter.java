package com.abc.sz.app.adapter_phase2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity_phase2.wdmap.WdMapActivity;
import com.abc.sz.app.bean.WdMap;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.ViewUtils;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;

public class WdListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
	
	private ABCActivity activity;
	private List<WdMap> list = new ArrayList<>();
	private	double latitude = 0;
	private	double longitude = 0;
	
	public WdListAdapter(ABCActivity activity, List<WdMap> list, double latitude, 
			double longitude){
		this.activity = activity;
		this.list = list;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	@Override
	public int getItemCount() {
		return list.size();
	}
	
	@Override
	public void onBindViewHolder(ViewHolder viewHolder, final int i) {
		  if (viewHolder != null && viewHolder instanceof ItemView) {
			  ItemView itemView = (ItemView) viewHolder;
			  final WdMap wdMap = list.get(i);
			  if(wdMap != null){
				 FormsUtil.setTextViewTxt(itemView.tvBranchName, wdMap.getBranchName());
				 FormsUtil.setTextViewTxt(itemView.tvRange, wdMap.getDistance());
				 FormsUtil.setTextViewTxts(itemView.tvAddress, activity.getString(R.string.wdAddress), wdMap.getAddress());
				 FormsUtil.setTextViewTxts(itemView.tvPhone, activity.getString(R.string.wdPhone), wdMap.getPhone());
				 itemView.llContent.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Bundle bundle = new Bundle();
						bundle.putDouble("latitude", latitude);
						bundle.putDouble("longitude", longitude);
						bundle.putSerializable("wdMap", wdMap);
						bundle.putSerializable("wdMapList", (Serializable)list); 
						activity.callMe(WdMapActivity.class, bundle);
					}
				});
			  }
		  }
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int arg1) {
		View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_phase2_wdlist_item, 
				viewGroup, false);
		return new ItemView(itemView);
	}
	

	public class ItemView extends RecyclerView.ViewHolder {
		@ViewInject(R.id.llContent) LinearLayout llContent;
		@ViewInject(R.id.tvBranchName) TextView tvBranchName;
		@ViewInject(R.id.tvRange) TextView tvRange;
		@ViewInject(R.id.tvAddress) TextView tvAddress;
		@ViewInject(R.id.tvPhone) TextView tvPhone;

        public ItemView(View itemView) {
            super(itemView);
            ViewUtils.inject(this, itemView);
        }
    }
}
