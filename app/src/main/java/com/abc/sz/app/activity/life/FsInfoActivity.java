package com.abc.sz.app.activity.life;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.http.bean.jf.JfBean;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * 缴费详情
 *
 * @author llc
 */
@ContentView(R.layout.activity_fs_info)
public class FsInfoActivity extends ABCActivity {
    // 缴费详情
    public static String JF_INFO = "JF_INFO";

    // 单位编号
    @ViewInject(R.id.tv_company_num)
    private TextView tv_company_num;
    // 单位名称
    @ViewInject(R.id.tv_company_name)
    private TextView tv_company_name;
    // 缴款通知书号码
    @ViewInject(R.id.tv_notice_num)
    private TextView tv_notice_num;
    //缴款通知书名称
    @ViewInject(R.id.tv_notice_name)
    private TextView tv_notice_name;
    //缴款通知书类型
    @ViewInject(R.id.tv_notice_type)
    private TextView tv_notice_type;
    // 缴费人单位名称
    @ViewInject(R.id.tv_pay_man_company)
    private TextView tv_pay_man_company;
    // 应收总金额
    @ViewInject(R.id.tv_account_payable)
    private TextView tv_account_payable;
    // 应收总滞纳金
    @ViewInject(R.id.tv_acount_penal_sum)
    private TextView tv_acount_penal_sum;
    // 循环次数
    @ViewInject(R.id.tv_list_count)
    private TextView tv_list_count;
    // 备注
    @ViewInject(R.id.tv_remark)
    private TextView tv_remark;
    // 备注1
    @ViewInject(R.id.tv_remark1)
    private TextView tv_remark1;
    // 备注2
    @ViewInject(R.id.tv_remark2)
    private TextView tv_remark2;
    @ViewInject(R.id.appBar)
    private Toolbar toolbar;

    private JfBean bean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initData();
        initView();
    }

    private void initView() {
        // TODO Auto-generated method stub
        if (bean != null) {
            getCacheBean().clear(JF_INFO);
            tv_company_num.setText(bean.getCompanyNum().trim());
            tv_company_name.setText(bean.getCompanyName().trim());
            tv_notice_num.setText(bean.getNoticeNum().trim());
            tv_notice_name.setText(bean.getNoticeName().trim());
            tv_notice_type.setText(bean.getNoticeType().trim().equals("1") ? "缴款通知书" : "罚款通知书");
            tv_pay_man_company.setText(bean.getPayManCompany().trim());

            tv_account_payable.setText(bean.getAccountPayable().trim());
//			tv_list_count.setText(bean.getListCount().trim());
            tv_remark.setText(bean.getRemark().trim());
            tv_remark1.setText(bean.getRemark1().trim());
            tv_remark2.setText(bean.getRemark2().trim());
        }
    }

    private void initData() {
        // TODO Auto-generated method stub
        bean = (JfBean) getCacheBean().get(JF_INFO);

    }

}
