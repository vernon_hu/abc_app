package com.abc.sz.app.activity_phase2.creditcard;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.CreditcardAction;
import com.abc.sz.app.bean.cridetcard.BillInstallments;
import com.abc.sz.app.listener.ShakeListener;
import com.abc.sz.app.listener.ShakeListener.OnShakeListener;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * 账单分期摇一摇页面
 * 
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_billstaging_shake)
public class BillStagingShakeActivity extends ABCActivity {

	@ViewInject(R.id.appBar) Toolbar toolbar;
	@ViewInject(R.id.imageView) ImageView imageView;
	
	private CreditcardAction creditcardAction;
	private String cardNumber;
	// 摇晃监听
	private ShakeListener shakeListener;
	// 摇号结束时间
	private long lastTime;
	// 震动
	private Vibrator vibrator;
	// 声音
	private MediaPlayer player;
	
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 1:
				shakeListener.stop();
				imageView.setImageResource(R.drawable.icon_card_noshake);
				creditcardAction.queryBillInstallments(cardNumber).start();
				break;
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		init();
		initListener();
	}
	
	public void init(){
		// 初始化抖动监听
		shakeListener = new ShakeListener(this);
		// 初始化抖动对象
		vibrator = (Vibrator) getApplication().getSystemService(VIBRATOR_SERVICE);
		// 初始化声音对象
		player = MediaPlayer.create(this, R.raw.shake_sound_male);
		creditcardAction = (CreditcardAction) controller.getAction(this, CreditcardAction.class);
		cardNumber = cacheBean.getStringCache("cardNumber");
	}
	
	public void initListener(){
		shakeListener.setOnShakeListener(new OnShakeListener() {
			@Override
			public void onShake() {
				if(System.currentTimeMillis() - lastTime > 500){
					// 震动
					vibrator.vibrate(new long[] { 0, 800 }, -1);
					// 声音
					player.setLooping(false);
					player.start();
					lastTime = System.currentTimeMillis();
					imageView.setImageResource(R.drawable.icon_card_shaking);
					handler.sendEmptyMessageDelayed(1, 500);
				}
			}
		});
	}
	
	@Override
	protected void onResume() {
		shakeListener.start();
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		shakeListener.stop();
	}
	
	/**
     * 账单分期摇一摇抽奖成功回调
     */
    public void querySuccess(BillInstallments billInstallments) {
    	if(billInstallments != null){
    		shakeListener.start();
    	}
    }
    
    /**
     * 账单分期摇一摇抽奖失败回调
     */
    public void queryFail(RetCode retCode) {
    	shakeListener.start();
    }
	
}
