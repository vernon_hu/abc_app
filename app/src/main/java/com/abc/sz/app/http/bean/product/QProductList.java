package com.abc.sz.app.http.bean.product;

import com.abc.sz.app.bean.product.AttributeValueBean;

import java.util.ArrayList;
import java.util.List;


/**
 * 商品列表查询
 *
 * @author llc
 */
public class QProductList {
    //当前页码
    private String PageNum;
    //类别ID
    private String TypeId;
    //查询关键字
    private String KeyWord;
    //分类查询：1、默认2、按价格 3、按销量4、按好评率
    private String Conditions;

    //筛选条件：属性值<K,V>形式
    private List<QAttributeValueBean> Attribute;

    public String getPageNum() {
        return PageNum;
    }

    public void setPageNum(String pageNum) {
        PageNum = pageNum;
    }

    public String getTypeId() {
        return TypeId;
    }

    public void setTypeId(String typeId) {
        TypeId = typeId;
    }

    public String getKeyWord() {
        return KeyWord;
    }

    public void setKeyWord(String keyWord) {
        KeyWord = keyWord;
    }

    public String getConditions() {
        return Conditions;
    }

    public void setConditions(String conditions) {
        Conditions = conditions;
    }

    public List<QAttributeValueBean> getAttribute() {
        return Attribute;
    }

    public void setAttribute(List<AttributeValueBean> attribute) {
        if(attribute != null){
        	Attribute = new ArrayList<QAttributeValueBean>();
	        for (int i = 0; i < attribute.size(); i++) {
	        	String key = attribute.get(i).getKey();
	        	String value = attribute.get(i).getValue();
	        	if(value != null && !"".equals(value)){
	        		 QAttributeValueBean bean = new QAttributeValueBean(key, value);
	                 Attribute.add(bean);
	        	}
	        }
        }else{
        	Attribute = null;
        }
    }

}
