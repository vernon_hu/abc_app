package com.abc.sz.app.bean_phase2;

/**
 * 存贷通收益返还明细
 * 
 * @author hkj
 */
public class DLIncomeDetail {

	private String returnDate; //
	private String returnMoney; //
	private String returnDepositAccountDetail; //
	
	public String getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}
	public String getReturnMoney() {
		return returnMoney;
	}
	public void setReturnMoney(String returnMoney) {
		this.returnMoney = returnMoney;
	}
	public String getReturnDepositAccountDetail() {
		return returnDepositAccountDetail;
	}
	public void setReturnDepositAccountDetail(String returnDepositAccountDetail) {
		this.returnDepositAccountDetail = returnDepositAccountDetail;
	}
}
