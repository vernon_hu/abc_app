package com.abc.sz.app.activity_phase2.lightpay;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.LightpayAction;
import com.abc.sz.app.bean.lightpay.BindCardInfo;
import com.abc.sz.app.bean.lightpay.InitResponse;
import com.abc.sz.app.util.DialogUtil;
import com.abc.sz.app.view.CircleImageView;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.net.ViewLoading.OnRetryListener;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.view.toast.MyToast;
import com.kcip.util.Platform;
import com.kcip.util.SDKInitializer;

/**
 * Created by hwt on 4/21/15.
 */
@ContentView(R.layout.activity_lightpay)
public class LightPayActivity extends ABCActivity {
	
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.lightPayBtn) CircleImageView lightPayBtn;
    @ViewInject(R.id.agreementCheck) CheckBox agreementCheck;
    @ViewInject(R.id.accountTitle) TextView accountTitle;
    @ViewInject(R.id.agreement) TextView agreement;
    @ViewInject(R.id.accounts) TextView accounts;
    @ViewInject(R.id.loadingView) LoadingView loadingView;

    public static final int LIGHTPAY_IMAGE = 0;
    private boolean hasBindCard = false;
    private List<BindCardInfo> bindCardInfoList = new ArrayList<>();
    private LightpayAction lightpayAction;
    private String[] cardArray;
    private Platform platform;
    private BindCardInfo currentCard;
    private Handler mHandler = new Handler(){
    	public void handleMessage(android.os.Message msg) {
    		switch(msg.what){
	    		case LIGHTPAY_IMAGE:
	    			lightPayBtn.setClickable(true);
	        		lightPayBtn.setImageResource(R.drawable.icon_lightpay_default);
	    			break;
    		}
    	};
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        
        init();
    }
    
    private void init() {
    	platform = SDKInitializer.PayInitialize(this);
    	lightpayAction = (LightpayAction) controller.getAction(this, LightpayAction.class);
    	if(platform.kc_photonID() == null || "".equals(platform.kc_photonID())){
    		lightpayAction.payInit(platform.kc_generateAESKeySeed()).start(false);
    	}else{
    		queryBindCardRetry();
    	}
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_lightpay_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.bindCard) {
        	Bundle bundle = new Bundle();
        	bundle.putString("photonID", platform.kc_photonID());
            callMeForBack(LightBindCardActivity.class, bundle, 1);
        } else if (id == R.id.unBindCard) {
            Bundle bundle = new Bundle();
            bundle.putString("photonID", platform.kc_photonID());
            bundle.putSerializable("cardList", (Serializable)bindCardInfoList);
            callMeForBack(LightUnBindActivity.class, bundle, 1);
        } else if (id == R.id.freezeCard) {
        	lightpayAction.freezeCardAct(platform.kc_photonID()).start();
        } else if (id == R.id.unFreezeCard) {
        	lightpayAction.releaseCardAct(platform.kc_photonID()).start();
        } else if (id == R.id.pathPasswd) {
        	Bundle bundle = new Bundle();
        	bundle.putBoolean("updatePassword", true);
            goTo(LockActivity.class, bundle);
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.lightPayBtn, R.id.agreement, R.id.accounts})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lightPayBtn:
            	if(agreementCheck.isChecked()){
	            	if(hasBindCard){
	            		lightPayBtn.setClickable(false);
		                platform.kc_photonPayment(Integer.parseInt(currentCard.getCard_sequence()));
	                    lightPayBtn.setImageResource(R.drawable.icon_lightpay_selected);
		                mHandler.sendEmptyMessageDelayed(LIGHTPAY_IMAGE, 3000);
	            	}else{
	            		MyToast.showTEXT(this, "请前往绑定支付账户");
	            	}
            	}else{
            		MyToast.showTEXT(LightPayActivity.this, "请同意并使用光子支付协议");
            	}
                break;
            case R.id.agreement:

                break;
            case R.id.accounts:
            	if(hasBindCard){
            		if(cardArray.length > 1){
		            	DialogUtil.showToSelect(this, "", cardArray, new OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								accounts.setText(cardArray[which]);
								currentCard = bindCardInfoList.get(which);
							}
						});
            		}
            	}else{
            		Bundle bundle = new Bundle();
                	bundle.putString("photonID", platform.kc_photonID());
                	callMeForBack(LightBindCardActivity.class, bundle, 1);
            	}
                break;
        }
    }
    
    /**
     * 光子支付初始化成功
     */
    public void initSuccess(InitResponse initResponse){
    	if(initResponse != null){
	    	//短设备号存储
	    	platform.kc_saveShortDeviceID(0);
	    	//数字序列号存储
	    	platform.kc_saveSerialNumber(initResponse.getSequence_id());
	    	//3DES密钥存储
	    	platform.kc_save3DESKey(initResponse.getThree_des());
	    	//光id存储
	    	platform.kc_savePhotonID(initResponse.getUser_id());
	    	//绑定卡列表请求
	    	queryBindCardRetry();
    	}
    }
    
    /**
     * 光子支付获取已绑定卡的列表查询成功
     */
    public void querySuccess(List<BindCardInfo> list){
    	bindCardInfoList.clear();
    	if(list != null && list.size() > 0){
    		bindCardInfoList.addAll(list);
    		cardArray = new String[bindCardInfoList.size()];
    		for (int i = 0; i < bindCardInfoList.size(); i++) {
    			if(bindCardInfoList.get(i) != null){
    				cardArray[i] = bindCardInfoList.get(i).getHidden_card();
    			}
			}
    		hasBindCard = true;
    		currentCard = bindCardInfoList.get(0);
    		accountTitle.setText(getResources().getString(R.string.currentPayCard));
    		accounts.setText(currentCard.getHidden_card());
    	}else{
    		//无绑定卡
    		hasBindCard = false;
    		accountTitle.setText(getResources().getString(R.string.noBindCard));
    		accounts.setText(getResources().getString(R.string.goBindCard));
    	}
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	if(resultCode == RESULT_OK){
    		queryBindCardList();
    	}
    }
    
    public void queryBindCardRetry(){
    	queryBindCardList();
    	loadingView.retry(new OnRetryListener() {
			
			@Override
			public void retry() {
				queryBindCardList();
			}
		});
    }
    
    public void queryBindCardList(){
    	lightpayAction.queryBindCardList(platform.kc_photonID()).setLoadingView(loadingView).start();
    }
    
}


