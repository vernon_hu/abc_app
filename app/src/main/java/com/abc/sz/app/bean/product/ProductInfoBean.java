package com.abc.sz.app.bean.product;

import com.forms.library.base.BaseBean;

import java.util.List;

/**
 * 商品详情
 *
 * @author llc
 */
public class ProductInfoBean extends BaseBean {
    //商品唯一id
    private String ProductId;
    //私有属性列表
    private List<AttributeValueBean> AttributeList;
    //商品图片列表
    private List<String> PictureUrlList;
    //商品名称
    private String ProductName;
    //商品原价
    private String HisPrice;
    //商品价格
    private String ProductPrice;
    //优惠说明
    private String Reason;
    //月销量
    private String SalesVolume;
    //商品描述
    private String Description;
    //商户名称
    private String SellerName;
    //商品库存
    private String StockNum;
    //商品介绍
    private String ProductIntroduction;
    //商品售后
    private String CustomerService;
    //类目ID
    private String GroupId;
    //商品小图url
    private String ImageUrl;
    //商品链接url
    private String Url;
    //是否收藏,0代表未收藏,其它代表收藏id
    private String Gz;

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public List<AttributeValueBean> getAttributeList() {
        return AttributeList;
    }

    public void setAttributeList(List<AttributeValueBean> attributeList) {
        AttributeList = attributeList;
    }


    public List<String> getPictureUrlList() {
        return PictureUrlList;
    }

    public void setPictureUrlList(List<String> pictureUrlList) {
        PictureUrlList = pictureUrlList;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getHisPrice() {
        return HisPrice;
    }

    public void setHisPrice(String hisPrice) {
        HisPrice = hisPrice;
    }

    public String getProductPrice() {
        return ProductPrice;
    }

    public void setProductPrice(String productPrice) {
        ProductPrice = productPrice;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String getSalesVolume() {
        return SalesVolume;
    }

    public void setSalesVolume(String salesVolume) {
        SalesVolume = salesVolume;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getSellerName() {
        return SellerName;
    }

    public void setSellerName(String sellerName) {
        SellerName = sellerName;
    }

    public String getStockNum() {
        return StockNum;
    }

    public void setStockNum(String stockNum) {
        StockNum = stockNum;
    }

    public String getProductIntroduction() {
        return ProductIntroduction;
    }

    public void setProductIntroduction(String productIntroduction) {
        ProductIntroduction = productIntroduction;
    }

    public String getCustomerService() {
        return CustomerService;
    }

    public void setCustomerService(String customerService) {
        CustomerService = customerService;
    }

    public String getGroupId() {
        return GroupId;
    }

    public void setGroupId(String groupId) {
        GroupId = groupId;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

	public String getGz() {
		return Gz;
	}

	public void setGz(String gZ) {
		Gz = gZ;
	}

}
