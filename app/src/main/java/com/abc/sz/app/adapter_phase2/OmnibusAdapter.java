package com.abc.sz.app.adapter_phase2;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity.kb.KbInfoActivity;
import com.abc.sz.app.bean_phase2.ImageAD;
import com.abc.sz.app.bean_phase2.Omnibus;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.util.ShoppingCarCount;
import com.forms.base.XDRImageLoader;
import com.forms.library.base.BaseActivity;
import com.forms.library.baseUtil.view.ViewUtils;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;
import com.forms.view.viewflow.CircleFlowIndicator;
import com.forms.view.viewflow.ViewFlow;

/**
 * 达人精选数据适配器
 *
 * @author hkj
 */
public class OmnibusAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEMTYPE = 1;
    private static final int HEADERTYPE = 0;

    private Context mContext;
    private HeaderView headerView;
    private OmnibusViewHoler omnibusView;

    private XDRImageLoader mImageLoader;
    private List<ImageAD> mImageList = new ArrayList<>();
    private List<Omnibus> mOmnibusList = new ArrayList<>();

    private final int NEXTIMAGE = 1;
    private int imageADPosition = 0;

    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == NEXTIMAGE) {
                if (headerView != null) {
                    if (imageADPosition == mImageList.size() - 1) {
                        imageADPosition = 0;
                    } else {
                        imageADPosition += 1;
                    }
                    headerView.viewFlow.setSelection(imageADPosition);
                    handler.sendEmptyMessageDelayed(NEXTIMAGE, 3000);
                }
            }
        }
    };

    public OmnibusAdapter(Context context, XDRImageLoader imageLoader, List<ImageAD> imageList,
                          List<Omnibus> omnibusList) {
        mContext = context;
        mImageList = imageList;
        mOmnibusList = omnibusList;
        mImageLoader = imageLoader;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder = null;
        if (HEADERTYPE == i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                    R.layout.layout_phase2_ad_images, viewGroup, false);
            return viewHolder = new HeaderView(view);
        } else if (ITEMTYPE == i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                    R.layout.layout_phase2_omnibus_item, viewGroup, false);
            return viewHolder = new OmnibusViewHoler(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof HeaderView) {
            headerView = (HeaderView) viewHolder;
            headerView.setIsRecyclable(false);
            if (mImageList != null && mImageList.size() > 0) {
                handler.removeMessages(NEXTIMAGE);

                ViewGroup.LayoutParams params = headerView.viewFlow.getLayoutParams();
                params.height = FormsUtil.SCREEN_WIDTH / 5 * 3;
                headerView.viewFlow.setLayoutParams(params);
                headerView.viewFlow.setFlowIndicator(headerView.viewflowindic);
                ImageAdapter imageAdapter = new ImageAdapter(mContext, mImageLoader, mImageList);
                headerView.viewFlow.setAdapter(imageAdapter, mImageList.size());
                headerView.viewFlow.setOnViewSwitchListener(new ViewFlow.ViewSwitchListener() {
                    @Override
                    public void onSwitched(View view, int position) {
                        imageADPosition = headerView.viewFlow.getSelectedItemPosition();
                    }
                });
                handler.sendEmptyMessageDelayed(NEXTIMAGE, 3000);
            }
        } else if (viewHolder instanceof OmnibusViewHoler) {
            omnibusView = (OmnibusViewHoler) viewHolder;

            final Omnibus omnibus = mOmnibusList.get(i - 1);
            if (omnibus != null) {
                mImageLoader.displayImage(omnibus.getProductPic(), omnibusView.ivOmnibusImage, ImageViewUtil.getOption());
                omnibusView.tvHisPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                if ("1".equals(omnibus.getProductType())) {
                    omnibusView.tvScore.setVisibility(View.VISIBLE);
                    omnibusView.tvBargain.setVisibility(View.GONE);
                } else {
                    omnibusView.tvScore.setVisibility(View.GONE);
                    omnibusView.tvBargain.setVisibility(View.VISIBLE);
                }
                omnibusView.tvOmnibusName.setText(omnibus.getProductName());
                omnibusView.tvHisPrice.setText(ShoppingCarCount.formatMoney(omnibus.getHisPrice()));
                omnibusView.tvNowPrice.setText(ShoppingCarCount.formatMoney(omnibus.getProductPrice()));
                omnibusView.tvStarNum.setText(omnibus.getFavoriteNum());
                
                omnibusView.llContent.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						 ((BaseActivity) mContext).getCacheBean().putCache(omnibus);
						 ((BaseActivity) mContext).callMe(KbInfoActivity.class);
					}
				});
            }
        }
    }

    @Override
    public int getItemCount() {
        return mOmnibusList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADERTYPE;
        } else {
            return ITEMTYPE;
        }
    }

    public class OmnibusViewHoler extends RecyclerView.ViewHolder {

    	@ViewInject(R.id.llContent)
        LinearLayout llContent;
    	 
        @ViewInject(R.id.ivOmnibusImage)
        ImageView ivOmnibusImage;

        @ViewInject(R.id.tvScore)
        TextView tvScore;// 积分折扣

        @ViewInject(R.id.tvBargain)
        TextView tvBargain;// 特价活动

        @ViewInject(R.id.tvOmnibusName)
        TextView tvOmnibusName;// 名称

        @ViewInject(R.id.tvNowPrice)
        TextView tvNowPrice;// 现价

        @ViewInject(R.id.tvHisPrice)
        TextView tvHisPrice;// 历史价格

        @ViewInject(R.id.tvStarNum)
        TextView tvStarNum;// 收藏数量

        public OmnibusViewHoler(View itemView) {
            super(itemView);
            ViewUtils.inject(this, itemView);
        }
    }

    public class HeaderView extends RecyclerView.ViewHolder {

        @ViewInject(R.id.viewFlow)
        ViewFlow viewFlow;
        @ViewInject(R.id.viewflowindic)
        CircleFlowIndicator viewflowindic;

        public HeaderView(View itemView) {
            super(itemView);
            ViewUtils.inject(this, itemView);
        }
    }
}
