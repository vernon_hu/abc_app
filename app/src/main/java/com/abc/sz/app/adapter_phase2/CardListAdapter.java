package com.abc.sz.app.adapter_phase2;

import java.util.ArrayList;
import java.util.List;

import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.LightpayAction;
import com.abc.sz.app.bean.lightpay.BindCardInfo;
import com.abc.sz.app.util.DialogUtil;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.ViewUtils;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;

public class CardListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
	
	private ABCActivity activity;
	private List<BindCardInfo> list = new ArrayList<>();
	private LightpayAction lightpayAction;
	private String photonID;
	private int clickIndex;
	
	public CardListAdapter(ABCActivity activity, List<BindCardInfo> list, LightpayAction lightpayAction, 
			String photonID){
		this.activity = activity;
		this.list = list;
		this.lightpayAction = lightpayAction;
		this.photonID = photonID;
	}

	@Override
	public int getItemCount() {
		return list.size();
	}
	
	public int getClickIndex(){
		return clickIndex;
	}
	
	public void setClickIndex(int clickIndex){
		this.clickIndex = clickIndex;
	}

	@Override
	public void onBindViewHolder(ViewHolder viewHolder, final int i) {
		  if (viewHolder != null && viewHolder instanceof ItemView) {
			  ItemView itemView = (ItemView) viewHolder;
			  final BindCardInfo bindCardInfo = list.get(i);
			  if(bindCardInfo != null){
				  FormsUtil.setTextViewTxt(itemView.cardNum, bindCardInfo.getHidden_card());
				  itemView.cardNum.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						DialogUtil.showWithTwoBtn(activity, "是否确定解绑", "确定", "取消", new DialogInterface.OnClickListener() {
	                        @Override
	                        public void onClick(DialogInterface dialog, int which) {
	                        	lightpayAction.jkRemoveBindAct(photonID, bindCardInfo.getCard_sequence()).start();
	                        }
	                    }, new DialogInterface.OnClickListener() {
	                        @Override
	                        public void onClick(DialogInterface dialog, int which) {
	                            dialog.dismiss();
	                        }
	                    });
					}
				});
//				  itemView.cardNum.setOnClickListener(new OnClickListener() {
//						
//						@Override
//						public void onClick(View v) {
//							View dialogView = LayoutInflater.from(activity).inflate(R.layout.layout_dialog_remove_bind, null);
//		                    final AlertDialog dialog = new AlertDialog.Builder(activity).create();
//		                    dialog.setView(dialogView, 0, 0, 0, 0);
//		                    dialog.setInverseBackgroundForced(true);
//		                    dialog.show();
//		                    final EditText etRemoveBind = (EditText) dialogView.findViewById(R.id.et_remove_bind);
//		                    Button btnCancel = (Button) dialogView.findViewById(R.id.btn_cancel);
//		                    Button btnConfirm = (Button) dialogView.findViewById(R.id.btn_confirm);
//
//		                    btnCancel.setOnClickListener(new OnClickListener() {
//
//		                        @Override
//		                        public void onClick(View v) {
//		                        	dialog.dismiss();
//		                        }
//		                    });
//		                    btnConfirm.setOnClickListener(new OnClickListener() {
//
//		                        @Override
//		                        public void onClick(View v) {
//		                        	String cardId = bindCardInfo.getCard_sequence();
//									String phone = etRemoveBind.getText().toString();
//									if (TextUtils.isEmpty(phone)) {
//		                        		MyToast.showTEXT(activity, activity.getString(R.string.please_input_phone));
//		                    	    } else if (phone.length() != 11) {
//		                    	    	MyToast.showTEXT(activity, activity.getString(R.string.input_eleven_phone));
//		                    	    } else{
//		                    	    	lightpayAction.removeBindAct(photonID, cardId, phone).start();
//										setClickIndex(i);
//										dialog.dismiss();
//		                    	    }
//		                        }
//		                    });
//						}
//					});
			  }
		  }
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int arg1) {
		View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_phase2_cardlist_item, 
				viewGroup, false);
		return new ItemView(itemView);
	}
	

	public class ItemView extends RecyclerView.ViewHolder {
		@ViewInject(R.id.cardNum) TextView cardNum;

        public ItemView(View itemView) {
            super(itemView);
            ViewUtils.inject(this, itemView);
        }
    }
}
