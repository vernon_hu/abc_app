package com.abc.sz.app.activity.order;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.fragment.order.MovieOrderFragment;
import com.forms.base.ABCActivity;
import com.forms.base.ABCFragment;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * 电影订单
 *
 * @author ftl
 */
@ContentView(R.layout.activity_movie_order)
public class MovieOrderActivity extends ABCActivity {
	
    @ViewInject(R.id.appBar) Toolbar toolbar;
//    @ViewInject(R.id.tv_all_order) TextView tv_all_order;
//    @ViewInject(R.id.tv_already_consume) TextView tv_already_consume;
//    @ViewInject(R.id.tv_wait_consume) TextView tv_wait_consume;
//    @ViewInject(R.id.tv_wait_pay) TextView tv_wait_pay;
    @ViewInject(R.id.fl_content) FrameLayout fl_content;

    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    private List<MovieOrderFragment> fragmentList = new ArrayList<>();
    private MovieOrderFragment targFragment1;
    private MovieOrderFragment targFragment2;
    private MovieOrderFragment targFragment3;
    private MovieOrderFragment targFragment4;
//    private int targFragment2Index;
//    private int targFragment3Index;
//    private int targFragment4Index;
    //当期选中的排序
    private int indexSelect = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initData();
//        initListener();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        targFragment1 = new MovieOrderFragment();
        targFragment2 = new MovieOrderFragment();
        targFragment3 = new MovieOrderFragment();
        targFragment4 = new MovieOrderFragment();
        
        Bundle bundle1 = new Bundle();
        bundle1.putString("state", "");
        targFragment1.setArguments(bundle1);
        
        Bundle bundle2 = new Bundle();
        bundle2.putString("state", "00");
        targFragment2.setArguments(bundle2);
        
        Bundle bundle3 = new Bundle();
        bundle3.putString("state", "20");
        targFragment3.setArguments(bundle3);
        
        Bundle bundle4 = new Bundle();
        bundle4.putString("state", "10");
        targFragment4.setArguments(bundle4);
       
        fragmentManager = getSupportFragmentManager();
        addFragment(targFragment1);
        showFragment(indexSelect);
    }
    
    private void addFragment(MovieOrderFragment fragment){
    	 transaction = fragmentManager.beginTransaction();
         transaction.add(R.id.fl_content, fragment);
         transaction.commit();
         fragmentList.add(fragment);
    }
    
    /**
     * 切换Fragment
     */
    private void showFragment(int indexSelect) {
    	for (int i = 0; i < fragmentList.size(); i++) {
            ABCFragment fragment = fragmentList.get(i);
            if (!fragment.isHidden()) {
                fragmentManager.beginTransaction().hide(fragment).commit();
            }
        }
        fragmentManager.beginTransaction().show(fragmentList.get(indexSelect)).commit();
    }

//    private void initListener() {
//        tv_all_order.setOnClickListener(new MyTextClickListener());
//        tv_already_consume.setOnClickListener(new MyTextClickListener());
//        tv_wait_consume.setOnClickListener(new MyTextClickListener());
//        tv_wait_pay.setOnClickListener(new MyTextClickListener());
//    }

//    public class MyTextClickListener implements OnClickListener {
//
//        @Override
//        public void onClick(View v) {
//            changeShow(v.getId());
//            switch (v.getId()) {
//                case R.id.tv_all_order:
//                	indexSelect = 0;
//                    break;
//                case R.id.tv_already_consume:
//                	if(!fragmentList.contains(targFragment2)){
//                		 addFragment(targFragment2);
//                	     targFragment2Index = ++indexSelect;
//                	}
//                	indexSelect = targFragment2Index;
//                    break;
//                case R.id.tv_wait_consume:
//                	if(!fragmentList.contains(targFragment3)){
//                		 addFragment(targFragment3);
//	               	     targFragment3Index = ++indexSelect;
//                	}
//                	indexSelect = targFragment3Index;
//                    break;
//                case R.id.tv_wait_pay:
//                	if(!fragmentList.contains(targFragment4)){
//                		 addFragment(targFragment4);
//	               	     targFragment4Index = ++indexSelect;
//                	}
//                	indexSelect = targFragment4Index;
//                    break;
//            }
//            showFragment(indexSelect);
//        }
//    }
//
//    /**
//     * 改变显示
//     *
//     * @param id
//     */
//    public void changeShow(int id) {
//        changeShow(tv_all_order, id == R.id.tv_all_order ? true : false);
//        changeShow(tv_already_consume, id == R.id.tv_already_consume ? true : false);
//        changeShow(tv_wait_consume, id == R.id.tv_wait_consume ? true : false);
//        changeShow(tv_wait_pay, id == R.id.tv_wait_pay ? true : false);
//    }

//    /**
//     * 改变显示
//     *
//     * @param view
//     * @param isSelect
//     */
//    public void changeShow(TextView view, boolean isSelect) {
//        if (isSelect) {
//            view.setTextColor(getResources().getColor(R.color.white));
//            view.setBackgroundResource(R.color.colorPrimary);
//        } else {
//            view.setTextColor(getResources().getColor(R.color.colorPrimary));
//            view.setBackgroundResource(R.color.white);
//        }
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_order_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.refresh) {
        	fragmentList.get(indexSelect).onRefresh();
        }
        return super.onOptionsItemSelected(item);
    }

}
