package com.abc.sz.app.view.drawer;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hwt on 4/20/15.
 */
public class MenuItemGroup {
    private Boolean isItemClicked = false;
    private List<DrawerItemView> itemViewList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private int background = 0;
    private int backgroudPress = 0;

    public void addMenuItemView(DrawerItemView drawerItemView) {
        itemViewList.add(drawerItemView);
        drawerItemView.setIndex(itemViewList.size() - 1);
        drawerItemView.setBackgroundResource(background);
        drawerItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickListener != null) {
                    synchronized (isItemClicked) {
                        isItemClicked = true;
                        DrawerItemView drawerItemView = (DrawerItemView) v;
//                        for (int i = 0;i<itemViewList.size();i++){
//                            if(i==drawerItemView.getIndex()){
//                            }else{
//                            }
//                        }
                        itemClickListener.itemClick(v, drawerItemView.getIndex());
                        isItemClicked = false;
                    }
                }
            }
        });
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        public void itemClick(View view, int position);
    }

    public void recycle() {
        itemViewList.clear();
        itemClickListener = null;
        System.gc();
    }

    public int getBackground() {
        return background;
    }

    public MenuItemGroup setBackground(int background) {
        this.background = background;
        return this;
    }

    public int getBackgroudPress() {
        return backgroudPress;
    }

    public MenuItemGroup setBackgroudPress(int backgroudPress) {
        this.backgroudPress = backgroudPress;
        return this;
    }


}
