package com.abc.sz.app.fragment.product;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.adapter.ImageAdapter;
import com.abc.sz.app.bean.Advertisement;
import com.abc.sz.app.bean.product.AttributeValueBean;
import com.abc.sz.app.bean.product.ProductInfoBean;
import com.abc.sz.app.bean.product.TypeAttributeBean;
import com.forms.base.ABCFragment;
import com.forms.base.XDRImageLoader;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.view.viewflow.CircleFlowIndicator;
import com.forms.view.viewflow.ViewFlow;

/**
 * 产品详情页
 *
 * @author llc
 */
public class ProductInfoFragment extends ABCFragment {
    
    @ViewInject(R.id.tv_product_name) TextView tv_product_name;//商品名称
    @ViewInject(R.id.tv_yhxx) TextView tv_yhxx; //优惠信息
    @ViewInject(R.id.tv_price) TextView tv_price; //价格
    @ViewInject(R.id.tv_product_attribute_info) TextView tv_product_attribute_info;//属性值信息
    @ViewInject(R.id.tv_pull) TextView tv_pull;
    @ViewInject(R.id.iv_bg) ImageView iv_bg;
    @ViewInject(R.id.rl_picture) RelativeLayout rl_picture;//产品图片区
    @ViewInject(R.id.viewFlow) ViewFlow viewFlow;
    @ViewInject(R.id.viewflowindic) CircleFlowIndicator viewflowindic;
    @ViewInject(R.id.rl_product_attribute) RelativeLayout rl_product_attribute;//产品属性
    
    //是否为底部
    public static boolean isBelow = false;
    //产品属性界面
    private ProductAttributeFragment productAttributeFragment;
    // 图片加载器
    private XDRImageLoader imageLoader;
    private List<Advertisement> mAdvList;
    private ImageAdapter imageAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_product_info, container, false);
        return view;
    }

    @Override
    protected void initData() {
        // TODO 初始化数据
        super.initData();

        imageLoader = baseActivity.getImageLoader();
//        productAttributeFragment = ((ProductInfoActivity)baseActivity).getProductAttributeFragment();
    }

    @Override
    protected void initView() {
        // TODO 初始化界面
        super.initView();
//        LayoutParams params = viewFlow.getLayoutParams();
//        params.height = FormsUtil.SCREEN_WIDTH * 3/4;
//        viewFlow.setLayoutParams(params);
    }

    @Override
    protected void initListener() {
        // TODO 设置监听
        super.initListener();

        /**
         * 点击打开商品属性菜单
         */
        rl_product_attribute.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO 商品属性点击事件
//            	ProductInfoActivity.menuState = true;
//                ((ProductInfoActivity)baseActivity).getMenu().showMenu();
            }
        });

//		vp_product_picture.setOnTouchListener(new OnTouchListener() {
//			int cX = 0;
//			int dX = 0;
//			long cT = 0;
//			long dT = 0;
//			boolean isTouchable =false;
//
//			@Override
//			public boolean onTouch(View view, MotionEvent event) {
//				// TODO Auto-generated method stub
//				dX = (int) event.getX();
//				dT = System.currentTimeMillis();
//				switch (event.getAction()) {
//				case MotionEvent.ACTION_DOWN:
//					isTouchable = true;
//					cX = (int) event.getX();
//					cT = System.currentTimeMillis();
//					break;
//				case MotionEvent.ACTION_UP:
//					if (Math.abs(dX - cX) < 15 && (dT - cT) < 500) {
//						click(view);
//					}
//					isTouchable = false;
//					break;
//				}
//				return false;
//			}
//
//			public void click(View view) {
//					Bundle bundle = new Bundle();
//					ArrayList<String> pritureList = (ArrayList<String>) bean.getPictureUrlList();
//					bundle.putStringArrayList(PhotoActivity.URL_LIST,pritureList
//							);
//					baseActivity.callMe(PhotoActivity.class, bundle);
//			}
//		});
    }

    public ProductAttributeFragment getProductAttributeFragment() {
        return productAttributeFragment;
    }

    /**
     * 换页时操作
     *
     * @param isChange
     */
    public void changePager(boolean isChange) {
        if (isChange) {
            tv_pull.setText(R.string.pull_up);
            iv_bg.setBackgroundResource(R.drawable.gd_up_icon);
        } else {
            tv_pull.setText(R.string.pull_down);
            iv_bg.setBackgroundResource(R.drawable.gd_down_icon);
        }
    }


    /**
     * 详情回调
     *
     * @param bean
     */
    public void onResponse(ProductInfoBean bean) {
//		this.bean = bean;
//		listView.clear();
//		rg_picture.removeAllViews();
//		for (int i = 0; i < bean.getPictureUrlList().size(); i++) {
//			ImageView view = new ImageView(baseActivity);
//			view.setScaleType(ScaleType.FIT_XY);
//			mImageLoader.displayImage(bean.getPictureUrlList().get(i),view, ImageViewUtil.getOption());
//			rg_picture.addView(createRadioButton(i));
//			listView.add(view);
//		}
//		adapter = new ProductPictureAdapter(listView);
//		vp_product_picture.setRadioGroup(rg_picture);
//		vp_product_picture.setAdapter(adapter);
//		rg_picture.check(0);
    	if(bean != null){
	        mAdvList = new ArrayList<Advertisement>();
	        for (int i = 0; i < bean.getPictureUrlList().size(); i++) {
	            Advertisement advertisement = new Advertisement();
	            advertisement.setImgUrl(bean.getPictureUrlList().get(i));
	            advertisement.setAttribute(bean.getUrl());
	            mAdvList.add(advertisement);
	        }
	        imageAdapter = new ImageAdapter(mAdvList, baseActivity, imageLoader);
	        viewFlow.setAdapter(imageAdapter, mAdvList.size());
	        viewFlow.setFlowIndicator(viewflowindic);
	
	        tv_product_name.setText(bean.getProductName());
	        if ("".equals(bean.getReason().trim())) {
	            tv_yhxx.setVisibility(View.GONE);
	        } else {
	            tv_yhxx.setText(bean.getReason());
	        }
	        tv_price.setText("￥" + bean.getProductPrice());
	
	        String value = "";
	        for (AttributeValueBean vb : bean.getAttributeList()) {
	            value += vb.getValue() + " ";
	        }
	        productAttributeFragment.setProductAttribute(bean.getAttributeList());
	        tv_product_attribute_info.setText(value);
    	}
    }

    /**
     * 属性回调
     *
     * @param bean
     */
    public void onAttributeResponse(List<TypeAttributeBean> bean) {
        if (productAttributeFragment.getAttributeList().size() == 0) {
            productAttributeFragment.onAttributeResponse(bean);
        }
    }

    /**
     * 创建图片指示器
     *
     * @param index
     * @return
     */
    public RadioButton createRadioButton(int index) {
        RadioButton rb = new RadioButton(getActivity());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                20, LayoutParams.MATCH_PARENT);
        rb.setPadding(20, 0, 0, 0);
        rb.setId(index);
        rb.setClickable(false);
        rb.setLayoutParams(params);
        rb.setButtonDrawable(R.drawable.home_radio_selector);
        return rb;
    }


}
