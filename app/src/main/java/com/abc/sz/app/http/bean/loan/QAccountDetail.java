package com.abc.sz.app.http.bean.loan;

/**
 * Created by hwt on 4/30/15.
 */
public class QAccountDetail {
    private String accountNum;//贷款账号
    private String startTime;//开始时间
    private String endTime;//结束时间
    
    public QAccountDetail(){}

    public QAccountDetail(String accountNum, String startTime, String endTime) {
		super();
		this.accountNum = accountNum;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
