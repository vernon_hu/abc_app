package com.abc.sz.app.http.bean.card;

/**
 * 申请消费分期
 * 
 * @author ftl
 */
public class QApplyConsumer {

	private String cardNumber;// 信用卡号
	private String consumerID;// 消费账单id
	private String issueNumber;// 分期期数
	
	public QApplyConsumer(){}

	public QApplyConsumer(String cardNumber, String consumerID, String issueNumber) {
		super();
		this.cardNumber = cardNumber;
		this.consumerID = consumerID;
		this.issueNumber = issueNumber;
	}

	public String getConsumerID() {
		return consumerID;
	}

	public void setConsumerID(String consumerID) {
		this.consumerID = consumerID;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getIssueNumber() {
		return issueNumber;
	}

	public void setIssueNumber(String issueNumber) {
		this.issueNumber = issueNumber;
	}

}
