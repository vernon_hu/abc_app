package com.abc.sz.app.bean;

import com.forms.library.base.BaseBean;

/**
 * 用户地址
 *
 * @author ftl
 */
public class Address extends BaseBean {

    /* 地址id */
    private String addressId;
    /* 用户id */
    private String userId;
    /* 收货人 */
    private String person;
    /* 手机号码 */
    private String phone;
    /* 省份 */
    private String province;
    /* 城市 */
    private String city;
    /* 地区或者县城 */
    private String area;
    /* 详细地址 */
    private String address;
    /* 是否默认地址,1表示是,0表示否 */
    private String isDefault;

    public Address() {

    }

    public Address(String person, String phone, String province, String city,
                   String area, String address) {
        super();
        this.person = person;
        this.phone = phone;
        this.province = province;
        this.city = city;
        this.area = area;
        this.address = address;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }


}
