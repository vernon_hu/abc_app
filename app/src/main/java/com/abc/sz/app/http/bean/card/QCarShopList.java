package com.abc.sz.app.http.bean.card;

/**
 * 车商
 * 
 * @author ftl
 */
public class QCarShopList {

	private String pageNumber;//当前页 默认值传0
    private String brand;//品牌
    private String area;//区域
    private String cardShop;//车商
    
    public QCarShopList(){}
    
	public QCarShopList(String pageNumber, String brand, String area, String cardShop) {
		super();
		this.pageNumber = pageNumber;
		this.brand = brand;
		this.area = area;
		this.cardShop = cardShop;
	}
	
	public String getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(String pageNumber) {
		this.pageNumber = pageNumber;
	}

	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getCardShop() {
		return cardShop;
	}
	public void setCardShop(String cardShop) {
		this.cardShop = cardShop;
	}
    
}
