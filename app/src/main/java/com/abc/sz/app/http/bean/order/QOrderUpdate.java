package com.abc.sz.app.http.bean.order;

import java.util.List;


/**
 * 订单信息修改
 *
 * @author ftl
 */
public class QOrderUpdate {

	private String addressId;
	private String orderType;
    private List<QProduct> productList;
    
	public String getAddressId() {
		return addressId;
	}
	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public List<QProduct> getProductList() {
		return productList;
	}
	public void setProductList(List<QProduct> productList) {
		this.productList = productList;
	}
}
