package com.abc.sz.app.http.bean.personal;

/**
 * 消費列表
 *
 * @author ftl
 */
public class QConsumes {
    private String pageNum;
    private String startDate;
    private String endDate;

    public QConsumes(String startDate, String endDate) {
        super();
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public QConsumes(String pageNum, String startDate, String endDate) {
        super();
        this.pageNum = pageNum;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }


}
