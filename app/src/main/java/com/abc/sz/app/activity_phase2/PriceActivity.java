package com.abc.sz.app.activity_phase2;

import android.os.Bundle;
import android.webkit.WebView;

import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * 行情数据
 * @author ftl
 *
 */
@ContentView(R.layout.activity_calculator)
public class PriceActivity extends ABCActivity {
	
	@ViewInject(R.id.webView)
	private WebView webView;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		
    }
}
