package com.abc.sz.app.bean;

import com.forms.library.base.BaseBean;

/**
 * 评价
 *
 * @author ftl
 */
public class Evaluate extends BaseBean {

    /* 商品编号 */
    private String productId;
    /* 评价内容*/
    private String content;
    /* 评星数量*/
    private String starNum;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStarNum() {
        return starNum;
    }

    public void setStarNum(String starNum) {
        this.starNum = starNum;
    }

}
