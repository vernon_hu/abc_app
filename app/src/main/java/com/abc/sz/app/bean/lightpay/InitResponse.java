package com.abc.sz.app.bean.lightpay;


/**
 * 初始化响应
 * @author ftl
 *
 */
public class InitResponse{

	private String result_code; //响应码 0：成功，1：失败，2：冻结
    private String user_id;		//光id
    private String three_des;   //3DES密钥
    private String sequence_id; //256组序列号
    
	public String getResult_code() {
		return result_code;
	}
	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getThree_des() {
		return three_des;
	}
	public void setThree_des(String three_des) {
		this.three_des = three_des;
	}
	public String getSequence_id() {
		return sequence_id;
	}
	public void setSequence_id(String sequence_id) {
		this.sequence_id = sequence_id;
	}
    
}
