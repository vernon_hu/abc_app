package com.abc.sz.app.action_phase2;

import com.abc.sz.app.action.BaseAction;
import com.abc.sz.app.bean_phase2.ImageAD;
import com.abc.sz.app.bean_phase2.Omnibus;
import com.abc.sz.app.fragment_phase2.OmnibusFragment;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.product.QJxList;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.alibaba.fastjson.TypeReference;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;

import java.util.List;

/**
 * 精选Action
 *
 * @author hkj
 */
public class OmnibusAction extends BaseAction {

    // 广告请求
    public Http loadingAdv() {
        return APPRequestPhase2.advQuery("0", new ResponseNotify<List<ImageAD>>(
                new TypeReference<RespBean<List<ImageAD>>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<List<ImageAD>> response) {
                if(uiObject instanceof OmnibusFragment) {
                    List<ImageAD> list = response.getContent();
                    ((OmnibusFragment) uiObject).advSuccess(list);
                }
            }

            @Override
            public void onFailed(RetCode retCode) {
                if(uiObject instanceof OmnibusFragment) {
                    ((OmnibusFragment) uiObject).failed(retCode, "loadingAdv");
                }
            }
        });
    }

    /**
     * 精选快列表查询（原每日快报）
     *
     * @return
     */
    public Http loadingOmnibusList(int pageNum) {
        QJxList bean = new QJxList();
        bean.setPageNum(pageNum + "");
        RequestBean<QJxList> params = new RequestBean<QJxList>(bean);

        return APPRequestPhase2.jxListQuery(params, new ResponseNotify<List<Omnibus>>(
                new TypeReference<RespBean<List<Omnibus>>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<List<Omnibus>> response) {
                if(uiObject instanceof OmnibusFragment) {
                    List<Omnibus> list = response.getContent();
                    ((OmnibusFragment) uiObject).omnibusSuccess(list);
                }
            }

            @Override
            public void onFailed(RetCode retCode) {
                if(uiObject instanceof OmnibusFragment) {
                    ((OmnibusFragment) uiObject).failed(retCode, "loadingOmnibusList");
                }
            }
        });
    }

}
