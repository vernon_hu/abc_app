package com.abc.sz.app.bean;

import android.database.Cursor;

import com.abc.sz.app.util.AutoCompleteFilterObject;
import com.forms.library.base.BaseBean;
import com.forms.library.baseUtil.db.annotation.Column;
import com.forms.library.baseUtil.db.annotation.Id;
import com.forms.library.baseUtil.db.annotation.Table;
import com.forms.library.baseUtil.db.annotation.Unique;

/**
 * 搜索关键字
 *
 * @author ftl
 */
@Table(name = "searchKeywords")
public class SearchKeywords extends BaseBean implements AutoCompleteFilterObject {

    /* 搜索关键字名称 */
    @Unique
    @Column(column = "keywordsName")
    private String keywordsName;
    /* 搜索时间 */
    @Column(column = "searchTime")
    private String searchTime;

    public String getKeywordsName() {
        return keywordsName;
    }

    public void setKeywordsName(String keywordsName) {
        this.keywordsName = keywordsName;
    }

    public String getSearchTime() {
        return searchTime;
    }

    public void setSearchTime(String searchTime) {
        this.searchTime = searchTime;
    }

    @Override
    public String getSearchWord() {
        return keywordsName;
    }

    @Override
    public AutoCompleteFilterObject createObject(Cursor cursor) {
        if (!cursor.isClosed()) {
            SearchKeywords historyTag = new SearchKeywords();
            historyTag.keywordsName = cursor.getString(cursor.getColumnIndex("keywordsName"));
            historyTag.searchTime = cursor.getString(cursor.getColumnIndex("searchTime"));
            historyTag.id = cursor.getLong(cursor.getColumnIndex("id"));
            return historyTag;
        }
        return null;
    }
}
