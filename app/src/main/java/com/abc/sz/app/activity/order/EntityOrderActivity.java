package com.abc.sz.app.activity.order;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.OrderAction;
import com.abc.sz.app.bean.Order;
import com.abc.sz.app.fragment.order.EntityOrderFragment;
import com.abc.sz.app.util.DialogUtil;
import com.forms.base.ABCActivity;
import com.forms.base.ABCFragment;
import com.forms.library.baseUtil.net.Controller;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

/**
 * 实物订单
 *
 * @author ftl
 */
@ContentView(R.layout.activity_entity_order)
public class EntityOrderActivity extends ABCActivity {

    @ViewInject(R.id.tv_all_order)
    private TextView tv_all_order;
    @ViewInject(R.id.tv_wait_evaluate)
    private TextView tv_wait_evaluate;
    @ViewInject(R.id.tv_wait_harvest)
    private TextView tv_wait_harvest;
    @ViewInject(R.id.tv_wait_pay)
    private TextView tv_wait_pay;
    @ViewInject(R.id.fl_content)
    private FrameLayout fl_content;
    @ViewInject(R.id.appBar)
    private Toolbar toolbar;

    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    private List<EntityOrderFragment> fragmentList = new ArrayList<>();
    private EntityOrderFragment targFragment1;
    private EntityOrderFragment targFragment2;
    private EntityOrderFragment targFragment3;
    private EntityOrderFragment targFragment4;
    private int targFragment2Index;
    private int targFragment3Index;
    private int targFragment4Index;
    //当期选中的排序
    private int indexSelect = 0;
    //1表示来自实物订单
    //2表示来自票券订单
    private int fromPage = 1;
    //订单状态
    private String state = "";
    private OrderAction orderAction;
    private Order mOrderDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initData();
        initListener();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        orderAction = Controller.getInstance().getTargetAction(this, OrderAction.class);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fromPage = bundle.getInt("fromPage");
        }

        targFragment1 = new EntityOrderFragment();
        targFragment2 = new EntityOrderFragment();
        targFragment3 = new EntityOrderFragment();
        targFragment4 = new EntityOrderFragment();

        Bundle bundle1 = new Bundle();
        bundle1.putString("state", "");
        bundle1.putInt("fromPage", fromPage);
        targFragment1.setArguments(bundle1);

        Bundle bundle2 = new Bundle();
        bundle2.putString("state", OrderState.success.getCode());
        bundle2.putInt("fromPage", fromPage);
        targFragment2.setArguments(bundle2);

        Bundle bundle3 = new Bundle();
        bundle3.putString("state", fromPage == 1 ? OrderState.noGet.getCode() : OrderState.noUse.getCode());
        bundle3.putInt("fromPage", fromPage);
        targFragment3.setArguments(bundle3);

        Bundle bundle4 = new Bundle();
        bundle4.putString("state", OrderState.noPay.getCode());
        bundle4.putInt("fromPage", fromPage);
        targFragment4.setArguments(bundle4);

        fragmentManager = getSupportFragmentManager();
        addFragment(targFragment1);
        showFragment(indexSelect);

    }

    private void addFragment(EntityOrderFragment fragment) {
        transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.fl_content, fragment);
        transaction.commit();
        fragmentList.add(fragment);
    }

    /**
     * 切换Fragment
     */
    private void showFragment(int indexSelect) {
        for (int i = 0; i < fragmentList.size(); i++) {
            ABCFragment fragment = fragmentList.get(i);
            if (!fragment.isHidden()) {
                fragmentManager.beginTransaction().hide(fragment).commit();
            }
        }
        fragmentManager.beginTransaction().show(fragmentList.get(indexSelect)).commit();
    }

    private void initListener() {
        tv_all_order.setOnClickListener(new MyTextClickListener());
        tv_wait_evaluate.setOnClickListener(new MyTextClickListener());
        tv_wait_harvest.setOnClickListener(new MyTextClickListener());
        tv_wait_pay.setOnClickListener(new MyTextClickListener());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (fromPage == 2) {
//            tv_titleName.setText(getString(R.string.grzx_ticket_order));
            tv_wait_harvest.setText(getString(R.string.grzx_wait_consume));
        }
    }
//创建订单成功，是否现在支付。
    public void syncOrderStatus(final String orderId,String status) {
        if (OrderState.success.getCode().equals(status)) {
            //交易成功
            DialogUtil.showWithOneBtn(this, "当前订单已完成支付，点击确认后将刷新订单",
                    new DialogInterface.OnClickListener() {
                        @Override public void onClick(DialogInterface dialog, int which) {
                            fragmentList.get(indexSelect).onRefresh();
                        }
                    });
        } else {
            DialogUtil.showWithTwoBtn(this,
                    "提示",
                    "当前订单已失效，是否需要重新创建订单？",
                    "确定", "取消",
                    new DialogInterface.OnClickListener() {
                        @Override public void onClick(DialogInterface dialog, int which) {
                            Http http = orderAction.queryOrderDetails(orderId);
                            http.setOnRequestFinishListener(new Http.OnRequestFinishListener() {
                                @Override public void onFinish() {
                                    if(mOrderDetail!=null){
//                                        orderAction.createOrder();
                                    }
                                }
                            });
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override public void onClick(DialogInterface dialog, int which) {

                        }
                    });
        }
    }

    public void queryOrderDetail(Order content) {
        mOrderDetail = content;
    }

    public class MyTextClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            changeShow(v.getId());
            switch (v.getId()) {
                case R.id.tv_all_order:
                    indexSelect = 0;
                    state = "";
                    break;
                case R.id.tv_wait_evaluate:
                    if (!fragmentList.contains(targFragment2)) {
                        addFragment(targFragment2);
                        targFragment2Index = fragmentList.size() - 1;
                    }
                    indexSelect = targFragment2Index;
                    state = OrderState.success.getCode();
                    break;
                case R.id.tv_wait_harvest:
                    if (!fragmentList.contains(targFragment3)) {
                        addFragment(targFragment3);
                        targFragment3Index = fragmentList.size() - 1;
                    }
                    indexSelect = targFragment3Index;
                    state = fromPage == 1 ? OrderState.noGet.getCode() : OrderState.noUse.getCode();
                    break;
                case R.id.tv_wait_pay:
                    if (!fragmentList.contains(targFragment4)) {
                        addFragment(targFragment4);
                        targFragment4Index = fragmentList.size() - 1;
                    }
                    indexSelect = targFragment4Index;
                    state = OrderState.noPay.getCode();
                    break;
            }
            showFragment(indexSelect);
        }
    }

    /**
     * 改变显示
     *
     * @param id
     */
    public void changeShow(int id) {
        changeShow(tv_all_order, id == R.id.tv_all_order ? true : false);
        changeShow(tv_wait_evaluate, id == R.id.tv_wait_evaluate ? true : false);
        changeShow(tv_wait_harvest, id == R.id.tv_wait_harvest ? true : false);
        changeShow(tv_wait_pay, id == R.id.tv_wait_pay ? true : false);
    }

    /**
     * 改变显示
     *
     * @param view
     * @param isSelect
     */
    public void changeShow(TextView view, boolean isSelect) {
        if (isSelect) {
            view.setTextColor(getResources().getColor(R.color.white));
            view.setBackgroundResource(R.color.colorPrimary);
        } else {
            view.setTextColor(getResources().getColor(R.color.colorPrimary));
            view.setBackgroundResource(R.color.white);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_order_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.refresh) {
            fragmentList.get(indexSelect).onRefresh();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            targFragment1.onRefresh();
            if (requestCode == 1) {
                //刷新评价列表
                if (fragmentList.contains(targFragment2))
                    targFragment2.onRefresh();
            } else if (requestCode == 2) {
                onCommonRefresh();
            } else if (requestCode == 3) {
                //刷新收货列表
                if (fragmentList.contains(targFragment3))
                    targFragment3.onRefresh();
                //刷新支付列表
                if (fragmentList.contains(targFragment4))
                    targFragment4.onRefresh();
            }
        }
    }

    public void onRefresh() {
        targFragment1.onRefresh();
        onCommonRefresh();
    }

    public void onCommonRefresh() {
        if (OrderState.success.getCode().equals(state)) {
            //刷新评价列表
            if (fragmentList.contains(targFragment2))
                targFragment2.onRefresh();
        } else if (OrderState.noGet.getCode().equals(state)) {
            //刷新评价列表
            if (fragmentList.contains(targFragment2))
                targFragment2.onRefresh();
            //刷新收货列表
            if (fragmentList.contains(targFragment3))
                targFragment3.onRefresh();
        } else if (OrderState.noPay.getCode().equals(state)) {
            //刷新收货列表
            if (fragmentList.contains(targFragment3))
                targFragment3.onRefresh();
            //刷新支付列表
            if (fragmentList.contains(targFragment4))
                targFragment4.onRefresh();
        }
    }

    /**
     * 订单支付成功
     */
    public void paySuccess(String content) {
        Bundle bundle = new Bundle();
        bundle.putString("paymentyURL", content);
        callMeForBack(OrderAffirmPayActivity.class, bundle, 2);
    }


}
