package com.abc.sz.app.activity.order;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.ShoppingCarAction;
import com.abc.sz.app.activity.product.ProductTypeActivity;
import com.abc.sz.app.adapter.order.ShoppingCarAdapter;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.bean.ShoppingCar;
import com.abc.sz.app.util.ShoppingCarCount;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.net.ViewLoading.OnRetryListener;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.view.toast.MyToast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 购物车
 *
 * @author ftl
 */
@ContentView(R.layout.activity_shopping_car)
public class ShoppingCarActivity extends ABCActivity {
	
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.lv_product) ListView lv_product;
    @ViewInject(R.id.cb_choose) CheckBox cb_choose;
    @ViewInject(R.id.ll_have_product) LinearLayout ll_have_product;
    @ViewInject(R.id.ll_no_product) LinearLayout ll_no_product;
    @ViewInject(R.id.tv_countPrice) TextView tv_countPrice;
    @ViewInject(R.id.btn_account) Button btn_account;
    @ViewInject(R.id.loadingView) LoadingView loadingView;

    private ShoppingCarAdapter adapter;
    private List<ShoppingCar> list = new ArrayList<ShoppingCar>();
    private ShoppingCarAction shoppingCarAction;

    private MenuItem editMenuItem;
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            showMoneyAndCount();
            if (adapter.getMap().containsValue(false)) {
                cb_choose.setChecked(false);
            } else {
                cb_choose.setChecked(true);
            }
        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        
        initData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_edit_menu, menu);
        editMenuItem = menu.findItem(R.id.edit);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.edit) {
        	Bundle bundle = new Bundle();
        	bundle.putSerializable("shoppingCarList", (Serializable)list);
            callMeForBack(ShoppingCarEditActivity.class, bundle, 1);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * 初始化数据
     */
    private void initData() {
    	 shoppingCarAction = (ShoppingCarAction) controller.getAction(this, ShoppingCarAction.class);
    	 queryShoppingCar();
         loadingView.retry(new OnRetryListener() {
 			
 			@Override
 			public void retry() {
 				queryShoppingCar();
 			}
 		});
        cb_choose.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				 if (isChecked) {
	                    for (int i = 0; i < list.size(); i++) {
	                        adapter.getMap().put(i, true);
	                    }
	                } else {
	                    if (!adapter.getMap().containsValue(false)) {
	                        for (int i = 0; i < list.size(); i++) {
	                            adapter.getMap().put(i, false);
	                        }
	                    }
	                }
	                adapter.notifyDataSetChanged();
	                showMoneyAndCount();
				
			}
		});
//        cb_choose.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//            	 if (cb_choose.isChecked()) {
//                     for (int i = 0; i < list.size(); i++) {
//                         adapter.getMap().put(i, true);
//                     }
//                 } else {
//                     if (!adapter.getMap().containsValue(false)) {
//                         for (int i = 0; i < list.size(); i++) {
//                             adapter.getMap().put(i, false);
//                         }
//                     }
//                 }
//                 adapter.notifyDataSetChanged();
//                 showMoneyAndCount();
//            }
//        });
    }

    /**
     * 查询购物车成功
     */
    public void querySuccess(RetCode retCode, List<ShoppingCar> content) {
        loadingView.loaded();
        if (retCode != RetCode.noData && content != null && content.size() != 0) {
        	if(editMenuItem != null){
        		editMenuItem.setVisible(true);
        	}
            list = content;
            adapter = new ShoppingCarAdapter(this, list, handler, imageLoader);
            lv_product.setAdapter(adapter);
            cb_choose.setChecked(true);
        } else {
        	if(editMenuItem != null){
        		editMenuItem.setVisible(false);
        	}
            ll_have_product.setVisibility(View.GONE);
            ll_no_product.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 查询购物车失败
     *
     * @param retCode
     */
    public void queryFailed(RetCode retCode) {
    	if(editMenuItem != null){
    		editMenuItem.setVisible(false);
    	}
        loadingView.loaded();
        loadingView.failed(retCode.getRetMsg());
    }

    /**
     * 显示总金额与数量
     */
    public void showMoneyAndCount() {
        List<ShoppingCar> shoppingCarList = new ArrayList<ShoppingCar>();
        int count = 0;
        for (int i = 0; i < list.size(); i++) {
            if (adapter.getMap().get(i)) {//选中
                shoppingCarList.add(list.get(i));
                count += Integer.parseInt(list.get(i).getAmount());
            }
        }
        tv_countPrice.setText(ShoppingCarCount.countTotalMoney(shoppingCarList));
        if (count == 0) {
            btn_account.setText("结算");
        } else {
            btn_account.setText("结算(" + count + ")");
        }
    }

    @OnClick(value = {R.id.btn_account, R.id.btn_go_select})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_account:
                //没选中商品
                if (!adapter.getMap().containsValue(true)) {
                    MyToast.show(ShoppingCarActivity.this, "至少选择一个商品", MyToast.TEXT,
                            Gravity.CENTER, Toast.LENGTH_SHORT);
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("orderType", "1");
                    bundle.putString("sumPrice", tv_countPrice.getText().toString());
                    ArrayList<Product> productList = new ArrayList<Product>();
                    for (int i = 0; i < list.size(); i++) {
                        if (!adapter.getMap().get(i))
                            continue;
                        Product product = new Product();
                        product.setProductId(list.get(i).getProductId());
                        product.setNum(list.get(i).getAmount());
                        productList.add(product);
                    }
                    getCacheBean().putCache(productList);
                    callMeForBack(OrderAffirmActivity.class, bundle, 1);
                }
                break;
            case R.id.btn_go_select:
                callMe(ProductTypeActivity.class);
                break;
        }

    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	if(resultCode == RESULT_OK){
    		list.clear();
            btn_account.setText("结算");
            cb_choose.setChecked(false);
            tv_countPrice.setText(ShoppingCarCount.formatMoney(0));
            if(editMenuItem != null){
            	editMenuItem.setVisible(false);
            }
            queryShoppingCar();
    	}
    }
    
    public void queryShoppingCar(){
    	shoppingCarAction.queryShoppingCar().setLoadingView(loadingView).start();
    }

}
