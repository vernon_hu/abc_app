package com.abc.sz.app.http.bean.personal;

/**
 * Created by hwt on 14/11/4.
 */
public class RLogin {
    private String accessToken;
    private RUser User;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public RUser getUser() {
        return User;
    }

    public void setUser(RUser user) {
        User = user;
    }
}
