package com.abc.sz.app.http.bean.jf;

import com.forms.library.base.BaseBean;

import java.util.ArrayList;

public class JfBean extends BaseBean {

    //单位编码
    private String CompanyNum;
    //单位名称
    private String CompanyName;
    //缴款通知书号码
    private String NoticeNum;
    //通知书类别
    private String NoticeName;
    //通知书类型
    private String NoticeType;
    //缴费人单位名称
    private String PayManCompany;
    //应收总金额
    private String AccountPayable;
    //应收总滞纳金
    private String AcountPenalSum;
    //循环次数
    private String ListCount;
    //单位编码
    private ArrayList<JfContextBean> List;
    //备注
    private String Remark;
    //额外信息1
    private String Remark1;
    //额外信息2
    private String Remark2;

    public String getCompanyNum() {
        return CompanyNum;
    }

    public void setCompanyNum(String companyNum) {
        CompanyNum = companyNum;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getNoticeNum() {
        return NoticeNum;
    }

    public void setNoticeNum(String noticeNum) {
        NoticeNum = noticeNum;
    }

    public String getNoticeName() {
        return NoticeName;
    }

    public void setNoticeName(String noticeName) {
        NoticeName = noticeName;
    }

    public String getPayManCompany() {
        return PayManCompany;
    }

    public void setPayManCompany(String payManCompany) {
        PayManCompany = payManCompany;
    }

    public String getAccountPayable() {
        return AccountPayable;
    }

    public void setAccountPayable(String accountPayable) {
        AccountPayable = accountPayable;
    }

    public String getAcountPenalSum() {
        return AcountPenalSum;
    }

    public void setAcountPenalSum(String acountPenalSum) {
        AcountPenalSum = acountPenalSum;
    }

    public String getListCount() {
        return ListCount;
    }

    public void setListCount(String listCount) {
        ListCount = listCount;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getRemark1() {
        return Remark1;
    }

    public void setRemark1(String remark1) {
        Remark1 = remark1;
    }

    public String getRemark2() {
        return Remark2;
    }

    public void setRemark2(String remark2) {
        Remark2 = remark2;
    }

    public String getNoticeType() {
        return NoticeType;
    }

    public void setNoticeType(String noticeType) {
        NoticeType = noticeType;
    }

    public ArrayList<JfContextBean> getList() {
        return List;
    }

    public void setList(ArrayList<JfContextBean> list) {
        List = list;
    }


}
