package com.abc.sz.app.fragment.order;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.OrderAction;
import com.abc.sz.app.bean.Order;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCFragment;
import com.forms.library.base.BaseAdapter;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.net.ViewLoading.OnRetryListener;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;
import com.forms.view.pullToRefresh.PullToRefreshBase;
import com.forms.view.pullToRefresh.PullToRefreshListView;
import com.forms.view.toast.MyToast;

import java.util.ArrayList;
import java.util.List;

/**
 * 充值订单
 *
 * @author ftl
 */
public class PrepaidOrderFragment extends ABCFragment implements PullToRefreshBase.OnLastItemVisibleListener {

    @ViewInject(R.id.lv_all_order)
    private PullToRefreshListView lv_all_order;
    @ViewInject(R.id.loadingView)
    private LoadingView loadingView;

    private List<Order> list = new ArrayList<Order>();
    private BaseAdapter<Order> adapter;
    private OrderAction orderAction;
    private int pageNum = 0;
    private boolean isRefresh = false;
    private String state = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_order, container, false);
        return view;
    }


    @Override
    protected void initData() {
        super.initData();
        state = getArguments().getString("state");
        orderAction = (OrderAction) controller.getAction(baseActivity, this, OrderAction.class);
        lv_all_order.setOnLastItemVisibleListener(this);
        lv_all_order.setAdapter(adapter = new BaseAdapter<Order>(baseActivity, list, R.layout.layout_prepaid_phone_item) {

            @Override
            public void viewHandler(int position, Order order, View convertView) {
                if (order.getProductList() != null && order.getProductList().size() != 0) {
                    Product product = order.getProductList().get(0);
                    if (product != null) {
                        TextView tvOrderNum = ViewHolder.get(convertView, R.id.tvOrderNum);
                        TextView tvRechargeType = ViewHolder.get(convertView, R.id.tvRechargeType);
                        TextView tvRechargeNum = ViewHolder.get(convertView, R.id.tvRechargeNum);
                        TextView tvRechargeAgent = ViewHolder.get(convertView, R.id.tvRechargeAgent);
                        TextView tvRechargeMoney = ViewHolder.get(convertView, R.id.tvRechargeMoney);
                        TextView tvRechargeTime = ViewHolder.get(convertView, R.id.tvRechargeTime);
                        FormsUtil.setTextViewTxts(tvOrderNum, getString(R.string.orderNum), 
                        		order.getOrderId() == null ? "" : order.getOrderId());
                        String productName = product.getProductName();
                        if(productName == null){
                        	productName = "";
                        }else{
                        	if("FlowCharge".equals(productName)){
                        		productName = "流量充值";
                        		FormsUtil.setTextViewTxts(tvRechargeMoney, getString(R.string.rechargeMoney), 
                                		product.getFoo0() == null ? "" : product.getFoo0());
                        	}else{
                        		if("TalkCharge".equals(productName)){
                            		productName = "话费充值";
                            	}
                        		FormsUtil.setTextViewTxts(tvRechargeMoney, getString(R.string.rechargeMoney), 
                                		product.getPrice() == null ? "" : Integer.parseInt(product.getPrice())/100 + "元");
                        	}
                        }
                        FormsUtil.setTextViewTxts(tvRechargeType, getString(R.string.rechargeType), productName);
                        FormsUtil.setTextViewTxts(tvRechargeNum, getString(R.string.rechargeNum), 
                        		product.getIdentify() == null ? "" : product.getIdentify());
                        FormsUtil.setTextViewTxts(tvRechargeAgent, getString(R.string.rechargeAgent), 
                        		product.getAgentName() == null ? "" : product.getAgentName());
                        FormsUtil.setTextViewTxts(tvRechargeTime, getString(R.string.rechargeTime), 
                        		order.getPostTime() == null ? "" : order.getPostTime());
                    }
                }
            }

        });
        
        queryOrderList(false);
        loadingView.retry(new OnRetryListener() {
			
			@Override
			public void retry() {
				onRefresh();
			}
		});
    }


    @Override
    protected void initView() {
        super.initView();
    }


    @Override
    protected void initListener() {
        super.initListener();
    }

    public void queryOrderList(boolean isPullUp) {
    	Http http = orderAction.queryOrderList(pageNum, "4", state);
    	if(isPullUp){
    		http.start(false);
    	}else{
    		http.setLoadingView(loadingView).start();
    	}
    }

    @Override
    public void onLastItemVisible() {
        ++pageNum;
        queryOrderList(true);
    }

    public void onRefresh() {
        isRefresh = true;
        pageNum = 0;
        queryOrderList(false);
    }

    /**
     * 查询订单列表成功
     */
    public void querySuccess(RetCode retCode, List<Order> content) {
    	if (isRefresh) 
            list.clear();
        if (retCode != RetCode.noData && content != null && content.size() > 0) {
            list.addAll(content);
            adapter.notifyDataSetChanged();
        } else {
            if (list.size() > 0) {
                MyToast.showTEXT(baseActivity, getString(R.string.noMoreData));
            } else {
                loadingView.noData(getString(R.string.loadingNoData));
            }
        }
        isRefresh = false;
    }


    /**
     * 查询订单列表失败
     *
     * @param retCode
     */
    public void queryFailed(RetCode retCode) {
        if (list.size() > 0) {
            MyToast.showTEXT(baseActivity, retCode.getRetMsg());
        } else {
            loadingView.failed(retCode.getRetMsg());
        }
        isRefresh = false;
    }
}
