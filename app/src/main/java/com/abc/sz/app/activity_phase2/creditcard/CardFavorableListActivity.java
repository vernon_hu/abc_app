package com.abc.sz.app.activity_phase2.creditcard;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.CreditcardAction;
import com.abc.sz.app.bean.cridetcard.CardShop;
import com.abc.sz.app.util.ImageViewUtil;
import com.forms.base.ABCActivity;
import com.forms.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

/**
 * 信用卡优惠列表页面
 * 
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_cardfavorable_list)
public class CardFavorableListActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	@ViewInject(R.id.lvCardFavorable)
	private ListView lvCardFavorable;

	private BaseAdapter<CardShop> adapter;
	private List<CardShop> cardShopList = new ArrayList<CardShop>();
	private CreditcardAction creditcardAction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		creditcardAction = (CreditcardAction) controller.getAction(this, CreditcardAction.class);
		String searchText = cacheBean.getStringCache("searchText");
		creditcardAction.queryYOUShopList("", searchText).start();
		adapter = new BaseAdapter<CardShop>(this, cardShopList, R.layout.layout_phase2_cardfavorable_item) {

			@Override
			public void viewHandler(final int position, final CardShop t, View convertView) {
				LinearLayout lltItem = ViewHolder.get(convertView, R.id.lltItem);
				ImageView ivImgUrl = ViewHolder.get(convertView, R.id.ivImgUrl);
				TextView tvShopName = ViewHolder.get(convertView, R.id.tvShopName);
				TextView tvDistance = ViewHolder.get(convertView, R.id.tvDistance);
				TextView tvArea = ViewHolder.get(convertView, R.id.tvArea);
				TextView tvShopType = ViewHolder.get(convertView, R.id.tvShopType);
				
				if (t != null) {
					imageLoader.displayImage(t.getShopIconUrl(), ivImgUrl, ImageViewUtil.getOption());
					FormsUtil.setTextViewTxt(tvShopName, t.getShopName());
					FormsUtil.setTextViewTxt(tvDistance, t.getShopDistance());
					FormsUtil.setTextViewTxt(tvArea, t.getShopAddress());
					lltItem.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							cacheBean.put("shopID", t.getShopID());
							callMe(CardFavorableInfoActivity.class);
						}
					});
				}
			}
		};
		lvCardFavorable.setAdapter(adapter);
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_search_menu, menu);
        return true;
    }

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.search:
			callMe(CardFavorableSearchActivity.class);
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
     * 信用卡优惠商户列表查询成功回调
     */
    public void querySuccess(List<CardShop> list) {
        if (list != null && list.size() > 0) {
        	cardShopList.clear();
        	cardShopList.addAll(list);
            adapter.notifyDataSetChanged();
        }
    }
	
}
