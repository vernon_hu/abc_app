package com.abc.sz.app.action;

import java.util.List;

import com.abc.sz.app.activity.product.ProductInfoActivity;
import com.abc.sz.app.bean.product.ProductEvalutionBean;
import com.abc.sz.app.bean.product.ProductInfoBean;
import com.abc.sz.app.bean.product.TypeAttributeBean;
import com.abc.sz.app.fragment.product.ProductEvaluationFragment;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.product.QBzshInfo;
import com.abc.sz.app.http.bean.product.QEvaluationList;
import com.abc.sz.app.http.bean.product.QProductAttribute;
import com.abc.sz.app.http.bean.product.QProductInfo;
import com.abc.sz.app.http.bean.product.QShare;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.alibaba.fastjson.TypeReference;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;

/**
 * 商品详情Action
 *
 * @author llc
 */
public class ProductInfoAction extends BaseAction {

    /**
     * 商品详情
     *
     * @param params
     * @param isFirst
     * @return
     */
    public Http loadingProductInfo(RequestBean<QProductInfo> params, final boolean isFirst) {
        return APPRequestPhase2.productInfoQuery(params,
                new ResponseNotify<ProductInfoBean>(new TypeReference<RespBean<ProductInfoBean>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<ProductInfoBean> response) {
                        ((ProductInfoActivity) uiObject).querySuccess(response.getContent(), isFirst);
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        
                    }
                });
    }

    /**
     * 请求商品属性
     *
     * @param params
     * @return
     */
    public Http loadingAttribute(RequestBean<QProductAttribute> params) {
        return APPRequestPhase2.productAttributeQuery(params,
                new ResponseNotify<List<TypeAttributeBean>>(new TypeReference<RespBean<List<TypeAttributeBean>>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<List<TypeAttributeBean>> response) {
                        ((ProductInfoActivity) uiObject).queryAttributeSuccess(response.getContent());
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        ((ProductInfoActivity) uiObject).failed(retCode);
                    }
                });
    }

    /**
     * 请求商品详情图片列表
     *
     * @param params
     * @return
     */
    public Http loadingInfoPicture(RequestBean<QProductInfo> params) {
        return APPRequestPhase2.productInfoPictureQuery(params,
                new ResponseNotify<List<String>>(new TypeReference<RespBean<List<String>>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<List<String>> response) {
                        ((ProductInfoActivity) uiObject).queryProductInfoSuccess(response.getContent());
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        
                    }
                });

    }

    /**
     * 请求商品评论列表
     *
     * @param params
     * @return
     */
    public Http loadingEvaluation(RequestBean<QEvaluationList> params) {
        return APPRequestPhase2.evaluationQuery(params,
                new ResponseNotify<ProductEvalutionBean>(new TypeReference<RespBean<ProductEvalutionBean>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<ProductEvalutionBean> response) {
                        ((ProductInfoActivity) uiObject).queryEvaluationSuccess(response.getContent());
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        
                    }
                });
    }

    /**
     * 请求商品评论列表All
     *
     * @param params
     * @return
     */
    public Http loadingEvaluationAll(RequestBean<QEvaluationList> params) {
        return APPRequestPhase2.evaluationQuery(params,
                new ResponseNotify<ProductEvalutionBean>(new TypeReference<RespBean<ProductEvalutionBean>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<ProductEvalutionBean> response) {
                        ((ProductEvaluationFragment) uiObject).querySuccess(retCode, response.getContent());
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        ((ProductEvaluationFragment) uiObject).queryFailed(retCode);
                    }
                });
    }


    /**
     * 包装售后
     *
     * @param params
     * @return
     */
    public Http loadingBZSH(RequestBean<QBzshInfo> params) {
        return APPRequestPhase2.bzshQuery(params,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                          ((ProductInfoActivity) uiObject).queryBzshInfoSuccess(response.getContent());
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        
                    }
                });
    }


    /**
     * 分享
     *
     * @param params
     * @return
     */
    public Http loadingShare(RequestBean<QShare> params) {
        return APPRequestPhase2.shareProductInfo(params,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {

                    }

                    @Override
                    public void onFailed(RetCode retCode) {

                    }
                });
    }
}
