package com.abc.sz.app.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.view.zxing.util.ZXingUtil;

/**
 * Created by hwt on 14/11/6.
 */
@ContentView(R.layout.activity_inputcode)
public class InputCodeActivity extends ABCActivity {
    @ViewInject(R.id.codeImage) ImageView codeImage;
    @ViewInject(R.id.payCode) EditText payCode;
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @OnClick(R.id.toPay)
    public void onClick(View view) {
        if (TextUtils.isEmpty(payCode.getText())) {
            FormsUtil.setErrorHtmlTxt(payCode, "请输入支付码");
        } else {
            String text = payCode.getText().toString();
            Bitmap bitmap = ZXingUtil.createQRCodeBitmap(text, 300, 300);
            codeImage.setImageBitmap(bitmap);
        }
    }


}
