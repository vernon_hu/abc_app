package com.abc.sz.app.bean.product;

import com.abc.sz.app.bean.branch.BranchSearchRests;

import java.util.List;

/**
 * Created by monkey on 2015/8/13.
 */
public class BranchRests {
    private List<BranchSearchRests> BranchSearchRests;
    private long TotalCount;

    public List<com.abc.sz.app.bean.branch.BranchSearchRests> getBranchSearchRests() {
        return BranchSearchRests;
    }

    public void setBranchSearchRests(List<com.abc.sz.app.bean.branch.BranchSearchRests> branchSearchRests) {
        BranchSearchRests = branchSearchRests;
    }

    public long getTotalCount() {
        return TotalCount;
    }

    public void setTotalCount(long totalCount) {
        TotalCount = totalCount;
    }
}
