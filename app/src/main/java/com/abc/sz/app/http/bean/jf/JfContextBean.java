package com.abc.sz.app.http.bean.jf;


/**
 * 缴费列表明细
 *
 * @author llc
 */
public class JfContextBean {
    //收费项目编码
    private String FeeNum;
    //收费项目名称
    private String FeeName;
    //计费单位
    private String FeeCompany;
    //计费数量
    private String FeeCount;
    //收费标准
    private String FeeNorm;
    //应收金额
    private String FeeAcount;

    public String getFeeNum() {
        return FeeNum;
    }

    public void setFeeNum(String feeNum) {
        FeeNum = feeNum;
    }

    public String getFeeName() {
        return FeeName;
    }

    public void setFeeName(String feeName) {
        FeeName = feeName;
    }

    public String getFeeCompany() {
        return FeeCompany;
    }

    public void setFeeCompany(String feeCompany) {
        FeeCompany = feeCompany;
    }

    public String getFeeCount() {
        return FeeCount;
    }

    public void setFeeCount(String feeCount) {
        FeeCount = feeCount;
    }

    public String getFeeNorm() {
        return FeeNorm;
    }

    public void setFeeNorm(String feeNorm) {
        FeeNorm = feeNorm;
    }

    public String getFeeAcount() {
        return FeeAcount;
    }

    public void setFeeAcount(String feeAcount) {
        FeeAcount = feeAcount;
    }


}
