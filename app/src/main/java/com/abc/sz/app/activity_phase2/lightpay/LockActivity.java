package com.abc.sz.app.activity_phase2.lightpay;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.util.TimeUtil;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.widget.lockpattern.LockPatternView;
import com.forms.library.widget.lockpattern.external.PatternUtil;
import com.forms.library.widget.lockpattern.external.Point;

/**
 * Created by korey on 2015/5/11.
 */
@ContentView(R.layout.activity_lockpattern)
public class LockActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.patternView) LockPatternView lockPatternView;
    @ViewInject(R.id.tips) TextView tips;
    @ViewInject(R.id.forgetPswd) Button forgetPswd;

    private final static int CHECKPASSWORD = 0;
    private final static String ERRORCOLOR = "#F90808";
    private final static String CORRECTCOLOR = "#049681";
    private List<Point> mPattern = new ArrayList<>();
    private boolean isCreateLock = false;
    private final static int MAX_TRY_COUNT = 4;
    private final static int LOCKTIME = 1000*10;
    private int tryCount = MAX_TRY_COUNT;
    // 重置手势密码
    private boolean setPassword = false;
    // 修改手势密码
    private boolean updatePassword = false;
    // 用户手机号
    private String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lockPatternView.buildDrawables();
        lockPatternView.setPracticeMode(true);
        
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
        	setPassword = bundle.getBoolean("setPassword");
        	updatePassword = bundle.getBoolean("updatePassword");
        }
        phone = getBaseApp().getLoginUser().getPhone();
        String savePattern = PatternUtil.getPattern(LockActivity.this, phone);
        if (PatternUtil.isPattern(savePattern) && !setPassword) {
        	isCreateLock = false;
        	if (updatePassword) {
        		 toolbar.setTitle(R.string.updatePathPasswd);
        		 tips.setText(getString(R.string.msg_draw_old));
        	} else {
        		 toolbar.setTitle(R.string.patternUnlock);
        	}
            lockPatternView.setCurrentPattern(PatternUtil.parsePatterns(savePattern));
        } else {
        	isCreateLock = true;
            toolbar.setTitle(R.string.setPathPasswd);
            forgetPswd.setVisibility(View.GONE);
        }
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        lockPatternView.setOnCompleteListener(new LockPatternView.OnCompleteListener() {
            @Override
            public void onComplete(List<Point> pattern) {
                mPattern.clear();
                mPattern.addAll(pattern);

                if (mPattern.size() < 4) {
                    tips.setTextColor(Color.parseColor(ERRORCOLOR));
                    tips.setText(getString(R.string.msg_connect_4dots));
                    lockPatternView.not4Dot();
                    tryCount--;
                    return;
                }

                if (isCreateLock) {
                    tips.setText(getString(R.string.msg_redraw_pattern_to_confirm));
                } else {
                    tips.setText(getString(R.string.msg_draw_an_unlock_pattern));
                }
                List<Point> pointList = lockPatternView.getPattern();
                if (pointList != null && pointList.size() > 0) {
                    lockPatternView.testPracticePattern(new LockPatternView.OnTestListener() {
                        @Override
                        public void success() {
                            tips.setTextColor(Color.parseColor(CORRECTCOLOR));
                            if(updatePassword){
                            	updatePassword = false;
                            	isCreateLock = true;
                            	lockPatternView.setCurrentPattern(new ArrayList<Point>());
                            }else{
                            	 if(isCreateLock){
                                     tips.setText(getString(R.string.msg_your_new_unlock_pattern));
                                     PatternUtil.savePattern(LockActivity.this, phone, PatternUtil.patternToStr(mPattern));
                                 }else{
                                     tips.setText(getString(R.string.msg_pattern_recorded));
                                 }
                                 callMe(LightPayActivity.class);
                                 finish();
                            }
                        }


                        @Override
                        public void failed() {
                            tips.setTextColor(Color.parseColor(ERRORCOLOR));
                            if (isCreateLock) {
                                tips.setText(getString(R.string.msg_try_again));
                            } else {
                                if (tryCount == 5) {
                                    tips.setText(getString(R.string.msg_draw_error));
                                    tryCount--;
                                } else if (tryCount > 0) {
                                    tips.setText(MessageFormat.format(getString(R.string.msg_draw_error_count), tryCount--));
                                } else {
                                	countTime(LOCKTIME);
                                }
                            }
                        }

                        @Override
                        public void complete() {

                        }
                    });
                } else {
                    lockPatternView.setCurrentPattern(mPattern);
//                    PatternUtil.savePattern(LockActivity.this, PatternUtil.patternToStr(mPattern));
                }
            }
        });

//        if (PatternUtil.getMillisUntilFinished(this) > 0) {
//            long millisUntilFinished = LOCKTIME - new Date().getTime() - PatternUtil.getMillisUntilFinished(this);
//            countTime(millisUntilFinished);
//        }
    }

    private void countTime(long millisUntilFinished) {
    	lockPatternView.resetPracticesImmediate();
        lockPatternView.setPracticeMode(false);
        new CountDownTimer(millisUntilFinished, 1000) {

            public void onTick(long millisUntilFinished) {
                tips.setText(
                        TimeUtil.millisToStringMiddle(millisUntilFinished, false, true)
                                + "\n" +
                                getString(R.string.msg_try_later));
            }

            public void onFinish() {
                tryCount = MAX_TRY_COUNT;
                lockPatternView.setPracticeMode(true);
                String savePattern = PatternUtil.getPattern(LockActivity.this, phone);
                if (PatternUtil.isPattern(savePattern)) {
                    isCreateLock = false;
                    lockPatternView.setCurrentPattern(PatternUtil.parsePatterns(savePattern));
                }
                tips.setTextColor(Color.parseColor(CORRECTCOLOR));
                tips.setText(getString(R.string.msg_draw_an_unlock_pattern));
            }
        }.start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            long millisUntilFinished = new Date().getTime();
            PatternUtil.saveMillisUntilFinished(this, millisUntilFinished);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    
    @OnClick(value = {R.id.forgetPswd})
    public void OnClick(View view) {
    	switch(view.getId()){
    		case R.id.forgetPswd:
    			callMeForBack(CheckPasswrodActivity.class, CHECKPASSWORD);
    			break;
    	}
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	if (resultCode == Activity.RESULT_OK && requestCode == CHECKPASSWORD ) {
    		Bundle bundle = new Bundle();
    		bundle.putBoolean("setPassword", true);
    		callMe(LockActivity.class, bundle);
    		finish();
    	}
    }
}
