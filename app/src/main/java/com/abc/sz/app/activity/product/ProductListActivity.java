package com.abc.sz.app.activity.product;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.product.AttributeValueBean;
import com.abc.sz.app.fragment.product.AttributeSelectedFragment;
import com.abc.sz.app.fragment.product.ProductListFragment;
import com.abc.sz.app.http.bean.product.QProductList;
import com.abc.sz.app.view.MySlidingMenu;
import com.forms.base.ABCActivity;
import com.forms.base.ABCFragment;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.widget.slidingmenu.SlidingMenu;

/**
 * 商品列表
 *
 * @author llc
 */
@ContentView(R.layout.activity_product_list)
public class ProductListActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.fl_context) FrameLayout fl_context;
    @ViewInject(R.id.tv_normal) TextView tv_normal;// 默认
    @ViewInject(R.id.tv_price) TextView tv_price; // 价格
    @ViewInject(R.id.tv_sell) TextView tv_sell; // 销量
    @ViewInject(R.id.tv_good) TextView tv_good;// 好评率
    @ViewInject(R.id.fl_content) FrameLayout fl_content;
    // 参数
    public static String TYPE_ID = "TYPE_ID";
    // 类别名称
    public static String TYPE_NAME = "TYPE_NAME";
    // 类别名称
    public static String SEARCH_KEY = "SEARCH_KEY";
    // 搜索条件
    public static String SEARCH_CONDITIONS = "SEARCH_CONDITIONS";
    // 当前选中的排序
    private int indexSelect = 0;
    // 类型Id
    private String typeId;

    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    private List<ProductListFragment> fragmentList = new ArrayList<>();
    private ProductListFragment targFragment1 = new ProductListFragment();
    private ProductListFragment targFragment2 = new ProductListFragment();
    private ProductListFragment targFragment3 = new ProductListFragment();
    private ProductListFragment targFragment4 = new ProductListFragment();
    private int targFragment1Index;
    private int targFragment2Index;
    private int targFragment3Index;
    private int targFragment4Index;
    private QProductList qProductList = new QProductList();
    private MySlidingMenu menu;
    // 是否属于打开状态
    private boolean isOpen = false;
    private AttributeSelectedFragment attributeFragment;
    // 搜索关键字
    private String searchKey;
    private View searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.MATCH_PARENT,
                    Gravity.CENTER
            );
            View view = LayoutInflater.from(this).inflate(R.layout.layout_searchview, null);
            getSupportActionBar().setCustomView(view, params);
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            searchView = getSupportActionBar().getCustomView();
            searchView.setVisibility(View.VISIBLE);
            searchView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                	Bundle bundle = new Bundle();
                	bundle.putBoolean("isFromList", true);
                	callMeForBack(ProductSearchActivity.class, bundle, 1);
                }
            });
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        
        init();
        initListener();
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	if(resultCode == RESULT_OK && data != null){
    		Bundle bundle = data.getExtras();
    		if(bundle != null){
    			searchKey = bundle.getString(ProductListActivity.SEARCH_KEY);
    			qProductList.setKeyWord(searchKey);
    			qProductList.setTypeId(null);
    			qProductList.setAttribute(null);
    			attributeFragment.setTypeAttributeList(null);
    			onRefresh();
    		}
    	}
    }
    
    public void onRefresh(){
    	for (int i = 0; i < fragmentList.size(); i++) {
    		fragmentList.get(i).onRefresh();
		}
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_productlist_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.screen){
            if (isOpen)
                menu.showContent();
            else
                menu.showMenu();
        }
        return super.onOptionsItemSelected(item);
    }
    private void init() {
        typeId = getIntent().getStringExtra(TYPE_ID);
        searchKey = getIntent().getStringExtra(SEARCH_KEY);
        qProductList.setTypeId(typeId);
        qProductList.setKeyWord(searchKey);

        // 初始化筛选Fragment
        attributeFragment = new AttributeSelectedFragment();
        menu = new MySlidingMenu(this);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        // 设置从右侧滑出
        menu.setMode(SlidingMenu.RIGHT);
        menu.setShadowDrawable(R.drawable.shadowright);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        // 设置不可滑动出发
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, R.id.fl_context, SlidingMenu.SLIDING_CONTENT, false);
        menu.setMenu(R.layout.menu_frame);

        getSupportFragmentManager().beginTransaction().replace(R.id.menu_frame, attributeFragment).commit();
        Bundle bundle1 = new Bundle();
        bundle1.putString(SEARCH_CONDITIONS, "1");
        targFragment1.setArguments(bundle1);
        Bundle bundle2 = new Bundle();
        bundle2.putString(SEARCH_CONDITIONS, "2");
        targFragment2.setArguments(bundle2);
        Bundle bundle3 = new Bundle();
        bundle3.putString(SEARCH_CONDITIONS, "3");
        targFragment3.setArguments(bundle3);
        Bundle bundle4 = new Bundle();
        bundle4.putString(SEARCH_CONDITIONS, "4");
        targFragment4.setArguments(bundle4);
        fragmentManager = getSupportFragmentManager();
        changeViewColor(tv_normal.getId());
        addFragment(targFragment1);
        showFragment(indexSelect);
    }
    
    private void addFragment(ProductListFragment fragment){
   	 	transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.fl_content, fragment);
        transaction.commit();
        fragmentList.add(fragment);
    }
   
   /**
    * 切换Fragment
    */
   private void showFragment(int indexSelect) {
   		for (int i = 0; i < fragmentList.size(); i++) {
           ABCFragment fragment = fragmentList.get(i);
           if (!fragment.isHidden()) {
               fragmentManager.beginTransaction().hide(fragment).commit();
           }
       }
       fragmentManager.beginTransaction().show(fragmentList.get(indexSelect)).commit();
   }

    @OnClick(value = {R.id.tv_normal, R.id.tv_price, R.id.tv_sell, R.id.tv_good})
    public void btnOnClick(View view) {
    	changeViewColor(view.getId());
        switch (view.getId()) {
            case R.id.tv_normal:
            	if(!fragmentList.contains(targFragment1)){
           		 	addFragment(targFragment1);
           		 	targFragment1Index = fragmentList.size() + 1;
            	}
            	indexSelect = targFragment1Index;
                break;
            case R.id.tv_price:
            	if(!fragmentList.contains(targFragment2)){
           		 	addFragment(targFragment2);
           		 	targFragment2Index = fragmentList.size() - 1;
            	}
            	indexSelect = targFragment2Index;
                break;
            case R.id.tv_sell:
            	if(!fragmentList.contains(targFragment3)){
          		 	 addFragment(targFragment3);
              	     targFragment3Index = fragmentList.size() - 1;
            	}
            	indexSelect = targFragment3Index;
                break;
            case R.id.tv_good:
            	if(!fragmentList.contains(targFragment4)){
          		 	 addFragment(targFragment4);
              	     targFragment4Index = fragmentList.size() - 1;
            	}
            	indexSelect = targFragment4Index;
                break;
         }
         showFragment(indexSelect);
    }

    private void initListener() {

        menu.setOnOpenedListener(new SlidingMenu.OnOpenedListener() {

            @Override
            public void onOpened() {
                // TODO Auto-generated method stub
                isOpen = true;
            }
        });

        menu.setOnClosedListener(new SlidingMenu.OnClosedListener() {

            @Override
            public void onClosed() {
                // TODO Auto-generated method stub
                isOpen = false;
            }
        });
    }

    /**
     * 改变布局颜色
     *
     * @param index
     */
    private void changeViewColor(int id) {
    	changeViewColor(tv_normal, id == R.id.tv_normal ? true : false);
    	changeViewColor(tv_price, id == R.id.tv_price ? true : false);
    	changeViewColor(tv_sell, id == R.id.tv_sell ? true : false);
    	changeViewColor(tv_good, id == R.id.tv_good ? true : false);
    }
    
    private void changeViewColor(TextView textView, boolean isSelect) {
    	if(isSelect){
    		textView.setTextColor(getResources().getColor(R.color.colorPrimary));
    	}else{
    		textView.setTextColor(getResources().getColor(R.color.black));
    	}
    }

    public MySlidingMenu getMenu() {
        return menu;
    }

	public AttributeSelectedFragment getAttributeFragment() {
		return attributeFragment;
	}
	
	public QProductList getQProductList() {
		return qProductList;
	}

	public void setAttributeList(List<AttributeValueBean> attributeList) {
		if (attributeList != null){
	        qProductList.setAttribute(attributeList);
		}
	}
	
}
