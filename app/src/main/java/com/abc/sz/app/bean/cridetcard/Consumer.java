package com.abc.sz.app.bean.cridetcard;

/**
 * 消费
 * 
 * @author ftl
 */
public class Consumer {

	private String consumerID;// 消费账单id
	private String consumerDate;// 消费账单日期
	private String consumerMonery;// 消费账单金额

	public String getConsumerID() {
		return consumerID;
	}

	public void setConsumerID(String consumerID) {
		this.consumerID = consumerID;
	}

	public String getConsumerDate() {
		return consumerDate;
	}

	public void setConsumerDate(String consumerDate) {
		this.consumerDate = consumerDate;
	}

	public String getConsumerMonery() {
		return consumerMonery;
	}

	public void setConsumerMonery(String consumerMonery) {
		this.consumerMonery = consumerMonery;
	}

}
