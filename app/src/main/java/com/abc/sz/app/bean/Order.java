package com.abc.sz.app.bean;

import com.forms.library.base.BaseBean;

import java.util.List;

/**
 * 订单
 *
 * @author ftl
 */
public class Order extends BaseBean {

    /* 商户 ID*/
    private String SellerId;
    /* 商户名称*/
    private String SellerName;
    /* 订单 ID*/
    private String OrderId;
    /* 订单总额*/
    private Double amount;
    /* 订单总数*/
    private Double sumAmount;
    /* 订单商品列表*/
    private List<Product> ProductList;
    /* 用户留言*/
    private String postCode;
    /* 下单时间*/
    private String postTime;
    /* 付款时间*/
    private String payTime;
    /* 有效标志 0:无效,1:为有效*/
    private String Flag;
    /* 交易状态  99:交易失效 ,0:交易成功,1:未支付,2:未消费,3:待收货,4:待评价 5待审核*/
    private String State;
    /* 类型 1:商品类,2:业务类*/
    private int type;
    /* 订单描述*/
    private String mode;
    /* 商户订单ID*/
    private String sellerOrderId;

    public String getSellerId() {
        return SellerId;
    }

    public void setSellerId(String sellerId) {
        SellerId = sellerId;
    }

    public String getSellerName() {
        return SellerName;
    }

    public void setSellerName(String sellerName) {
        SellerName = sellerName;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public List<Product> getProductList() {
        return ProductList;
    }

    public void setProductList(List<Product> productList) {
        ProductList = productList;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getPostTime() {
        return postTime;
    }

    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public String getFlag() {
        return Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getSumAmount() {
        return sumAmount;
    }

    public void setSumAmount(Double sumAmount) {
        this.sumAmount = sumAmount;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getSellerOrderId() {
        return sellerOrderId;
    }

    public void setSellerOrderId(String sellerOrderId) {
        this.sellerOrderId = sellerOrderId;
    }
    
}
