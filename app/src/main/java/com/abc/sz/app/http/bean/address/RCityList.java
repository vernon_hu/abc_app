package com.abc.sz.app.http.bean.address;

import com.abc.sz.app.bean.branch.City;
import com.forms.library.baseUtil.net.RespBean;

import java.util.List;


/**
 * 城市列表响应
 *
 * @author ftl
 */
public class RCityList extends RespBean {

    private List<City> content;

    public List<City> getContent() {
        return content;
    }

    public void setContent(List<City> content) {
        this.content = content;
    }

}
