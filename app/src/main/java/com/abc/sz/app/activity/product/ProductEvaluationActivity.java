package com.abc.sz.app.activity.product;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.product.ProductEvalutionBean;
import com.abc.sz.app.fragment.product.ProductEvaluationFragment;
import com.abc.sz.app.http.bean.product.QEvaluationList;
import com.forms.base.ABCActivity;
import com.forms.base.ABCFragment;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;

/**
 * 商品评论详情页
 *
 * @author llc
 */
@ContentView(R.layout.activity_evluation)
public class ProductEvaluationActivity extends ABCActivity {
	
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.ll_all) LinearLayout ll_all;// 全部Item
    @ViewInject(R.id.ll_good) LinearLayout ll_good;// 好评Item
    @ViewInject(R.id.ll_mid) LinearLayout ll_mid;// 中评Item
    @ViewInject(R.id.ll_bad) LinearLayout ll_bad;// 差评Item
    @ViewInject(R.id.tv_all) TextView tv_all;// 全部
    @ViewInject(R.id.tv_good) TextView tv_good;// 好评
    @ViewInject(R.id.tv_mid) TextView tv_mid;// 中评
    @ViewInject(R.id.tv_bad) TextView tv_bad;// 差评
    @ViewInject(R.id.tv_all_count) TextView tv_all_count;// 全部数
    @ViewInject(R.id.tv_good_count) TextView tv_good_count;// 好评数
    @ViewInject(R.id.tv_mid_count) TextView tv_mid_count;// 中评数
    @ViewInject(R.id.tv_bad_count) TextView tv_bad_count; // 差评数

    public static String PRODUCT_ID = "PRODUCT_ID";
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    private List<ProductEvaluationFragment> fragmentList = new ArrayList<>();
    private ProductEvaluationFragment targFragment1 = new ProductEvaluationFragment();
    private ProductEvaluationFragment targFragment2 = new ProductEvaluationFragment();
    private ProductEvaluationFragment targFragment3 = new ProductEvaluationFragment();
    private ProductEvaluationFragment targFragment4 = new ProductEvaluationFragment();
    private int targFragment1Index;
    private int targFragment2Index;
    private int targFragment3Index;
    private int targFragment4Index;
    private QEvaluationList qEvaluationList = new QEvaluationList();
    // 产品ID
    private String productId;
    // 当前选中的排序
    private int indexSelect = 0;
    // 初始化界面
    private boolean isFirst = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
       
        initDate();
    }

    /**
     * 一次性初始化
     */
    private void initDate() {
        productId = getIntent().getStringExtra(PRODUCT_ID);
        qEvaluationList.setProductId(productId);
        qEvaluationList.setType("0");
        fragmentManager = getSupportFragmentManager();
        changeViewColor(ll_all.getId());
        addFragment(targFragment1);
        showFragment(indexSelect);
    }
    
    private void addFragment(ProductEvaluationFragment fragment){
   	 	transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.fl_content, fragment);
        transaction.commit();
        fragmentList.add(fragment);
    }
   
   /**
    * 切换Fragment
    */
   private void showFragment(int indexSelect) {
   		for (int i = 0; i < fragmentList.size(); i++) {
           ABCFragment fragment = fragmentList.get(i);
           if (!fragment.isHidden()) {
               fragmentManager.beginTransaction().hide(fragment).commit();
           }
       }
       fragmentManager.beginTransaction().show(fragmentList.get(indexSelect)).commit();
   }

    /**
     * 请求返回，更改界面
     *
     * @param index
     * @param bean
     */
    public void initView(ProductEvalutionBean bean) {
    	if(isFirst){
    		 tv_all_count.setText("(" + bean.getAllReviews() + ")");
    	     tv_good_count.setText("(" + bean.getGood() + ")");
    	     tv_mid_count.setText("(" + bean.getNormal() + ")");
    	     tv_bad_count.setText("(" + bean.getBad() + ")");
    	     isFirst = false;
    	}
    }

    @OnClick(value = {R.id.ll_all, R.id.ll_good, R.id.ll_mid, R.id.ll_bad})
    public void btnOnClick(View view) {
    	changeViewColor(view.getId());
        switch (view.getId()) {
            case R.id.ll_all:
            	if(!fragmentList.contains(targFragment1)){
           		 	addFragment(targFragment1);
           		 	targFragment1Index = fragmentList.size() - 1;
            	}
            	indexSelect = targFragment1Index;
                break;
            case R.id.ll_good:
            	if(!fragmentList.contains(targFragment2)){
           		 	addFragment(targFragment2);
           		 	targFragment2Index = fragmentList.size() - 1;
            	}
            	indexSelect = targFragment2Index;
                break;
            case R.id.ll_mid:
            	if(!fragmentList.contains(targFragment3)){
          		 	 addFragment(targFragment3);
              	     targFragment3Index = fragmentList.size() - 1;
            	}
            	indexSelect = targFragment3Index;
                break;
            case R.id.ll_bad:
            	if(!fragmentList.contains(targFragment4)){
          		 	 addFragment(targFragment4);
              	     targFragment4Index = fragmentList.size() + 1;
            	}
            	indexSelect = targFragment4Index;
                break;
         }
         qEvaluationList.setType(String.valueOf(indexSelect));
         showFragment(indexSelect);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_order_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.refresh) {
        	if(fragmentList.size() != 0){
        		fragmentList.get(indexSelect).onRefresh();
        	}
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * 改变布局颜色
     *
     * @param index
     */
    private void changeViewColor(int id) {
    	changeViewColor(tv_all, id == R.id.ll_all ? true : false);
    	changeViewColor(tv_good, id == R.id.ll_good ? true : false);
    	changeViewColor(tv_mid, id == R.id.ll_mid ? true : false);
    	changeViewColor(tv_bad, id == R.id.ll_bad ? true : false);
    }
    
    private void changeViewColor(TextView textView, boolean isSelect) {
    	if(isSelect){
    		textView.setTextColor(getResources().getColor(R.color.colorPrimary));
    	}else{
    		textView.setTextColor(getResources().getColor(R.color.black));
    	}
    }
    
	public QEvaluationList getqEvaluationList() {
		return qEvaluationList;
	}

}
