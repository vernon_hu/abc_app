package com.abc.sz.app.activity.product;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import android.graphics.Paint;
import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.ProductInfoAction;
import com.abc.sz.app.action.ProductOtherAction;
import com.abc.sz.app.activity.order.ShoppingCarActivity;
import com.abc.sz.app.adapter.ImageAdapter;
import com.abc.sz.app.adapter.product.AttributeItemAdapter;
import com.abc.sz.app.bean.Advertisement;
import com.abc.sz.app.bean.product.AttributeValueBean;
import com.abc.sz.app.bean.product.EvalutionBean;
import com.abc.sz.app.bean.product.ProductEvalutionBean;
import com.abc.sz.app.bean.product.ProductInfoBean;
import com.abc.sz.app.bean.product.TypeAttributeBean;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.product.QBzshInfo;
import com.abc.sz.app.http.bean.product.QEvaluationList;
import com.abc.sz.app.http.bean.product.QProductAttribute;
import com.abc.sz.app.http.bean.product.QProductInfo;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.util.ShoppingCarCount;
import com.abc.sz.app.view.CountPMView;
import com.abc.sz.app.view.LoadingView;
import com.abc.sz.app.view.ShareUtil;
import com.forms.base.ABCActivity;
import com.forms.base.XDRImageLoader;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.HttpQueue;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.view.toast.MyToast;
import com.forms.view.viewflow.CircleFlowIndicator;
import com.forms.view.viewflow.ViewFlow;

/**
 * 商品详情
 *
 * @author llc
 */
@ContentView(R.layout.activity_product_info)
public class ProductInfoActivity extends ABCActivity {

    public static String PRODUCT_ID = "PRODUCT_ID";
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.main) View main;//正文布局
    @ViewInject(R.id.loadingView) LoadingView loadingView;
    @ViewInject(R.id.tv_product_name) TextView tv_product_name;//商品名称
    @ViewInject(R.id.tv_price) TextView tv_price; //价格
    @ViewInject(R.id.tv_oldprice) TextView tv_oldprice; //原价
    @ViewInject(R.id.tv_description) TextView tv_description; //商品描述
    @ViewInject(R.id.tv_product_attribute_info) TextView tv_attribute;//属性值信息
    @ViewInject(R.id.iv_favorite) ImageView iv_favorite;
    @ViewInject(R.id.rl_picture) RelativeLayout rl_picture;//产品图片区
    @ViewInject(R.id.viewFlow) ViewFlow viewFlow;
    @ViewInject(R.id.viewflowindic) CircleFlowIndicator viewflowindic;
    @ViewInject(R.id.ll_evaluation) LinearLayout ll_evaluation;
    @ViewInject(R.id.tv_evaluation) TextView tv_evaluation;
    @ViewInject(R.id.rb_ratingbar) RatingBar rb_ratingbar;//评价等级
    @ViewInject(R.id.tv_date) TextView tv_date;//评价日期
    @ViewInject(R.id.tv_name) TextView tv_name;//评价姓名
    @ViewInject(R.id.tv_content) TextView tv_content;//评价内容
    @ViewInject(R.id.ll_bzsh) LinearLayout ll_bzsh;
    @ViewInject(R.id.tv_bzsh) TextView tv_bzsh; //商品描述
    @ViewInject(R.id.ll_picture_list) LinearLayout ll_picture_list;//图片详情
    @ViewInject(R.id.tvAddshop) TextView tvAddshop;

    private CountPMView pm_edit;

    //商品id
    private String productId;
    //收藏id
    private String favouriteId;
    //商品详情对象
    private ProductInfoBean bean;
    private ProductInfoAction piAction;
    //其他功能请求
    private ProductOtherAction otherAction;
    private ShareUtil shareUtil;
    private PopupWindow pop;
    private List<TypeAttributeBean> typeAttributeList;
    private List<AttributeValueBean> attributeList;
    private String groupId;
    private HttpQueue httpQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initData();
    }

    private void initData() {
        // TODO 数据初始化
        productId = getIntent().getStringExtra(PRODUCT_ID);

        LayoutParams layoutParams = viewFlow.getLayoutParams();
        layoutParams.height = FormsUtil.SCREEN_WIDTH;
        viewFlow.setLayoutParams(layoutParams);

        //设置请求商品id
        QProductInfo productInfo = new QProductInfo();
        productInfo.setProductId(productId);
        RequestBean<QProductInfo> params = new RequestBean<QProductInfo>(productInfo);
        if (getBaseApp().getLoginUser() != null) {
            params.setToken(getBaseApp().getLoginUser().getAccessToken());
        }
        piAction = (ProductInfoAction) controller.getAction(this, ProductInfoAction.class);
        otherAction = (ProductOtherAction) controller.getAction(this, ProductOtherAction.class);
        piAction.loadingProductInfo(params, true).setLoadingView(loadingView).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_product_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.shopCar) {
            if (isLogin()) {
                callMe(ShoppingCarActivity.class);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(value = {R.id.tv_share, R.id.ll_favorite, R.id.rl_product_attribute,
            R.id.tv_look_evaluation, R.id.tvAddshop, R.id.tvBuy,})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_share:
                //分享
                if (bean.getPictureUrlList() != null && bean.getPictureUrlList().size() > 0) {
                    if (shareUtil == null) {
                        shareUtil = new ShareUtil(this, main, bean.getProductName(), bean.getHisPrice(),
                                bean.getProductPrice(), null, bean.getImageUrl(), bean.getUrl(),
                                "1", bean.getProductId(), imageLoader);
                    }
                    shareUtil.showShareWindow();
                }
                break;
            case R.id.ll_favorite:
                //收藏
                if (isLogin()) {
                    if ("0".equals(favouriteId)) {
                        otherAction.addFavorite(productId).start(true);
                    } else {
                        otherAction.removeFavorite(favouriteId).start(true);
                    }
                }
                break;
            case R.id.rl_product_attribute:
                //属性选择
                if (pop == null) {
                    View popView = View.inflate(this, R.layout.layout_product_popwindow, null);
                    View footerView = View.inflate(this, R.layout.layout_product_popwindow_footer, null);
                    pop = new PopupWindow(popView, FormsUtil.SCREEN_WIDTH, FormsUtil.SCREEN_WIDTH, true);
                    pop.setAnimationStyle(R.style.popAnimation);
                    pop.setBackgroundDrawable(new PaintDrawable());
                    pop.setOutsideTouchable(true);
                    ListView lv_attribute = (ListView) popView.findViewById(R.id.lv_attribute);
                    pm_edit = (CountPMView) footerView.findViewById(R.id.pm_edit);
                    TextView tvAddshop = (TextView) popView.findViewById(R.id.tvAddshop);
                    TextView tvBuy = (TextView) popView.findViewById(R.id.tvBuy);
                    final AttributeItemAdapter itemAdapter = new AttributeItemAdapter(attributeList, typeAttributeList);
                    lv_attribute.addFooterView(footerView);
                    lv_attribute.setAdapter(itemAdapter);
                    tvAddshop.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (isLogin()) {
                                httpQueue = controller.getQueue(ProductInfoActivity.this);
                                QProductInfo productInfo = new QProductInfo();
                                productInfo.setGroupId(groupId);
                                productInfo.setAttribute(itemAdapter.getAttributeList());
                                RequestBean<QProductInfo> params = new RequestBean<QProductInfo>(productInfo);
                                if (getBaseApp().getLoginUser() != null) {
                                    params.setToken(getBaseApp().getLoginUser().getAccessToken());
                                }
                                httpQueue.addRequest(piAction.loadingProductInfo(params, false));
                                httpQueue.addRequest(otherAction.addShopingCar("", "", false));
                                httpQueue.start();
                            }
                        }
                    });
                    tvBuy.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
//                            double price = Double.parseDouble(bean.getProductPrice()) * pm_edit.getmCount();
                            otherAction.addShopingCar(productId, String.valueOf(pm_edit.getmCount()), true).start();
                        }
                    });
                }
                pop.showAtLocation(main, Gravity.BOTTOM, 0, 0);
                break;
            case R.id.tv_look_evaluation:
                //查看更多评论
                Bundle bundle = new Bundle();
                bundle.putString(ProductEvaluationActivity.PRODUCT_ID, productId);
                callMe(ProductEvaluationActivity.class, bundle);
                break;
            case R.id.tvAddshop:
                //加入购物车
                if (isLogin()) {
                    otherAction.addShopingCar(productId, "1", false).start();
                }
                break;
            case R.id.tvBuy:
                //立即购买
                if (isLogin()) {
                	otherAction.addShopingCar(productId, "1", true).start();
                }
                break;
        }
    }

    /**
     * 立即购买
     */
//    public void immediatelyBuy(String price, String num) {
//        Bundle bundle = new Bundle();
//        bundle.putString("sumPrice", price);
//        ArrayList<Product> productList = new ArrayList<Product>();
//        Product product = new Product();
//        product.setProductId(bean.getProductId());
//        product.setProductName(bean.getProductName());
//        product.setNum(num);
//        productList.add(product);
//        getCacheBean().putCache(productList);
//        callMe(OrderAffirmActivity.class, bundle);
//    }

    /**
     * 收藏成功
     */
    public void addSuccess(String response) {
        MyToast.showTEXT(this, "收藏成功");
        iv_favorite.setImageResource(R.drawable.icon_collection_selected);
        favouriteId = response;
    }

    /**
     * 删除收藏
     */
    public void removeSuccess() {
        MyToast.showTEXT(this, "取消收藏成功");
        iv_favorite.setImageResource(R.drawable.icon_collection_default);
        favouriteId = "0";
    }

    /**
     * 添加购物车
     */
    public void addShopingCarSuccess(boolean immediatelyBuy) {
    	if(immediatelyBuy){
    		callMe(ShoppingCarActivity.class);
    	}else{
    		MyToast.showTEXT(this, "添加成功，在购物车等亲~");
    	}
    }


    /**
     * 商品详情请求成功回调
     */
    public void querySuccess(ProductInfoBean bean, boolean isFirst) {
        if (bean != null) {
            this.bean = bean;
            this.productId = bean.getProductId();
            if (pm_edit != null) {
                Http addShopingCar = otherAction.addShopingCar(productId, 
                		String.valueOf(pm_edit.getmCount()), false);
                httpQueue.replaceHttp(1, addShopingCar);
            }
            this.groupId = bean.getGroupId();
            if (isFirst) {
                List<Advertisement> mAdvList = new ArrayList<Advertisement>();
                for (int i = 0; i < bean.getPictureUrlList().size(); i++) {
                    Advertisement advertisement = new Advertisement();
                    advertisement.setImgUrl(bean.getPictureUrlList().get(i));
                    advertisement.setAttribute(bean.getUrl());
                    mAdvList.add(advertisement);
                }
                ImageAdapter imageAdapter = new ImageAdapter(mAdvList, this, imageLoader);
                viewFlow.setAdapter(imageAdapter, 0);
                viewFlow.setFlowIndicator(viewflowindic);

                FormsUtil.setTextViewTxt(tv_product_name, bean.getProductName());
                FormsUtil.setTextViewTxt(tv_price, ShoppingCarCount.formatMoney(bean.getProductPrice()));
                FormsUtil.setTextViewTxt(tv_oldprice, MessageFormat.format(tv_oldprice.getText().toString(),
                        TextUtils.isEmpty(bean.getHisPrice()) ? "-" : ShoppingCarCount.formatMoney(bean.getHisPrice())));
                tv_oldprice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                FormsUtil.setTextViewTxt(tv_description, bean.getDescription());
                favouriteId = bean.getGz();
                if (!"0".equals(bean.getGz())) {
                    iv_favorite.setImageResource(R.drawable.icon_collection_selected);
                }
                String value = "";
                for (AttributeValueBean vb : bean.getAttributeList()) {
                    value += vb.getValue() + " ";
                }
                attributeList = bean.getAttributeList();
                FormsUtil.setTextViewTxt(tv_attribute, value);

                //查询商品属性
                if (attributeList != null && attributeList.size() != 0) {
                    QProductAttribute attribute = new QProductAttribute();
                    attribute.setProductId(bean.getProductId());
                    attribute.setType(String.valueOf(0));
                    RequestBean<QProductAttribute> attributeParams = new RequestBean<QProductAttribute>(attribute);
                    piAction.loadingAttribute(attributeParams).start(false);
                }
                //查询评价
                QEvaluationList evaluationBean = new QEvaluationList();
                evaluationBean.setProductId(productId);
                evaluationBean.setPageNum("0");
                evaluationBean.setType("0");
                RequestBean<QEvaluationList> evaluationParams = new RequestBean<QEvaluationList>(evaluationBean);
                piAction.loadingEvaluation(evaluationParams).start(false);
                //查询包装售后
                QBzshInfo qBzshInfo = new QBzshInfo();
                qBzshInfo.setProductId(productId);
                RequestBean<QBzshInfo> bzshInfoParams = new RequestBean<QBzshInfo>(qBzshInfo);
                piAction.loadingBZSH(bzshInfoParams).setLoadingView(loadingView).start();
                //查询图片详情
                QProductInfo productInfo = new QProductInfo();
                productInfo.setProductId(productId);
                RequestBean<QProductInfo> pictureParams = new RequestBean<QProductInfo>(productInfo);
                piAction.loadingInfoPicture(pictureParams).start(false);
            }
        }
    }

    /**
     * TODO:商品属性查询请求回调
     */
    public void queryAttributeSuccess(List<TypeAttributeBean> bean) {
        if (bean != null) {
            typeAttributeList = bean;
        }
    }

    /**
     * 请求失败回调
     *
     * @param retCode
     */
    public void failed(RetCode retCode) {
    	MyToast.showTEXT(this, retCode.getRetMsg());
    }

    /**
     * TODO:商品评价查询请求成功
     */
    public void queryEvaluationSuccess(ProductEvalutionBean bean) {
        List<EvalutionBean> list = bean.getOrderAppraiseDetailList();
        if (list != null && list.size() != 0) {
            EvalutionBean evalutionBean = list.get(0);
            if (evalutionBean != null) {
                FormsUtil.setTextViewTxt(tv_evaluation, MessageFormat.format(tv_evaluation.getText().toString(),
                        String.valueOf(list.size())));
                float score = Float.parseFloat(evalutionBean.getScore());
                rb_ratingbar.setRating(score);
                FormsUtil.setTextViewTxt(tv_date, evalutionBean.getDateTime());
                FormsUtil.setTextViewTxt(tv_name, evalutionBean.getUserName());
                FormsUtil.setTextViewTxt(tv_content, evalutionBean.getContent());
            }
            ll_evaluation.setVisibility(View.VISIBLE);
        } 
    }

    /**
     * TODO:商品包装售后查询请求成功
     */
    public void queryBzshInfoSuccess(String bean) {
    	ll_bzsh.setVisibility(View.VISIBLE);
        FormsUtil.setTextViewTxt(tv_bzsh, bean);
    }

    /**
     * TODO:商品图片列表查询请求成功
     */
    public void queryProductInfoSuccess(List<String> list) {
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ImageView iv = new ImageView(this);
                iv.setScaleType(ScaleType.FIT_XY);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 300);
                layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
                layoutParams.topMargin = 10;
                iv.setLayoutParams(layoutParams);
                imageLoader.displayImage(list.get(i), iv, ImageViewUtil.getOption(), 1);
                ll_picture_list.addView(iv);
            }
        }
    }

    public String getProductId() {
        return productId;
    }

    public XDRImageLoader getImageLoader() {
        return imageLoader;
    }

}
