package com.abc.sz.app.http.bean.card;

/**
 * 汽车分期贷款额度
 * 
 * @author ftl
 */
public class QCarLoanCommitment {

	private String brand;//意向品牌
    private String carPrice;//意向车价
    private String phone;//手机号码
    private String checkCode;//验证码
    private String agreedToCall;//是否愿意客户经理联系
    
    public QCarLoanCommitment(){}
    
	public QCarLoanCommitment(String brand, String carPrice, String phone,
			String checkCode, String agreedToCall) {
		super();
		this.brand = brand;
		this.carPrice = carPrice;
		this.phone = phone;
		this.checkCode = checkCode;
		this.agreedToCall = agreedToCall;
	}



	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getCarPrice() {
		return carPrice;
	}

	public void setCarPrice(String carPrice) {
		this.carPrice = carPrice;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	public String getAgreedToCall() {
		return agreedToCall;
	}

	public void setAgreedToCall(String agreedToCall) {
		this.agreedToCall = agreedToCall;
	}
	
}
