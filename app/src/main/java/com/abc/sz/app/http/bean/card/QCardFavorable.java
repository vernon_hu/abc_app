package com.abc.sz.app.http.bean.card;

/**
 * 信用卡特优惠
 * 
 * @author ftl
 */
public class QCardFavorable {

    private String idCardNum;//身份证号码后六位
    private String cardNum;//卡号码后7位
    
    public QCardFavorable(){}
    
	public QCardFavorable(String idCardNum, String cardNum) {
		super();
		this.idCardNum = idCardNum;
		this.cardNum = cardNum;
	}
	
	public String getIdCardNum() {
		return idCardNum;
	}
	public void setIdCardNum(String idCardNum) {
		this.idCardNum = idCardNum;
	}
	public String getCardNum() {
		return cardNum;
	}
	public void setCardNum(String cardNum) {
		this.cardNum = cardNum;
	}
    
}
