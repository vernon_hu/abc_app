package com.abc.sz.app.bean;

import com.abc.sz.app.bean.product.AttributeValueBean;
import com.forms.library.base.BaseBean;

import java.util.List;

/**
 * 商品
 *
 * @author ftl
 */
public class Product extends BaseBean {
    /* 商品编号 */
    private String productId;
    /* 商品类别 */
    private String typeId;
    /* 商品类型 1、商户 2、自营 */
    private String mode;
    /* 商品描述 */
    private String description;
    /* 所属商户id */
    private String sellerId;
    /* 所属商户名称*/
    private String sellerName;

    /* 商品名称 */
    private String productName;
    /* 图片url */
    private String imageUrl;
    /* 商品价格*/
    private String productPrice;
    /* 历史价格*/
    private String hisPrice;
    /* 商品数量*/
    private String num;
    /* 优惠列表*/
    private List<Discount> list;
    //私有属性列表
    private List<AttributeValueBean> AttributeList;
    //商品小图
    private String smallImgUrl;
    //接入商户商品的详情页面 URL
    private String Url;
    //留言
    private String postCode;
    //商品种类
    private String commodityType;
    //充值账号
    private String identify;
    //充值单位
    private String agentName;
    //话费值
    private String price;
    //流量值
    private String foo0;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getHisPrice() {
        return hisPrice;
    }

    public void setHisPrice(String hisPrice) {
        this.hisPrice = hisPrice;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public List<Discount> getList() {
        return list;
    }

    public void setList(List<Discount> list) {
        this.list = list;
    }

    public List<AttributeValueBean> getAttributeList() {
        return AttributeList;
    }

    public void setAttributeList(List<AttributeValueBean> attributeList) {
        AttributeList = attributeList;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getSmallImgUrl() {
        return smallImgUrl;
    }

    public void setSmallImgUrl(String smallImgUrl) {
        this.smallImgUrl = smallImgUrl;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getCommodityType() {
		return commodityType;
	}

	public void setCommodityType(String commodityType) {
		this.commodityType = commodityType;
	}

	public String getIdentify() {
		return identify;
	}

	public void setIdentify(String identify) {
		this.identify = identify;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getFoo0() {
		return foo0;
	}

	public void setFoo0(String foo0) {
		this.foo0 = foo0;
	}
    
}
