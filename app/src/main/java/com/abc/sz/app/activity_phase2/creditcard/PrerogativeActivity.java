package com.abc.sz.app.activity_phase2.creditcard;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;

import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * 优特权页面
 * 
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_prerogative)
public class PrerogativeActivity extends ABCActivity {

	@ViewInject(R.id.appBar) Toolbar toolbar;
//	@ViewInject(R.id.etCertNo) EditText etCertNo;
//	@ViewInject(R.id.etCardNo) EditText etCardNo;
	@ViewInject(R.id.webView) WebView webView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			if(webView != null && webView.canGoBack()){
				webView.goBack();
				return false;
			}
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		if(webView != null && webView.canGoBack()){
			webView.goBack();
		}else{
			super.onBackPressed();
		}
	}

	
//	@OnClick(R.id.btnQuery)
//	public void onClick(View v) {
//		switch (v.getId()) {
//		case R.id.btnQuery:
//			String idCardNum = etCertNo.getText().toString();
//			String cardNum = etCardNo.getText().toString();
//			if("".equals(idCardNum) || "".equals(cardNum)){
//				return;
//			}
//			cacheBean.put("idCardNum", idCardNum);
//			cacheBean.put("cardNum", cardNum);
//			callMe(MyPrerogativeActivity.class);
//			break;
//		}
//	}
	
}
