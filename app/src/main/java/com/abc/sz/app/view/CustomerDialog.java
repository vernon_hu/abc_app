package com.abc.sz.app.view;

import android.support.v7.app.AlertDialog;
import android.content.Context;
import android.graphics.Point;
import android.view.*;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.abc.ABC_SZ_APP.R;
import com.forms.library.tools.ScreenUtils;

import java.util.List;

/**
 * Created by korey on 2015/5/15.
 */
public class CustomerDialog {
    private static CustomerDialog mCustomerDialog;
    private Context mContext;
    private View mDialogView;

    private CustomerDialog(Context mContext, View dialogview) {
        this.mContext = mContext;
        mDialogView = dialogview;
    }

    public static CustomerDialog getInstance(Context context) {
        if (mCustomerDialog == null) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_customer_dialog, null);
            mCustomerDialog = new CustomerDialog(context, view);
        }
        return mCustomerDialog;
    }

    public static CustomerDialog getInstance(Context context, View dialogView) {
        if (mCustomerDialog == null) {
            mCustomerDialog = new CustomerDialog(context, dialogView);
        }
        return mCustomerDialog;
    }

    public static CustomerDialog getInstance(Context context, int layoutResId) {
        if (mCustomerDialog == null) {
            View view = LayoutInflater.from(context).inflate(layoutResId, null);
            mCustomerDialog = new CustomerDialog(context, view);
        }
        return mCustomerDialog;
    }

    public static void resetDialogView(View dialogView) {
        if (mCustomerDialog != null) {
            mCustomerDialog.mDialogView = dialogView;
        }
    }

    public static void resetDialogView(int layoutResId) {
        if (mCustomerDialog != null) {
            mCustomerDialog.mDialogView = LayoutInflater.from(mCustomerDialog.mContext)
                    .inflate(layoutResId, null);
        }
    }

    public AlertDialog buildOptionsDialog(List<String> optionsList) {
        if (optionsList != null) {
            Point point = new Point();
            WindowManager windowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            windowManager.getDefaultDisplay().getSize(point);

            RadioGroup radioGroup = (RadioGroup) mDialogView.findViewById(R.id.itemOptions);
            for (String option : optionsList) {
                RadioButton radioButton = new RadioButton(mContext);
                radioButton.setButtonDrawable(R.drawable.shape_button_whiteyellow);
                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.height = (int) ScreenUtils.dpToPx(mContext, 48);
                radioButton.setLayoutParams(layoutParams);
                radioButton.setText(option);
                radioGroup.addView(radioButton);
            }

            AlertDialog dialog = new AlertDialog.Builder(mContext).create();
            dialog.setView(mDialogView);
            Window window = dialog.getWindow();
            window.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
            window.setWindowAnimations(R.style.SlideButtonAnimation);  //添加动画
            WindowManager.LayoutParams lp = window.getAttributes();

            lp.width = point.x;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

            return dialog;
        }
        return new AlertDialog.Builder(mContext).create();
    }

}
