package com.abc.sz.app.bean_phase2;

/**
 * 存贷通贷款账号
 * 
 * @author hkj
 */
public class DLLoanAmount {

	private String cardId; // 卡ID
	private String cardNum; // 卡号
	private String loanAmount; // 贷款金额
	private boolean isSelect; // 是否选中

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getCardNum() {
		return cardNum;
	}

	public void setCardNum(String cardNum) {
		this.cardNum = cardNum;
	}

	public String getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}

	public boolean isSelect() {
		return isSelect;
	}

	public void setSelect(boolean isSelect) {
		this.isSelect = isSelect;
	}

}
