package com.abc.sz.app.fragment.product;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.ProductListAction;
import com.abc.sz.app.activity.product.ProductListActivity;
import com.abc.sz.app.adapter.product.ProductListAdapter;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.bean.product.ProductListBean;
import com.abc.sz.app.bean.product.TypeAttributeBean;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.product.QProductAttribute;
import com.abc.sz.app.http.bean.product.QProductList;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCFragment;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.view.pullToRefresh.PullToRefreshBase;
import com.forms.view.pullToRefresh.PullToRefreshListView;
import com.forms.view.toast.MyToast;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品列表
 *
 * @author ftl
 */
public class ProductListFragment extends ABCFragment implements PullToRefreshBase.OnLastItemVisibleListener {

    @ViewInject(R.id.lv_product_list)
    private PullToRefreshListView lv_product_list;
    @ViewInject(R.id.loadingView)
    private LoadingView loadingView;

    private List<Product> list = new ArrayList<Product>();
    private ProductListAdapter adapter;
    private ProductListAction plAction;
    private QProductList qProductList;
    private AttributeSelectedFragment attributeFragment;
    private int pageNum = 0;
    private String conditions = "";
    private boolean isRefresh = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_list, container, false);
    }


    @Override
    protected void initData() {
        super.initData();
        conditions = getArguments().getString(ProductListActivity.SEARCH_CONDITIONS);
        qProductList = ((ProductListActivity)baseActivity).getQProductList();
        plAction = (ProductListAction) controller.getAction(baseActivity, this, ProductListAction.class);
        attributeFragment = ((ProductListActivity)baseActivity).getAttributeFragment();
        
        lv_product_list.setOnLastItemVisibleListener(this);
        adapter = new ProductListAdapter(baseActivity.getImageLoader(), list);
        lv_product_list.setAdapter(adapter);

        queryProductList(false);
    }


    @Override
    protected void initView() {
        super.initView();
    }


    @Override
    protected void initListener() {
        super.initListener();
    }

    public void queryProductList(boolean isPullUp) {
    	qProductList.setPageNum(String.valueOf(pageNum));
    	qProductList.setConditions(conditions);
    	RequestBean<QProductList> params = new RequestBean<QProductList>(qProductList);
    	Http http = plAction.loadingProductList(params);
    	if(isPullUp){
    		http.start(false);
    	}else{
    		http.setLoadingView(loadingView).start();
    	}
    }

    @Override
    public void onLastItemVisible() {
    	++pageNum;
    	queryProductList(true);
    }
    
    public void onRefresh() {
        isRefresh = true;
        pageNum = 0;
        queryProductList(false);
    }

    /**
     * 查询产品列表成功
     */
    public void querySuccess(RetCode retCode, List<ProductListBean> content) {
    	if (isRefresh) 
             list.clear();
        if (retCode != RetCode.noData && content != null && content.size() > 0) {
        	if(attributeFragment.getTypeAttributeList().size() == 0){
	        	// 发送属性请求
	            QProductAttribute attribute = new QProductAttribute();
	            attribute.setProductId(content.get(0).getTypeList().get(0).getProductId());
	            // 共有属性
	            attribute.setType("1");
	            RequestBean<QProductAttribute> params = new RequestBean<QProductAttribute>(attribute);
	            plAction.loadingAttribute(params).setLoadingView(attributeFragment.getLoadingView()).start();
        	}
        	
            for (int i = 0; i < content.size(); i++) {
                 ProductListBean bean = content.get(i);
                 for (Product product : bean.getTypeList()) {
                	 list.add(product);
                 }
            }
            adapter.notifyDataSetChanged();
        } else {
            if (list.size() > 0) {
                MyToast.showTEXT(baseActivity, getString(R.string.noMoreData));
            } else {
                loadingView.noData(getString(R.string.loadingNoData));
            }
        }
        isRefresh = false;
    }

    /**
     * 查询产品属性列表成功
     */
    public void queryAttributeSuccess(List<TypeAttributeBean> list) {
    	if (list != null && list.size() > 0) {
    		attributeFragment.setTypeAttributeList(list);
    	}else{
    		loadingView.noData(getString(R.string.loadingNoData));
    	}
    }

    /**
     * 查询列表失败
     *
     * @param retCode
     */
    public void queryFailed(RetCode retCode) {
        if (list.size() > 0) {
            MyToast.showTEXT(baseActivity, retCode.getRetMsg());
        } else {
            loadingView.failed(retCode.getRetMsg());
        }
        isRefresh = false;
    }
}
