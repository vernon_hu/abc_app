package com.abc.sz.app.activity.personal;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.UserAction;
import com.abc.sz.app.activity.AddressListActivity;
import com.abc.sz.app.http.bean.personal.RUserScore;
import com.abc.sz.app.util.DialogUtil;
import com.forms.base.ABCActivity;
import com.forms.base.LoginUser;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.view.toast.MyToast;

/**
 * Created by hwt on 14/11/10.
 * 个人中心管理页面
 */
@ContentView(R.layout.activity_myinfo)
public class MyInfoActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.ivPhoto) ImageView ivPhoto;//头像
    @ViewInject(R.id.tvMyPhone) TextView tvMyPhone;//帐号
    @ViewInject(R.id.tvMyNickName) TextView tvMyNickName;//呢称
    @ViewInject(R.id.myScore) TextView myScore;//积分

    private static final int UPATE_PASSWORD = 1;
    private LoginUser loginUser;
    private UserAction userAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        userAction = (UserAction) controller.getAction(this, UserAction.class);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.activity_myinfo_menu, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.sign && isLogin()) {
//            userAction.checkin().start();
//        }
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
        userAction.queryUserScore().start();
    }

    private void initData() {
        loginUser = getBaseApp().getLoginUser();
        if(loginUser != null){
	        String gender = loginUser.getGender();
	        String phone = loginUser.getPhone();
	        String nickName = loginUser.getNickName();
	        if (gender == null || "1".equals(gender)) {
	            gender = getString(R.string.gender_man);
	        } else {
	            gender = getString(R.string.gender_woman);
	        }
	        
	        if(phone != null && !"".equals(phone)){
	        	phone = phone.substring(0, 3) + "****" + phone.substring(7);
	        }
	        FormsUtil.setTextViewTxts(tvMyPhone, phone);
	        if(nickName != null && !"".equals(nickName)){
	        	tvMyNickName.setVisibility(View.VISIBLE);
	        	FormsUtil.setTextViewTxts(tvMyNickName, getString(R.string.myinfo_tvnickname), nickName);
	        }else{
	        	tvMyNickName.setVisibility(View.GONE);
	        }
        }
    }

    /**
     * 查询个人总积分成功
     *
     * @param userScore
     */
    public void querySuccess(RUserScore userScore) {
        FormsUtil.setRedText(myScore, "我的积分：{0}", userScore.getScore() + " 分");
        getBaseApp().getLoginUser().setPoints(Long.parseLong(userScore.getScore()));
    }

    /**
     * 查询个人总积分失败
     *
     * @param retCode
     */
    public void queryFailed(RetCode retCode) {
        FormsUtil.setRedText(myScore, myScore.getText().toString(), "0 分");
        MyToast.showTEXT(this, retCode.getRetMsg());
    }

    @OnClick(value = {R.id.tollAdress,
            R.id.tollModifyPwd,
            R.id.tollCardList,
            R.id.logout,
            R.id.rlUserInfo})
    public void onClick(View view) {
        Bundle bundle = new Bundle();
        switch (view.getId()) {
            case R.id.tollAdress:
                bundle.putInt("fromPage", 0);
                callMe(AddressListActivity.class, bundle);
                break;
            case R.id.tollModifyPwd:
                callMeForBack(UpdatePasswordActivity.class, UPATE_PASSWORD);
                break;
            case R.id.tollCardList:
                bundle.putInt("fromPage", 0);
                callMe(CardManagerActivity.class, bundle);
                break;
            case R.id.logout:
                DialogUtil.showWithTwoBtn(this, getString(R.string.logout_alert),
                        getString(R.string.action_sure),
                        getString(R.string.action_cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                getBaseApp().logout();
                                MyInfoActivity.this.setResult(RESULT_OK);
                                MyInfoActivity.this.finish();
                            }
                        }, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }
                );
                break;
            case R.id.rlUserInfo:
                callMe(PerfectUserInfoActivity.class);
                break;
//            case R.id.tollModifyPhone:
//                callMe(UpdatePhoneActivity.class);
//                break;
        }
    }

    public void signSuccess() {
        MyToast.show(this, "签到成功!", MyToast.TEXT,Gravity.CENTER, Toast.LENGTH_SHORT);
        userAction.queryUserScore().start();
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	if(requestCode == UPATE_PASSWORD && resultCode == RESULT_OK){
    		finish();
    	}
    }
}
