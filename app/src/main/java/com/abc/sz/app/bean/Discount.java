package com.abc.sz.app.bean;

import java.io.Serializable;


/**
 * 折扣
 *
 * @author ftl
 */
public class Discount implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/* 折扣 ID*/
    private String discountId;
    /* 优惠分类   1:积分折扣  2:特价活动  0：无*/
    private String preferentialType;
    /* 优惠说明*/
    private String reason;
    /* 折扣价*/
    private String discountPrice;

    public String getDiscountId() {
        return discountId;
    }

    public void setDiscountId(String discountId) {
        this.discountId = discountId;
    }

    public String getPreferentialType() {
        return preferentialType;
    }

    public void setPreferentialType(String preferentialType) {
        this.preferentialType = preferentialType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

}
