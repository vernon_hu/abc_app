package com.abc.sz.app.http.bean.product;

/**
 * 分享请求
 *
 * @author llc
 */
public class QShare {
    //分享类型：1、商品  2、快报
    private String Type;
    //分享内容：商品ID或快报ID
    private String Attribute;
    //分享平台：1、朋友圈，2、微博
    private String Category;

    public QShare() {

    }

    public QShare(String type, String attribute, String category) {
        super();
        Type = type;
        Attribute = attribute;
        Category = category;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getAttribute() {
        return Attribute;
    }

    public void setAttribute(String attribute) {
        Attribute = attribute;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }


}
