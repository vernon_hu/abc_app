package com.abc.sz.app.http.bean.product;

import java.util.List;

/**
 * 商品详情查询
 *
 * @author llc
 */
public class QProductInfo {

    //商品唯一id
    private String productId;
    //类目id
    private String groupId;
    //商品属性值列表
    private List<QAttributeValueBean> Attribute;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public List<QAttributeValueBean> getAttribute() {
        return Attribute;
    }

    public void setAttribute(List<QAttributeValueBean> attribute) {
        Attribute = attribute;
    }


}
