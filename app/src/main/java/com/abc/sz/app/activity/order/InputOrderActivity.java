package com.abc.sz.app.activity.order;


import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.OrderAction;
import com.abc.sz.app.bean.Order;
import com.abc.sz.app.bean.Product;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.HttpQueue;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;

/**
 * Created by hwt on 14/12/5.
 */
@ContentView(R.layout.activity_inputorder)
public class InputOrderActivity extends ABCActivity {
	
	@ViewInject(R.id.appBar) Toolbar toolBar;
    @ViewInject(R.id.orderNum) EditText orderNum;
    
    private OrderAction orderAction;
    private HttpQueue httpQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolBar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        orderAction = (OrderAction) controller.getAction(this, OrderAction.class);
    }


    @OnClick(value = {R.id.commit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.commit: 
            	String orderId = orderNum.getText().toString();
                if("".equals(orderId)){
                	FormsUtil.setErrorHtmlTxt(this, orderNum, R.string.inputorder_label);
                }else{
                	httpQueue = controller.getQueue(this);
                	httpQueue.addRequest(orderAction.queryOrderDetails(orderId));
                	httpQueue.addRequest(orderAction.orderImmediatePay(orderId, null, null, null, "1"));
                	httpQueue.start();
                }
                break;
            
        }
    }
    
    /**
     * 查询订单详情成功
     *
     * @param response
     */
    public void querySuccess(Order response) {
    	List<Product> list = new ArrayList<>();
    	Product product = new Product();
    	product.setProductId("SH012345678910");
    	product.setProductName("商户端扫码商品");
    	product.setProductPrice(String.valueOf(response.getAmount()));
    	product.setNum("1");
    	list.add(product);
    	Http orderImmediatePay = orderAction.orderImmediatePay(response.getOrderId(), 
    			String.valueOf(response.getAmount()), null, list, "1");
        httpQueue.replaceHttp(1, orderImmediatePay);
    }
    
    /**
     * 订单支付成功
     */
    public void paySuccess(String content){
    	Bundle bundle = new Bundle();
    	bundle.putString("paymentyURL", content);
    	callMe(OrderAffirmPayActivity.class, bundle);
    }
    
}
