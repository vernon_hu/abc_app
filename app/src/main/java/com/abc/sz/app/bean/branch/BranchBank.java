package com.abc.sz.app.bean.branch;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by monkey on 2015/8/13.
 * 支行网点
 */
public class BranchBank implements Parcelable {
    private String Address;
    private String Borough;
    private String BoroughId;
    private String BranchId;
    private String City;
    private String CityId;
    private String FullAddress;
    private String HasPhoto;
    private String Id;
    private Double Latitude;
    private Double Longitude;
    private String Name;
    private String PhotoExtension;
    private String Province;
    private String ProvinceId;
    private String BranchClass;
    private String BranchClassId;
    private String BranchLevel;
    private String BranchLevelId;
    private String BusinessCategory;
    private String BusinessCategoryId;
    private String BusinessStatusId;
    private String Code;
    private String Introduction;
    private String Is24hrSelfHelp;
    private String IsAccepteQueue;
    private String PhoneNumber;
    private String SuperiorLevel1;
    private String SuperiorLevel2;
    private String SuperiorLevelBranch;

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getBorough() {
        return Borough;
    }

    public void setBorough(String borough) {
        Borough = borough;
    }

    public String getBoroughId() {
        return BoroughId;
    }

    public void setBoroughId(String boroughId) {
        BoroughId = boroughId;
    }

    public String getBranchId() {
        return BranchId;
    }

    public void setBranchId(String branchId) {
        BranchId = branchId;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getCityId() {
        return CityId;
    }

    public void setCityId(String cityId) {
        CityId = cityId;
    }

    public String getFullAddress() {
        return FullAddress;
    }

    public void setFullAddress(String fullAddress) {
        FullAddress = fullAddress;
    }

    public String getHasPhoto() {
        return HasPhoto;
    }

    public void setHasPhoto(String hasPhoto) {
        HasPhoto = hasPhoto;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhotoExtension() {
        return PhotoExtension;
    }

    public void setPhotoExtension(String photoExtension) {
        PhotoExtension = photoExtension;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public String getProvinceId() {
        return ProvinceId;
    }

    public void setProvinceId(String provinceId) {
        ProvinceId = provinceId;
    }

    public String getBranchClass() {
        return BranchClass;
    }

    public void setBranchClass(String branchClass) {
        BranchClass = branchClass;
    }

    public String getBranchClassId() {
        return BranchClassId;
    }

    public void setBranchClassId(String branchClassId) {
        BranchClassId = branchClassId;
    }

    public String getBranchLevel() {
        return BranchLevel;
    }

    public void setBranchLevel(String branchLevel) {
        BranchLevel = branchLevel;
    }

    public String getBranchLevelId() {
        return BranchLevelId;
    }

    public void setBranchLevelId(String branchLevelId) {
        BranchLevelId = branchLevelId;
    }

    public String getBusinessCategory() {
        return BusinessCategory;
    }

    public void setBusinessCategory(String businessCategory) {
        BusinessCategory = businessCategory;
    }

    public String getBusinessCategoryId() {
        return BusinessCategoryId;
    }

    public void setBusinessCategoryId(String businessCategoryId) {
        BusinessCategoryId = businessCategoryId;
    }

    public String getBusinessStatusId() {
        return BusinessStatusId;
    }

    public void setBusinessStatusId(String businessStatusId) {
        BusinessStatusId = businessStatusId;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getIntroduction() {
        return Introduction;
    }

    public void setIntroduction(String introduction) {
        Introduction = introduction;
    }

    public String getIs24hrSelfHelp() {
        return Is24hrSelfHelp;
    }

    public void setIs24hrSelfHelp(String is24hrSelfHelp) {
        Is24hrSelfHelp = is24hrSelfHelp;
    }

    public String getIsAccepteQueue() {
        return IsAccepteQueue;
    }

    public void setIsAccepteQueue(String isAccepteQueue) {
        IsAccepteQueue = isAccepteQueue;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getSuperiorLevel1() {
        return SuperiorLevel1;
    }

    public void setSuperiorLevel1(String superiorLevel1) {
        SuperiorLevel1 = superiorLevel1;
    }

    public String getSuperiorLevel2() {
        return SuperiorLevel2;
    }

    public void setSuperiorLevel2(String superiorLevel2) {
        SuperiorLevel2 = superiorLevel2;
    }

    public String getSuperiorLevelBranch() {
        return SuperiorLevelBranch;
    }

    public void setSuperiorLevelBranch(String superiorLevelBranch) {
        SuperiorLevelBranch = superiorLevelBranch;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Address);
        dest.writeString(this.Borough);
        dest.writeString(this.BoroughId);
        dest.writeString(this.BranchId);
        dest.writeString(this.City);
        dest.writeString(this.CityId);
        dest.writeString(this.FullAddress);
        dest.writeString(this.HasPhoto);
        dest.writeString(this.Id);
        dest.writeDouble(this.Latitude);
        dest.writeDouble(this.Longitude);
        dest.writeString(this.Name);
        dest.writeString(this.PhotoExtension);
        dest.writeString(this.Province);
        dest.writeString(this.ProvinceId);
        dest.writeString(this.BranchClass);
        dest.writeString(this.BranchClassId);
        dest.writeString(this.BranchLevel);
        dest.writeString(this.BranchLevelId);
        dest.writeString(this.BusinessCategory);
        dest.writeString(this.BusinessCategoryId);
        dest.writeString(this.BusinessStatusId);
        dest.writeString(this.Code);
        dest.writeString(this.Introduction);
        dest.writeString(this.Is24hrSelfHelp);
        dest.writeString(this.IsAccepteQueue);
        dest.writeString(this.PhoneNumber);
        dest.writeString(this.SuperiorLevel1);
        dest.writeString(this.SuperiorLevel2);
        dest.writeString(this.SuperiorLevelBranch);
    }

    public BranchBank() {
    }

    protected BranchBank(Parcel in) {
        this.Address = in.readString();
        this.Borough = in.readString();
        this.BoroughId = in.readString();
        this.BranchId = in.readString();
        this.City = in.readString();
        this.CityId = in.readString();
        this.FullAddress = in.readString();
        this.HasPhoto = in.readString();
        this.Id = in.readString();
        this.Latitude = in.readDouble();
        this.Longitude = in.readDouble();
        this.Name = in.readString();
        this.PhotoExtension = in.readString();
        this.Province = in.readString();
        this.ProvinceId = in.readString();
        this.BranchClass = in.readString();
        this.BranchClassId = in.readString();
        this.BranchLevel = in.readString();
        this.BranchLevelId = in.readString();
        this.BusinessCategory = in.readString();
        this.BusinessCategoryId = in.readString();
        this.BusinessStatusId = in.readString();
        this.Code = in.readString();
        this.Introduction = in.readString();
        this.Is24hrSelfHelp = in.readString();
        this.IsAccepteQueue = in.readString();
        this.PhoneNumber = in.readString();
        this.SuperiorLevel1 = in.readString();
        this.SuperiorLevel2 = in.readString();
        this.SuperiorLevelBranch = in.readString();
    }

    public static final Creator<BranchBank> CREATOR = new Creator<BranchBank>() {
        public BranchBank createFromParcel(Parcel source) {
            return new BranchBank(source);
        }

        public BranchBank[] newArray(int size) {
            return new BranchBank[size];
        }
    };
}
