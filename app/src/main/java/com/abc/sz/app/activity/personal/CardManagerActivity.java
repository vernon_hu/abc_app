package com.abc.sz.app.activity.personal;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.UserAction;
import com.abc.sz.app.http.bean.personal.RUserCard;
import com.abc.sz.app.util.DialogUtil;
import com.abc.sz.app.view.LoadingView;
import com.abc.sz.app.view.SlideItem;
import com.abc.sz.app.view.SlideListView;
import com.abc.sz.app.view.SlideView;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.net.ViewLoading.OnRetryListener;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;
import com.forms.view.toast.MyToast;

/**
 * Created by hwt on 14/11/11.
 * 我的银行卡
 */
@ContentView(R.layout.activity_cardmanager)
public class CardManagerActivity extends ABCActivity implements SlideView.OnSlideListener, OnClickListener {
    @ViewInject(R.id.cardList) SlideListView cardListView;
    @ViewInject(R.id.loadingView) LoadingView loadingView;
    @ViewInject(R.id.appBar) Toolbar toolbar;

    private List<SlideItem> cardItemList = new ArrayList<SlideItem>();

    private SlideView mLastSlideViewWithStatusOn;
    private SlideAdapter userCardAdapter;
    private SlideItem defaultItem = null;
    private Integer unBindItemPosition = null;
    private UserAction userAction;
    //0代表来自我的卡号
    //1代表来自订支付
    private int fromPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fromPage = bundle.getInt("fromPage");
        }

        userAction = (UserAction) controller.getAction(this, UserAction.class);
        userCardAdapter = new SlideAdapter();
        cardListView.setAdapter(userCardAdapter);
        cardListView.setOnmItemClickListener(new SlideListView.OnmItemClickListener() {
            @Override
            public void onItemClick(SlideView slideView, int position) {
                defaultItem = cardItemList.get(position);
                if (fromPage == 0) {
                	final RUserCard card = (RUserCard) defaultItem.card;
                	if("0".equals(card.getIsDefault())){
	                    DialogUtil.showWithTwoBtn(CardManagerActivity.this, "是否设置为默认支付卡", "确定", "取消", new DialogInterface.OnClickListener() {
	                        @Override
	                        public void onClick(DialogInterface dialog, int which) {
	                            userAction.setDefaultCard(card).start();
	                        }
	                    }, new DialogInterface.OnClickListener() {
	                        @Override
	                        public void onClick(DialogInterface dialog, int which) {
	                            dialog.dismiss();
	                        }
	                    });
                	}else{
                		MyToast.showTEXT(CardManagerActivity.this, "已经是默认支付卡");
                	}
                } else if (fromPage == 1) {
                    getCacheBean().putCache((RUserCard) defaultItem.card);
                    finishForBack(RESULT_OK);
                }
            }
        });

        userAction.queryBindCardList().setLoadingView(loadingView).start();
        loadingView.retry(new OnRetryListener() {
			
			@Override
			public void retry() {
				userAction.queryBindCardList().setLoadingView(loadingView).start();
			}
		});
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_cardmanager_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.bindCard){
            callMeForBack(BindNewCardActivity.class, 99);
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.selideDelete: {
                unBindItemPosition = cardListView.getPositionForView(v);
                if (unBindItemPosition != SlideListView.INVALID_POSITION) {
                    final SlideItem unBindItem = cardItemList.get(unBindItemPosition);
                    View dialogView = LayoutInflater.from(this).inflate(R.layout.layout_dialog_remove_bind, null);
                    final AlertDialog dialog = new AlertDialog.Builder(this).create();
                    dialog.setView(dialogView, 0, 0, 0, 0);
                    dialog.setInverseBackgroundForced(true);
                    dialog.show();
                    final EditText etRemoveBind = (EditText) dialogView.findViewById(R.id.et_remove_bind);
                    Button btnCancel = (Button) dialogView.findViewById(R.id.btn_cancel);
                    Button btnConfirm = (Button) dialogView.findViewById(R.id.btn_confirm);

                    btnCancel.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							dialog.dismiss();
						}
					});
                    btnConfirm.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                        	String phone = etRemoveBind.getText().toString();
                        	if (TextUtils.isEmpty(phone)) {
                        		MyToast.showTEXT(CardManagerActivity.this, getString(R.string.please_input_phone));
                    	    } else if (phone.length() != 11) {
                    	    	MyToast.showTEXT(CardManagerActivity.this, getString(R.string.input_eleven_phone));
                    	    } else{
                    	    	userAction.unBindCard((RUserCard) unBindItem.card, phone).start();
    							dialog.dismiss();
                    	    }
                        }
                    });
                }
                Log.d(getClass().getSimpleName(), "onClick v=" + v);
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99 && resultCode == BindNewCardActivity.RESULT_BINDCARD) {
            MyToast.showTEXT(this, "绑定成功");
            cardItemList.clear();
            userAction.queryBindCardList().setLoadingView(loadingView).start();
        }

    }

//    @OnItemClick(R.id.cardList)
//    public void cardListItemClick(AdapterView<?> parent, View view, int position, long id) {
//        defaultItem = cardItemList.get(position);
//        DialogUtil.showWithTwoBtn(this, "是否设置为默认支付卡", "确定", "取消", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                userAction.setDefaultCard((RUserCard) defaultItem.card).startRequest();
//            }
//        }, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//    }

    @Override
    public void onSlide(View view, int status) {
        if (mLastSlideViewWithStatusOn != null && mLastSlideViewWithStatusOn != view) {
            mLastSlideViewWithStatusOn.shrink();
        }

        if (status == SLIDE_STATUS_ON) {
            mLastSlideViewWithStatusOn = (SlideView) view;
        }
    }

    /**
     * 用户绑定卡列表载入 完成
     *
     * @param retCode
     * @param content
     */
    public void loadedCardList(RetCode retCode, List<RUserCard> content) {
        if (retCode == RetCode.success && content.size() > 0) {
            for (RUserCard userCard : content) {
                SlideItem slideItem = new SlideItem();
                slideItem.card = userCard;
                cardItemList.add(slideItem);
            }
            userCardAdapter.notifyDataSetChanged();
        } else if (cardItemList.size() > 0) {
            MyToast.showTEXT(this, getString(R.string.noMoreData));
        }
    }

    /**
     * 用户绑定卡列表载入 失败
     *
     * @param retCode
     */
    public void loadedCardListFailed(RetCode retCode) {
        String failedTipsTxt = retCode != null ? retCode.getRetMsg() : getString(R.string.defaultFailedTipsTxt);
        if (cardItemList.size() > 0) {
            MyToast.showTEXT(this, failedTipsTxt);
        }
    }

    /**
     * 设为默认卡
     *
     * @param retCode
     */
    public void setDefaultSuccess(RetCode retCode) {
        if (defaultItem != null) {
            for (int i = 0; i < cardItemList.size(); i++) {
                if (cardItemList.get(i).equals(defaultItem)) {
                    ((RUserCard) defaultItem.card).setIsDefault(String.valueOf(1));
                } else {
                    ((RUserCard) cardItemList.get(i).card).setIsDefault(String.valueOf(0));
                }
            }

        }
        userCardAdapter.notifyDataSetChanged();
    }

    /**
     * 解绑成功
     */
    public void unBindCardSuccess() {
        if (unBindItemPosition != null) {
            cardItemList.remove(unBindItemPosition.intValue());
            userCardAdapter.notifyDataSetChanged();
        }
    }


    class SlideAdapter extends android.widget.BaseAdapter {
        private LayoutInflater mInflater;

        SlideAdapter() {
            super();
            mInflater = getLayoutInflater();
        }

        @Override
        public int getCount() {
            return cardItemList.size();
        }

        @Override
        public Object getItem(int position) {
            return cardItemList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            SlideView slideView = (SlideView) convertView;
            if (slideView == null) {
                View itemView = mInflater.inflate(R.layout.layout_cardlist_item, null);

                slideView = new SlideView(CardManagerActivity.this);
                slideView.setContentView(itemView);

                holder = new ViewHolder(slideView);
                slideView.setOnSlideLinstener(CardManagerActivity.this);
                slideView.setTag(holder);
            } else {
                holder = (ViewHolder) slideView.getTag();
            }
            SlideItem item = cardItemList.get(position);
            if (item != null) {
                item.slideView = slideView;
                item.slideView.shrink();
                item.slideView.setButtonText("解绑");
                RUserCard card = (RUserCard) item.card;
                String cardType = card.getCardType();
                if (cardType != null) {
                    if ("1".equals(cardType)) {
                        cardType = getString(R.string.debit_card);
                    } else if ("3".equals(cardType)) {
                        cardType = getString(R.string.credit_card);
                    }
                    if ("1".equals(card.getIsDefault())) {
                        holder.select.setVisibility(View.VISIBLE);
                    } else {
                        holder.select.setVisibility(View.INVISIBLE);
                    }
                    FormsUtil.setTextViewTxt(holder.cardType, cardType);
                }
                FormsUtil.setTextViewTxt(holder.cardNum, card.getCardNum());
                holder.deleteHolder.setOnClickListener(CardManagerActivity.this);
            }
            return slideView;
        }
    }

    private static class ViewHolder {
        public ImageView cardLogo;
        public TextView cardNum;
        public Button deleteHolder;
        public TextView cardType;
        public ImageView select;

        ViewHolder(View view) {
            cardLogo = (ImageView) view.findViewById(R.id.cardLogo);
            cardNum = (TextView) view.findViewById(R.id.cardNum);
            deleteHolder = (Button) view.findViewById(R.id.selideDelete);
            cardType = (TextView) view.findViewById(R.id.cardType);
            select = (ImageView) view.findViewById(R.id.iv_select);
        }
    }
}
