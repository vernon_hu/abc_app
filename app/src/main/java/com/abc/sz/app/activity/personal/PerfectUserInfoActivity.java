package com.abc.sz.app.activity.personal;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.UserAction;
import com.abc.sz.app.http.bean.personal.RUser;
import com.abc.sz.app.util.InputVerifyUtil;
import com.forms.base.ABCActivity;
import com.forms.base.LoginUser;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.baseUtil.view.annotation.event.OnRadioGroupCheckedChange;
import com.forms.library.tools.FormsUtil;
import com.forms.view.toast.MyToast;

import java.util.regex.Pattern;

/**
 * 完善用戶信息
 *
 * @author ftl
 */
@ContentView(R.layout.activity_perfect_userinfo)
public class PerfectUserInfoActivity extends ABCActivity {
    @ViewInject(R.id.nickname) EditText nickname;
    @ViewInject(R.id.gender) RadioGroup gender;
    @ViewInject(R.id.email) EditText email;
    @ViewInject(R.id.appBar) Toolbar toolbar;
    
    private UserAction userAction;
    private String sex = "1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        userAction = (UserAction) controller.getAction(this, UserAction.class);

        LoginUser loginUser = getBaseApp().getLoginUser();
        if (loginUser != null) {
            RUser rUser = loginUser.getLoginUser();
            if (rUser != null) {
                nickname.setText(rUser.getNickname());
                email.setText(rUser.getEmail());
                if (rUser.getGender() == null || "1".equals(rUser.getGender())) {
                    gender.check(R.id.man);
                } else {
                    gender.check(R.id.woman);
                }
            }
        }
    }

    @OnRadioGroupCheckedChange(R.id.gender)
    public void onGenderChange(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.man:
                sex = "1";
                break;
            case R.id.woman:
                sex = "0";
                break;
        }
    }

    @OnClick(R.id.btn_confirm)
    public void onClick(View view) {
        if (check()) {
            userAction.perfectUserInfo(nickname.getText().toString(), sex,
                    email.getText().toString()).start();
        }
    }

    /**
     * 提交前检验用户输入信息
     *
     * @return
     */
    private boolean check() {
        if (TextUtils.isEmpty(email.getText())) {
            FormsUtil.setErrorHtmlTxt(this, email, R.string.please_input_email);
            return false;
        } else if (!InputVerifyUtil.isEmail(email.getText().toString())) {
            FormsUtil.setErrorHtmlTxt(this, email, R.string.input_email_iserror);
            return false;
        }
        return true;
    }

    public void perfectInfoSuccess() {
        //更新缓存用户
        LoginUser loginUser = getBaseApp().getLoginUser();
        loginUser.getLoginUser().setNickname(nickname.getText().toString());
        loginUser.getLoginUser().setGender(sex);
        loginUser.getLoginUser().setEmail(email.getText().toString());
        MyToast.show(this, "完善信息成功!", MyToast.TEXT, Gravity.CENTER, Toast.LENGTH_SHORT);
        finish();
    }
}
