package com.abc.sz.app.http.bean.lightpay;

public class QGZBindCard {

	private String accName;//用户名
    private String certNo;//证件号码
    private String accNo;	//银行卡号
    private String phoneNum;//电话号码
    private String checkCode;//验证码
    private String userId;   //光id
    private String operateType;//操作类型

    public QGZBindCard(){}
    
	public QGZBindCard(String accName, String certNo, String accNo,
			String phoneNum) {
		super();
		this.accName = accName;
		this.certNo = certNo;
		this.accNo = accNo;
		this.phoneNum = phoneNum;
	}

	public QGZBindCard(String accNo, String phoneNum, String checkCode,
			String userId, String operateType) {
		super();
		this.accNo = accNo;
		this.phoneNum = phoneNum;
		this.checkCode = checkCode;
		this.userId = userId;
		this.operateType = operateType;
	}

	public String getAccName() {
		return accName;
	}

	public void setAccName(String accName) {
		this.accName = accName;
	}

	public String getCertNo() {
		return certNo;
	}

	public void setCertNo(String certNo) {
		this.certNo = certNo;
	}

	public String getAccNo() {
		return accNo;
	}

	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOperateType() {
		return operateType;
	}

	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}
}
