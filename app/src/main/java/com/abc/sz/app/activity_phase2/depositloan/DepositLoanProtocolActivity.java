package com.abc.sz.app.activity_phase2.depositloan;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;

/**
 * 个人贷款存贷通协议页面
 * 
 * @author hkj
 */
@ContentView(R.layout.activity_phase2_depositloanprotocol)
public class DepositLoanProtocolActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	@ViewInject(R.id.tvPrompt)
	private TextView tvPrompt;
	@ViewInject(R.id.tvProtocol)
	private TextView tvProtocol;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		SpannableStringBuilder style = new SpannableStringBuilder(tvPrompt.getText());
		style.setSpan(new ForegroundColorSpan(Color.RED), 0, 5, Spannable.SPAN_EXCLUSIVE_INCLUSIVE); // 设置指定位置文字的颜色
		tvPrompt.setText(style);
		tvProtocol.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
	}

	@OnClick({ R.id.btnAgree })
	public void calculate(View v) {
		switch (v.getId()) {
		case R.id.btnAgree:
			callMe(DepositLoanOpenActivity.class);
			break;
		}
	}
	
}
