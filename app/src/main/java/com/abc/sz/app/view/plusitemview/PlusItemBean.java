package com.abc.sz.app.view.plusitemview;

import com.forms.library.baseUtil.db.annotation.Column;
import com.forms.library.baseUtil.db.annotation.Id;
import com.forms.library.baseUtil.db.annotation.Table;

/**
 * Created by hwt on 4/27/15.
 */
@Table(name = "tb_plus_item")
public class PlusItemBean implements Comparable<PlusItemBean> {
    @Id private int id_;
    @Column(column = "index_") private int index_;
    @Column(column = "priority") private int priority;
    @Column(column = "iconResId") private int iconResId;
    @Column(column = "text") private String text;
    @Column(column = "isChecked") private boolean isChecked;
    @Column(column = "targetClass") private String targetClass;

    public int getId_() {
        return id_;
    }

    public void setId_(int id_) {
        this.id_ = id_;
    }

    public int getIndex_() {
        return index_;
    }

    public PlusItemBean setIndex_(int index_) {
        this.index_ = index_;
        return this;
    }

    public int getPriority() {
        return priority;
    }

    public PlusItemBean setPriority(int priority) {
        this.priority = priority;
        return this;
    }

    public int getIconResId() {
        return iconResId;
    }

    public PlusItemBean setIconResId(int iconResId) {
        this.iconResId = iconResId;
        return this;
    }

    public String getText() {
        return text;
    }

    public PlusItemBean setText(String text) {
        this.text = text;
        return this;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public PlusItemBean setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
        return this;
    }

    public String getTargetClass() {
        return targetClass;
    }

    public PlusItemBean setTargetClass(String targetClass) {
        this.targetClass = targetClass;
        return this;
    }

    @Override
    public int compareTo(PlusItemBean another) {
        if (another != null) {
            int p = priority - another.getPriority();
            if (p == 0) {
                return index_ - another.getIndex_();
            } else {
                return p;
            }
        }
        return 1;
    }
}
