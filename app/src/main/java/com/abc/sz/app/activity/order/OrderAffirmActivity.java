package com.abc.sz.app.activity.order;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.AddressAction;
import com.abc.sz.app.action.OrderAction;
import com.abc.sz.app.activity.AddAddressActivity;
import com.abc.sz.app.activity.AddressListActivity;
import com.abc.sz.app.adapter.order.OrderAffirmAdapter;
import com.abc.sz.app.bean.Address;
import com.abc.sz.app.bean.Order;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.util.DialogUtil;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.net.HttpTrans;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.net.ViewLoading.OnRetryListener;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.view.toast.MyToast;

/**
 * 订单创建
 *
 * @author ftl
 */
@ContentView(R.layout.activity_order_affirm)
public class OrderAffirmActivity extends ABCActivity {
	
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.loadingView) LoadingView loadingView;
    @ViewInject(R.id.tv_countPrice) TextView tv_countPrice;
    @ViewInject(R.id.lv_order) ListView lv_order;

    private OrderAffirmAdapter adapter;
    private List<Product> list = new ArrayList<>();
    private HttpTrans httpTrans;
    private OrderAction orderAction;
    private AddressAction addressAction;
    private TextView tv_userName;
    private TextView tv_userPhone;
    private TextView tv_userAddress;
    private LinearLayout ll_address;
    private LinearLayout ll_addAddress;
    private Address address;

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
//            String orderId = (String) msg.obj;
//            for (int i = 0; i < list.size(); i++) {
//                Order order = list.get(i);
//                if (orderId.equals(order.getOrderId())) {
//                    List<Product> productList = order.getProductList();
//                    double sumPrice = 0;
//                    for (int j = 0; j < productList.size(); j++) {
//                        String price = productList.get(j).getProductPrice();
//                        String num = productList.get(j).getNum();
//                        if (price != null && num != null) {
//                            sumPrice += Double.parseDouble(price) * Double.parseDouble(num);
//                        }
//                    }
//                    order.setAmount(sumPrice);
//                    adapter.notifyDataSetChanged();
//                }
//            }
//            int totalPrice = 0;
//            for (int i = 0; i < list.size(); i++) {
//                totalPrice += list.get(i).getAmount();
//            }
//            tv_countPrice.setText(ShoppingCarCount.formatMoney(totalPrice + ""));
        };
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initData();
        initView();
    }

    /**
     * 初始化数据
     */
    @SuppressWarnings("unchecked")
    private void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
        	String sumPrice = bundle.getString("sumPrice");
        	orderAction = (OrderAction) controller.getAction(this, OrderAction.class);
            addressAction = (AddressAction) controller.getAction(this, AddressAction.class);
    		List<Product> productList = getCacheBean().getCache(ArrayList.class);
    		httpTrans = controller.getTrans(this);
            httpTrans.addRequest(orderAction.orderAccount(productList));
            httpTrans.addRequest(addressAction.queryAddress("1"));
            httpTrans.setLoadingView(loadingView);
            httpTrans.start();
            FormsUtil.setTextViewTxt(tv_countPrice, sumPrice, null);
            loadingView.retry(new OnRetryListener() {
    			
    			@Override
    			public void retry() {
    				httpTrans.start();
    			}
    		});
        }
    }

    /**
     * 初始化界面
     */
    private void initView() {
        View headerView = LayoutInflater.from(this).inflate(R.layout.layout_order_affirm, null);
        ll_address = (LinearLayout) headerView.findViewById(R.id.ll_address);
        tv_userName = (TextView) headerView.findViewById(R.id.tv_userName);
        tv_userPhone = (TextView) headerView.findViewById(R.id.tv_userPhone);
        tv_userAddress = (TextView) headerView.findViewById(R.id.tv_userAddress);
        ll_addAddress = (LinearLayout) headerView.findViewById(R.id.ll_addAddress);
        final Bundle bundle = new Bundle();
        ll_address.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                bundle.putInt("fromPage", 1);
                callMeForBack(AddressListActivity.class, bundle, 1);
            }
        });
        ll_addAddress.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                bundle.putInt("fromPage", 3);
                callMeForBack(AddAddressActivity.class, bundle, 2);
            }
        });
        lv_order.addHeaderView(headerView);

        adapter = new OrderAffirmAdapter(list, this, handler, imageLoader);
        lv_order.setAdapter(adapter);
    }

    @OnClick(value = {R.id.btn_affirm_pay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_affirm_pay:
                if (ll_addAddress.getVisibility() == View.GONE &&
                        ll_address.getVisibility() == View.VISIBLE) {
                	DialogUtil.showWithTwoBtn(this, "是否确定创建订单", "确定", "取消", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
			                	orderAction.createOrder(address.getAddressId(), list).start();
						}
					}, new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							
						}
					});
                } else {
                    MyToast.show(this, "请选择收货地址", MyToast.TEXT, Gravity.CENTER, Toast.LENGTH_SHORT);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1 || requestCode == 2) {
                address = getCacheBean().getCache(Address.class);
                if (address != null) {
                    ll_addAddress.setVisibility(View.GONE);
                    ll_address.setVisibility(View.VISIBLE);
                    FormsUtil.setTextViewTxt(tv_userName, address.getPerson(), null);
                    FormsUtil.setTextViewTxt(tv_userPhone, address.getPhone(), null);
                    FormsUtil.setTextViewTxt(tv_userAddress, address.getProvince() + address.getCity() +
                            address.getArea() + address.getAddress(), null);
                }else{
                	 ll_addAddress.setVisibility(View.VISIBLE);
                     ll_address.setVisibility(View.GONE);
                }
            }else if(requestCode == 3){
            	 finishForBack(RESULT_OK);
            }
        }
    }

    /**
     * 查询默认地址成功
     */
    public void queryAddressSuccess(RetCode retCode, List<Address> content) {
        if (retCode != RetCode.noData && content != null && content.size() != 0) {
            ll_address.setVisibility(View.VISIBLE);
            address = content.get(0);
            if (address != null) {
                FormsUtil.setTextViewTxt(tv_userName, address.getPerson(), null);
                FormsUtil.setTextViewTxt(tv_userPhone, address.getPhone(), null);
                FormsUtil.setTextViewTxt(tv_userAddress, address.getProvince() + address.getCity() +
                        address.getArea() + address.getAddress(), null);
            }
        } else {
            ll_addAddress.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 查询结算列表成功
     */
    public void querySuccess(List<Product> response) {
        list.clear();
        list.addAll(response);
        adapter.notifyDataSetChanged();
    }

    /**
     * 查询结算列表失败
     *
     * @param retCode
     */
    public void queryFailed(RetCode retCode) {
        DialogUtil.showWithNoTitile(this, retCode.getRetMsg());
    }
    
    /**
     * 生成订单成功
     */
    public void createOrderSuccess(List<Order> list) {
    	if(list != null && list.size() > 0){
    		Bundle bundle = new Bundle();
    		bundle.putString("address", tv_userAddress.getText().toString());
    		bundle.putSerializable("orderList", (Serializable)list);
    		callMeForBack(OrderPayListActivity.class, bundle, 3);
    	}
    }

}
