package com.abc.sz.app.activity.order;

/**
 * 订单状态
 * 0-订单交易失败
 * 1-订单完成
 * 2-订单未支付
 * 3-订单未消费
 * 4-订单未收货
 * 5-确认付款中
 * 6-已评价
 * 7-待确认
 * 8-已发货
 * 9-已退款
 * 99订单取消
 *
 * @author llc
 */
public enum OrderState {

    error("99"),       //交易取消
    failed("0"),       //交易失败
    success("1"),      //交易成功,即未评价
    noPay("2"),        //未支付
    noUse("3"),        //未消费
    noGet("4"),        //待收货
    wait("5"),         //待审核
    valuation("6"),    //交易成功,并且已经评价
    waitAffirm("7"),   //待确认
    refund("9");   //已退款


    private String code;

    private OrderState(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


}
