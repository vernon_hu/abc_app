package com.abc.sz.app.activity.pay;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * K令支付:1、刮刮卡  2、K令
 *
 * @author llc
 */
@ContentView(R.layout.activity_kl_commite)
public class KLingActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initData();
        initView();
    }


    private void initData() {
        // TODO Auto-generated method stub

    }


    private void initView() {
        // TODO Auto-generated method stub

    }

}
