package com.abc.sz.app.action_phase2;

import com.abc.sz.app.action.BaseAction;
import com.abc.sz.app.bean_phase2.ImageAD;
import com.abc.sz.app.bean_phase2.MarketItem;
import com.abc.sz.app.fragment_phase2.MarketFragment;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.alibaba.fastjson.TypeReference;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;

import java.util.List;

/**
 * 商城Action
 *
 * @author hkj
 */
public class MarketAction extends BaseAction {

    // 首页——广告请求
    public Http loadingAdv() {
        return APPRequestPhase2.advQuery("0", new ResponseNotify<List<ImageAD>>(
                new TypeReference<RespBean<List<ImageAD>>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<List<ImageAD>> response) {
                List<ImageAD> list = response.getContent();
                if (uiObject instanceof MarketFragment) {
                    ((MarketFragment) uiObject).advSuccess(list);
                }
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }

    // 商品列表请求
    public Http loadingProductList(String pageNum) {
        return APPRequestPhase2.productListQuery(null, pageNum,
                new ResponseNotify<List<MarketItem>>(
                        new TypeReference<RespBean<List<MarketItem>>>() {
                        }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<List<MarketItem>> response) {
                        if (uiObject instanceof MarketFragment) {
                            List<MarketItem> list = response.getContent();
                            ((MarketFragment) uiObject).productListSuccess(list);
                        }
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                       
                    }
                });
    }

}
