package com.abc.sz.app.http.bean;

import com.alibaba.fastjson.JSON;
import com.forms.library.baseUtil.logger.Logger;

/**
 * 请求对象
 *
 * @author llc
 */
public class RequestBean<T> {
    //TOKEN
    private String token;
    //参数
    private T param;
    private final static String CHARSET = "utf-8";

    public RequestBean(String token, T param) {
        this.token = token;
        this.param = param;
    }

    public RequestBean(T param) {
        this.param = param;
    }

    public RequestBean(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getParam() {
//        try {
//        String requestParam = JSON.toJSONString(param);
//        Logger.json(requestParam);
//            return URLEncoder.encode(requestParam, CHARSET);
        return JSON.toJSONString(param);
//        } catch (UnsupportedEncodingException e) {
//            LogUtils.d(e.getMessage(), e);
//        }
//        return null;
    }

    public void setParam(T param) {
        this.param = param;
    }


}
