package com.abc.sz.app.http.bean.order;


/**
 * 购物车
 *
 * @author ftl
 */
public class QShoppingCar {

    /* 商品ID*/
    private String productId;
    /* 商品数量*/
    private String amount;

    public QShoppingCar(String productId, String amount) {
        super();
        this.productId = productId;
        this.amount = amount;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

}
