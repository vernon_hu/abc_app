package com.abc.sz.app.activity.pay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.PayAction;
import com.abc.sz.app.action.UserAction;
import com.abc.sz.app.activity.personal.CardManagerActivity;
import com.abc.sz.app.bean.Order;
import com.abc.sz.app.http.bean.pay.AESBean;
import com.abc.sz.app.http.bean.personal.RUserCard;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.net.HttpTrans;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.encrypt.AESUtils;
import com.forms.library.tools.encrypt.DESUtils;

import java.util.List;

/**
 * K码支付
 *
 * @author llc
 *         <p/>
 *         请求参数： 卡号，登录密码，待支付订单列表号。
 */
@ContentView(R.layout.activity_km_commite)
public class KMaActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.et_card_code) EditText et_card_code;
    @ViewInject(R.id.et_password) EditText et_password;
    @ViewInject(R.id.tv_money) TextView tv_money;
    //绑定卡请求
    private UserAction userAction;
    //支付请求
    private PayAction payAction;
    //订单信息
    private List<Order> list;
    //加密因子
    private String seed = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initData();
    }


    private void initData() {
        userAction = (UserAction) controller.getAction(this, UserAction.class);
        payAction = (PayAction) controller.getAction(this, PayAction.class);

        HttpTrans httpTrans = controller.getTrans(this);
        httpTrans.addRequest(userAction.queryBindCardList());
        httpTrans.addRequest(payAction.getAesKey());
        httpTrans.start(true);

        FormsUtil.setTextViewTxt(tv_money, getCacheBean().getStringCache("sumPrice"));
        list = getCacheBean().getCache(List.class);
    }

    @OnClick(value = {R.id.et_card_code, R.id.btn_confirm})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.et_card_code:
                Bundle bundle = new Bundle();
                bundle.putInt("fromPage", 1);
                callMeForBack(CardManagerActivity.class, bundle, 1);
                break;
            case R.id.btn_confirm:
                if (check()) {
                    String password = AESUtils.encrypt(seed, et_password.getText().toString());
                    Log.d("KMaActivity", password);
                }
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                RUserCard userCard = getCacheBean().getCache(RUserCard.class);
                if (userCard != null) {
                    et_card_code.setText(userCard.getCardNum());
                }
            }
        }
    }

    private boolean check() {
        if (TextUtils.isEmpty(et_password.getText())) {
            FormsUtil.setErrorHtmlTxt(this, et_password, R.string.input_password_isempty);
            return false;
        }
        return true;
    }


    public void querySuccess(AESBean bean) {
        String userId = getBaseApp().getLoginUser().getUserId();
        String desKey = "";
        if (userId.length() > 8) {
            desKey = userId.substring(0, 7);
        } else if (userId.length() < 8) {
            desKey = userId;
            for (int i = 0; i < 8 - userId.length(); i++) {
                desKey += "0";
            }
        } else {
            desKey = userId;
        }
        System.out.println("userId:" + userId);
        System.out.println("desKey:" + desKey);
        try {
            seed = DESUtils.decryptBase64(desKey.getBytes(), desKey.getBytes(), bean.getKey());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 用户绑定卡列表载入 完成
     *
     * @param retCode
     * @param content
     */
    public void loadedCardList(RetCode retCode, List<RUserCard> content) {
        if (retCode == RetCode.success && content.size() > 0) {
            for (RUserCard userCard : content) {
                if ("1".equals(userCard.getIsDefault())) {
                    et_card_code.setText(userCard.getCardNum());
                    break;
                }
            }
        }
    }

}
