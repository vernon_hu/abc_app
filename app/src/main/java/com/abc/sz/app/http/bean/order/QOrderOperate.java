package com.abc.sz.app.http.bean.order;


/**
 * 订单取消或者详情查询
 *
 * @author ftl
 */
public class QOrderOperate {

    //订单 ID
    private String orderId;

    public QOrderOperate(String orderId) {
        super();
        this.orderId = orderId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

}
