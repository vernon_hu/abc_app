package com.abc.sz.app.action;

import android.app.Activity;
import android.content.Context;
import com.abc.sz.app.activity.AddAddressActivity;
import com.abc.sz.app.activity.AddressListActivity;
import com.abc.sz.app.activity.AddressListEditActivity;
import com.abc.sz.app.activity.order.OrderAffirmActivity;
import com.abc.sz.app.bean.Address;
import com.abc.sz.app.bean.branch.City;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.address.QAddress;
import com.abc.sz.app.http.bean.address.QCity;
import com.abc.sz.app.http.bean.address.QDeleteAddress;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.alibaba.fastjson.TypeReference;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.view.toast.MyToast;

import java.util.List;


/**
 * 地址管理
 *
 * @author ftl
 */
public class AddressAction extends BaseAction {

    /**
     * 增加新的收货地址
     *
     * @param person
     * @param phone
     * @param province
     * @param city
     * @param area
     * @param address
     * @param isDefault
     * @return
     */
    public Http addAddress(String person, String phone, String province, String city,
                           String area, String address, String isDefault, final int fromPage) {
        QAddress qAddress = new QAddress(null, person, phone, province, city, area, address);
        qAddress.setIsDefault(isDefault);
        RequestBean<QAddress> params = new RequestBean<QAddress>(getAccessToken(), qAddress);
        return APPRequestPhase2.addAddress(params, new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
        }) {

            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
                if (fromPage == 1) {
                    ((AddAddressActivity) uiObject).finishForBack(Activity.RESULT_OK);
                } else if (fromPage == 3) {
                    ((AddAddressActivity) uiObject).addAndUseSuccess();
                }
            }

            @Override
            public void onFailed(RetCode retCode) {
                MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }

    /**
     * 查询收货地址
     *
     * @param isDefault
     * @return
     */
    public Http queryAddress(final String isDefault) {
        QAddress qAddress = new QAddress();
        qAddress.setIsDefault(isDefault);
        RequestBean<QAddress> params = new RequestBean<QAddress>(getAccessToken(), qAddress);
        return APPRequestPhase2.queryAddress(params, new ResponseNotify<List<Address>>(new TypeReference<RespBean<List<Address>>>() {
        }) {

            @Override
            public void onResponse(RetCode retCode, RespBean<List<Address>> response) {
                if (uiObject instanceof AddressListActivity) {
                    ((AddressListActivity) uiObject).querySuccess(retCode, response.getContent());
                } else if (uiObject instanceof OrderAffirmActivity) {
                    ((OrderAffirmActivity) uiObject).queryAddressSuccess(retCode, response.getContent());
                }
            }

            @Override
            public void onFailed(RetCode retCode) {
                if (uiObject instanceof AddressListActivity) {
                    ((AddressListActivity) uiObject).queryFailed(retCode);
                }
            }
        });
    }

    /**
     * 删除收货地址
     *
     * @param addressId
     * @return
     */
    public Http deleteAddress(String addressId) {
        QDeleteAddress qDeleteAddress = new QDeleteAddress();
        qDeleteAddress.setAddressId(addressId);
        RequestBean<QDeleteAddress> params = new RequestBean<QDeleteAddress>(getAccessToken(), qDeleteAddress);
        return APPRequestPhase2.deleteAddress(params, new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
        }) {

            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
                ((AddressListEditActivity) uiObject).deleteSuccess();
            }

            @Override
            public void onFailed(RetCode retCode) {
                MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }

    /**
     * 修改收货地址
     *
     * @param addressId
     * @param person
     * @param phone
     * @param province
     * @param city
     * @param area
     * @param address
     * @return
     */
    public Http updateAddress(String addressId, String person, String phone, String province,
                              String city, String area, String address) {
        QAddress qAddress = new QAddress(addressId, person, phone, province, city, area, address);
        RequestBean<QAddress> params = new RequestBean<QAddress>(getAccessToken(), qAddress);
        return APPRequestPhase2.updateAddress(params, new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
        }) {

            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
                ((ABCActivity) uiObject).finishForBack(Activity.RESULT_OK);
            }

            @Override
            public void onFailed(RetCode retCode) {
                MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }

        });
    }

    /**
     * 设置默认收货地址
     *
     * @param addressId
     * @return
     */
    public Http setDefaultAddress(String addressId) {
        QDeleteAddress qDeleteAddress = new QDeleteAddress();
        qDeleteAddress.setAddressId(addressId);
        RequestBean<QDeleteAddress> params = new RequestBean<QDeleteAddress>(getAccessToken(), qDeleteAddress);
        return APPRequestPhase2.setDefaultAddress(params, new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
        }) {

            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
                ((AddressListActivity) uiObject).setDefaultSuccess();
            }

            @Override
            public void onFailed(RetCode retCode) {
                MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }

        });
    }

    /**
     * 查询城市列表
     *
     * @param cityId
     * @param lvl
     * @return
     */
    public Http queryCity(String cityId, String lvl) {
        QCity qCity = new QCity(cityId, lvl);
        RequestBean<QCity> params = new RequestBean<QCity>(qCity);
        return APPRequestPhase2.queryCity(params, new ResponseNotify<List<City>>(new TypeReference<RespBean<List<City>>>() {
        }) {

            @Override
            public void onResponse(RetCode retCode, RespBean<List<City>> response) {
                ((AddAddressActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }
}
