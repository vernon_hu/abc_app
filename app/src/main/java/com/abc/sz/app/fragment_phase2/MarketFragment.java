package com.abc.sz.app.fragment_phase2;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.ProductTypeAction;
import com.abc.sz.app.action_phase2.MarketAction;
import com.abc.sz.app.activity.product.ProductListActivity;
import com.abc.sz.app.adapter_phase2.ImageAdapter;
import com.abc.sz.app.bean.product.ProductListBean;
import com.abc.sz.app.bean_phase2.ImageAD;
import com.abc.sz.app.bean_phase2.MarketItem;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.product.QProductType;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCFragment;
import com.forms.base.BaseAdapter;
import com.forms.library.baseUtil.net.HttpTrans;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;
import com.forms.view.viewflow.CircleFlowIndicator;
import com.forms.view.viewflow.ViewFlow;

/**
 * Created by hwt on 4/15/15.
 */

public class MarketFragment extends ABCFragment {

//    @ViewInject(R.id.commodityView)
//    RecyclerView recyclerView;
//    @ViewInject(R.id.swipeRefreshLayout)
//    SwipeRefreshLayout swipeRefreshLayout;
    @ViewInject(R.id.loadingView)
    LoadingView loadingView;
    @ViewInject(R.id.viewFlow)
    ViewFlow viewFlow;
    @ViewInject(R.id.viewflowindic)
    CircleFlowIndicator viewflowindic;
    @ViewInject(R.id.llFavorable)
    LinearLayout llFavorable;
    @ViewInject(R.id.gridView)
    GridView gridView;

    private MarketAction marketAction;
    private ProductTypeAction ptAction;
    private List<ImageAD> imageList = new ArrayList<>();
    private List<ProductListBean> productList = new ArrayList<>();
//    private List<MarketItem> productList = new ArrayList<>();
//    private MarketAdapter marketAdapter;
    private ImageAdapter imageAdapter;
    private BaseAdapter<ProductListBean> typeAdapter;
    private final int NEXTIMAGE = 1;
    private int imageADPosition = 0;
    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == NEXTIMAGE) {
                if (imageADPosition == imageList.size() - 1) {
                    imageADPosition = 0;
                } else {
                    imageADPosition += 1;
                }
                viewFlow.setSelection(imageADPosition);
                handler.sendEmptyMessageDelayed(NEXTIMAGE, 3000);
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_phase2_market, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setLayoutManager(linearLayoutManager);
//        imageList.add(new ImageAD("3", "", "", ""));
//        marketAdapter = new MarketAdapter(baseActivity, baseActivity.getImageLoader(), imageList, productList);
//        recyclerView.setAdapter(marketAdapter);
//
//        swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
//
//            @Override
//            public void onRefresh() {
//                swipeRefreshLayout.setRefreshing(true);
//                request(null).start(false);
//            }
//        });
        ViewGroup.LayoutParams params = viewFlow.getLayoutParams();
        params.height = FormsUtil.SCREEN_WIDTH / 5 * 3;
        viewFlow.setLayoutParams(params);
        viewFlow.setFlowIndicator(viewflowindic);
        imageList.add(new ImageAD("3", "", "", ""));
        imageAdapter = new ImageAdapter(baseActivity, baseActivity.getImageLoader(), imageList);
        viewFlow.setAdapter(imageAdapter, 0);
        viewFlow.setOnViewSwitchListener(new ViewFlow.ViewSwitchListener() {
            @Override
            public void onSwitched(View view, int position) {
                imageADPosition = viewFlow.getSelectedItemPosition();
            }
        });
        gridView.setFocusable(false);
        gridView.setAdapter(typeAdapter = new BaseAdapter<ProductListBean>(baseActivity, productList, 
        		R.layout.layout_phase2_market_footer) {

			@Override
			public void viewHandler(int position, final ProductListBean t, View convertView) {
				 RelativeLayout rlContent = ViewHolder.get(convertView, R.id.rlContent);
				 ViewGroup.LayoutParams layoutParams = rlContent.getLayoutParams();
				 layoutParams.height = FormsUtil.SCREEN_WIDTH / gridView.getNumColumns();
			     layoutParams.width = FormsUtil.SCREEN_WIDTH / gridView.getNumColumns();
			     rlContent.setLayoutParams(layoutParams);
			     rlContent.invalidate();
				 ImageView cellIcon = ViewHolder.get(convertView, R.id.cellIcon);
				 TextView cellText = ViewHolder.get(convertView, R.id.cellText);
				 baseActivity.getImageLoader().displayImage(t.getPictureUrl(), cellIcon, ImageViewUtil.getOption());
				 FormsUtil.setTextViewTxt(cellText, t.getTypeName());
				 rlContent.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Bundle bundle = new Bundle();
//						bundle.putString(ProductListActivity.TYPE_ID, t.getTypeId());
	                    bundle.putString(ProductListActivity.SEARCH_KEY, t.getTypeName());
	                    baseActivity.callMe(ProductListActivity.class, bundle);
					}
				});
			     
			}
		});
        marketAction = (MarketAction) getController().getAction(baseActivity, this, MarketAction.class);
        ptAction = (ProductTypeAction) controller.getAction(baseActivity, this, ProductTypeAction.class);
        request(loadingView).start();
    }

    private HttpTrans request(LoadingView loadingView) {
    	marketAction.loadingAdv().start(false);
        HttpTrans httpTrans = getController().getTrans(baseActivity);
//        httpTrans.addRequest(marketAction.loadingAdv());
//        httpTrans.addRequest(marketAction.loadingProductList("0"));
        // 获得上界面传递参数
        QProductType bean = new QProductType();
        bean.setTypeId("0");
        bean.setGrade("1");
        RequestBean<QProductType> params = new RequestBean<QProductType>(bean);
        httpTrans.addRequest(ptAction.loadingProductType(params));
        if (loadingView != null) {
            httpTrans.setLoadingView(loadingView);
        }
//        httpTrans.setFinshListener(new HttpTrans.OnFinshListener() {
//            @Override
//            public void onFinish() {
//                swipeRefreshLayout.setRefreshing(false);
//            }
//        });
        return httpTrans;
    }

    /**
     * 广告请求成功回调
     */
    public void advSuccess(List<ImageAD> list) {
//        swipeRefreshLayout.setRefreshing(false);
        if (list != null && list.size() > 0) {
            imageList.clear();
            imageList.addAll(list);
//            marketAdapter.notifyItemChanged(0);
            imageAdapter.notifyDataSetChanged();
            if(list.size() > 1){
            	handler.sendEmptyMessageDelayed(NEXTIMAGE, 3000);
            }
        }
    }

    /**
     * 商品列表请求成功回调
     */
    public void productListSuccess(List<MarketItem> list) {
//        swipeRefreshLayout.setRefreshing(false);
        if (list != null && list.size() > 0) {
//            productList.clear();
//            productList.addAll(list);
//            marketAdapter.notifyDataSetChanged();
        }
    }
    
    /**
     * 优惠活动请求成功回调
     */
    public void favorableSuccess(List<String> list){
    	if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ImageView iv = new ImageView(baseActivity);
                iv.setScaleType(ScaleType.FIT_XY);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 300);
                layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
                layoutParams.topMargin = 10;
                iv.setLayoutParams(layoutParams);
                baseActivity.getImageLoader().displayImage(list.get(i), iv, ImageViewUtil.getOption(), 1);
                llFavorable.addView(iv);
            }
        }
    }
    
    /**
     * 商品类型请求成功回调
     */
    public void typeListSuccess(List<ProductListBean> list){
    	if (list != null && list.size() != 0) {
    		productList.clear();
    		productList.addAll(list);
	        typeAdapter.notifyDataSetChanged();
    	} 
    }

}
