package com.abc.sz.app.activity.set;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.UserAction;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.view.toast.MyToast;

/**
 * 意见反馈
 *
 * @author ftl
 */
@ContentView(R.layout.activity_suggestion)
public class SuggestionActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.et_suggestion)
    private EditText et_suggestion;
    private UserAction userAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        userAction = (UserAction) controller.getAction(this, UserAction.class);
    }

    @OnClick(R.id.btn_send)
    public void btn_send(View view) {
        if (isLogin()) {
            String content = et_suggestion.getText().toString();
            if ("".equals(content.trim())) {
                MyToast.show(this, "请输入反馈内容!", MyToast.TEXT,
                        Gravity.CENTER, Toast.LENGTH_SHORT);
            } else {
                PackageInfo packageInfo = null;
                try {
                    packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    userAction.sendSuggestion(content, packageInfo.versionName, "android").start();
                } catch (NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
