package com.abc.sz.app.action;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.Gravity;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity.order.ChoiceCardPayActivity;
import com.abc.sz.app.activity.pay.KMaActivity;
import com.abc.sz.app.activity.personal.BindNewCardActivity;
import com.abc.sz.app.activity.personal.CardManagerActivity;
import com.abc.sz.app.activity.personal.ConsumeActivity;
import com.abc.sz.app.activity.personal.LoginActivity;
import com.abc.sz.app.activity.personal.MyFavoritesActivity;
import com.abc.sz.app.activity.personal.MyInfoActivity;
import com.abc.sz.app.activity.personal.PayCodeActivity;
import com.abc.sz.app.activity.personal.PerfectUserInfoActivity;
import com.abc.sz.app.activity.personal.RegisterActivity;
import com.abc.sz.app.activity.personal.ResetPasswordActivity;
import com.abc.sz.app.activity.personal.UpdatePhoneActivity;
import com.abc.sz.app.activity_phase2.lightpay.CheckPasswrodActivity;
import com.abc.sz.app.bean.PayCode;
import com.abc.sz.app.bean.personal.Favorites;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.other.QSuggestion;
import com.abc.sz.app.http.bean.personal.QConsumes;
import com.abc.sz.app.http.bean.personal.QLogin;
import com.abc.sz.app.http.bean.personal.QPerfectUserInfo;
import com.abc.sz.app.http.bean.personal.QRegister;
import com.abc.sz.app.http.bean.personal.QResetPassword;
import com.abc.sz.app.http.bean.personal.QUpdatePhone;
import com.abc.sz.app.http.bean.personal.QUser;
import com.abc.sz.app.http.bean.personal.QUserCard;
import com.abc.sz.app.http.bean.personal.RConsume;
import com.abc.sz.app.http.bean.personal.RLogin;
import com.abc.sz.app.http.bean.personal.RUserCard;
import com.abc.sz.app.http.bean.personal.RUserScore;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.abc.sz.app.util.DialogUtil;
import com.abc.sz.app.view.drawer.UserInfoView;
import com.alibaba.fastjson.TypeReference;
import com.forms.base.ABCActivity;
import com.forms.base.LoginUser;
import com.forms.library.base.BaseActivity;
import com.forms.library.baseUtil.asynchttpclient.RequestParams;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.view.crouton.Crouton;
import com.forms.view.crouton.Style;
import com.forms.view.toast.MyToast;

import java.util.List;

/**
 * Created by hwt on 14/11/4.
 * 用户相关接口
 */
public class UserAction extends BaseAction {
    /**
     * 用户登录
     *
     * @param username
     * @param passwd
     * @return
     */
    public Http login(String username, String passwd) {
        QLogin login = new QLogin(username, passwd, Build.VERSION.RELEASE, Build.MODEL, "1");
        RequestBean<QLogin> params = new RequestBean<QLogin>(null, login);
        return APPRequestPhase2.userLogin(params,
                new ResponseNotify<RLogin>(new TypeReference<RespBean<RLogin>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<RLogin> response) {
                        if (uiObject instanceof LoginActivity) {
                            LoginUser loginUser = new LoginUser(response.getContent());
                            getApplication().login(loginUser);
                            ((LoginActivity) uiObject).loginSuccess();
                        } else if (uiObject instanceof CheckPasswrodActivity) {
                            ((CheckPasswrodActivity) uiObject).checkSuccess();
                        }
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        if (uiObject instanceof LoginActivity) {
                            ((LoginActivity) uiObject).loginFailed(retCode);
                        } else if (uiObject instanceof CheckPasswrodActivity) {
                            ((CheckPasswrodActivity) uiObject).checkFailed(retCode);
                        }
                    }
                });
    }

    /**
     * 用户登出
     *
     * @return
     */
    public Http logout() {

        getApplication().logout();
        return APPRequestPhase2.userLogout(getAccessToken(),
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        Crouton.showText((ABCActivity) uiObject, R.string.userlogout_success, Style.INFO);
                        getApplication().logout();
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        Crouton.showText((ABCActivity) uiObject, R.string.userlogout_failed, Style.INFO);
                    }
                });
    }

    /**
     * 获取注册短信验证码
     *
     * @param phone
     * @return
     */
    public Http getRegisterCode(String phone) {
        return APPRequestPhase2.getRegisterCode(phone,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        ((RegisterActivity) uiObject).requestSuccess();
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        MyToast.showFAILED((ABCActivity) uiObject, retCode.getRetMsg());
                    }
                });
    }

    /**
     * 用户注册
     *
     * @param qRegister
     * @return
     */
    public Http register(QRegister qRegister) {
        RequestBean<QRegister> params = new RequestBean<QRegister>(null, qRegister);
        return APPRequestPhase2.userRegister(params,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        ((RegisterActivity) uiObject).registerSucceess(retCode, response.getContent());
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        ((RegisterActivity) uiObject).registerFailed(retCode);
                    }
                });
    }

    /**
     * 查询用户已绑定卡列表
     *
     * @return
     */
    public Http queryBindCardList() {
        return APPRequestPhase2.queryBindCardList(getAccessToken(),
                new ResponseNotify<List<RUserCard>>(new TypeReference<RespBean<List<RUserCard>>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<List<RUserCard>> response) {
                        if (uiObject instanceof CardManagerActivity) {
                            ((CardManagerActivity) uiObject).loadedCardList(retCode, response.getContent());
                        } else if (uiObject instanceof KMaActivity) {
                            ((KMaActivity) uiObject).loadedCardList(retCode, response.getContent());
                        }else if (uiObject instanceof ChoiceCardPayActivity) {
                            ((ChoiceCardPayActivity) uiObject).loadedCardList(retCode, response.getContent());
                        }
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        if (uiObject instanceof CardManagerActivity) {
                            ((CardManagerActivity) uiObject).loadedCardListFailed(retCode);
                        }else if (uiObject instanceof ChoiceCardPayActivity) {
                            ((ChoiceCardPayActivity) uiObject).loadedCardListFailed(retCode);
                        }
                    }
                });
    }

    /**
     * 解除指定卡绑定
     *
     * @param userCard
     * @param phone
     * @return
     */
    public Http unBindCard(RUserCard userCard, String phone) {
        return APPRequestPhase2.unBindCard(getAccessToken(), userCard.getCardId(), userCard.getCardNum(), phone,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        if (uiObject instanceof CardManagerActivity) {
                            CardManagerActivity activity = ((CardManagerActivity) uiObject);
                            activity.unBindCardSuccess();
                            MyToast.showTEXT(activity, "已解除绑定");
                        }
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
                    }
                });
    }

    /**
     * 设置为默认支付卡
     *
     * @param userCard
     * @return
     */
    public Http setDefaultCard(RUserCard userCard) {
        return APPRequestPhase2.setDefaultCard(getAccessToken(), userCard.getCardId(),
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        if (uiObject instanceof CardManagerActivity) {
                            CardManagerActivity activity = ((CardManagerActivity) uiObject);
                            activity.setDefaultSuccess(retCode);
                            MyToast.showTEXT(activity, activity.getString(R.string.setDefaultCarded));
                        }
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        DialogUtil.show((Context) uiObject, retCode.getRetMsg());
                    }
                });
    }

    /**
     * 新增绑卡
     *
     * @param qUserCard
     * @return
     */
    public Http bindNewCard(QUserCard qUserCard) {
        RequestBean<QUserCard> params = new RequestBean<QUserCard>(getAccessToken(), qUserCard);
        return APPRequestPhase2.bindNewCard(params,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        BindNewCardActivity cardActivity = (BindNewCardActivity) uiObject;
                        cardActivity.finishForBack(BindNewCardActivity.RESULT_BINDCARD);
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        MyToast.showFAILED((ABCActivity) uiObject, retCode.getRetMsg());
                    }
                });
    }

    /**
     * 查询用户消费列表
     *
     * @param pageNum
     * @param startDate
     * @param endDate
     * @return
     */
    public Http queryConsumes(Integer pageNum, String startDate, String endDate) {
        QConsumes qConsumes = new QConsumes(String.valueOf(pageNum), startDate, endDate);
        RequestBean<QConsumes> params = new RequestBean<QConsumes>(getAccessToken(), qConsumes);
        return APPRequestPhase2.queryConsumes(params,
                new ResponseNotify<List<RConsume>>(new TypeReference<RespBean<List<RConsume>>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<List<RConsume>> response) {
                        ((ConsumeActivity) uiObject).queryFinish(retCode, response.getContent());
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        ((ConsumeActivity) uiObject).queryFailed(retCode);
                    }
                });
    }

    /**
     * 查询收藏列表
     *
     * @param pageNum
     * @return
     */
    public Http queryFavorites(Integer pageNum) {
        return APPRequestPhase2.queryFavourites(getAccessToken(), pageNum,
                new ResponseNotify<List<Favorites>>(new TypeReference<RespBean<List<Favorites>>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<List<Favorites>> response) {
                        ((MyFavoritesActivity) uiObject).loadFavouritesSuccess(retCode, response.getContent());
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        ((MyFavoritesActivity) uiObject).loadFavouritesFailed(retCode);
                    }
                });
    }

    /**
     * 修改用户密码
     *
     * @param oldPass
     * @param newPass
     * @return
     */
    public Http modifyPassword(String oldPass, String newPass) {
        QUser user = new QUser(oldPass, newPass);
        RequestBean<QUser> params = new RequestBean<QUser>(getAccessToken(), user);
        return APPRequestPhase2.modifyPasswd(params,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        final ABCActivity activity = (ABCActivity) uiObject;
                        DialogUtil.showWithTwoBtn(activity, "修改密码成功，请重新登录",
                                activity.getString(R.string.action_sure),
                                activity.getString(R.string.action_cancel),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        activity.getBaseApp().logout();
                                        activity.callMe(LoginActivity.class);
                                        activity.finishForBack(Activity.RESULT_OK);

                                    }
                                }, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        activity.finish();
                                    }
                                }
                        );
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        MyToast.show((BaseActivity) uiObject, retCode.getRetMsg(), MyToast.TEXT,
                                Gravity.CENTER, Toast.LENGTH_SHORT);
                    }
                });
    }

    /**
     * 意见反馈
     *
     * @param content
     * @param version
     * @param platform
     * @return
     */
    public Http sendSuggestion(String content, String version, String platform) {
        QSuggestion qSuggestion = new QSuggestion(content, version, platform);
        RequestBean<QSuggestion> params = new RequestBean<QSuggestion>(getAccessToken(), qSuggestion);
        return APPRequestPhase2.sendSuggestion(params,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        BaseActivity activity = (BaseActivity) uiObject;
                        MyToast.show(activity, "意见反馈成功!", MyToast.TEXT,
                                Gravity.CENTER, Toast.LENGTH_SHORT);
                        activity.finish();
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
                    }
                });
    }

    /**
     * 查询支付码列表
     *
     * @return
     */
    public Http queryPayCodeList(String pageNum) {
        return APPRequestPhase2.queryPayCodeList(getAccessToken(), pageNum,
                new ResponseNotify<List<PayCode>>(new TypeReference<RespBean<List<PayCode>>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<List<PayCode>> response) {
                        ((PayCodeActivity) uiObject).querySuccess(retCode, response.getContent());
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        ((PayCodeActivity) uiObject).queryFailed(retCode);
                    }
                });
    }

    /**
     * 获取重置密码短信验证码
     *
     * @param phone
     * @return
     */
    public Http getResetPasswordCode(String phone) {
        RequestParams requestParams = new RequestParams();
        requestParams.put("param", "{\"phone\":\"" + phone + "\"}");
        return APPRequestPhase2.getResetPasswordCode(requestParams,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        ((ResetPasswordActivity) uiObject).requestSuccess();
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        MyToast.show((BaseActivity) uiObject, retCode.getRetMsg(), MyToast.TEXT,
                                Gravity.CENTER, Toast.LENGTH_SHORT);
                    }
                });
    }

    /**
     * 重置用户密码
     *
     * @param phone
     * @param email
     * @param newPassWord
     * @return
     */
    public Http resetPassword(String phone, String email,
                              String newPassWord, String checkCode) {
        QResetPassword qResetPassword = new QResetPassword(phone, email, newPassWord, checkCode);
        RequestBean<QResetPassword> params = new RequestBean<QResetPassword>(getAccessToken(), qResetPassword);
        return APPRequestPhase2.resetPasswd(params,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        BaseActivity activity = (BaseActivity) uiObject;
                        MyToast.show(activity, "重置密码成功!", MyToast.TEXT,
                                Gravity.CENTER, Toast.LENGTH_SHORT);
                        activity.finish();
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        MyToast.show((BaseActivity) uiObject, retCode.getRetMsg(), MyToast.TEXT,
                                Gravity.CENTER, Toast.LENGTH_SHORT);
                    }
                });
    }

    /**
     * 完善用戶信息
     *
     * @param userNickName
     * @param gender
     * @param email
     * @return
     */
    public Http perfectUserInfo(String userNickName, String gender, String email) {
        QPerfectUserInfo qPerfectUserInfo = new QPerfectUserInfo(userNickName, gender, email);
        RequestBean<QPerfectUserInfo> params = new RequestBean<QPerfectUserInfo>(getAccessToken(), qPerfectUserInfo);
        return APPRequestPhase2.perfectUserInfo(params,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        ((PerfectUserInfoActivity) uiObject).perfectInfoSuccess();
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        MyToast.show((BaseActivity) uiObject, retCode.getRetMsg(), MyToast.TEXT,
                                Gravity.CENTER, Toast.LENGTH_SHORT);
                    }
                });
    }

    /**
     * 获取修改手机号码短信验证码
     *
     * @return
     */
    public Http getUpdatePhoneCode() {
        return APPRequestPhase2.getUpdatePhoneCode(getAccessToken(),
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        ((UpdatePhoneActivity) uiObject).requestSuccess();
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        MyToast.show((BaseActivity) uiObject, retCode.getRetMsg(), MyToast.TEXT,
                                Gravity.CENTER, Toast.LENGTH_SHORT);
                    }
                });
    }

    /**
     * 修改手机号码
     *
     * @param newphone
     * @param checkcode
     * @return
     */
    public Http updatePhone(String newphone, String checkcode) {
        QUpdatePhone qUpdatePhone = new QUpdatePhone(newphone, checkcode);
        RequestBean<QUpdatePhone> params = new RequestBean<QUpdatePhone>(getAccessToken(), qUpdatePhone);
        return APPRequestPhase2.updatePhone(params,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        BaseActivity activity = (BaseActivity) uiObject;
                        MyToast.show(activity, "修改手机号码成功!", MyToast.TEXT,
                                Gravity.CENTER, Toast.LENGTH_SHORT);
                        activity.finish();
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        MyToast.show((BaseActivity) uiObject, retCode.getRetMsg(), MyToast.TEXT,
                                Gravity.CENTER, Toast.LENGTH_SHORT);
                    }
                });
    }

    /**
     * 签到
     *
     * @return
     */
    public Http checkin() {
        return APPRequestPhase2.checkin(getAccessToken(),
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        if (uiObject instanceof MyInfoActivity) {
                            ((MyInfoActivity) uiObject).signSuccess();
                        }

                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        MyToast.show((BaseActivity) uiObject, retCode.getRetMsg(), MyToast.TEXT,
                                Gravity.CENTER, Toast.LENGTH_SHORT);
                    }
                });
    }

    /**
     * 查询个人总积分
     *
     * @return
     */
    public Http queryUserScore() {
        return APPRequestPhase2.queryUserScore(getAccessToken(),
                new ResponseNotify<RUserScore>(new TypeReference<RespBean<RUserScore>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<RUserScore> response) {
                        if (uiObject instanceof MyInfoActivity) {
                            ((MyInfoActivity) uiObject).querySuccess(response.getContent());
                        } else if (uiObject instanceof UserInfoView) {
                            uiObject.requestSuccess(retCode, response.getContent());
                        }
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        if (uiObject instanceof MyInfoActivity) {
                            ((MyInfoActivity) uiObject).queryFailed(retCode);
                        } else if (uiObject instanceof UserInfoView) {
                            uiObject.requestFalied(retCode);
                        }
                    }
                });
    }
}
