package com.abc.sz.app.activity_phase2.creditcard;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.CreditcardAction;
import com.abc.sz.app.bean.cridetcard.CarShop;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;

/**
 * 车商信息页面
 * 
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_carshopinfo)
public class CarShopInfoActivity extends ABCActivity {

	@ViewInject(R.id.appBar) Toolbar toolbar;
	@ViewInject(R.id.loadingView) LoadingView loadingView;
	@ViewInject(R.id.tvShopName) TextView tvShopName;
	@ViewInject(R.id.tvShopAddress) TextView tvShopAddress;
	@ViewInject(R.id.tvClientPhone) TextView tvClientPhone;
	@ViewInject(R.id.tvPoundage) TextView tvPoundage;
	
	private CreditcardAction creditcardAction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		creditcardAction = (CreditcardAction) controller.getAction(this, CreditcardAction.class);
		String carShopID = cacheBean.getStringCache("carShopID");
		creditcardAction.queryCarShopDetail(carShopID).setLoadingView(loadingView).start();
	}
	
	/**
     * 车商详情查询成功回调
     */
    public void querySuccess(CarShop carShop) {
        if (carShop != null) {
			FormsUtil.setTextViewTxt(tvShopName, carShop.getCarShopName());
			FormsUtil.setTextViewTxt(tvShopAddress, carShop.getCarShopAddress());
			FormsUtil.setTextViewTxt(tvClientPhone, carShop.getCarShopPhone());
			FormsUtil.setTextViewTxt(tvPoundage, carShop.getCarShopPoundage());
        }
    }
}
