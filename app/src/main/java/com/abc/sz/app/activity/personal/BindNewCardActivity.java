package com.abc.sz.app.activity.personal;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.PayAction;
import com.abc.sz.app.action.UserAction;
import com.abc.sz.app.action_phase2.LightpayAction;
import com.abc.sz.app.http.bean.pay.AESBean;
import com.abc.sz.app.http.bean.personal.QUserCard;
import com.abc.sz.app.http.bean.personal.RUserCard;
import com.abc.sz.app.util.DialogUtil;
import com.abc.sz.app.view.CountDownButton;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.log.LogUtils;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.baseUtil.view.annotation.event.OnRadioGroupCheckedChange;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.encrypt.AESUtils;
import com.forms.library.tools.encrypt.DESUtils;
import com.forms.view.toast.MyToast;

/**
 * Created by hwt on 14/11/20.
 * 绑定新卡
 */
@ContentView(R.layout.activity_bind_newcard)
public class BindNewCardActivity extends ABCActivity {

    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.name) EditText name;
    @ViewInject(R.id.newCardType) TextView newCardType;
    @ViewInject(R.id.newCardNum) EditText newCardNum;
    @ViewInject(R.id.idType) TextView idType;
    @ViewInject(R.id.idNum) EditText idNum;
    @ViewInject(R.id.phoneNo) EditText phoneNo;
    @ViewInject(R.id.messageCode) EditText messageCode;
    @ViewInject(R.id.btnAuthCode) CountDownButton btnAuthCode;

    private QUserCard qUserCard = new QUserCard();
    private AlertDialog alertDialog;
    private String[] cardTypes = null;
    private String[] cardTypeValues = null;
    private UserAction userAction;
    public final static int RESULT_BINDCARD = 100;

    private LightpayAction lightpayAction;
    //支付请求
    private PayAction payAction;
    //加密因子
    private String seed = "";
    private String[] certificateKey;
    private String[] certificateValue;
    private String certificate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        cardTypes = getResources().getStringArray(R.array.selected_cardType);
        cardTypeValues = getResources().getStringArray(R.array.selected_cardType_values);
        alertDialog = DialogUtil.getListDialog(this, getString(R.string.newcard_cardType),
                cardTypes, 0, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        qUserCard.setCardType(cardTypeValues[which]);
                        FormsUtil.setTextViewTxt(newCardType, cardTypes[which]);
                        dialog.dismiss();
                    }
                });
        qUserCard.setIsDefault(String.valueOf(0));
        certificateKey = getResources().getStringArray(R.array.certtype_key);
        certificateValue = getResources().getStringArray(R.array.certtype_value);
        userAction = (UserAction) controller.getAction(this, UserAction.class);
        lightpayAction = (LightpayAction) controller.getAction(this, LightpayAction.class);
        payAction = (PayAction) controller.getAction(this, PayAction.class);
        payAction.getAesKey().start();
    }

    /**
     * @param group
     * @param checkedId
     */
    @OnRadioGroupCheckedChange(R.id.isDefault)
    public void isDefaultCheckedChange(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.setDefault:
                qUserCard.setIsDefault(String.valueOf(1));
                break;
            case R.id.noDefault:
                qUserCard.setIsDefault(String.valueOf(0));
                break;
        }
    }

    /**
     * @param view
     */
    @OnClick(value = {R.id.bindNewCardBtn, R.id.newCardType, R.id.idType, R.id.btnAuthCode})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.newCardType:
                alertDialog.show();
                break;
            case R.id.bindNewCardBtn:
                if (checknNewCard()) {
                    if (TextUtils.isEmpty(messageCode.getText().toString())) {
                        FormsUtil.setErrorHtmlTxt(this, messageCode, R.string.input_authcode_isempty);
                        return;
                    }
                    try {
                        qUserCard.setCardNum(AESUtils.encryptMyBase64(seed, newCardNum.getText().toString()));
                        qUserCard.setPhone(phoneNo.getText().toString());
                        qUserCard.setCheckCode(messageCode.getText().toString());
                        qUserCard.setCustName(name.getText().toString());
                        qUserCard.setIdType(certificate);
                        qUserCard.setIdNumber(idNum.getText().toString());
                        userAction.bindNewCard(qUserCard).start();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.idType:
                DialogUtil.showToSelect(this, null, certificateValue, new OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FormsUtil.setTextViewTxt(idType, certificateValue[which]);
                        certificate = certificateKey[which];
                    }
                });
                break;
            case R.id.btnAuthCode:
                if (checknNewCard()) {
                    String custName = name.getText().toString();
                    String idNumber = idNum.getText().toString();
                    String cardNum = newCardNum.getText().toString();
                    String phone = phoneNo.getText().toString();
                    try {
                        cardNum = AESUtils.encryptMyBase64(seed, cardNum);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    lightpayAction.checkCode(custName, certificate, idNumber, cardNum, phone).start();
                }
                break;
        }
    }

    /**
     * 检查输入项
     *
     * @return
     */
    private boolean checknNewCard() {
        String name = this.name.getText().toString();
        String newCardType = this.newCardType.getText().toString();
        String newCardNum = this.newCardNum.getText().toString();
        String idType = this.idType.getText().toString();
        String idNum = this.idNum.getText().toString();
        String phoneNo = this.phoneNo.getText().toString();
        if (TextUtils.isEmpty(name)) {
            FormsUtil.setErrorHtmlTxt(this, this.name, R.string.input_username_isempty);
            return false;
        } else if (TextUtils.isEmpty(idType)) {
            MyToast.show(this, "请选择证件类型", MyToast.TEXT, Gravity.CENTER, Toast.LENGTH_SHORT);
            return false;
        } else if (TextUtils.isEmpty(idNum)) {
            FormsUtil.setErrorHtmlTxt(this, this.idNum, R.string.input_certNo_isempty);
            return false;
        } else if (TextUtils.isEmpty(newCardType)) {
            MyToast.show(this, "请选择卡类型", MyToast.TEXT, Gravity.CENTER, Toast.LENGTH_SHORT);
            return false;
        } else if (TextUtils.isEmpty(newCardNum)) {
            FormsUtil.setErrorHtmlTxt(this, this.newCardNum, R.string.input_newCardNum_isEmpty);
            return false;
        } else if (newCardNum.length() != 19) {
            FormsUtil.setErrorHtmlTxt(this, this.newCardNum, R.string.input_nineteen_cardNum);
            return false;
        } else if (TextUtils.isEmpty(phoneNo)) {
            FormsUtil.setErrorHtmlTxt(this, this.phoneNo, R.string.please_input_phone);
            return false;
        } else if (phoneNo.length() != 11) {
            FormsUtil.setErrorHtmlTxt(this, this.phoneNo, R.string.input_eleven_phone);
            return false;
        } else if ("".equals(seed)) {
            payAction.getAesKey().start();
            return false;
        }
        return true;
    }

    /**
     * 查询加密因子成功
     *
     * @param bean
     */
    public void querySuccess(AESBean bean) {
        String userId = getBaseApp().getLoginUser().getUserId();
        String desKey = "";
        if (userId.length() < 8) {
            desKey = userId + String.format("%1$0" + (8 - userId.length()) + "d", 0);
        } else {
            desKey = userId;
        }
        if (bean != null) {
            try {
                seed = DESUtils.decryptBase64(desKey.getBytes(), desKey.getBytes(), bean.getKey());
                LogUtils.d("seed = " + seed);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * 身份验证发送成功
     *
     * @param content
     */
    public void requestSuccess(String content) {
        btnAuthCode.timer.start();
        qUserCard.setFollow(content);
    }

    public String formatNumber(String num) {
        StringBuffer sb = new StringBuffer();
        for (int i = num.length() - 1; i > 0; i--) {
            sb.append(num.charAt(i));
            if ((num.length() - i) % 3 == 0) {
                sb.append(",");
            }
        }
        return sb.toString();
    }

}
