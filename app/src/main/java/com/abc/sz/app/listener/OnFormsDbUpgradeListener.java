package com.abc.sz.app.listener;

import com.forms.library.baseUtil.db.DbUtils;
import com.forms.library.baseUtil.log.LogUtils;

/**
 * Created by huwentao on 2014/5/20.
 */
public class OnFormsDbUpgradeListener implements DbUtils.DbUpgradeListener {
    @Override
    public void onUpgrade(DbUtils db, int oldVersion, int newVersion) {
        LogUtils.i("----------------------数据库升级----------------------");
        LogUtils.i("oldVersion:" + oldVersion + "\t  newVersion:" + newVersion);
        if (newVersion > oldVersion) {

        }
    }

}
