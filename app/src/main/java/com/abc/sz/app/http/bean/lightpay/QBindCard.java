package com.abc.sz.app.http.bean.lightpay;

public class QBindCard {

	private String custName;//用户名
    private String idType;  //证件类型
    private String idNumber;//证件号码
    private String cardNum;	//银行卡号
    private String phone;	//电话号码
    private String checkCode;//验证码
    private String userId;   //光id
    private String follow;   //系统跟踪号
    private String operateType;//操作类型

    public QBindCard(){}

	public QBindCard(String custName, String idType, String idNumber,
			String cardNum, String phone) {
		super();
		this.custName = custName;
		this.idType = idType;
		this.idNumber = idNumber;
		this.cardNum = cardNum;
		this.phone = phone;
	}

	public QBindCard(String cardNum, String phone, String checkCode,
			String userId, String follow, String operateType) {
		super();
		this.cardNum = cardNum;
		this.phone = phone;
		this.checkCode = checkCode;
		this.userId = userId;
		this.follow = follow;
		this.operateType = operateType;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getCardNum() {
		return cardNum;
	}

	public void setCardNum(String cardNum) {
		this.cardNum = cardNum;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCheckCode() {
		return checkCode;
	}

	public void setCheckCode(String checkCode) {
		this.checkCode = checkCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFollow() {
		return follow;
	}

	public void setFollow(String follow) {
		this.follow = follow;
	}

	public String getOperateType() {
		return operateType;
	}

	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}
	
}
