package com.abc.sz.app.adapter.order;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.OrderAction;
import com.abc.sz.app.activity.order.ChoiceCardPayActivity;
import com.abc.sz.app.activity.order.OrderDetailsActivity;
import com.abc.sz.app.activity.order.OrderState;
import com.abc.sz.app.activity.order.ProductEvaluateActivity;
import com.abc.sz.app.bean.Order;
import com.abc.sz.app.util.ShoppingCarCount;
import com.forms.base.ABCActivity;
import com.forms.base.XDRImageLoader;
import com.forms.library.baseUtil.net.Controller;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

import java.util.List;

/**
 * 实物订单列表适配
 *
 * @author ftl
 */
public class EntityOrderAdapter extends BaseAdapter {

    private List<Order> list;
    private ABCActivity activity;
    private OrderAction orderAction;
    private XDRImageLoader mImageLoader;

    public EntityOrderAdapter(List<Order> list, ABCActivity activity) {
        this.list = list;
        this.activity = activity;
        this.orderAction = (OrderAction) Controller.getInstance().getAction(activity, OrderAction.class);
        this.mImageLoader = activity.getImageLoader();
    }

    public void setList(List<Order> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Order order = list.get(position);
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_entity_order_item, null);
        }
        ListView lvOrder = ViewHolder.get(convertView, R.id.lv_order);
        EntityOrderProductAdapter adapter = new EntityOrderProductAdapter(order.getProductList(), mImageLoader);
        lvOrder.setAdapter(adapter);
        TextView tvSellerName = ViewHolder.get(convertView, R.id.tv_sellerName);
        final TextView tvPrice = ViewHolder.get(convertView, R.id.tv_price);
        TextView tvOperate = ViewHolder.get(convertView, R.id.tv_operate);
        TextView tvState = ViewHolder.get(convertView, R.id.tv_state);
        if (order != null) {
            final String state = order.getState();
            FormsUtil.setTextViewTxt(tvSellerName, order.getSellerName(), null);
            tvPrice.setText(ShoppingCarCount.formatMoney(order.getAmount()));
            if (state != null) {
                if (OrderState.success.getCode().equals(state)) {
                    //评价
                    tvOperate.setText(activity.getResources().getString(R.string.product_evaluate));
                } else if (OrderState.noGet.getCode().equals(state)) {
                    //确认收货
                    tvOperate.setText(activity.getResources().getString(R.string.product_affirm_harvest));
                } else if (OrderState.noPay.getCode().equals(state)
                        || OrderState.failed.getCode().equals(state)) {
                    //支付
                    tvOperate.setText(activity.getResources().getString(R.string.product_pay));
                } else if (OrderState.waitAffirm.getCode().equals(state)) {
                    //待确认
                    orderAction.syncOrderStatusRequest(order.getOrderId());
                }
                if (OrderState.error.getCode().equals(state)) {
                    tvOperate.setVisibility(View.GONE);
                    tvState.setVisibility(View.VISIBLE);
                    tvState.setText(activity.getResources().getString(R.string.trade_close));
                    tvState.setTextColor(activity.getResources().getColor(R.color.red));
                } else if (OrderState.valuation.getCode().equals(state)) {
                    tvOperate.setVisibility(View.GONE);
                    tvState.setVisibility(View.VISIBLE);
                    tvState.setText(activity.getResources().getString(R.string.trade_success));
                    tvState.setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                } else {
                    tvOperate.setVisibility(View.VISIBLE);
                    tvState.setVisibility(View.GONE);
                }
                tvOperate.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        if (OrderState.success.getCode().equals(state)) {
                            activity.getCacheBean().putCache(order);
                            activity.callMeForBack(ProductEvaluateActivity.class, 1);
                        } else if (OrderState.noGet.getCode().equals(state)) {
                            new AlertDialog.Builder(activity).setMessage("是否确认收货?")
                                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            orderAction.conFirmReceiveGood(order.getOrderId()).start();
                                        }
                                    })
                                    .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).create().show();
                        } else if (OrderState.noPay.getCode().equals(state)
                                || OrderState.failed.getCode().equals(state)) {
                            Bundle bundle = new Bundle();
                            bundle.putString("orderId", order.getOrderId());
                            activity.callMe(ChoiceCardPayActivity.class, bundle);
//                            orderAction.orderImmediatePay(order.getOrderId(), String.valueOf(order.getAmount()),
//                                    "", order.getProductList(), "1").start();
                        }
                    }
                });
            }
            lvOrder.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    activity.getCacheBean().putCache(order);
                    activity.callMeForBack(OrderDetailsActivity.class, 2);
                }
            });
        }
        return convertView;
    }


}
