package com.abc.sz.app.activity_phase2.creditcard;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.view.CellView;
import com.abc.sz.app.view.CellView.CellViewBean;
import com.forms.base.ABCActivity;
import com.forms.library.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.ViewHolder;

/**
 * 信用卡页面
 * 
 * @author hkj
 */
@ContentView(R.layout.activity_phase2_creditcard)
public class CreditCardActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	@ViewInject(R.id.gridView)
	private GridView gridView;

	private List<Integer> resIds = new ArrayList<Integer>() {
		{
			add(R.drawable.icon_card_yysq); add(R.drawable.icon_card_jdxx);
			add(R.drawable.icon_card_ytq); add(R.drawable.icon_card_xykyh);
			add(R.drawable.icon_card_qcfq); add(R.drawable.icon_card_jzfq);
			add(R.drawable.icon_card_fqlcjsq);
			//add(R.drawable.icon_card_zdfq); 
			//add(R.drawable.icon_card_xffq);
			//add(R.drawable.icon_card_xjfq); 
		}
	};

	private List<String> functions = new ArrayList<String>() {
		{
			add("预约申请"); add("进度查询"); add("YOU特权"); add("信用卡you惠");
			add("汽车分期"); add("家装分期"); add("分期理财计算器");
			//add("账单分期");
			//add("消费分期");
			//add("现金分期"); 
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		List<CellViewBean> cellList = new ArrayList<>();
		for (int i = 0; i < functions.size(); i++) {
			cellList.add(new CellViewBean(resIds.get(i), functions.get(i)));
		}

		BaseAdapter<CellViewBean> adapter = new BaseAdapter<CellViewBean>(this, cellList,
				R.layout.layout_phase2_creditcard_item) {
			@Override
			public void viewHandler(final int position, CellViewBean cellViewBean, View convertView) {
				CellView cellView = ViewHolder.get(convertView, R.id.cellView);
				cellView.reset(cellViewBean, gridView.getMeasuredWidth() / gridView.getNumColumns());
				cellView.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						switch (position) {
						case 0:
							callMe(CardApplyActivity.class);
							break;
						case 1:
							callMe(ApplyScheduleActivity.class);
							break;
						case 2:
							callMe(PrerogativeActivity.class);
							break;
						case 3:
							callMe(CardFavorableActivity.class);
							break;
						case 4:
							callMe(CarStagingListActivity.class);
							break;
						case 5:
							callMe(FitmentStagingActivity.class);
							break;
//						case 6:
//							callMe(BillStagingActivity.class);
//							break;
//						case 7:
//							callMe(ConsumeStagingActivity.class);
//							break;
//						case 8:
//							callMe(CashStagingActivity.class);
//							break;
						case 6:
							callMe(CFPCalculatorActivity.class);
							break;
						}	
					}
				});
			}
		};
		gridView.setAdapter(adapter);
	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

}
