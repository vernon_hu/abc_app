package com.abc.sz.app.http.bean.wdmap;


/**
 * 网点地图
 * @author ftl
 *
 */
public class QWdMap{

	private String custname;  //网点搜索关键字
	private String area;	  //区域
	private String distance;  //距离
	private String lon; 	  //经度
	private String lat;       //纬度
	private String pageNum;   //页数
	
	public QWdMap(){}
	
	public QWdMap(String custname, String area, String distance, String lon,
			String lat, String pageNum) {
		super();
		this.custname = custname;
		this.area = area;
		this.distance = distance;
		this.lon = lon;
		this.lat = lat;
		this.pageNum = pageNum;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getCustname() {
		return custname;
	}
	public void setCustname(String custname) {
		this.custname = custname;
	}
	public String getPageNum() {
		return pageNum;
	}
	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}
	
}
