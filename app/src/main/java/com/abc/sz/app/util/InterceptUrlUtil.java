package com.abc.sz.app.util;

import java.net.URLDecoder;

import android.net.Uri;

import com.abc.sz.app.action.OrderAction;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.log.LogUtils;

public class InterceptUrlUtil {

	public static void intercept(ABCActivity activity, String url){
		Uri uri = Uri.parse(url);
		String param = "";
		if(uri != null){
			try {
				param = URLDecoder.decode(uri.toString(), "utf-8");
				String[] array = param.replaceAll("\\s", "").split("param=");
				param = array[1];
			} catch (Exception e) {
	            LogUtils.d(e.getMessage(), e);
	        }
		}
		OrderAction orderAction = (OrderAction) activity.getController().getAction(activity, OrderAction.class);
		orderAction.addOrder(param).start();
	}
}
