package com.abc.sz.app.http.bean.card;


/**
 * 消费详情
 * 
 * @author ftl
 */
public class RConsumerDetail {

	private String cardID;// 信用卡id
	private String cardNumber;// 信用卡号
	private String billOn;// 账单日
	private String paymentDueDate;// 到期还款日
	private String repaymentAmount;// 本期还款金额
	private String minimumRepayment;// 最低还款金额
	
	public String getCardID() {
		return cardID;
	}
	public void setCardID(String cardID) {
		this.cardID = cardID;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getBillOn() {
		return billOn;
	}
	public void setBillOn(String billOn) {
		this.billOn = billOn;
	}
	public String getPaymentDueDate() {
		return paymentDueDate;
	}
	public void setPaymentDueDate(String paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}
	public String getRepaymentAmount() {
		return repaymentAmount;
	}
	public void setRepaymentAmount(String repaymentAmount) {
		this.repaymentAmount = repaymentAmount;
	}
	public String getMinimumRepayment() {
		return minimumRepayment;
	}
	public void setMinimumRepayment(String minimumRepayment) {
		this.minimumRepayment = minimumRepayment;
	}

}
