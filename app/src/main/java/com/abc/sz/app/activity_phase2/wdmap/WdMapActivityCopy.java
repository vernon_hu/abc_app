package com.abc.sz.app.activity_phase2.wdmap;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.WdMap;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationConfiguration.LocationMode;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.overlayutil.DrivingRouteOverlay;
import com.baidu.mapapi.overlayutil.TransitRouteOverlay;
import com.baidu.mapapi.overlayutil.WalkingRouteOverlay;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.route.DrivingRoutePlanOption;
import com.baidu.mapapi.search.route.DrivingRouteResult;
import com.baidu.mapapi.search.route.OnGetRoutePlanResultListener;
import com.baidu.mapapi.search.route.PlanNode;
import com.baidu.mapapi.search.route.RoutePlanSearch;
import com.baidu.mapapi.search.route.TransitRoutePlanOption;
import com.baidu.mapapi.search.route.TransitRouteResult;
import com.baidu.mapapi.search.route.WalkingRoutePlanOption;
import com.baidu.mapapi.search.route.WalkingRouteResult;
import com.forms.base.ABCActivity;
import com.forms.library.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;
import com.forms.view.toast.MyToast;

import java.util.ArrayList;
import java.util.List;

/**
 * 网点地图
 * @author ftl
 *
 */
@ContentView(R.layout.activity_phase2_wdmap_copy)
public class WdMapActivityCopy extends ABCActivity implements BaiduMap.OnMapClickListener,
	OnGetRoutePlanResultListener{
	
	@ViewInject(R.id.appBar) Toolbar toolbar;
	@ViewInject(R.id.mapView) MapView mMapView;
	@ViewInject(R.id.drawerLayout) DrawerLayout drawerLayout;
	@ViewInject(R.id.llContent) LinearLayout llContent;
	@ViewInject(R.id.tvBranchName) TextView tvBranchName;
	@ViewInject(R.id.tvAddress) TextView tvAddress;
	@ViewInject(R.id.listView) ListView listView;
	
	//定位相关
	LocationClient mLocClient;
	//定位监听
	MyLocationListenner myListener = new MyLocationListenner();
	//顶部路径规划切换菜单
	RadioGroup rgMenu;
	//公交对象
	RadioButton rbTransit;
	//驾车对象
	RadioButton rbDriving;
	//步行对象
	RadioButton rbWalking;
	//当前选中对象
	RadioButton rbCurrent;
	//百度地图
	BaiduMap mBaiduMap;
	//目标bitmap信息，不用时及时 recycle
	BitmapDescriptor bd;
	//是否首次定位
	boolean isFirstLoc = true;
	//右侧菜单是否打开
	boolean isOpen = false;
	//是否是路径规划
	boolean isRoute = false;
	//所在位置纬度
	double latitude = 0;
	//所在位置经度
	double longitude = 0;
	//目标地点
	WdMap wdMap;
	//网点列表
	List<WdMap> wdMapList = new ArrayList<>();
	//添加目标点图标
	static final int ADD_RARGET = 0;
	//切换网点
	static final int CHANGE_WD = 1;
	//搜索相关
    RoutePlanSearch mSearch = null;
	//消息队列
	Handler mHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			switch(msg.what){
				case ADD_RARGET:
					//添加目标位置
					bd = BitmapDescriptorFactory.fromResource(R.drawable.icon_gcoding);
					LatLng latLng = new LatLng(Double.parseDouble(wdMap.getLat()), 
							Double.parseDouble(wdMap.getLon()));
					OverlayOptions oo = new MarkerOptions().position(latLng).icon(bd).zIndex(5).draggable(true);
					mBaiduMap.addOverlay(oo);
					break;
				case CHANGE_WD:
					rgMenu.setVisibility(View.GONE);
					llContent.setVisibility(View.VISIBLE);
					drawerLayout.closeDrawer(listView);
					mBaiduMap.clear();
					mHandler.sendEmptyMessage(ADD_RARGET);
					break;
			}
		};
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		if (getSupportActionBar() != null) {
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                   ActionBar.LayoutParams.MATCH_PARENT,
                   ActionBar.LayoutParams.MATCH_PARENT,
                   Gravity.CENTER
            );
            View view = LayoutInflater.from(this).inflate(R.layout.layout_phase2_wdmap, null);
            rgMenu = (RadioGroup) view.findViewById(R.id.rgMenu);
            rbTransit = (RadioButton) view.findViewById(R.id.rbTransit);
            rbDriving = (RadioButton) view.findViewById(R.id.rbDriving);
            rbWalking = (RadioButton) view.findViewById(R.id.rbWalking);
            rbTransit.setOnClickListener(new MyOnclickListener());
            rbDriving.setOnClickListener(new MyOnclickListener());
            rbWalking.setOnClickListener(new MyOnclickListener());
            rbCurrent = rbDriving;
            rgMenu.setVisibility(View.GONE);
            getSupportActionBar().setCustomView(view, params);
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	        getSupportActionBar().setHomeButtonEnabled(true);
	    }
		
	    init();
		initListener();
	}
	
	@SuppressWarnings("unchecked")
	private void init(){
		// 地图初始化
		mBaiduMap = mMapView.getMap();
		// 隐藏缩放控件
		mMapView.showZoomControls(false);
		// 开启定位图层
		mBaiduMap.setMyLocationEnabled(true);
		mBaiduMap.setMyLocationConfigeration(new MyLocationConfiguration(
				LocationMode.NORMAL, true, null));
		
		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			latitude = bundle.getDouble("latitude");
			longitude = bundle.getDouble("longitude");
			wdMap = (WdMap) bundle.get("wdMap");
			toolbar.setTitle(wdMap.getBranchName());
			FormsUtil.setTextViewTxt(tvBranchName, wdMap.getBranchName());
			FormsUtil.setTextViewTxts(tvAddress, getString(R.string.wdAddress), wdMap.getAddress());
			wdMapList = (List<WdMap>)bundle.get("wdMapList");
			listView.setAdapter(new BaseAdapter<WdMap>(this, wdMapList, R.layout.layout_phase2_wdlist_item) {

				@Override
				public void viewHandler(int position, final WdMap wdMap, View convertView) {
					ImageView ivArrow = ViewHolder.get(convertView, R.id.ivArrow);
					TextView tvBranchName = ViewHolder.get(convertView, R.id.tvBranchName);
					TextView tvRange = ViewHolder.get(convertView, R.id.tvRange);
					TextView tvAddress = ViewHolder.get(convertView, R.id.tvAddress);
					TextView tvPhone = ViewHolder.get(convertView, R.id.tvPhone);
					ivArrow.setVisibility(View.GONE);
					if(wdMap != null){
						 FormsUtil.setTextViewTxt(tvBranchName, wdMap.getBranchName());
						 FormsUtil.setTextViewTxt(tvRange, wdMap.getDistance());
						 FormsUtil.setTextViewTxts(tvAddress, getString(R.string.wdAddress), wdMap.getAddress());
						 FormsUtil.setTextViewTxts(tvPhone, getString(R.string.wdPhone), wdMap.getPhone());
					}
				}
			});
	        if(latitude != 0 && longitude != 0){
	        	MyLocationData locData = new MyLocationData.Builder()
					// 此处设置开发者获取到的方向信息，顺时针0-360
					.direction(100).latitude(latitude).longitude(longitude).build();
	        	mBaiduMap.setMyLocationData(locData);
	        }else{
	        	// 定位初始化
	    		mLocClient = new LocationClient(this);
	    		mLocClient.registerLocationListener(myListener);
	    		LocationClientOption option = new LocationClientOption();
	    		option.setOpenGps(true);// 打开gps
	    		option.setCoorType("bd09ll"); // 设置坐标类型
	    		option.setScanSpan(1000);
	    		mLocClient.setLocOption(option);
	    		mLocClient.start();
	        }
	        //网点所在位置纬度
			double lat = Double.parseDouble(wdMap.getLat());
			//网点所在位置经度
			double lon = Double.parseDouble(wdMap.getLon());
			//设定中心点坐标 
	        LatLng cenpt = new LatLng(lat, lon); 
	        //定义地图状态
	        MapStatus mMapStatus = new MapStatus.Builder().target(cenpt).zoom(16).build();
	        //定义MapStatusUpdate对象，以便描述地图状态将要发生的变化
	        MapStatusUpdate mMapStatusUpdate = MapStatusUpdateFactory.newMapStatus(mMapStatus);
	        //改变地图状态
	        mBaiduMap.setMapStatus(mMapStatusUpdate);
	        
	        mHandler.sendEmptyMessage(ADD_RARGET);
		}
	}
	
	private void initListener() {
		//右侧菜单划动监听
		drawerLayout.setDrawerListener(new DrawerListener() {
			
			@Override
			public void onDrawerStateChanged(int arg0) {
				
			}
			
			@Override
			public void onDrawerSlide(View arg0, float arg1) {
				
			}
			
			@Override
			public void onDrawerOpened(View arg0) {
				isOpen = true;
			}
			
			@Override
			public void onDrawerClosed(View arg0) {
				isOpen = false;
			}
		});
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				wdMap = wdMapList.get(position);
				toolbar.setTitle(wdMap.getBranchName());
				FormsUtil.setTextViewTxt(tvBranchName, wdMap.getBranchName());
				FormsUtil.setTextViewTxts(tvAddress, getString(R.string.wdAddress), wdMap.getAddress());
				if(isRoute){
					searchButtonProcess(rbCurrent);
				}
				drawerLayout.closeDrawer(listView);
			}
		});
		
		//地图点击事件处理
        mBaiduMap.setOnMapClickListener(this);
        // 初始化搜索模块，注册事件监听
        mSearch = RoutePlanSearch.newInstance();
        mSearch.setOnGetRoutePlanResultListener(this);
	}
	
	@OnClick(value = {R.id.llContent})
    public void onClick(View view){
    	switch(view.getId()){
    	case R.id.llContent:
    		if(latitude == 0 && longitude == 0){
    			MyToast.showTEXT(this, "正在定位请稍后...");
    		}else{
    			isRoute = true;
        		rgMenu.setVisibility(View.VISIBLE);
    			llContent.setVisibility(View.GONE);
    			rbTransit.setChecked(true);
        		searchButtonProcess(rbTransit);
    		}
    		break;
    	}
    }
	
	public class MyOnclickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			searchButtonProcess(v);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	      getMenuInflater().inflate(R.menu.activity_wdmap_menu, menu);
	      return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == R.id.more){
			if(isOpen){
				drawerLayout.closeDrawer(listView);
			}else{
				drawerLayout.openDrawer(listView);
			}
		}else if(item.getItemId() == android.R.id.home){
			//是路径规划页面
			if(isRoute){
				isRoute = false;
				mHandler.sendEmptyMessage(CHANGE_WD);
				return false;
			}
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onGetDrivingRouteResult(DrivingRouteResult result) {
		if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
            Toast.makeText(this, "抱歉，未找到结果", Toast.LENGTH_SHORT).show();
        }
        if (result.error == SearchResult.ERRORNO.AMBIGUOUS_ROURE_ADDR) {
            //起终点或途经点地址有岐义，通过以下接口获取建议查询信息
            //result.getSuggestAddrInfo()
            return;
        }
        if (result.error == SearchResult.ERRORNO.NO_ERROR) {
        	DrivingRouteOverlay overlay = new DrivingRouteOverlay(mBaiduMap);
            mBaiduMap.setOnMarkerClickListener(overlay);
            overlay.setData(result.getRouteLines().get(0));
            overlay.addToMap();
            overlay.zoomToSpan();
        }
	}

	@Override
	public void onGetTransitRouteResult(TransitRouteResult result) {
		if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
            Toast.makeText(this, "抱歉，未找到结果", Toast.LENGTH_SHORT).show();
        }
        if (result.error == SearchResult.ERRORNO.AMBIGUOUS_ROURE_ADDR) {
            //起终点或途经点地址有岐义，通过以下接口获取建议查询信息
            //result.getSuggestAddrInfo()
            return;
        }
        if (result.error == SearchResult.ERRORNO.NO_ERROR) {
            TransitRouteOverlay overlay = new TransitRouteOverlay(mBaiduMap);
            mBaiduMap.setOnMarkerClickListener(overlay);
            overlay.setData(result.getRouteLines().get(0));
            overlay.addToMap();
            overlay.zoomToSpan();
        }
	}

	@Override
	public void onGetWalkingRouteResult(WalkingRouteResult result) {
		if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
            Toast.makeText(this, "抱歉，未找到结果", Toast.LENGTH_SHORT).show();
        }
        if (result.error == SearchResult.ERRORNO.AMBIGUOUS_ROURE_ADDR) {
            //起终点或途经点地址有岐义，通过以下接口获取建议查询信息
            //result.getSuggestAddrInfo()
            return;
        }
        if (result.error == SearchResult.ERRORNO.NO_ERROR) {
        	WalkingRouteOverlay overlay = new WalkingRouteOverlay(mBaiduMap);
            mBaiduMap.setOnMarkerClickListener(overlay);
            overlay.setData(result.getRouteLines().get(0));
            overlay.addToMap();
            overlay.zoomToSpan();
        }
		
	}

	@Override
	public void onMapClick(LatLng arg0) {
		
	}

	@Override
	public boolean onMapPoiClick(MapPoi arg0) {
		return false;
	}
	

    /**
     * 发起路线规划搜索
     *
     * @param v
     */
    public void searchButtonProcess(View v) {
    	mBaiduMap.clear();
    	if(wdMap != null){
	    	PlanNode stNode = PlanNode.withLocation(new LatLng(latitude, longitude));
	    	PlanNode enNode = PlanNode.withLocation(new LatLng(Double.parseDouble(wdMap.getLat()), 
	    			Double.parseDouble(wdMap.getLon())));
	    	 if (v.getId() == R.id.rbTransit) {
	             mSearch.transitSearch((new TransitRoutePlanOption()).city("深圳").from(stNode).to(enNode));
	             rbCurrent = rbTransit;
	         } else if (v.getId() == R.id.rbDriving) {
	             mSearch.drivingSearch((new DrivingRoutePlanOption()).from(stNode).to(enNode));
	             rbCurrent = rbDriving;
	         } else if (v.getId() == R.id.rbWalking) {
	             mSearch.walkingSearch((new WalkingRoutePlanOption()).from(stNode).to(enNode));
	             rbCurrent = rbWalking;
	         }
    	}
    	mHandler.sendEmptyMessage(ADD_RARGET);
    }
    
    /**
	 * 定位SDK监听函数
	 */
	public class MyLocationListenner implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			// map view 销毁后不在处理新接收的位置
			if (location == null || mMapView == null)
				return;
			if (isFirstLoc) {
				isFirstLoc = false;
				latitude = location.getLatitude();
				longitude = location.getLongitude();
				MyLocationData locData = new MyLocationData.Builder()
					// 此处设置开发者获取到的方向信息，顺时针0-360
					.direction(100).latitude(latitude).longitude(longitude).build();
				mBaiduMap.setMyLocationData(locData);
			}
		}

		public void onReceivePoi(BDLocation poiLocation) {
			
		}
	}

	@Override
	protected void onPause() {
		mMapView.onPause();
		super.onPause();
	}

	@Override
	protected void onResume() {
		mMapView.onResume();
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// 退出时销毁定位
		mLocClient.stop();
		// 关闭定位图层
		mBaiduMap.setMyLocationEnabled(false);
		mMapView.onDestroy();
		mMapView = null;
		// 回收 bitmap 资源
		bd.recycle();
		super.onDestroy();
	}
	
}
