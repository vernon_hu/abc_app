package com.abc.sz.app.fragment_phase2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity.personal.LoginActivity;
import com.abc.sz.app.activity_phase2.FinancePlusActivity;
import com.abc.sz.app.activity_phase2.FreeWIFIActivity;
import com.abc.sz.app.activity_phase2.WebViewActivity;
import com.abc.sz.app.activity_phase2.lightpay.LockActivity;
import com.abc.sz.app.view.CellView;
import com.abc.sz.app.view.plusitemview.PlusItemBean;
import com.forms.base.ABCFragment;
import com.forms.base.BaseConfig;
import com.forms.base.LoginUser;
import com.forms.library.base.BaseAdapter;
import com.forms.library.baseUtil.db.sqlite.Selector;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

/**
 * Created by hwt on 4/14/15.
 */
public class FinanceFragment extends ABCFragment{
    @ViewInject(R.id.financeView) GridView gridView;

    List<CellView.CellViewBean> cellViewBeanList = new ArrayList<>();
    private BaseAdapter<CellView.CellViewBean> financeAdapter;
    private static final int LOGIN_RESULTCODE = 0;
    private static final int TO_LOCKPATTERN = 1;
    private static final int TO_PLUSITEM = 2;
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initCellData();
 
        financeAdapter = new BaseAdapter<CellView.CellViewBean>
                (getActivity(), cellViewBeanList, R.layout.layout_phase2_finance_cell) {
            @Override
            public void viewHandler(int position, CellView.CellViewBean cellViewBean, View convertView) {
                CellView cellView = ViewHolder.get(convertView, R.id.cellView);
                cellView.reset(cellViewBean, FormsUtil.SCREEN_WIDTH / gridView.getNumColumns());
                cellView.setClickListener(new CellView.ClickListener() {
                    @Override
                    public void onClick(View view, CellView.CellViewBean cellViewBean) {
                        if (FreeWIFIActivity.class.isAssignableFrom(cellViewBean.getTargetClass())) {
                        	SharedPreferences sharePref = baseActivity.getSharedPreferences(BaseConfig.LOGIN_INFO, Context.MODE_PRIVATE);
                        	String phone = sharePref.getString(BaseConfig.LOGIN_USER_PHONE, null);
                        	if(phone == null){
                        		Intent intent = new Intent(baseActivity, LoginActivity.class);
                                startActivityForResult(intent, LOGIN_RESULTCODE);
                        	}else{
                        		toConnectWifi(phone);
                        	}
                        } else if (FinancePlusActivity.class.isAssignableFrom(cellViewBean.getTargetClass())) {
                            Intent intent = new Intent(getActivity(), FinancePlusActivity.class);
                            startActivityForResult(intent, TO_PLUSITEM);
                        } else if (LockActivity.class.isAssignableFrom(cellViewBean.getTargetClass())) {
                        	if(baseActivity.isLogin()){
                        		getABCActivity().goTo(cellViewBean.getTargetClass());
                        	}
                        } else if (WebViewActivity.class.isAssignableFrom(cellViewBean.getTargetClass())) {
                        	if(baseActivity.isLogin()){
                        		Bundle bundle = new Bundle();
                        		if(cellViewBean.getCellTextStr().equals(getString(R.string.calculator))){
                        			bundle.putString(WebViewActivity.TARGET_URL, BaseConfig.MANAGE_MONEY_URL);
                        		}else if(cellViewBean.getCellTextStr().equals(getString(R.string.price))){
                        			bundle.putString(WebViewActivity.TARGET_URL, BaseConfig.PRICE_DATA_URL);
                        		}else if(cellViewBean.getCellTextStr().equals(getString(R.string.trafficabc))){
                        			bundle.putString(WebViewActivity.TARGET_URL, BaseConfig.TRAFFIC_FINE_URL);
                        			bundle.putBoolean(WebViewActivity.USERAGENT, true);
                        		}else if(cellViewBean.getCellTextStr().equals(getString(R.string.broadband))){
                        			bundle.putString(WebViewActivity.TARGET_URL, BaseConfig.BROADBAND_PAY_URL);
                        			bundle.putBoolean(WebViewActivity.USERAGENT, true);
                        		}else if(cellViewBean.getCellTextStr().equals(getString(R.string.phonePay))){
                        			bundle.putString(WebViewActivity.TARGET_URL, BaseConfig.PHONE_PAY_URL);
                        			bundle.putBoolean(WebViewActivity.USERAGENT, true);
                        		}else if(cellViewBean.getCellTextStr().equals(getString(R.string.phoneFlow))){
                        			bundle.putString(WebViewActivity.TARGET_URL, BaseConfig.PHONE_FLOW_URL);
                        			bundle.putBoolean(WebViewActivity.USERAGENT, true);
                        		}
                        		getABCActivity().goTo(cellViewBean.getTargetClass(), bundle);
                        	}
                        } else {
                        	getABCActivity().goTo(cellViewBean.getTargetClass());
                        }
                    }
                });
            }
        };
        gridView.setAdapter(financeAdapter);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TO_LOCKPATTERN) {

        } else if (resultCode == Activity.RESULT_OK && requestCode == TO_PLUSITEM) {
            initCellData();
            financeAdapter.notifyDataSetChanged();
        } else if (resultCode == Activity.RESULT_OK && requestCode == LOGIN_RESULTCODE ) {
        	if (getABCActivity().isLogin()) {
                LoginUser loginUser = getABCActivity().getBaseApp().getLoginUser();
                toConnectWifi(loginUser.getPhone());
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    
    public void toConnectWifi(String phone){
    	Bundle bundle = new Bundle();
		bundle.putString("phone", phone);
		getABCActivity().callMe(FreeWIFIActivity.class, bundle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_finance, container, false);
    }

    private void initCellData() {
        cellViewBeanList.clear();
        List<PlusItemBean> plusItemBeans = getABCActivity().getDBUtils().findAll(
                Selector.from(PlusItemBean.class).where("isChecked", "=", true)
        );
        if (plusItemBeans != null) {
            Collections.sort(plusItemBeans);
            for (PlusItemBean plusItemBean : plusItemBeans) {
                cellViewBeanList.add(
                        new CellView.CellViewBean(plusItemBean.getIconResId(), plusItemBean.getText()).
                                setIndex(plusItemBean.getIndex_()).setTargetClass(plusItemBean.getTargetClass())
                );
            }
        }
        cellViewBeanList.add(
                new CellView.CellViewBean(R.drawable.jrzs_add, "更多").setIndex(100)
                        .setTargetClass(FinancePlusActivity.class)
        );
    }
    
}
