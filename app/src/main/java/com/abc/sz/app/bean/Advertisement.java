package com.abc.sz.app.bean;

import com.forms.library.base.BaseBean;

/**
 * 广告Bean
 *
 * @author llc
 */
public class Advertisement extends BaseBean {
    //跳转类型，1：商品 2：快报 ID  3：跳转 URL
    private String Type;
    //广告简述
    private String Desc;
    //广告图片url
    private String ImgUrl;
    //跟据 type 字段确定为快报 ID 或商品 ID 或 URL
    private String Attribute;

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    public String getImgUrl() {
        return ImgUrl;
    }

    public void setImgUrl(String imgUrl) {
        ImgUrl = imgUrl;
    }

    public String getAttribute() {
        return Attribute;
    }

    public void setAttribute(String attribute) {
        Attribute = attribute;
    }


}
