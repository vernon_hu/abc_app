package com.abc.sz.app.adapter.person;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.personal.Consume;
import com.abc.sz.app.http.bean.personal.RConsume;
import com.forms.library.tools.FormsUtil;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * 我的消费适配器
 *
 * @author llc
 */
public class ConsumeAdapter extends BaseAdapter {

    private List<RConsume> mList;

    private final int TOP = 0;
    private final int OTHER = 1;

    private TopHolder topHolder;
    private ViewHolder listHolder;
    private NumberFormat numberFormat;

    public ConsumeAdapter(List<RConsume> list) {
        mList = list;
        numberFormat = DecimalFormat.getCurrencyInstance(Locale.CHINA);
        numberFormat.setMaximumFractionDigits(2);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mList.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        if (position == 0)
            return new Consume();
        else
            return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        // TODO Auto-generated method stub
        if (position == 0)
            return TOP;
        else
            return OTHER;
    }

    @Override
    public int getViewTypeCount() {
        // TODO Auto-generated method stub
        return 2;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RConsume bean = null;
        if (position != 0) {
            bean = mList.get(position - 1);
        }

        int type = getItemViewType(position);

        if (convertView == null) {
            if (type == TOP) {
                convertView = new LinearLayout(parent.getContext());
                convertView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, FormsUtil.dip2px(80)));
                topHolder = new TopHolder();
                convertView.setTag(topHolder);
            } else {
                convertView = View.inflate(parent.getContext(), R.layout.layout_consume_item, null);
                listHolder = new ViewHolder();
                convertView.setTag(listHolder);
                listHolder.tv_date = (TextView) convertView.findViewById(R.id.tv_date);
                listHolder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
                listHolder.tv_price = (TextView) convertView.findViewById(R.id.tv_price);
            }
        } else {
            if (convertView.getTag() instanceof TopHolder) {
                topHolder = (TopHolder) convertView.getTag();
            } else {
                listHolder = (ViewHolder) convertView.getTag();
            }
        }

        if (type != TOP) {
            FormsUtil.setTextViewTxt(listHolder.tv_date, bean.getPostTime());
            String productNames = bean.getProductNames();
            if("FlowCharge".equals(productNames)){
            	productNames = "流量充值";
            }else if("TalkCharge".equals(productNames)){
            	productNames = "话费充值";
            }
            FormsUtil.setTextViewTxt(listHolder.tv_name, productNames);
            FormsUtil.setTextViewTxt(listHolder.tv_price, numberFormat.format(bean.getAmt()));
        }

        // TODO Auto-generated method stub
        return convertView;
    }


    private class ViewHolder {
        public TextView tv_date;
        public TextView tv_name;
        public TextView tv_price;
    }

    private class TopHolder {
    	
    };

}
