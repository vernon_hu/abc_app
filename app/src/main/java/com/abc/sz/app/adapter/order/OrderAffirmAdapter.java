package com.abc.sz.app.adapter.order;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity.product.ProductInfoActivity;
import com.abc.sz.app.bean.Discount;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.util.ShoppingCarCount;
import com.forms.base.XDRImageLoader;
import com.forms.library.base.BaseActivity;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

/**
 * 订单确认列表适配
 *
 * @author ftl
 */
public class OrderAffirmAdapter extends BaseAdapter {

    private List<Product> list;
    private BaseActivity activity;
    private Handler handler;
    private XDRImageLoader mImageLoader;
    private Map<String, String> map;

    public OrderAffirmAdapter(List<Product> list, BaseActivity activity, Handler handler,
                              XDRImageLoader mImageLoader) {
        this.list = list;
        this.activity = activity;
        this.handler = handler;
        this.mImageLoader = mImageLoader;
        map = new HashMap<String, String>();
        for (int i = 0; i < list.size(); i++) {
            map.put(list.get(i).getProductId(), list.get(i).getProductPrice());
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Product product = list.get(position);
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_order_affirm_item, null);
        }
        LinearLayout llProduct = ViewHolder.get(convertView, R.id.ll_product);
        TextView tvProductName = ViewHolder.get(convertView, R.id.tv_productName);
        TextView tv_productPrice = ViewHolder.get(convertView, R.id.tv_productPrice);
        TextView tvAmount = ViewHolder.get(convertView, R.id.tv_amount);
        CheckBox cbChoose = ViewHolder.get(convertView, R.id.cb_choose);
        LinearLayout llPostCode = ViewHolder.get(convertView, R.id.ll_postCode);
        final TextView tvPostCode = ViewHolder.get(convertView, R.id.tv_postCode);
        ImageView ivProductImage = ViewHolder.get(convertView, R.id.iv_productImage);
        if (product != null) {
        	 mImageLoader.displayImage(product.getImageUrl(), ivProductImage, ImageViewUtil.getOption());
             FormsUtil.setTextViewTxt(tvProductName, product.getProductName());
             FormsUtil.setTextViewTxt(tvAmount, product.getNum());
             tv_productPrice.setText(ShoppingCarCount.formatMoney(product.getProductPrice()));
             List<Discount> discountList = product.getList();
             if (discountList != null && discountList.size() != 0) {
                 final Discount discount = discountList.get(0);
                 final String reason = discount.getReason();
                 final String discountPrice = discount.getDiscountPrice();
                 if (reason != null && "".equals(reason) && discountPrice != null &&
                         "".equals(discountPrice)) {
                     cbChoose.setVisibility(View.VISIBLE);
                     cbChoose.setText(reason);
                     cbChoose.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                         @Override
                         public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                             //选中优惠信息
                             if (isChecked) {
                                 product.setProductPrice(discountPrice);
                             } else {
                                 product.setProductPrice(map.get(product.getProductId()));
                             }
                             notifyDataSetChanged();
                             Message message = new Message();
                             message.obj = product.getProductId();
                             handler.sendMessage(message);
                         }
                     });

                 } else {
                     cbChoose.setVisibility(View.INVISIBLE);
                 }
             }else{
            	 cbChoose.setVisibility(View.INVISIBLE);
             }
             llProduct.setOnClickListener(new OnClickListener() {

                 @Override
                 public void onClick(View v) {
                     Bundle bundle = new Bundle();
                     bundle.putString(ProductInfoActivity.PRODUCT_ID, product.getProductId());
                     activity.callMe(ProductInfoActivity.class, bundle);
                 }
             });
             llPostCode.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    View dialogView = LayoutInflater.from(activity).inflate(R.layout.layout_dialog_leave_note, null);
                    final AlertDialog dialog = new AlertDialog.Builder(activity).create();
                    dialog.setView(dialogView, 0, 0, 0, 0);
                    dialog.setInverseBackgroundForced(true);
                    dialog.show();
                    final EditText etLeaveNote = (EditText) dialogView.findViewById(R.id.et_leave_note);
                    Button btnCancel = (Button) dialogView.findViewById(R.id.btn_cancel);
                    Button btnConfirm = (Button) dialogView.findViewById(R.id.btn_confirm);
                    etLeaveNote.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100)});
                    etLeaveNote.setText(product.getPostCode());
                    etLeaveNote.setSelection(etLeaveNote.length());

                    btnCancel.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    btnConfirm.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                        	tvPostCode.setText(etLeaveNote.getText().toString());
                        	product.setPostCode(etLeaveNote.getText().toString());
                            notifyDataSetChanged();
                            dialog.dismiss();
                        }
                    });
                }
            });
        }
        return convertView;
    }

}
