package com.abc.sz.app.adapter.product;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.product.AttributeValueBean;
import com.abc.sz.app.bean.product.TypeAttributeBean;
import com.abc.sz.app.http.bean.product.QAttributeValueBean;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;


/**
 * 属性条目
 *
 * @author llc
 */
public class AttributeItemAdapter extends BaseAdapter {

    private List<TypeAttributeBean> mList;
    private List<AttributeValueBean> attributeList;
//    private AttributeValueAdapter vAdapter;

    public AttributeItemAdapter(List<AttributeValueBean> attributeList, List<TypeAttributeBean> list) {
        mList = list;
        this.attributeList = attributeList;
        for (TypeAttributeBean typeAttributeBean : mList) {
            isSame(typeAttributeBean.getList());
        }
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        // TODO Auto-generated method stub
        TypeAttributeBean bean = mList.get(position);
        final List<AttributeValueBean> valueList = bean.getList();
        List<TextView> list = new ArrayList<TextView>();
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_attribute_info, null);
            //属性值布局初始化
//            vAdapter = new AttributeValueAdapter(valueList);
            RelativeLayout rlContent = ViewHolder.get(convertView, R.id.rlContent);
            
            float width = FormsUtil.SCREEN_WIDTH - 60 * FormsUtil.density;
            int padding = 4 * (int)FormsUtil.density;
        	int totalWidth = 0;
        	int totalNum = 1;
            for (int i = 0; i < valueList.size(); i++) {
            	final AttributeValueBean attributeValueBean = valueList.get(i);
            	View view = View.inflate(parent.getContext(), R.layout.layout_product_attribute_value, null);
                final TextView tv_value = (TextView) view.findViewById(R.id.tv_value);
                view.setId(i + 1);
                
                String value = attributeValueBean.getValue();
    			tv_value.setText(value);
    			list.add(tv_value);
    			if(attributeValueBean.isChecked()){
    				setTextViewStyle(true, tv_value, parent.getContext());
    			}else{
    				setTextViewStyle(false, tv_value, parent.getContext());
    			}
    			
    			int viewWidth = 0;
    			if(value.length() == 1){
    				viewWidth = (int)tv_value.getPaint().getTextSize() * 2 + tv_value.getPaddingLeft() * 2;
    			}else{
    				viewWidth = (int)tv_value.getPaint().getTextSize() * value.length() + tv_value.getPaddingLeft() * 2;
    			}
    			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(viewWidth, LayoutParams.WRAP_CONTENT);
    			
    			if(i == 0){
    				totalWidth = viewWidth;
    			}else{
    				if(totalWidth + padding * totalNum + viewWidth > width){
    					totalWidth = viewWidth;
    					totalNum = 1;
    		    		lp.topMargin = padding;
    		    		lp.addRule(RelativeLayout.BELOW, rlContent.getChildAt(i-1).getId());
    		    	}else{
    		    		totalWidth += viewWidth;
    		    		totalNum += 1;
    		    		lp.leftMargin = padding;
    		    		lp.addRule(RelativeLayout.ALIGN_TOP, rlContent.getChildAt(i-1).getId());
    		    		lp.addRule(RelativeLayout.RIGHT_OF, rlContent.getChildAt(i-1).getId());
    		    	}
    			}
    			rlContent.addView(view, i, lp);
    			
    			tv_value.setOnClickListener(new MyOnClickListener(list, tv_value, valueList, parent.getContext()));
    			
			}
//            GridView gv_attribute_value = ViewHolder.get(convertView, R.id.gv_attribute_value);
//
//            gv_attribute_value.setAdapter(vAdapter);
//            gv_attribute_value.setOnItemClickListener(new OnItemClickListener() {
//
//                @Override
//                public void onItemClick(AdapterView<?> viewGroup, View view, int index,
//                                        long arg3) {
//                    // TODO 属性点击事件
//                    for (int i = 0; i < viewGroup.getChildCount(); i++) {
//                        if (i == index) {
//                            valueList.get(i).setChecked(true);
//                            RelativeLayout rl_bg = ViewHolder.get(view, R.id.rl_bg);
//                            TextView tv_value = ViewHolder.get(view, R.id.tv_value);
//
//                            rl_bg.setBackgroundResource(R.drawable.shape_attribute_bg_checked);
//                            tv_value.setTextColor(view.getContext().getResources().getColor(R.color.colorPrimaryDark));
//                        } else {
//                            View v = viewGroup.getChildAt(i);
//                            valueList.get(i).setChecked(false);
//                            RelativeLayout rl_bg = ViewHolder.get(v, R.id.rl_bg);
//                            TextView tv_value = ViewHolder.get(v, R.id.tv_value);
//
//                            rl_bg.setBackgroundResource(R.drawable.shape_attribute_bg);
//                            tv_value.setTextColor(v.getContext().getResources().getColor(R.color.textGray));
//                        }
//                    }
//                }
//            });
//        } else {
//            vAdapter.setmList(valueList);
//            vAdapter.notifyDataSetChanged();
//        }
        }
        TextView tv_attribute_name = ViewHolder.get(convertView, R.id.tv_attribute_name);
        tv_attribute_name.setText(bean.getAttributeName());
        return convertView;
    }

    public List<TypeAttributeBean> getmList() {
        return mList;
    }

    public void setmList(List<TypeAttributeBean> mList) {
        this.mList = mList;
    }

    public List<QAttributeValueBean> getAttributeList() {
        List<QAttributeValueBean> list = new ArrayList<QAttributeValueBean>();
        for (int i = 0; i < mList.size(); i++) {
            List<AttributeValueBean> attributeList = mList.get(i).getList();
            for (int j = 0; j < attributeList.size(); j++) {
                if (attributeList.get(j).isChecked()) {
                    QAttributeValueBean bean = new QAttributeValueBean(mList.get(i).getAttributeId(),
                            attributeList.get(j).getKey());
                    list.add(bean);
                }
            }
        }
        return list;
    }

    @Override
    public void notifyDataSetChanged() {
        // TODO Auto-generated method stub
        for (TypeAttributeBean typeAttributeBean : mList) {
            isSame(typeAttributeBean.getList());
        }
        super.notifyDataSetChanged();
    }

    private void isSame(List<AttributeValueBean> bean) {
        for (int i = 0; i < attributeList.size(); i++) {
            AttributeValueBean value = attributeList.get(i);
            for (AttributeValueBean tag : bean) {
                if (tag.getValue().equals(value.getValue())) {
                    tag.setChecked(true);
                    return;
                }
            }
        }
    }
    
    private class MyOnClickListener implements OnClickListener{
    	
    	List<TextView> list;
    	TextView tv_value;
    	List<AttributeValueBean> valueList;
    	Context context;
    	
    	MyOnClickListener(List<TextView> list, TextView tv_value, List<AttributeValueBean> valueList, 
    			Context context){
    		this.list = list;
    		this.tv_value = tv_value;
    		this.valueList = valueList;
    		this.context = context;
    	}

		@Override
		public void onClick(View v) {
			for (int i = 0; i < list.size(); i++) {
				TextView textView = list.get(i);
				if(tv_value == textView){
					valueList.get(i).setChecked(true);
					setTextViewStyle(true, textView, context);
				}else{
					valueList.get(i).setChecked(false);
					setTextViewStyle(false, textView, context);
				}
			}
		}
    	
    }

    private void setTextViewStyle(boolean isCheck, TextView textView, Context context){
    	if(isCheck){
    		textView.setBackgroundResource(R.drawable.shape_attribute_bg_checked);
			textView.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
    	}else{
    		textView.setBackgroundResource(R.drawable.shape_attribute_bg);
			textView.setTextColor(context.getResources().getColor(R.color.textGray));
    	}
    }
}
