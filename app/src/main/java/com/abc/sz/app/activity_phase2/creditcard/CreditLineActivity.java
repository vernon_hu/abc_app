package com.abc.sz.app.activity_phase2.creditcard;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;

/**
 * 预授信额度页面
 * 
 * @author hkj
 */
@ContentView(R.layout.activity_phase2_creditline)
public class CreditLineActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;

//	@ViewInject(R.id.tvCreditLine)
//	private TextView tvCreditLine;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//		init();
	}

//	private void init() {
//		Bundle bundle = getIntent().getExtras();
//		if(bundle != null){
//			Double money = bundle.getDouble("money");
//			FormsUtil.setTextViewTxts(tvCreditLine, getString(R.string.tenThousand), money);
//		}
//	}

	@OnClick(R.id.btnFinish)
	public void onClick(View v) {
		finish();
	}
	
}
