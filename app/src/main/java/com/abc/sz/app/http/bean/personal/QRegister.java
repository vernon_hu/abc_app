package com.abc.sz.app.http.bean.personal;

/**
 * Created by hwt on 14/11/18.
 */
public class QRegister {
    private String phone;//手机号
    private String passWord;//用户密码
    private String checkCode;//验证码

    public QRegister(String phone, String passWord, String checkCode) {
        super();
        this.phone = phone;
        this.passWord = passWord;
        this.checkCode = checkCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getCheckCode() {
        return checkCode;
    }

    public void setCheckCode(String checkCode) {
        this.checkCode = checkCode;
    }

}
