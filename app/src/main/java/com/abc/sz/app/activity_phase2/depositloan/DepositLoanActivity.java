package com.abc.sz.app.activity_phase2.depositloan;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.DepositloanAction;
import com.abc.sz.app.util.DialogUtil;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;

/**
 * 存贷通专区页面
 * 
 * @author hkj
 */
@ContentView(R.layout.activity_phase2_depositloan)
public class DepositLoanActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	
	private DepositloanAction depositloanAction;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		depositloanAction = (DepositloanAction) controller.getAction(this, DepositloanAction.class);
	}

	@OnClick({ R.id.tvOpen, R.id.tvDepositRelate, R.id.tvLoanRelate, R.id.tvQuery, R.id.tvCalculate, R.id.tvDetail })
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tvOpen:
			depositloanAction.queryCheckUserCondition().start();
			break;
		case R.id.tvDepositRelate:
			callMe(DepositRelateRepayActivity.class);
			break;
		case R.id.tvLoanRelate:
			callMe(DepositRelateLoanActivity.class);
			break;
		case R.id.tvQuery:
			break;
		case R.id.tvCalculate:
			callMe(DepositLoanCalculatorActivity.class);
			break;
		case R.id.tvDetail:
			callMe(DepositLoanIncomeQueryActivity.class);
			break;
		}
	}
	
	/**
     * 查询是否符合存贷通开立条件成功回调
     */
	public void querySuccess(String state){
		if(state != null && !"".equals(state)){
			String content = "";
			if("3".equals(state)){
				content = "您无符合存贷通开立条件的贷款账户";
				DialogUtil.show(this, content);
			}else{
				if("1".equals(state)){
					content = "您有符合存贷通开立条件的贷款账户";
				}else if("2".equals(state)){
					content = "您已开通存贷通业务，感谢您对我行业务的支持。";
				}
				DialogUtil.showWithTwoBtnAndTitle(this, "提示", content, "开立存贷通", "取消", new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						callMe(DepositLoanProtocolActivity.class);
					}
				}, new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
			}
		}
	}

}
