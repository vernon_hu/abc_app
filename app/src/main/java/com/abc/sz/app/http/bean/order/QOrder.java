package com.abc.sz.app.http.bean.order;

import java.util.List;



/**
 * 订单
 *
 * @author ftl
 */
public class QOrder{

    /* 商户 ID*/
    private String sellerId;
    /* 商户名称*/
    private String sellerName;
    /* 订单 ID*/
    private String orderId;
    /* 订单总额*/
    private Double sumPrice;
    /* 订单商品列表*/
    private List<QProduct> productList;
    /* 类型 1:商品类,2:业务类*/
    private int orderType;
    /* 商户订单ID*/
    private String sellerOrderId;
    /* 备注*/
    private String memo;
    /* 地址id*/
    private String addressId;
    
	public String getSellerId() {
		return sellerId;
	}
	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}
	public String getSellerName() {
		return sellerName;
	}
	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Double getSumPrice() {
		return sumPrice;
	}
	public void setSumPrice(Double sumPrice) {
		this.sumPrice = sumPrice;
	}
	public List<QProduct> getProductList() {
		return productList;
	}
	public void setProductList(List<QProduct> productList) {
		this.productList = productList;
	}
	public int getOrderType() {
		return orderType;
	}
	public void setOrderType(int orderType) {
		this.orderType = orderType;
	}
	public String getSellerOrderId() {
		return sellerOrderId;
	}
	public void setSellerOrderId(String sellerOrderId) {
		this.sellerOrderId = sellerOrderId;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getAddressId() {
		return addressId;
	}
	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

}
