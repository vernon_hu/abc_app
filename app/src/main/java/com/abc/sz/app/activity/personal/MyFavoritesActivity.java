package com.abc.sz.app.activity.personal;

import java.util.ArrayList;
import java.util.List;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.ProductOtherAction;
import com.abc.sz.app.action.UserAction;
import com.abc.sz.app.activity.product.ProductInfoActivity;
import com.abc.sz.app.bean.personal.Favorites;
import com.abc.sz.app.listener.ListClickListener;
import com.abc.sz.app.util.DialogUtil;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.library.base.BaseAdapter;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.net.ViewLoading.OnRetryListener;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnItemClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;
import com.forms.view.pullToRefresh.PullToRefreshBase;
import com.forms.view.pullToRefresh.PullToRefreshListView;
import com.forms.view.toast.MyToast;

/**
 * Created by hwt on 14/11/12.
 * 我的收藏
 */
@ContentView(R.layout.activity_myfavorites)
public class MyFavoritesActivity extends ABCActivity implements PullToRefreshBase.OnLastItemVisibleListener {
    @ViewInject(R.id.favoritesListview) PullToRefreshListView favoritesListView;
    @ViewInject(R.id.loadingView) LoadingView loadingView;
    @ViewInject(R.id.appBar) Toolbar toolbar;

    private BaseAdapter<Favorites> favoritesAdapter;
    private List<Favorites> favoritesList = new ArrayList<Favorites>();
    private String productPricesTxt = "";
    private boolean isEditState = false;
    private UserAction userAction;
    private Integer pageNum = 0;
    //其他功能请求
    private ProductOtherAction otherAction;
    private Integer deletePosition = null;
    private MenuItem editMenuItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
//        favoritesListView.setOnRefreshListener(this);
        favoritesListView.setOnLastItemVisibleListener(this);

        productPricesTxt = getString(R.string.favorites_price);

        loadingView.loading(0);
        userAction = (UserAction) controller.getAction(this, UserAction.class);
        otherAction = (ProductOtherAction) controller.getAction(this, ProductOtherAction.class);
        userAction.queryFavorites(pageNum).start(false);
        loadingView.retry(new OnRetryListener() {
			
			@Override
			public void retry() {
				userAction.queryFavorites(pageNum).start(false);
			}
		});
        initData();
        initListener();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_edit_menu,menu);
        editMenuItem = menu.findItem(R.id.edit);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.edit){
            if (isEditState) {
                isEditState = false;
                editMenuItem.setTitle(R.string.editText);
                favoritesAdapter.notifyDataSetChanged();
            } else {
                isEditState = true;
                editMenuItem.setTitle(R.string.finishEdit);
                favoritesAdapter.notifyDataSetChanged();
            }
        }
        return super.onOptionsItemSelected(item);
    }
    /**
     * 初始化监听
     */
    private void initListener() {
        // TODO Auto-generated method stub
        favoritesListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                // TODO Auto-generated method stub
                String productId = favoritesList.get(position - 1).getProductId();
                Bundle bundle = new Bundle();
                bundle.putString(ProductInfoActivity.PRODUCT_ID, productId);
                callMe(ProductInfoActivity.class, bundle);
            }
        });
    }

    /**
     * 数据加载准备
     */
    private void initData() {
        favoritesListView.setAdapter(favoritesAdapter = new BaseAdapter<Favorites>(this, favoritesList,
                R.layout.layout_favoritesitem) {
            @Override
            public void viewHandler(int position, Favorites favorites, View convertView) {
                ImageView ivPhoto = ViewHolder.get(convertView, R.id.ivPhoto);
                TextView productName = ViewHolder.get(convertView, R.id.productName);
                TextView productDesc = ViewHolder.get(convertView, R.id.productDesc);
                TextView productPrice = ViewHolder.get(convertView, R.id.productPrice);
                ImageView delete = ViewHolder.get(convertView, R.id.delete);
                ImageView right = ViewHolder.get(convertView, R.id.right);

                imageLoader.displayImage(favorites.getSmallPicture(), ivPhoto, ImageViewUtil.getOption());
                FormsUtil.setTextViewTxt(productName, favorites.getProductName());
                FormsUtil.setTextViewTxt(productDesc, favorites.getDescription());
                FormsUtil.setCurrencyText(productPrice, productPricesTxt, favorites.getPrice());
                if (isEditState) {
                    delete.setVisibility(View.VISIBLE);
                    right.setVisibility(View.GONE);
                } else {
                    delete.setVisibility(View.GONE);
                    right.setVisibility(View.VISIBLE);
                }
                delete.setOnClickListener(new ListClickListener(position) {
                    @Override
                    public void listClick(View v, final int position) {
                        DialogUtil.showWithTwoBtnAndTitle(MyFavoritesActivity.this, "提示", "是否确定取消收藏该商品？", "确定", "取消", new OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                deletePosition = position;
                                otherAction.removeFavorite(favoritesList.get(position).getFavouriteId()).start();
                            }
                        }, new OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                    }
                });
            }

        });
    }

    @OnItemClick(R.id.favoritesListview)
    public void favoritesListviewItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

//    @Override
//    public void onRefresh(PullToRefreshBase refreshView) {
//        LogUtils.d("refresh....");
//        favoritesListView.onRefreshComplete();
//    }

    @Override
    public void onLastItemVisible() {
        userAction.queryFavorites(++pageNum).start(false);
    }

    public void loadFavouritesSuccess(RetCode retCode, List<Favorites> content) {
        loadingView.loaded();
        if (retCode != RetCode.noData && content != null && content.size() > 0) {
        	editMenuItem.setVisible(true);
            favoritesList.addAll(content);
        } else {
            if (favoritesList.size() > 0) {
            	editMenuItem.setVisible(true);
                MyToast.showTEXT(this, getString(R.string.noMoreData));
            } else {
                loadingView.noData(getString(R.string.loadingNoData));
            }
        }
    }

    public void loadFavouritesFailed(RetCode retCode) {
        if (favoritesList.size() > 0) {
            MyToast.showTEXT(this, retCode.getRetMsg());
        } else {
        	editMenuItem.setVisible(false);
            loadingView.failed(retCode.getRetMsg());
        }
    }

    /**
     * 取消收藏成功
     */
    public void removeSuccess() {
        if (favoritesList.size() != 0) {
            if (deletePosition != null) {
                favoritesList.remove(deletePosition.intValue());
                favoritesAdapter.notifyDataSetChanged();
            }
        }
    }

}

