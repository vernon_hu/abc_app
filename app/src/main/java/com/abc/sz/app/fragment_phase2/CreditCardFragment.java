package com.abc.sz.app.fragment_phase2;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.GridView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity_phase2.creditcard.ApplyScheduleActivity;
import com.abc.sz.app.activity_phase2.creditcard.CarStagingListActivity;
import com.abc.sz.app.activity_phase2.creditcard.FitmentStagingActivity;
import com.abc.sz.app.view.CellView;
import com.abc.sz.app.view.CellView.CellViewBean;
import com.forms.base.ABCFragment;
import com.forms.base.ABCWebViewActivity;
import com.forms.base.BaseConfig;
import com.forms.library.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

/**
 * 信用卡页面
 * 
 * @author ftl
 */
public class CreditCardFragment extends ABCFragment {

	@ViewInject(R.id.gridView)
	private GridView gridView;

	private List<Integer> resIds = new ArrayList<Integer>() {
		{
			add(R.drawable.xyk_yysq); add(R.drawable.xyk_jdxx);
			add(R.drawable.xyk_ytq); add(R.drawable.xyk_xykyh);
			add(R.drawable.xyk_qcfq); add(R.drawable.xyk_jzfq);
			add(R.drawable.xyk_fqlcjsq);
		}
	};

	private List<String> functions = new ArrayList<String>() {
		{
			add("预约申请"); add("进度查询"); add("YOU特权"); add("YOU惠");
			add("汽车分期"); add("家装分期"); add("分期计算器");
			//add("账单分期");
			//add("消费分期");
			//add("现金分期"); 
		}
	};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_phase2_creditcard, container, false);
        return view;
    }
    
    @Override
	protected void initData() {
		super.initData();
		
		List<CellViewBean> cellList = new ArrayList<>();
		for (int i = 0; i < functions.size(); i++) {
			cellList.add(new CellViewBean(resIds.get(i), functions.get(i)));
		}

		BaseAdapter<CellViewBean> adapter = new BaseAdapter<CellViewBean>(baseActivity, cellList,
				R.layout.layout_phase2_creditcard_item) {
			@Override
			public void viewHandler(final int position, CellViewBean cellViewBean, View convertView) {
				CellView cellView = ViewHolder.get(convertView, R.id.cellView);
				cellView.reset(cellViewBean, FormsUtil.SCREEN_WIDTH / gridView.getNumColumns());
				cellView.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Bundle bundle = new Bundle();
						switch (position) {
						case 0:
							//信用卡申请
							bundle.putString(ABCWebViewActivity.TITLE_TEXT, getString(R.string.card_apply));
							bundle.putString(ABCWebViewActivity.TARGET_URL, BaseConfig.CARD_APPLY_URL);
							baseActivity.callMe(ABCWebViewActivity.class, bundle);
							break;
						case 1:
							baseActivity.callMe(ApplyScheduleActivity.class);
							break;
						case 2:
							//优特权页面
							bundle.putString(ABCWebViewActivity.TITLE_TEXT, getString(R.string.prerogative));
							bundle.putString(ABCWebViewActivity.TARGET_URL, BaseConfig.MY_PREROGATIVE_URL);
							baseActivity.callMe(ABCWebViewActivity.class, bundle);
							break;
						case 3:
							//信用卡优惠
							bundle.putString(ABCWebViewActivity.TITLE_TEXT, getString(R.string.card_favorable));
							bundle.putString(ABCWebViewActivity.TARGET_URL, BaseConfig.CARD_PREROGATIVE_URL);
							baseActivity.callMe(ABCWebViewActivity.class, bundle);
							break;
						case 4:
							baseActivity.callMe(CarStagingListActivity.class);
							break;
						case 5:
							baseActivity.callMe(FitmentStagingActivity.class);
							break;
						case 6:
							//分期理财计算器
							bundle.putString(ABCWebViewActivity.TITLE_TEXT, getString(R.string.cfp_calculator));
							bundle.putString(ABCWebViewActivity.TARGET_URL, BaseConfig.CALCULATOR_URL);
							baseActivity.callMe(ABCWebViewActivity.class, bundle);
							break;
						}	
					}
				});
			}
		};
		gridView.setAdapter(adapter);
    }

}
