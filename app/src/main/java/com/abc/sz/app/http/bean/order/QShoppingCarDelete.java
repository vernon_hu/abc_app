package com.abc.sz.app.http.bean.order;

import java.util.List;


/**
 * 购物车商品删除请求
 *
 * @author ftl
 */
public class QShoppingCarDelete {

    private List<ShoppingCarDelete> list;

    public QShoppingCarDelete(List<ShoppingCarDelete> list) {
        super();
        this.list = list;
    }

    public List<ShoppingCarDelete> getList() {
        return list;
    }

    public void setList(List<ShoppingCarDelete> list) {
        this.list = list;
    }

}
