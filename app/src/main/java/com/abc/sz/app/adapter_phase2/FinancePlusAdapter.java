package com.abc.sz.app.adapter_phase2;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.view.plusitemview.PlusItemBean;
import com.abc.sz.app.view.plusitemview.PlusItemView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.ScreenUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hwt on 4/27/15.
 */
public class FinancePlusAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int DEVIDE_TYPE = 1;
    private static final int ITEM_TYPE = 0;
    List<PlusItemBean> plusItemBeans = new ArrayList<>();


    public FinancePlusAdapter(List<PlusItemBean> plusItemBeans) {
        this.plusItemBeans = plusItemBeans;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == DEVIDE_TYPE) {
            TextView textView = new TextView(viewGroup.getContext());
            textView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            ViewGroup.LayoutParams layoutParams = textView.getLayoutParams();
            layoutParams.height = Math.round(ScreenUtils.dpToPx(viewGroup.getContext(), 0.5f));
            textView.setLayoutParams(layoutParams);
            textView.offsetLeftAndRight(Math.round(ScreenUtils.dpToPx(viewGroup.getContext(), 16f)));
            textView.setBackgroundResource(R.color.grzx_bg);
            return new DevideView(textView);
        } else if (i == ITEM_TYPE) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                    R.layout.layout_financeplus_item, viewGroup, false
            );
            return new ItemView(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder itemView, int i) {
        if (itemView != null && itemView instanceof ItemView) {
            ItemView view = (ItemView) itemView;
            PlusItemBean plusItemBean = plusItemBeans.get(i / 2);
            view.plusItemView.setPlusItemBean(plusItemBean);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 1) {
            return DEVIDE_TYPE;
        } else {
            return ITEM_TYPE;
        }
    }

    @Override
    public int getItemCount() {
        return plusItemBeans.size() + plusItemBeans.size() - 1;
    }

    public class DevideView extends RecyclerView.ViewHolder {
        View itemView;

        public DevideView(View itemView) {
            super(itemView);
        }
    }


    public class ItemView extends RecyclerView.ViewHolder {
        @ViewInject(R.id.plusItem) PlusItemView plusItemView;

        public ItemView(View itemView) {
            super(itemView);
            plusItemView = (PlusItemView) itemView.findViewById(R.id.plusItem);
            plusItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PlusItemBean plusItemBean = plusItemView.getPlusItemBean();
                    if (plusItemBean != null) {
                        if (plusItemBean.isChecked()) {
                            plusItemBean.setIsChecked(false);
                        } else {
                            plusItemBean.setIsChecked(true);
                        }
                        plusItemView.setItemChecked(plusItemBean.isChecked());
                    }
                }
            });
        }
    }
}
