package com.abc.sz.app.activity.personal;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.UserAction;
import com.abc.sz.app.view.CountDownButton;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.view.toast.MyToast;

/**
 * 修改手机号码
 *
 * @author ftl
 */
@ContentView(R.layout.activity_update_phone)
public class UpdatePhoneActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.et_newPhone) EditText newPhone;
    @ViewInject(R.id.et_authCode) EditText authCode;
    @ViewInject(R.id.btn_authCode) CountDownButton btn_authCode;

    private UserAction userAction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        userAction = (UserAction) controller.getAction(this, UserAction.class);
    }

    @OnClick(value = {R.id.btn_confirm, R.id.btn_authCode})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_confirm:
                if (check()) {
                    userAction.updatePhone(newPhone.getText().toString(),
                            authCode.getText().toString())
                            .start();
                }
                break;
            case R.id.btn_authCode:
                userAction.getUpdatePhoneCode().start();
                break;
        }

    }

    private boolean check() {
        if (TextUtils.isEmpty(newPhone.getText())) {
            FormsUtil.setErrorHtmlTxt(this, newPhone, R.string.input_newPhone_isempty);
            return false;
        } else if (newPhone.getText().toString().length() != 11) {
            FormsUtil.setErrorHtmlTxt(this, newPhone, R.string.input_eleven_phone);
            return false;
        } else if (TextUtils.isEmpty(authCode.getText())) {
            FormsUtil.setErrorHtmlTxt(this, authCode, R.string.input_authcode_isempty);
            return false;
        }
        return true;
    }

    public void requestSuccess() {
        MyToast.show(this, "请求发送成功!", MyToast.TEXT,
                Gravity.CENTER, Toast.LENGTH_SHORT);
        btn_authCode.timer.start();
    }

}
