package com.abc.sz.app.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.Address;
import com.forms.library.tools.FormsUtil;

import java.util.List;

/**
 * 收货地址列表适配
 *
 * @author ftl
 */
public class AddressAdapter extends BaseAdapter {

    private List<Address> list;
    private MyHolder holder;

    public AddressAdapter(List<Address> list) {
        this.list = list;
    }

    public void setList(List<Address> addressList) {
        list.clear();
        if (addressList != null && addressList.size() > 0) {
            for (int i = 0; i < addressList.size(); i++) {
                list.add(addressList.get(i));
            }
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Address address = list.get(position);
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_address_item, null);
            holder = new MyHolder();
            holder.tvPerson = (TextView) convertView.findViewById(R.id.tv_person);
            holder.tvPhone = (TextView) convertView.findViewById(R.id.tv_phone);
            holder.tvArea = (TextView) convertView.findViewById(R.id.tv_area);
            holder.tvAddress = (TextView) convertView.findViewById(R.id.tv_address);
            holder.ivSelect = (ImageView) convertView.findViewById(R.id.iv_select);
            convertView.setTag(holder);
        } else {
            holder = (MyHolder) convertView.getTag();
        }
        if (address != null) {
            FormsUtil.setTextViewTxt(holder.tvPerson, address.getPerson(), null);
            FormsUtil.setTextViewTxt(holder.tvPhone, address.getPhone(), null);
            FormsUtil.setTextViewTxt(holder.tvArea, address.getProvince() + address.getCity()
                    + address.getArea(), null);
            FormsUtil.setTextViewTxt(holder.tvAddress, address.getAddress(), null);
            if ("1".equals(address.getIsDefault())) {
                holder.ivSelect.setVisibility(View.VISIBLE);
            } else {
                holder.ivSelect.setVisibility(View.GONE);
            }
        }
        return convertView;
    }

    public class MyHolder {
        public TextView tvPerson;
        public TextView tvPhone;
        public TextView tvArea;
        public TextView tvAddress;
        public ImageView ivSelect;
    }

}
