package com.abc.sz.app.adapter.order;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.Evaluate;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.bean.product.AttributeValueBean;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.util.ShoppingCarCount;
import com.forms.base.XDRImageLoader;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

import java.util.List;

/**
 * 商品评价适配
 *
 * @author ftl
 */
public class ProductEvaluateAdapter extends BaseAdapter {

    private List<Product> list;
    private List<Evaluate> evaluateList;
    private Context context;
    private XDRImageLoader mImageLoader;

    public ProductEvaluateAdapter(List<Product> list, List<Evaluate> evaluateList, Context context,
                                  XDRImageLoader mImageLoader) {
        this.list = list;
        this.evaluateList = evaluateList;
        this.context = context;
        this.mImageLoader = mImageLoader;
        for (int i = 0; i < list.size(); i++) {
            Evaluate evaluate = new Evaluate();
            evaluate.setProductId(list.get(i).getProductId());
            evaluate.setStarNum("0");
            this.evaluateList.add(evaluate);
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Product product = list.get(position);
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_product_evaluate_item, null);
        }
        TextView tvProductName = ViewHolder.get(convertView, R.id.tv_productName);
        TextView tvProductPrice = ViewHolder.get(convertView, R.id.tv_productPrice);
        TextView tvAttribute = ViewHolder.get(convertView, R.id.tv_attribute);
        TextView tvAmount = ViewHolder.get(convertView, R.id.tv_amount);
        LinearLayout llEvaluate = ViewHolder.get(convertView, R.id.ll_evaluate);
        final RatingBar rbRatingBar = ViewHolder.get(convertView, R.id.rb_ratingbar);
        if (product != null) {
            ImageView ivProductImage = ViewHolder.get(convertView, R.id.iv_productImage);
            mImageLoader.displayImage(product.getImageUrl(), ivProductImage, ImageViewUtil.getOption());
            FormsUtil.setTextViewTxt(tvProductName, product.getProductName(), null);
            FormsUtil.setTextViewTxt(tvAmount, product.getNum(), null);
            tvProductPrice.setText(ShoppingCarCount.formatMoney(product.getProductPrice()));
            List<AttributeValueBean> attributeList = product.getAttributeList();
            if (attributeList != null && attributeList.size() > 0) {
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < attributeList.size(); i++) {
                    if (i != attributeList.size() - 1) {
                        sb.append(attributeList.get(i).getValue() + " ");
                    } else {
                        sb.append(attributeList.get(i).getValue());
                    }
                }
                tvAttribute.setText(sb.toString());
            }
        }
        final Evaluate evaluate = evaluateList.get(position);
        rbRatingBar.setRating(Float.parseFloat(evaluate.getStarNum()));

        llEvaluate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                View dialogView = LayoutInflater.from(context).inflate(R.layout.layout_dialog_evaluate, null);
                final AlertDialog dialog = new AlertDialog.Builder(context).create();
                dialog.setView(dialogView, 0, 0, 0, 0);
                dialog.setInverseBackgroundForced(true);
                dialog.show();
                final EditText etEvaluate = (EditText) dialogView.findViewById(R.id.et_evaluate);
                final RatingBar ratingBar = (RatingBar) dialogView.findViewById(R.id.rb_ratingbar);
                Button btnCancel = (Button) dialogView.findViewById(R.id.btn_cancel);
                Button btnConfirm = (Button) dialogView.findViewById(R.id.btn_confirm);

                etEvaluate.setText(evaluate.getContent());
                etEvaluate.setSelection(etEvaluate.length());
                ratingBar.setRating(rbRatingBar.getRating());
                btnCancel.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                btnConfirm.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        evaluate.setContent(etEvaluate.getText().toString());
                        evaluate.setStarNum(String.valueOf((int) ratingBar.getRating()));
                        notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });
            }
        });
        return convertView;
    }

}
