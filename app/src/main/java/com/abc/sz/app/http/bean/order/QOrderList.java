package com.abc.sz.app.http.bean.order;


/**
 * 订单列表查询
 *
 * @author ftl
 */
public class QOrderList {

    //当前页码
    private String pageNum;
    //    //订单 ID
//    private String orderId;
    //1:商品类,2:业务类
    private String type;
    //99:交易失效,0:交易成功,1:未支付,2:未消费,3:待收货,4:待评价
    private String state;
    //起始日期
    private String startDate;
    //结束日期
    private String endDate;

    public QOrderList(String pageNum, String type,
                      String state, String startDate, String endDate) {
        super();
        this.pageNum = pageNum;
//		this.orderId = orderId;
        this.type = type;
        this.state = state;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    //	String orderId,
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

}
