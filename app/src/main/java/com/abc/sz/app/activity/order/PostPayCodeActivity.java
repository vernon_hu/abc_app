package com.abc.sz.app.activity.order;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.abc.ABC_SZ_APP.R;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;

public class PostPayCodeActivity extends AppCompatActivity {
    @ViewInject(R.id.phoneLastNum) EditText phoneLastNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_pay_code);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_post_pay_code, menu);
        return true;
    }

    @OnClick(R.id.payComplete)
    public void payComplete(View view) {
        if (TextUtils.isEmpty(phoneLastNum.getText())) {
            phoneLastNum.setError(getString(R.string.input_authcode_isempty));
        } else if (phoneLastNum.getText().length() != 6) {
            phoneLastNum.setError(getString(R.string.input_authcode_error));
        } else {

        }
    }
}
