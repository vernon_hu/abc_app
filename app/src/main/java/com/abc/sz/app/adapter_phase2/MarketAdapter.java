package com.abc.sz.app.adapter_phase2;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity.product.ProductInfoActivity;
import com.abc.sz.app.activity.product.ProductListActivity;
import com.abc.sz.app.bean_phase2.ImageAD;
import com.abc.sz.app.bean_phase2.MarketItem;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.util.ShoppingCarCount;
import com.forms.base.XDRImageLoader;
import com.forms.library.base.BaseActivity;
import com.forms.library.baseUtil.view.ViewUtils;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;
import com.forms.view.viewflow.CircleFlowIndicator;
import com.forms.view.viewflow.ViewFlow;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hwt on 4/15/15.
 */
public class MarketAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEMTYPE = 1;
    private static final int HEADERTYPE = 0;

    private Context mContext;
    private HeaderView headerView;
    private MarketViewHoler marketView;

    private XDRImageLoader mImageLoader;
    private List<MarketItem> mMarketItems = new ArrayList<>();
    private List<ImageAD> mImageADs = new ArrayList<>();

    private final int NEXTIMAGE = 1;
    private int imageADPosition = 0;

    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == NEXTIMAGE) {
                if (headerView != null) {
                    if (imageADPosition == mImageADs.size() - 1) {
                        imageADPosition = 0;
                    } else {
                        imageADPosition += 1;
                    }
                    headerView.viewFlow.setSelection(imageADPosition);
                    handler.sendEmptyMessageDelayed(NEXTIMAGE, 3000);
                }
            }
        }
    };

    public MarketAdapter(Context context, XDRImageLoader imageLoader, List<ImageAD> imageADs,
                         List<MarketItem> marketItems) {
        mContext = context;
        mImageADs = imageADs;
        mMarketItems = marketItems;
        mImageLoader = imageLoader;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder = null;
        if (HEADERTYPE == i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                    R.layout.layout_phase2_ad_images, viewGroup, false);
            return viewHolder = new HeaderView(view);
        } else if (ITEMTYPE == i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                    R.layout.layout_phase2_market_items, viewGroup, false);
            return viewHolder = new MarketViewHoler(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof HeaderView) {
            headerView = (HeaderView) viewHolder;
            headerView.setIsRecyclable(false);
            if (mImageADs != null && mImageADs.size() > 0) {
                handler.removeMessages(NEXTIMAGE);

                ViewGroup.LayoutParams params = headerView.viewFlow.getLayoutParams();
                params.height = FormsUtil.SCREEN_WIDTH / 5 * 3;
                headerView.viewFlow.setLayoutParams(params);
                headerView.viewFlow.setFlowIndicator(headerView.viewflowindic);
                ImageAdapter imageAdapter = new ImageAdapter(mContext, mImageLoader, mImageADs);
                headerView.viewFlow.setAdapter(imageAdapter, 0);
                headerView.viewFlow.setOnViewSwitchListener(new ViewFlow.ViewSwitchListener() {
                    @Override
                    public void onSwitched(View view, int position) {
                        imageADPosition = headerView.viewFlow.getSelectedItemPosition();
                        ;
                    }
                });
                if(mImageADs!=null&&mImageADs.size()>1) {
                    handler.sendEmptyMessageDelayed(NEXTIMAGE, 3000);
                }
            }
        } else if (viewHolder instanceof MarketViewHoler) {
            marketView = (MarketViewHoler) viewHolder;

            final MarketItem item = mMarketItems.get(i - 1);
            final List<Product> list = item.getTypeList();
            marketView.tv_type_name.setText(item.getTypeName());
            if (list.size() > 1) {
                mImageLoader.displayImage(list.get(0).getImageUrl(), marketView.iv_zssc_commodity_image1, ImageViewUtil.getOption());
                marketView.tv_left_name.setText(list.get(0).getProductName());
                marketView.tv_left_description.setText(list.get(0).getDescription());
                marketView.tv_left_price.setText(ShoppingCarCount.formatMoney(list.get(0).getProductPrice()));

                mImageLoader.displayImage(list.get(1).getImageUrl(), marketView.iv_zssc_commodity_image2, ImageViewUtil.getOption());
                marketView.tv_right_name.setText(list.get(1).getProductName());
                marketView.tv_right_description.setText(list.get(1).getDescription());
                marketView.tv_right_price.setText(ShoppingCarCount.formatMoney(list.get(1).getProductPrice()));

                marketView.ll_right_item.setVisibility(View.VISIBLE);
            } else if (list.size() > 0) {
                mImageLoader.displayImage(list.get(0).getImageUrl(), marketView.iv_zssc_commodity_image1, ImageViewUtil.getOption());
                marketView.tv_left_name.setText(list.get(0).getProductName());
                marketView.tv_left_description.setText(list.get(0).getDescription());
                marketView.tv_left_price.setText(ShoppingCarCount.formatMoney(list.get(0).getProductPrice()));

                marketView.ll_right_item.setVisibility(View.INVISIBLE);
            }

            marketView.ll_left_item.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO 左侧Item点击事件
                    String productId = list.get(0).getProductId();
                    Bundle bundle = new Bundle();
                    bundle.putString(ProductInfoActivity.PRODUCT_ID, productId);
                    ((BaseActivity) mContext).callMe(ProductInfoActivity.class, bundle);
                }
            });

            marketView.ll_right_item.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO 右侧Item点击事件
                    String productId = list.get(1).getProductId();
                    Bundle bundle = new Bundle();
                    bundle.putString(ProductInfoActivity.PRODUCT_ID, productId);
                    ((BaseActivity) mContext).callMe(ProductInfoActivity.class, bundle);
                }
            });

            marketView.tv_more.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO 更多
                    String typeId = item.getTypeId();
                    Bundle bundle = new Bundle();
                    bundle.putString(ProductListActivity.TYPE_ID, typeId);
                    ((BaseActivity) mContext).callMe(ProductListActivity.class, bundle);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mMarketItems.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return HEADERTYPE;
        } else {
            return ITEMTYPE;
        }
    }

    public class MarketViewHoler extends RecyclerView.ViewHolder {

        @ViewInject(R.id.ll_left_item)
        LinearLayout ll_left_item;// 左侧商品
        @ViewInject(R.id.ll_right_item)
        LinearLayout ll_right_item;// 右侧商品
        @ViewInject(R.id.tv_more)
        TextView tv_more;// 更多
        @ViewInject(R.id.tv_type_name)
        TextView tv_type_name;// 类别

        @ViewInject(R.id.iv_zssc_commodity_image1)
        ImageView iv_zssc_commodity_image1;// 左侧商品图片
        @ViewInject(R.id.tv_left_name)
        TextView tv_left_name;// 左侧商品名称
        @ViewInject(R.id.tv_left_description)
        TextView tv_left_description;// 左侧商品描述
        @ViewInject(R.id.tv_left_price)
        TextView tv_left_price;// 左侧商品价格

        @ViewInject(R.id.iv_zssc_commodity_image2)
        ImageView iv_zssc_commodity_image2;// 右侧商品图片
        @ViewInject(R.id.tv_right_name)
        TextView tv_right_name;// 右侧商品名称
        @ViewInject(R.id.tv_right_description)
        TextView tv_right_description;// 右侧商品描述
        @ViewInject(R.id.tv_right_price)
        TextView tv_right_price;// 右侧商品价格

        public MarketViewHoler(View itemView) {
            super(itemView);
            ViewUtils.inject(this, itemView);
        }
    }

    public class HeaderView extends RecyclerView.ViewHolder {

        @ViewInject(R.id.viewFlow)
        ViewFlow viewFlow;
        @ViewInject(R.id.viewflowindic)
        CircleFlowIndicator viewflowindic;

        public HeaderView(View itemView) {
            super(itemView);
            ViewUtils.inject(this, itemView);
        }
    }
}
