package com.abc.sz.app.action;

import com.abc.sz.app.activity.kb.KbInfoActivity;
import com.abc.sz.app.bean_phase2.Omnibus;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.product.QJxInfo;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.alibaba.fastjson.TypeReference;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;


/**
 * 快报详情
 *
 * @author llc
 */
public class KbAction extends BaseAction {


    /**
     * 快报详情
     *
     * @return
     */
    public Http loadingBulletinInfo(String dailyId) {
        QJxInfo bean = new QJxInfo();
        bean.setDailyId(dailyId);
        RequestBean<QJxInfo> params = new RequestBean<QJxInfo>(bean);

        return APPRequestPhase2.jxInfoQuery(params, new ResponseNotify<Omnibus>(new TypeReference<RespBean<Omnibus>>() {
        }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<Omnibus> response) {
                ((KbInfoActivity) uiObject).success(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                ((KbInfoActivity) uiObject).failed(retCode, "jxInfoQuery");
            }
        });
    }
}
