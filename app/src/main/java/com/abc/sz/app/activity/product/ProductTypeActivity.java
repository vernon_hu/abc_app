package com.abc.sz.app.activity.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.ProductTypeAction;
import com.abc.sz.app.bean.product.ProductListBean;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.product.QProductType;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.base.BaseAdapter;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

/**
 * 商品类别列表
 *
 * @author llc
 */
@ContentView(R.layout.activity_product_type)
public class ProductTypeActivity extends ABCActivity {
	
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.loadingView) LoadingView loadingView;
    @ViewInject(R.id.lvTypeFirst) ListView lvTypeFirst;
    @ViewInject(R.id.lvTypeOther) ListView lvTypeOther;
    
    public static String PRODUCT_TYPE_ID = "PRODUCT_TYPE_ID";
    public static String PRODUCT_GRADE_ID = "PRODUCT_GRADE_ID";
    public static String PRODUCT_TAG = "PRODUCT_TAG";

    private BaseAdapter<ProductListBean> typeFirstAdapter;
    private BaseAdapter<ProductListBean> typeOtherAdapter;
    private List<ProductListBean> mList = new ArrayList<>();
    private List<ProductListBean> otherList = new ArrayList<>();
    private Map<Integer, Boolean> mMap = new HashMap<>();
    private ProductTypeAction ptAction;
    private String typeId;
    private String gradeId;
    private String upActivityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initData();
        initListener();
    }

    private void initListener() {
    	lvTypeFirst.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				for (int i = 0; i < mList.size(); i++) {
					if(i == position){
						mMap.put(i, true);
					}else{
						mMap.put(i, false);
					}
				}
				typeFirstAdapter.notifyDataSetChanged();
				otherList.clear();
				if(mList.get(position) != null){
					otherList.addAll(mList.get(position).getChildCategoryList());
					typeOtherAdapter.notifyDataSetChanged();
				}
			}
		});
    }

    private void initData() {
        // 设置标题
        typeId = getIntent().getStringExtra(PRODUCT_TYPE_ID);
        gradeId = getIntent().getStringExtra(PRODUCT_GRADE_ID);

        if (typeId == null || "".equals(typeId)) {
            typeId = "0";
        }
        if (gradeId == null || "".equals(gradeId)) {
            gradeId = "1";
        }

        ptAction = (ProductTypeAction) controller.getAction(this, ProductTypeAction.class);
        // 获得上界面传递参数
        QProductType bean = new QProductType();
        bean.setTypeId(typeId);
        bean.setGrade(gradeId);

        RequestBean<QProductType> params = new RequestBean<QProductType>(bean);
        ptAction.loadingProductType(params).setLoadingView(loadingView).start();
        
        lvTypeFirst.setAdapter(typeFirstAdapter = new BaseAdapter<ProductListBean>(this, mList, R.layout.layout_type_first_item) {

			@Override
			public void viewHandler(int position, ProductListBean t, View convertView) {
				RelativeLayout rlContent = ViewHolder.get(convertView, R.id.rlContent);
				TextView tvTypeName = ViewHolder.get(convertView, R.id.tvTypeName);
				TextView tvLine = ViewHolder.get(convertView, R.id.tvLine);
				FormsUtil.setTextViewTxt(tvTypeName, t.getTypeName());
				if(mMap.get(position)){
					tvTypeName.setTextColor(getResources().getColor(R.color.colorPrimary));
					rlContent.setBackgroundColor(getResources().getColor(R.color.white));
					tvLine.setVisibility(View.GONE);
				}else{
					tvTypeName.setTextColor(getResources().getColor(R.color.black));
					rlContent.setBackgroundColor(getResources().getColor(R.color.grzx_bg));
					tvLine.setVisibility(View.VISIBLE);
				}
			}
		});
        
        lvTypeOther.setAdapter(typeOtherAdapter = new BaseAdapter<ProductListBean>(this, otherList, R.layout.layout_type_other_item) {

			@Override
			public void viewHandler(int position, ProductListBean t, View convertView) {
				TextView tvTypeName = ViewHolder.get(convertView, R.id.tvTypeName);
				final GridView gridView = ViewHolder.get(convertView, R.id.gridView);
				final List<ProductListBean> categoryList = t.getChildCategoryList();
				if(categoryList != null && categoryList.size() != 0){
					gridView.setAdapter(new BaseAdapter<ProductListBean>(ProductTypeActivity.this, categoryList, R.layout.layout_type_other_gridview_item) {
	
						@Override
						public void viewHandler(int position, final ProductListBean t, View convertView) {
							 RelativeLayout rlContent = ViewHolder.get(convertView, R.id.rlContent);
							 ViewGroup.LayoutParams layoutParams = rlContent.getLayoutParams();
							 int width = FormsUtil.SCREEN_WIDTH - (int)FormsUtil.density * 104;
							 layoutParams.height = width / gridView.getNumColumns() + (int)FormsUtil.density * 16;
						     layoutParams.width = width / gridView.getNumColumns();
						     rlContent.setLayoutParams(layoutParams);
						     rlContent.invalidate();
							 ImageView cellIcon = ViewHolder.get(convertView, R.id.cellIcon);
							 TextView cellText = ViewHolder.get(convertView, R.id.cellText);
							 getImageLoader().displayImage(t.getPictureUrl(), cellIcon, ImageViewUtil.getOption());
							 FormsUtil.setTextViewTxt(cellText, t.getTypeName());
							 rlContent.setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									Bundle bundle = new Bundle();
				                    bundle.putString(ProductListActivity.TYPE_ID, t.getTypeId());
				                    bundle.putString(ProductListActivity.TYPE_NAME, t.getTypeName());
				                    callMe(ProductListActivity.class, bundle);
								}
							});
							
						}
					});
					FormsUtil.setTextViewTxt(tvTypeName, t.getTypeName());
				}
			}
		});
        
        
    }

    /**
     * 商品类别列表请求成功
     */
    public void typeListSuccess(List<ProductListBean> list) {
    	if (list != null && list.size() != 0) {
	    	mList.clear();
	        mList.addAll(list);
	        typeFirstAdapter.notifyDataSetChanged();
	        if(mList.get(0) != null){
				otherList.addAll(mList.get(0).getChildCategoryList());
				typeOtherAdapter.notifyDataSetChanged();
			}
	        for (int i = 0; i < list.size(); i++) {
	        	if(i == 0){
	        		mMap.put(i, true);
	        	}else{
	        		mMap.put(i, false);
	        	}
			}
    	} else {
    		loadingView.noData(getString(R.string.loadingNoData));
    	}
    }

    /**
     * 请求失败回调
     *
     * @param retCode
     */
    public void failed(RetCode retCode) {
        loadingView.failed(retCode.getRetMsg());
    }

    public String getUpActivityName() {
        return upActivityName;
    }

    public void setUpActivityName(String upActivityName) {
        this.upActivityName = upActivityName;
    }
    
}
