package com.abc.sz.app.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.forms.library.tools.FormsUtil;

/**
 * 自定义加减按钮
 *
 * @author llc
 */
public class CountPMView extends ViewGroup {

    private View mLayout;
    //减号的Background
    private int minusBackground;
    //加号的Background
    private int plusBackground;

    private ImageView iv_minus;

    private ImageView iv_plus;

    private TextView tv_count;

    private int mChildWidthMeasureSpec;

    private int mChildHeightMeasureSpec;

    private int count;
    //总数
    private int mCount = 1;

    private boolean isLongTouch = false;


    public CountPMView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.CountPM);

        minusBackground = a.getResourceId(R.styleable.CountPM_minusBackground, R.drawable.select_countpm_min);
        plusBackground = a.getResourceId(R.styleable.CountPM_plusBackground, R.drawable.select_countpm_plus);

        mLayout = View.inflate(context, R.layout.layout_pm, null);
        iv_minus = (ImageView) mLayout.findViewById(R.id.iv_minus);
        iv_plus = (ImageView) mLayout.findViewById(R.id.iv_plus);
        tv_count = (TextView) mLayout.findViewById(R.id.tv_count);

        iv_minus.setBackgroundResource(minusBackground);
        iv_plus.setBackgroundResource(plusBackground);
        tv_count.setText(mCount + "");

        initListener();

        addView(mLayout);

        a.recycle();
    }

    /**
     * 初始化监听
     */
    private void initListener() {
        iv_minus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                synchronized (tv_count) {
                    if (mCount > 1) {
                        mCount--;
                        tv_count.setText(mCount + "");
                    }
                }
            }
        });

        iv_minus.setOnLongClickListener(new OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                isLongTouch = true;
                new LongTouchThread(false).start();
                return true;
            }
        });


        iv_minus.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO 点击事件
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        isLongTouch = false;
                        break;
                }
                return false;
            }
        });

        iv_plus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                synchronized (tv_count) {
                    if (mCount < 100) {
                        mCount++;
                        tv_count.setText(mCount + "");
                    }
                }
            }
        });

        iv_plus.setOnLongClickListener(new OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                isLongTouch = true;
                new LongTouchThread(true).start();
                return true;
            }
        });

        iv_plus.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO 点击事件
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        isLongTouch = false;
                        break;
                }
                return false;
            }
        });
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(getDefaultSize(0, widthMeasureSpec),
                getDefaultSize(0, heightMeasureSpec));
        mChildWidthMeasureSpec = widthMeasureSpec;
        mChildHeightMeasureSpec = heightMeasureSpec;
        count = getChildCount();
        for (int i = 0; i < count; ++i) {
            final ViewGroup child = (ViewGroup) getChildAt(i);
            child.measure(mChildWidthMeasureSpec, mChildHeightMeasureSpec);

            child.getChildAt(0).measure(mChildHeightMeasureSpec, mChildHeightMeasureSpec);
            child.getChildAt(1).measure(mChildWidthMeasureSpec - 2 * MeasureSpec.getSize(mChildHeightMeasureSpec) - 8* (int)FormsUtil.density, 
            		mChildHeightMeasureSpec);
            child.getChildAt(2).measure(mChildHeightMeasureSpec, mChildHeightMeasureSpec);
        }
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        // TODO 设置位置
        for (int i = 0; i < count; i++) {
            ViewGroup child = (ViewGroup) getChildAt(i);
            int childLeft = getPaddingLeft();
            int childTop = getPaddingTop();
            child.layout(childLeft, childTop,
                    childLeft + child.getMeasuredWidth(),
                    childTop + child.getMeasuredHeight());
            int firstStart = childLeft;
            child.getChildAt(0).layout(firstStart, childTop, firstStart + child.getChildAt(0).getMeasuredWidth(), childTop + child.getChildAt(0).getMeasuredHeight());
            int secondStart = firstStart + child.getChildAt(0).getMeasuredWidth() + 4 * (int)FormsUtil.density;
            child.getChildAt(1).layout(secondStart, childTop, secondStart + child.getChildAt(1).getMeasuredWidth(), childTop + child.getChildAt(1).getMeasuredHeight());
            int theardStart = secondStart + child.getChildAt(1).getMeasuredWidth() + 4 * (int)FormsUtil.density;
            child.getChildAt(2).layout(theardStart, childTop, theardStart + child.getChildAt(2).getMeasuredWidth(), childTop + child.getChildAt(2).getMeasuredHeight());
        }
    }

    public class LongTouchThread extends Thread {
        boolean isPlus;

        public LongTouchThread(boolean isPlus) {
            this.isPlus = isPlus;
        }

        @Override
        public void run() {
            // TODO 开始循环
            while (isLongTouch) {
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (isPlus) {
                    if (mCount < 100) {
                        tv_count.post(new Runnable() {

                            @Override
                            public void run() {
                                if (mCount < 100) {
                                    mCount++;
                                    tv_count.setText(mCount + "");
                                }
                            }
                        });
                    }
                } else {
                    if (mCount > 1) {
                        tv_count.post(new Runnable() {

                            @Override
                            public void run() {
                                if (mCount > 1) {
                                    mCount--;
                                    tv_count.setText(mCount + "");
                                }
                            }
                        });
                    }
                }
            }
            super.run();
        }
    }

    public int getmCount() {
        return mCount;
    }


}
