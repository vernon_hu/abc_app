package com.abc.sz.app.action;

import android.content.Context;

import com.abc.sz.app.activity.personal.BindNewCardActivity;
import com.abc.sz.app.activity_phase2.lightpay.LightBindCardActivity;
import com.abc.sz.app.http.bean.pay.AESBean;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.alibaba.fastjson.TypeReference;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.view.toast.MyToast;


/**
 * 支付相关请求
 *
 * @author llc
 */
public class PayAction extends BaseAction {

    public Http getAesKey() {
        return APPRequestPhase2.aesKeyIvQuery(getAccessToken(),
                new ResponseNotify<AESBean>(new TypeReference<RespBean<AESBean>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<AESBean> response) {
                    	if(uiObject instanceof BindNewCardActivity){
                    		((BindNewCardActivity) uiObject).querySuccess(response.getContent());
                    	}else if(uiObject instanceof LightBindCardActivity){
                    		((LightBindCardActivity) uiObject).querySuccess(response.getContent());
                    	}
                        
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
                    }
                });
    }
}
