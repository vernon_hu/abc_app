package com.abc.sz.app.adapter;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import java.util.List;

public class AdvertisementAdapter extends PagerAdapter {

    private List<View> views;

    public AdvertisementAdapter(List<View> views) {
        this.views = views;
    }

    @Override
    public int getCount() {
        return views.size();
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
//		((ViewPager)container).removeView(views.get(position));
    }

    @Override
    public Object instantiateItem(View container, int position) {
        View view = views.get(position);
        try {
            ((ViewPager) container).addView(view);
        } catch (Exception e) {

        }
        return view;
    }

}
