package com.abc.sz.app.http.bean.address;


/**
 * 用户地址
 *
 * @author ftl
 */
public class QAddress {

    /* 地址id */
    private String addressId;
    /* 收货人 */
    private String person;
    /* 手机号码 */
    private String phone;
    /* 省份 */
    private String province;
    /* 城市 */
    private String city;
    /* 地区或者县城 */
    private String area;
    /* 详细地址 */
    private String address;
    /* 是否默认地址,1表示是,0表示否 */
    private String isDefault;

    public QAddress() {

    }

    public QAddress(String addressId, String person, String phone, String province, String city,
                    String area, String address) {
        super();
        this.addressId = addressId;
        this.person = person;
        this.phone = phone;
        this.province = province;
        this.city = city;
        this.area = area;
        this.address = address;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }
}
