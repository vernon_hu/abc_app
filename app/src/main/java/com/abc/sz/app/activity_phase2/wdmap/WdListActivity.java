package com.abc.sz.app.activity_phase2.wdmap;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.adapter_phase2.BranchBankListAdapter;
import com.abc.sz.app.bean.branch.BranchSearchRests;
import com.abc.sz.app.bean.product.BranchRests;
import com.abc.sz.app.util.DialogUtil;
import com.abc.sz.app.view.LoadingView;
import com.abc.sz.app.view.TwoSelectedView;
import com.alibaba.fastjson.JSON;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.forms.base.ABCActivity;
import com.forms.base.BaseConfig;
import com.forms.library.baseUtil.asynchttpclient.AsyncHttpClient;
import com.forms.library.baseUtil.asynchttpclient.TextHttpResponseHandler;
import com.forms.library.baseUtil.logger.Logger;
import com.forms.library.baseUtil.net.Controller;
import com.forms.library.baseUtil.net.HttpUtil;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.NetworkUtils;
import com.forms.library.widget.other.XdrDialog;

import org.apache.http.Header;

import java.lang.ref.WeakReference;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 网点地图
 *
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_wdlist)
public class WdListActivity extends ABCActivity {

    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.loadingView) LoadingView loadingView;
    @ViewInject(R.id.recyclerView) RecyclerView recyclerView;
    @ViewInject(R.id.twoSelectView) TwoSelectedView twoSelectView;

    AsyncHttpClient httpClient = null;

    LocationClient mLocationClient;
    double latitude = 22.548687;
    double lontitude = 114.117596;
    XdrDialog progress;
    List<BranchSearchRests> branchBanks = new ArrayList<>();
    List<BranchSearchRests> branchBanksBackup = new ArrayList<>();
    BranchBankListAdapter branchBankListAdapter;
    BDLocation mBdLocation;//当前位置


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.MATCH_PARENT,
                    Gravity.CENTER
            );
            View view = LayoutInflater.from(this).inflate(R.layout.layout_phase2_wdlist_item_searchview, null);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callMe(WdSearchActivity.class);
                }
            });
            getSupportActionBar().setCustomView(view, params);
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        httpClient = Controller.getInstance().getHttpClient();
        //初始化网点数据源
        branchBankListAdapter = new BranchBankListAdapter(branchBanks);
        //////////////////////////////////////////////////////////////////
        //取得当前位置查询附近网点数据
        loadingView.loading(0);
        mLocationClient = getBaseApp().getLocationClient();
        mLocationClient.registerLocationListener(new BDLocationListener() {
            @Override
            public void onReceiveLocation(BDLocation bdLocation) {
                if (progress != null && progress.isShowing()) {
                    progress.dismiss();
                }
                if (bdLocation.getLocType() == BDLocation.TypeGpsLocation ||
                        bdLocation.getLocType() == BDLocation.TypeNetWorkLocation ||
                        bdLocation.getLocType() == BDLocation.TypeOffLineLocation) {
                    latitude = bdLocation.getLatitude();
                    lontitude = bdLocation.getLongitude();
                    mLocationClient.stop();
                    mBdLocation = bdLocation;
                    //定位成功查询网点数据
                    queryWdInfo(0, true);
                }
            }
        });
        mLocationClient.start();

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(branchBankListAdapter);

        //查询网点
        myHandler.sendEmptyMessageDelayed(1, 5000);

        twoSelectView.setOnSelectedItemListener(new TwoSelectedView.onSelectedItemListener() {
            @Override
            public void onSelectedItem(int index, String selectedValue1, String selectedValue2) {
                if (index == 0) {
                    loadingView.loading(0);
                    branchBanks.clear();
                    for (BranchSearchRests searchRests : branchBanksBackup) {
                        if ("0".equals(selectedValue1)) {
                            //选择全部网点
                            branchBanks.addAll(branchBanksBackup);
                        } else if (selectedValue1.equals(searchRests.getTypeId())) {
                            branchBanks.add(searchRests);
                        }
                    }
                    branchBankListAdapter.notifyDataSetChanged();
                    if (branchBanks.size() == 0) {
                        loadingView.noData(getString(R.string.loadingNoData));
                    } else {
                        loadingView.loaded();
                    }
                } else {
                    queryWdInfo(0, true);
                }
            }
        });
    }

    private MyHandler myHandler = new MyHandler(this);

    /**
     * 等待定位设置
     */
    public static class MyHandler extends Handler {
        WeakReference<WdListActivity> weakReference = null;

        public MyHandler(WdListActivity wdListActivity) {
            this.weakReference = new WeakReference<>(wdListActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            final WdListActivity wdListActivity = weakReference.get();
            switch (msg.what) {
                case 1:
                    if (wdListActivity.progress != null && wdListActivity.progress.isShowing()) {
                        wdListActivity.progress.dismiss();
                    }
                    if (wdListActivity.mLocationClient.isStarted()) {
                        //定位失败
                        wdListActivity.mLocationClient.stop();
                        wdListActivity.progress = new XdrDialog(wdListActivity);
                        DialogUtil.showWithRetryBtn(wdListActivity, "获取当前位置失败，请稍后重试",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        wdListActivity.progress.show();
                                        wdListActivity.mLocationClient.start();
                                        wdListActivity.myHandler.sendEmptyMessageDelayed(1, 5000);
                                    }
                                }, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        wdListActivity.progress.dismiss();
                                        dialog.dismiss();
                                        wdListActivity.finish();
                                    }
                                });
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_wdlist_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.locatoin) {
            String message = null;
            if (mBdLocation != null) {
                message = mBdLocation.getAddrStr();
            } else {
                message = "正在获取当前位置，请稍后";
            }
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * 查询网点数据
     *
     * @param pageIndex
     */
    private void queryWdInfo(final int pageIndex, final boolean showLoadingView) {
        if (!NetworkUtils.isNetworkAvailable(this)) {
            Toast.makeText(this, RetCode.netError.getRetMsg(), Toast.LENGTH_SHORT).show();
            return;
        }
        if (showLoadingView) {
            loadingView.loading(0);
        }
        String requestUrl = MessageFormat.format(BaseConfig.QUERY_NEARBRANCH_URL, new Object[]{
                String.valueOf(lontitude),
                String.valueOf(latitude),
                twoSelectView.getValueSecond(),
                twoSelectView.getValueFirst(),
                String.valueOf(pageIndex)
        });
        Logger.d("requestUrl=%s", requestUrl);
        httpClient.get(this, requestUrl, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                RetCode retCode = HttpUtil.parseFailedInfo(throwable);
                if (loadingView.isLoading()) {
                    loadingView.loaded();
                    loadingView.failed(retCode.getRetMsg(), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            queryWdInfo(pageIndex, showLoadingView);
                        }
                    });
                } else {
                    Toast.makeText(WdListActivity.this, retCode.getRetMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Logger.json(responseString);
                if (loadingView.isLoading()) {
                    loadingView.loaded();
                }
                BranchRests branchBanksTemp = JSON.parseObject(responseString, BranchRests.class);
                branchBanks.clear();
                branchBanksBackup.clear();
                branchBanks.addAll(branchBanksTemp.getBranchSearchRests());
                branchBanksBackup.addAll(branchBanks);
                branchBankListAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mLocationClient.isStarted()) {
            mLocationClient.stop();
        }
    }


}
