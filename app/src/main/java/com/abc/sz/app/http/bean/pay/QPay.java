package com.abc.sz.app.http.bean.pay;

import java.util.List;

/**
 * 支付
 * @author ftl
 *
 */
public class QPay {

    private String PayTypeID;//交易类型 
    private String OrderDate;//订单日期 
    private String OrderTime;//订单时间 
    private String ExpiredDate;//设定订单保存时间 
    private String CurrencyCode;//交易币种(156：人民币)
    private String OrderNo;//订单编号
    private String OrderAmount;//交易金额
    private String Fee;//手续费金额
    private String OrderURL;//订单说明 
    private String ReceiverAddress;//收货地址
    private String InstallmentMark;//分期标识(1：分期；0：不分期 )
    private String InstallmentCode;//分期代码(分期标识为“1”时必须设定)
    private String InstallmentNum;//分期期数(分期标识为“1”时必须设定,0-99)
    private String CommodityType;//商品种类
    private String BuyIP;//客户IP
    private String OrderDesc;//订单说明
    private String orderTimeoutDate;//订单有效期
    private List<QProduct> DetailList;
    private String PaymentType;//支付类型 (必须)
    private String PaymentLinkType;//支付接入方式(必须)
    private String UnionPayLinkType;//银联跨行移动支付接入方式
    private String ReceiveAccount;//收款方账号
    private String ReceiveAccName;//收款方户名
    private String NotifyType;//通知方式(必须)
    private String ResultNotifyURL;//通知URL地址(必须) 
    private String MerchantRemarks;//附言 
    private String IsBreakAccount;//交易是否分账(必须) 
    private String SplitAccTemplate;//分账模版编号
    private String isSelect;//1：查询；0：不查询
    
	public String getPayTypeID() {
		return PayTypeID;
	}
	public void setPayTypeID(String payTypeID) {
		PayTypeID = payTypeID;
	}
	public String getOrderDate() {
		return OrderDate;
	}
	public void setOrderDate(String orderDate) {
		OrderDate = orderDate;
	}
	public String getOrderTime() {
		return OrderTime;
	}
	public void setOrderTime(String orderTime) {
		OrderTime = orderTime;
	}
	public String getCurrencyCode() {
		return CurrencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}
	public String getOrderNo() {
		return OrderNo;
	}
	public void setOrderNo(String orderNo) {
		OrderNo = orderNo;
	}
	public String getOrderAmount() {
		return OrderAmount;
	}
	public void setOrderAmount(String orderAmount) {
		OrderAmount = orderAmount;
	}
	public String getInstallmentMark() {
		return InstallmentMark;
	}
	public void setInstallmentMark(String installmentMark) {
		InstallmentMark = installmentMark;
	}
	public String getInstallmentCode() {
		return InstallmentCode;
	}
	public void setInstallmentCode(String installmentCode) {
		InstallmentCode = installmentCode;
	}
	public String getInstallmentNum() {
		return InstallmentNum;
	}
	public void setInstallmentNum(String installmentNum) {
		InstallmentNum = installmentNum;
	}
	public String getCommodityType() {
		return CommodityType;
	}
	public void setCommodityType(String commodityType) {
		CommodityType = commodityType;
	}
	public List<QProduct> getDetailList() {
		return DetailList;
	}
	public void setDetailList(List<QProduct> detailList) {
		DetailList = detailList;
	}
	public String getPaymentType() {
		return PaymentType;
	}
	public void setPaymentType(String paymentType) {
		PaymentType = paymentType;
	}
	public String getPaymentLinkType() {
		return PaymentLinkType;
	}
	public void setPaymentLinkType(String paymentLinkType) {
		PaymentLinkType = paymentLinkType;
	}
	public String getNotifyType() {
		return NotifyType;
	}
	public void setNotifyType(String notifyType) {
		NotifyType = notifyType;
	}
	public String getResultNotifyURL() {
		return ResultNotifyURL;
	}
	public void setResultNotifyURL(String resultNotifyURL) {
		ResultNotifyURL = resultNotifyURL;
	}
	public String getIsBreakAccount() {
		return IsBreakAccount;
	}
	public void setIsBreakAccount(String isBreakAccount) {
		IsBreakAccount = isBreakAccount;
	}
	public String getReceiverAddress() {
		return ReceiverAddress;
	}
	public void setReceiverAddress(String receiverAddress) {
		ReceiverAddress = receiverAddress;
	}
	public String getExpiredDate() {
		return ExpiredDate;
	}
	public void setExpiredDate(String expiredDate) {
		ExpiredDate = expiredDate;
	}
	public String getFee() {
		return Fee;
	}
	public void setFee(String fee) {
		Fee = fee;
	}
	public String getOrderURL() {
		return OrderURL;
	}
	public void setOrderURL(String orderURL) {
		OrderURL = orderURL;
	}
	public String getBuyIP() {
		return BuyIP;
	}
	public void setBuyIP(String buyIP) {
		BuyIP = buyIP;
	}
	public String getOrderDesc() {
		return OrderDesc;
	}
	public void setOrderDesc(String orderDesc) {
		OrderDesc = orderDesc;
	}
	public String getOrderTimeoutDate() {
		return orderTimeoutDate;
	}
	public void setOrderTimeoutDate(String orderTimeoutDate) {
		this.orderTimeoutDate = orderTimeoutDate;
	}
	public String getUnionPayLinkType() {
		return UnionPayLinkType;
	}
	public void setUnionPayLinkType(String unionPayLinkType) {
		UnionPayLinkType = unionPayLinkType;
	}
	public String getReceiveAccount() {
		return ReceiveAccount;
	}
	public void setReceiveAccount(String receiveAccount) {
		ReceiveAccount = receiveAccount;
	}
	public String getReceiveAccName() {
		return ReceiveAccName;
	}
	public void setReceiveAccName(String receiveAccName) {
		ReceiveAccName = receiveAccName;
	}
	public String getMerchantRemarks() {
		return MerchantRemarks;
	}
	public void setMerchantRemarks(String merchantRemarks) {
		MerchantRemarks = merchantRemarks;
	}
	public String getSplitAccTemplate() {
		return SplitAccTemplate;
	}
	public void setSplitAccTemplate(String splitAccTemplate) {
		SplitAccTemplate = splitAccTemplate;
	}
	public String getIsSelect() {
		return isSelect;
	}
	public void setIsSelect(String isSelect) {
		this.isSelect = isSelect;
	}
    
}
