package com.abc.sz.app.action;

import com.abc.sz.app.activity.personal.MyFavoritesActivity;
import com.abc.sz.app.activity.product.ProductInfoActivity;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.order.QShoppingCar;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.alibaba.fastjson.TypeReference;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.view.toast.MyToast;


/**
 * 商品其他请求
 *
 * @author user
 */
public class ProductOtherAction extends BaseAction {

    /**
     * 添加收藏
     *
     * @return
     */
    public Http addFavorite(String productId) {
        return APPRequestPhase2.favourite(getAccessToken(), productId,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        ((ProductInfoActivity) uiObject).addSuccess(response.getContent());
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        ((ProductInfoActivity) uiObject).failed(retCode);
                    }
                });
    }

    /**
     * 取消收藏
     *
     * @param favouriteId
     * @return
     */
    public Http removeFavorite(String favouriteId) {
        return APPRequestPhase2.delFavourite(getAccessToken(), favouriteId,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        if (uiObject instanceof ProductInfoActivity) {
                            ((ProductInfoActivity) uiObject).removeSuccess();
                        } else if (uiObject instanceof MyFavoritesActivity) {
                            ((MyFavoritesActivity) uiObject).removeSuccess();
                        }
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        if (uiObject instanceof ProductInfoActivity) {
                            ((ProductInfoActivity) uiObject).failed(retCode);
                        } else if (uiObject instanceof MyFavoritesActivity) {
                        	MyToast.showTEXT((MyFavoritesActivity) uiObject, retCode.getRetMsg());
                        }
                    }
                });
    }

    /**
     * 加入购物车
     *
     * @param productId
     * @return
     */
    public Http addShopingCar(String productId, String amount, final boolean immediatelyBuy) {
        QShoppingCar qShoppingCar = new QShoppingCar(productId, amount);
        RequestBean<QShoppingCar> params = new RequestBean<QShoppingCar>(qShoppingCar);
        params.setToken(getAccessToken());
        return APPRequestPhase2.addShoppingCar(params,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        ((ProductInfoActivity) uiObject).addShopingCarSuccess(immediatelyBuy);
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        ((ProductInfoActivity) uiObject).failed(retCode);
                    }
                });
    }


}
