package com.abc.sz.app.http.bean.pay;

/**
 * 加密因子
 *
 * @author llc
 */
public class AESBean {

    private String key;

    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "AESBean [key=" + key + ", value=" + value + "]";
    }


}
