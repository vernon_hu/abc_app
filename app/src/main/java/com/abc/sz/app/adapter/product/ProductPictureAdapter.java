package com.abc.sz.app.adapter.product;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import java.util.List;

/**
 * 产品详情图片适配器
 *
 * @author llc
 */
public class ProductPictureAdapter extends PagerAdapter {

    private List<View> mViews;

    public ProductPictureAdapter(List<View> views) {
        mViews = views;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mViews.size();
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public Object instantiateItem(View container, int position) {
        if (mViews == null || mViews.size() == 0)
            return null;

        try {
            ((ViewPager) container).addView(mViews.get(position));
        } catch (Exception e) {

        }
        return mViews.get(position);
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
//		((ViewPager)container).removeView(views.get(position));
    }

    public void setmViews(List<View> mViews) {
        this.mViews = mViews;
    }

//	@Override
//	public int getItemPosition(Object object) {
//		// TODO Auto-generated method stub
//		return POSITION_NONE;
//	}


}
