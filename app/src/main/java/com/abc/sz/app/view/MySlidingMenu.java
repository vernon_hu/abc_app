package com.abc.sz.app.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.forms.base.ABCFragment;
import com.forms.library.widget.slidingmenu.SlidingMenu;

/**
 * 可嵌入Framgent里的SlidingMenu
 *
 * @author llc
 */
public class MySlidingMenu extends SlidingMenu {

    public static final int SLIDING_WINDOW = 0;
    public static final int SLIDING_CONTENT = 1;
    private boolean mActionbarOverlay = false;

    /**
     * Constant value for use with setTouchModeAbove(). Allows the SlidingMenu to be opened with a swipe
     * gesture on the screen's margin
     */
    public static final int TOUCHMODE_MARGIN = 0;

    /**
     * Constant value for use with setTouchModeAbove(). Allows the SlidingMenu to be opened with a swipe
     * gesture anywhere on the screen
     */
    public static final int TOUCHMODE_FULLSCREEN = 1;

    /**
     * Constant value for use with setTouchModeAbove(). Denies the SlidingMenu to be opened with a swipe
     * gesture
     */
    public static final int TOUCHMODE_NONE = 2;

    /**
     * Constant value for use with setMode(). Puts the menu to the left of the content.
     */
    public static final int LEFT = 0;

    /**
     * Constant value for use with setMode(). Puts the menu to the right of the content.
     */
    public static final int RIGHT = 1;

    /**
     * Constant value for use with setMode(). Puts menus to the left and right of the content.
     */
    public static final int LEFT_RIGHT = 2;


    /**
     * Instantiates a new SlidingMenu.
     *
     * @param context the associated Context
     */
    public MySlidingMenu(Context context) {
        this(context, null);
    }

    /**
     * Instantiates a new SlidingMenu and attach to Activity.
     *
     * @param activity   the activity to attach slidingmenu
     * @param slideStyle the slidingmenu style
     */
    public MySlidingMenu(ABCFragment activity, int slideStyle) {
        this(activity.getActivity(), null);
        this.attachToFragment(activity, slideStyle, false);
    }

    /**
     * 把SlidingMenu加到某布局内
     *
     * @param context the associated Context
     * @param attrs   the attrs
     */
    public MySlidingMenu(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public void attachToActivity(Activity activity, int rid, int slideStyle, boolean actionbarOverlay) {
        if (slideStyle != SLIDING_WINDOW && slideStyle != SLIDING_CONTENT)
            throw new IllegalArgumentException("slideStyle must be either SLIDING_WINDOW or SLIDING_CONTENT");

        if (getParent() != null)
            throw new IllegalStateException("This SlidingMenu appears to already be attached");

        // get the window background
        TypedArray a = activity.getTheme().obtainStyledAttributes(new int[]{android.R.attr.windowBackground});
        int background = a.getResourceId(0, 0);
        a.recycle();

        switch (slideStyle) {
            case SLIDING_WINDOW:
                mActionbarOverlay = false;
                ViewGroup decor = (ViewGroup) activity.getWindow().getDecorView();
                ViewGroup decorChild = (ViewGroup) decor.getChildAt(0);
                // save ActionBar themes that have transparent assets
                decorChild.setBackgroundResource(background);
                decor.removeView(decorChild);
                decor.addView(this);
                setContent(decorChild);
                break;
            case SLIDING_CONTENT:
                mActionbarOverlay = actionbarOverlay;
                // take the above view out of
                ViewGroup contentParent = (ViewGroup) activity.findViewById(rid);
                View content = contentParent.getChildAt(0);
                contentParent.removeView(content);
                contentParent.addView(this);
                setContent(content);
//			 save people from having transparent backgrounds
                if (content.getBackground() == null)
                    content.setBackgroundResource(background);
                break;
        }
    }


    /**
     * 把SlidingMenu加入到Fragment中
     *
     * @param fragment         需要侧滑菜单的Fragment
     * @param slideStyle       either SLIDING_CONTENT or SLIDING_WINDOW
     * @param actionbarOverlay whether or not the ActionBar is overlaid
     * @author llc
     */
    public void attachToFragment(ABCFragment fragment, int slideStyle, boolean actionbarOverlay) {
        if (slideStyle != SLIDING_WINDOW && slideStyle != SLIDING_CONTENT)
            throw new IllegalArgumentException("slideStyle must be either SLIDING_WINDOW or SLIDING_CONTENT");

        if (getParent() != null)
            throw new IllegalStateException("This SlidingMenu appears to already be attached");

        // get the window background
        TypedArray a = fragment.getActivity().getTheme().obtainStyledAttributes(new int[]{android.R.attr.windowBackground});
        int background = a.getResourceId(0, 0);
        a.recycle();

        switch (slideStyle) {
            case SLIDING_WINDOW:
                mActionbarOverlay = false;
                ViewGroup decor = (ViewGroup) fragment.getActivity().getWindow().getDecorView();
                ViewGroup decorChild = (ViewGroup) decor.getChildAt(0);
                // save ActionBar themes that have transparent assets
                decorChild.setBackgroundResource(background);
                decor.removeView(decorChild);
                decor.addView(this);
                setContent(decorChild);
                break;
            case SLIDING_CONTENT:
                mActionbarOverlay = actionbarOverlay;
                // take the above view out of
                ViewGroup contentParent = (ViewGroup) fragment.getView();
                View content = contentParent.getChildAt(0);
                if(content instanceof FrameLayout){
                    FrameLayout frameLayout = (FrameLayout) content;
                    frameLayout.addView(this);
                }
//			contentParent.removeView(content);
//                recyclerView.addView(this);
//			setContent(content);
//			// save people from having transparent backgrounds
                if (content.getBackground() == null)
                    content.setBackgroundResource(background);
                break;
        }
    }


}
