package com.abc.sz.app.bean.product;

import com.forms.library.base.BaseBean;

/**
 * 评论内容
 *
 * @author llc
 */
public class EvalutionBean extends BaseBean {
    //用户名
    private String userName;
    //评论时间
    private String dateTime;
    //评分
    private String score;
    //内容
    private String content;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


}
