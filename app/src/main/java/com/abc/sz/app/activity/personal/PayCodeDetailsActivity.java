package com.abc.sz.app.activity.personal;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity.order.OrderDetailsActivity;
import com.abc.sz.app.activity.order.OrderState;
import com.abc.sz.app.bean.Order;
import com.abc.sz.app.bean.PayCode;
import com.abc.sz.app.util.ImageViewUtil;
import com.alibaba.fastjson.JSON;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.log.LogUtils;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.encrypt.Base64;
import com.forms.view.zxing.util.ZXingUtil;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * 支付码详情
 *
 * @author ftl
 */
@ContentView(R.layout.activity_pay_code_details)
public class PayCodeDetailsActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    //	// 详情条目
    @ViewInject(R.id.ll_item)
    private LinearLayout ll_item;


    @ViewInject(R.id.iv_pictureId)
    private ImageView iv_pictureId;
    @ViewInject(R.id.tv_productName)
    private TextView tv_productName;
    @ViewInject(R.id.tv_period)
    private TextView tv_period;
    @ViewInject(R.id.tv_paymentId)
    private TextView tv_paymentId;
    @ViewInject(R.id.iv_codeImage)
    private ImageView iv_codeImage;


    private PayCode payCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        payCode = cacheBean.getCache(PayCode.class);
        if (payCode != null) {
            FormsUtil.setTextViewTxt(tv_productName, payCode.getProductName());
            FormsUtil.setTextViewTxt(tv_period, payCode.getPeriod());
            FormsUtil.setTextViewTxt(tv_paymentId, payCode.getPaymentId());
            imageLoader.displayImage(payCode.getPictureId(), iv_pictureId, ImageViewUtil.getOption());
            Map<String, String> payContent = new HashMap<String, String>();
            payContent.put("paymentId", payCode.getPaymentId());
            try {
                String paymentCode = Base64.encode(
                        (getString(R.string.barcode_header) + JSON.toJSONString(payContent))
                                .getBytes("UTF-8"));
                Bitmap bitmap = ZXingUtil.createQRCodeBitmap(paymentCode,
                        FormsUtil.dip2px(320),
                        FormsUtil.dip2px(320));
                iv_codeImage.setImageBitmap(bitmap);
            } catch (UnsupportedEncodingException e) {
                LogUtils.d(e.getMessage(), e);
            }

        }

        initListener();
    }

    /**
     * 初始化监听
     */
    private void initListener() {
        // TODO Auto-generated method stub
    }

    @OnClick(value = {R.id.ll_item})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_item:
                Order order = new Order();
                order.setOrderId(payCode.getOrderId());
                order.setState(OrderState.noUse + "");
                getCacheBean().putCache(order);
                callMe(OrderDetailsActivity.class);
                break;
        }

    }


}
