package com.abc.sz.app.bean.cridetcard;


/**
 * 账单分期
 * 
 * @author ftl
 */
public class BillInstallments{

    private String hasBillInstallments;//是否有已进行账单分期（有/无）
    private String numberOfLottery;//剩余抽奖次数
    private String prize;//获奖信息说明
    
	public String getHasBillInstallments() {
		return hasBillInstallments;
	}
	public void setHasBillInstallments(String hasBillInstallments) {
		this.hasBillInstallments = hasBillInstallments;
	}
	public String getNumberOfLottery() {
		return numberOfLottery;
	}
	public void setNumberOfLottery(String numberOfLottery) {
		this.numberOfLottery = numberOfLottery;
	}
	public String getPrize() {
		return prize;
	}
	public void setPrize(String prize) {
		this.prize = prize;
	}
    
}
