package com.abc.sz.app.bean_phase2;

import java.util.List;

import com.abc.sz.app.bean.Product;

/**
 * 首页商品
 * <p/>
 * Created by hwt on 4/15/15.
 */
public class MarketItem {

    // 商品类别ID
    private String typeId;
    // 商品类别名称
    private String typeName;
    // 类别图标URL
    private String PictureUrl;
    // 是否为最后属性：0：不是 1：是
    private String IsLast;
    // 商品类别下的商品列表
    private List<Product> typeList;

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public List<Product> getTypeList() {
        return typeList;
    }

    public void setTypeList(List<Product> typeList) {
        this.typeList = typeList;
    }

    public String getPictureUrl() {
        return PictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        PictureUrl = pictureUrl;
    }

    public String getIsLast() {
        return IsLast;
    }

    public void setIsLast(String isLast) {
        IsLast = isLast;
    }

}
