package com.abc.sz.app.bean.cridetcard;


import java.util.List;

/**
 * 消费列表
 * 
 * @author ftl
 */
public class ConsumerList {

	private String cardID;// 信用卡id
	private String cardNumber;// 信用卡号
	private List<Consumer> consumerList;// 消费列表
	
	public String getCardID() {
		return cardID;
	}
	public void setCardID(String cardID) {
		this.cardID = cardID;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public List<Consumer> getConsumerList() {
		return consumerList;
	}
	public void setConsumerList(List<Consumer> consumerList) {
		this.consumerList = consumerList;
	}

}
