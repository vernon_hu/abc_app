package com.abc.sz.app.activity_phase2;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.view.MyWebView;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * 
 * @author ftl
 *
 */
@ContentView(R.layout.activity_calculator)
public class WebViewActivity extends ABCActivity {
	
	@ViewInject(R.id.loadingBar)
	private ProgressBar loadingBar;
	@ViewInject(R.id.webView)
	private WebView webView;
	
	public final static String TARGET_URL = "targetUrl";
	public final static String USERAGENT = "userAgent";
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    	
        String targetUrl = getIntent().getStringExtra(TARGET_URL);
        boolean userAgent = getIntent().getBooleanExtra(USERAGENT, false);
    	loadingBar.setMax(100);
    	MyWebView myWebView = new MyWebView(this, webView, loadingBar);
		myWebView.initWebViewConfig();
		if(userAgent){
			WebSettings setting = webView.getSettings();
			System.out.println("before" + setting.getUserAgentString());
			setting.setUserAgentString(setting.getUserAgentString() + myWebView.getUserAgent());
			System.out.println("after" + setting.getUserAgentString());
			webView.loadUrl(targetUrl + myWebView.getUserAgent());
		}else{
			webView.loadUrl(targetUrl);
		}
    }
    
}
