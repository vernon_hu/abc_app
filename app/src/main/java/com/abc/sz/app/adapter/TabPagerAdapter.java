package com.abc.sz.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCActivity;
import com.forms.base.ABCFragment;

public class TabPagerAdapter extends FragmentPagerAdapter {

    private Class[] fragments;
    private FragmentManager manager;
    private ABCActivity activity;

    public TabPagerAdapter(FragmentActivity activity, Class fragments[]) {
        super(activity.getSupportFragmentManager());
        this.activity = (ABCActivity) activity;
        manager = activity.getSupportFragmentManager();
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        ABCFragment fragment = null;
        try {
            fragment = (ABCFragment) manager.findFragmentByTag("android:switcher:" + R.id.fl_containerLayout + ":" + position);
            if (fragment == null) {
                fragment = (ABCFragment) (fragments[position].newInstance());
                activity.getBaseApp().getFragmentList().put(position, fragment);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return fragments.length;
    }


}
