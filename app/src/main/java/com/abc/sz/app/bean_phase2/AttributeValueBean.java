package com.abc.sz.app.bean_phase2;

import com.forms.library.base.BaseBean;

/**
 * 属性值对象
 *
 * @author llc
 */
public class AttributeValueBean extends BaseBean {

    // 属性值id
    private String key;
    // 属性值
    private String value;
    private boolean isChecked;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

}
