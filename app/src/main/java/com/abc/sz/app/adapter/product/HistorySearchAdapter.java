package com.abc.sz.app.adapter.product;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.SearchKeywords;
import com.forms.library.tools.FormsUtil;

import java.util.List;

/**
 * 历史搜索列表适配
 *
 * @author ftl
 */
public class HistorySearchAdapter extends BaseAdapter {

    private List<SearchKeywords> list;
    private MyHolder holder;

    public HistorySearchAdapter(List<SearchKeywords> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SearchKeywords searchKeywords = list.get(position);
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_history_search_item, null);
            holder = new MyHolder();
            holder.tvSearchName = (TextView) convertView.findViewById(R.id.tv_searchName);
            convertView.setTag(holder);
        } else {
            holder = (MyHolder) convertView.getTag();
        }
        FormsUtil.setTextViewTxt(holder.tvSearchName, searchKeywords.getKeywordsName(), null);
        return convertView;
    }

    public class MyHolder {
        public TextView tvSearchName;
    }

}
