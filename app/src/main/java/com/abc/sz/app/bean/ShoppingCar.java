package com.abc.sz.app.bean;

import java.util.List;

import com.abc.sz.app.bean.product.AttributeValueBean;
import com.forms.library.base.BaseBean;


/**
 * 购物车
 *
 * @author ftl
 */
public class ShoppingCar extends BaseBean {

    private String ProductId;
    private String HisPrice;
    private String ProductName;
    private String ProductPrice;
    private String Amount;
    private String ImageUrl;
    //私有属性列表
    private List<AttributeValueBean> AttributeList;

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public String getHisPrice() {
        return HisPrice;
    }

    public void setHisPrice(String hisPrice) {
        HisPrice = hisPrice;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductPrice() {
        return ProductPrice;
    }

    public void setProductPrice(String productPrice) {
        ProductPrice = productPrice;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

	public List<AttributeValueBean> getAttributeList() {
		return AttributeList;
	}

	public void setAttributeList(List<AttributeValueBean> attributeList) {
		AttributeList = attributeList;
	}

}
