package com.abc.sz.app.fragment_phase2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity_phase2.WebViewActivity;
import com.abc.sz.app.activity_phase2.creditcard.CreditCardActivity;
import com.forms.base.ABCFragment;
import com.forms.base.BaseConfig;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;

/**
 * 生活助手
 *
 * @author hkj
 */
public class LifeFragment extends ABCFragment {
	
    @ViewInject(R.id.webView)
    private WebView webView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_phase2_life, container, false);
        return view;
    }
    
    @Override
	protected void initData() {
		super.initData();
//		MyWebView myWebView = new MyWebView(baseActivity, webView);
//		myWebView.initWebViewConfig();
//		webView.loadUrl("file:///android_asset/example.html");
	}



	@OnClick(value = {R.id.iv_gljf, R.id.iv_slyd, R.id.iv_ctcp, R.id.iv_yxdk, R.id.iv_sjcz, R.id.iv_xykyh,
            R.id.iv_mstd, R.id.iv_yhsh, R.id.iv_zsdy})
    public void onClick(View view) {
		if(baseActivity.isLogin()){
			Bundle bundle = new Bundle();
	        switch (view.getId()) {
	            case R.id.iv_gljf:
	            	//宽带
	            	bundle.putString(WebViewActivity.TARGET_URL, BaseConfig.BROADBAND_PAY_URL);
	                baseActivity.callMe(WebViewActivity.class, bundle);
	                break;
	            case R.id.iv_xykyh:
	                baseActivity.callMe(CreditCardActivity.class);
	                break;
	            case R.id.iv_zsdy:
	            	//交通罚款
	            	bundle.putString(WebViewActivity.TARGET_URL, BaseConfig.TRAFFIC_FINE_URL);
	            	baseActivity.callMe(WebViewActivity.class, bundle);
	        		break;
	            case R.id.iv_yxdk:
	            	//流量
	            	bundle.putString(WebViewActivity.TARGET_URL, BaseConfig.PHONE_FLOW_URL);
	            	baseActivity.callMe(WebViewActivity.class, bundle);
	            	break;
	            case R.id.iv_sjcz:
	            	//话费
	            	bundle.putString(WebViewActivity.TARGET_URL, BaseConfig.PHONE_PAY_URL);
	            	baseActivity.callMe(WebViewActivity.class, bundle);
	        		break;
	        }
		}
    }

}
