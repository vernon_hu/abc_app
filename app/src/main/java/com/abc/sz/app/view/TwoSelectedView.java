package com.abc.sz.app.view;

import android.content.Context;
import android.graphics.drawable.PaintDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCActivity;
import com.forms.base.BaseAdapter;
import com.forms.library.baseUtil.logger.Logger;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by monkey on 2015/8/14.
 */
public class TwoSelectedView extends LinearLayout implements View.OnClickListener {
    private List<String> branchTypes = new ArrayList<>();
    private String[] branchTypeValues = null;
    private BaseAdapter branchTypeAdapter = null;
    private List<String> rangeValues = new ArrayList<>();
    private String[] ranges = null;
    private BaseAdapter rangesAdapter = null;
    private String selectedRange = "1000";//默认距离
    private String selectedBranchType = "0";//默认机构类型
    private PopupWindow branchPopWindow;//机构类型选择弹窗
    private PopupWindow rangePopWindown;//距离选择弱窗
    private TextView tvBranchTypes;
    private TextView tvRange;
    private onSelectedItemListener onSelectedItemListener;

    public TwoSelectedView(Context context) {
        super(context);
    }

    public TwoSelectedView(Context context, AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_layout_twoselect, this);
        tvBranchTypes = (TextView) view.findViewById(R.id.tvBranchTypes);
        tvRange = (TextView) view.findViewById(R.id.tvRange);
        tvBranchTypes.setOnClickListener(this);
        tvRange.setOnClickListener(this);
        ////////////////////////////////////////////////////////////////
        //初始化数据()
        initBranchTypeData();
        initRangeData();
        //////////////////////////////////////////////////////////////////
        //初始化弹窗
        initAdapter();
        branchPopWindow = createPopWindow(branchTypeAdapter, new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                branchPopWindow.dismiss();
                selectedBranchType = branchTypeValues[position];
                FormsUtil.setTextViewTxt(tvBranchTypes, branchTypes.get(position));
                Logger.d("selectedBranchType=%s\tselectedRange=%s", selectedBranchType, selectedRange);
                if (onSelectedItemListener != null) {
                    onSelectedItemListener.onSelectedItem(0, selectedBranchType, selectedRange);
                }
            }
        });
        rangePopWindown = createPopWindow(rangesAdapter, new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                rangePopWindown.dismiss();
                selectedRange = ranges[position];
                FormsUtil.setTextViewTxt(tvRange, rangeValues.get(position));
                Logger.d("selectedBranchType=%s\tselectedRange=%s", selectedBranchType, selectedRange);
                if (onSelectedItemListener != null) {
                    onSelectedItemListener.onSelectedItem(1, selectedBranchType, selectedRange);
                }
            }
        });
    }

    /**
     * 加载网点类型数据
     */
    private void initBranchTypeData() {
        String[] branchTypesTemp = getResources().getStringArray(R.array.branch_type);
        branchTypes = Arrays.asList(branchTypesTemp);
        branchTypeValues = getResources().getStringArray(R.array.branch_type_value);
        tvBranchTypes.setText(branchTypesTemp[0]);
    }

    /**
     * 加载距离数据
     */
    private void initRangeData() {
        String[] rangesTemp = getResources().getStringArray(R.array.range_value);
        rangeValues = Arrays.asList(rangesTemp);
        ranges = getResources().getStringArray(R.array.range_key);
        tvRange.setText(rangesTemp[0]);
    }

    /**
     * 初始化弹窗的源数据
     */
    private void initAdapter() {
        if (!isInEditMode()) {
            rangesAdapter = new BaseAdapter<String>((ABCActivity) getContext(),
                    rangeValues,
                    R.layout.layout_phase2_district_brand_item) {
                @Override
                public void viewHandler(int position, String range, View convertView) {
                    TextView tvContent = ViewHolder.get(convertView, R.id.tvContent);
                    FormsUtil.setTextViewTxt(tvContent, range);
                }
            };

            branchTypeAdapter = new BaseAdapter<String>((ABCActivity) getContext(),
                    branchTypes,
                    R.layout.layout_phase2_district_brand_item) {
                @Override
                public void viewHandler(int position, String s, View convertView) {
                    TextView tvContent = ViewHolder.get(convertView, R.id.tvContent);
                    FormsUtil.setTextViewTxt(tvContent, s);
                }
            };
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvBranchTypes:
                if (branchPopWindow != null && !branchPopWindow.isShowing()) {
                    branchPopWindow.showAsDropDown(view);
                }
                break;
            case R.id.tvRange:
                if (rangePopWindown != null && !rangePopWindown.isShowing()) {
                    rangePopWindown.showAsDropDown(view);
                }
                break;
        }
    }

    /**
     * 创建弹窗
     *
     * @param baseAdapter         弹窗加载的数据源
     * @param onItemClickListener 数据选择事件
     * @return
     */
    private PopupWindow createPopWindow(BaseAdapter<?> baseAdapter, AdapterView.OnItemClickListener onItemClickListener) {
        View layout = LayoutInflater.from(getContext()).inflate(R.layout.layout_phase2_district_brand_list, null);
        final PopupWindow popupWindow = new PopupWindow(layout, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        ListView listView = (ListView) layout.findViewById(R.id.listView);
        listView.setAdapter(baseAdapter);
        listView.setOnItemClickListener(onItemClickListener);

        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(new PaintDrawable());
        popupWindow.setFocusable(true);// 注意必须设置可获得焦点
        return popupWindow;
    }

    public TwoSelectedView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getValueFirst() {
        return selectedBranchType;
    }

    public String getValueSecond() {
        return selectedRange;
    }

    public void setOnSelectedItemListener(TwoSelectedView.onSelectedItemListener onSelectedItemListener) {
        this.onSelectedItemListener = onSelectedItemListener;
    }

    public interface onSelectedItemListener {
        /**
         * 更改选中项值时
         * @param index 当前更改项索引
         * @param selectedValue1 项1更改后的值
         * @param selectedValue2 项2更改后的值
         */
        public void onSelectedItem(int index, String selectedValue1, String selectedValue2);
    }
}
