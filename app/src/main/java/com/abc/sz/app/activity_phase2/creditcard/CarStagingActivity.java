package com.abc.sz.app.activity_phase2.creditcard;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.CreditcardAction;
import com.abc.sz.app.bean.cridetcard.DistrictAndBrand;
import com.abc.sz.app.util.DialogUtil;
import com.abc.sz.app.view.CountDownButton;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.view.toast.MyToast;

/**
 * 汽车分期页面
 * 
 * @author hkj
 */
@ContentView(R.layout.activity_phase2_carstaging)
public class CarStagingActivity extends ABCActivity {

	@ViewInject(R.id.appBar) Toolbar toolbar;
	@ViewInject(R.id.loadingView) LoadingView loadingView;
	@ViewInject(R.id.tvBrand)  TextView tvBrand;
	@ViewInject(R.id.etCarPrice) EditText etCarPrice;
	@ViewInject(R.id.etPhone) EditText etPhone;
	@ViewInject(R.id.etMessageCode)  EditText etMessageCode;
	@ViewInject(R.id.btnAuthCode) CountDownButton btnAuthCode;
	@ViewInject(R.id.cbContact) CheckBox cbContact;
	
	private CreditcardAction creditcardAction;
	private String agreedToCall = "0";
	private String[] brandKeyArray;
	private String[] brandValueArray;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		creditcardAction = (CreditcardAction) controller.getAction(this, CreditcardAction.class);
		creditcardAction.queryDistrictAndBrandList().setLoadingView(loadingView).start();
		cbContact.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					agreedToCall = "1";
				}else{
					agreedToCall = "0";
				}
			}
		});
	}

	@OnClick(value={R.id.tvBrand, R.id.btnAuthCode, R.id.btnNext})
	public void onClick(View view) {
		String phone = etPhone.getText().toString();
		switch (view.getId()) {
			case R.id.tvBrand:
				DialogUtil.showToSelect(this, null, brandValueArray, new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						FormsUtil.setTextViewTxt(tvBrand, brandValueArray[which]);
					}
				});
				break;
			case R.id.btnAuthCode:
				if (TextUtils.isEmpty(phone)) {
			         FormsUtil.setErrorHtmlTxt(this, etPhone, R.string.please_input_phone);
			    } else if (phone.length() != 11) {
			         FormsUtil.setErrorHtmlTxt(this, etPhone, R.string.input_eleven_phone);
			    } else {
			    	 creditcardAction.getCarSmsCheck(phone).start();
			    }
				break;
			case R.id.btnNext:
				String brand = tvBrand.getText().toString();
				String carPrice = etCarPrice.getText().toString();
				String checkCode = etMessageCode.getText().toString();
				if (TextUtils.isEmpty(brand)) {
					MyToast.showTEXT(this, "请选择意向品牌");
			    } else if (TextUtils.isEmpty(carPrice)) {
			         FormsUtil.setErrorHtmlTxt(this, etCarPrice, R.string.input_carPrice_isempty);
			    } else if (TextUtils.isEmpty(phone)) {
			         FormsUtil.setErrorHtmlTxt(this, etPhone, R.string.please_input_phone);
			    } else if (phone.length() != 11) {
			         FormsUtil.setErrorHtmlTxt(this, etPhone, R.string.input_eleven_phone);
			    } else if(TextUtils.isEmpty(checkCode)){
			    	 FormsUtil.setErrorHtmlTxt(this, etMessageCode, R.string.please_input_authCode);
			    } else {
			    	creditcardAction.queryCarLoanCommitment(brand, carPrice, phone, checkCode, agreedToCall).start();
			    }
				break;
		}
	}
	

	/**
     * 汽车分期贷款额度查询成功回调
     */
    public void querySuccess(String result) {
    	callMe(CreditLineActivity.class);
//    	finish();
    }
    
    /**
     * 品牌查询成功回调
     */
    public void districtAndBrandSuccess(DistrictAndBrand districtAndBrand){
    	if(districtAndBrand != null){
    		Map<String, String> brandMap = districtAndBrand.getBrandList();
    		if(brandMap != null && brandMap.size() > 0){
    			brandKeyArray = new String[brandMap.size()];
    			brandValueArray = new String[brandMap.size()];
    			Iterator<Entry<String,String>> iterator = brandMap.entrySet().iterator();
    			int index = 0;
    			while(iterator.hasNext()){
    				Entry<String,String> entry = iterator.next();
    				brandKeyArray[index] = entry.getKey();
    				brandValueArray[index] = entry.getValue();
    				++index;
    			}
    		}
    	}
    }
	
    /**
     * 获取验证码请求成功
     * @param content
     */
    public void requestSuccess() {
        btnAuthCode.timer.start();
    }
}
