package com.abc.sz.app.adapter.product;

import java.util.List;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.product.AttributeValueBean;
import com.forms.library.tools.ViewHolder;

public class AttributeValueAdapter extends BaseAdapter {
    //属性值数据
    List<AttributeValueBean> mList;

    public AttributeValueAdapter(List<AttributeValueBean> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AttributeValueBean bean = mList.get(position);
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_product_attribute_value, null);
        }

//        RelativeLayout rl_bg = ViewHolder.get(convertView, R.id.rl_bg);
        TextView tv_value = ViewHolder.get(convertView, R.id.tv_value);
        tv_value.setText(bean.getValue());

        if (bean.isChecked()) {
            //判断是否为选中
//            rl_bg.setBackgroundResource(R.drawable.shape_attribute_bg_checked);
            tv_value.setTextColor(parent.getContext().getResources().getColor(R.color.colorPrimaryDark));
        } else {
//            rl_bg.setBackgroundResource(R.drawable.shape_attribute_bg);
            tv_value.setTextColor(parent.getContext().getResources().getColor(R.color.textGray));
        }
        return convertView;
    }

    public void setmList(List<AttributeValueBean> mList) {
        this.mList = mList;
    }


}
