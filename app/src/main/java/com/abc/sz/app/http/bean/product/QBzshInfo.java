package com.abc.sz.app.http.bean.product;

/**
 * 请求包装售后
 *
 * @author llc
 */
public class QBzshInfo {
    //商品id
    private String ProductId;

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }
}
