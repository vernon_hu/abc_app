package com.abc.sz.app.bean.lightpay;

import java.util.List;

public class RBindCardInfo{

	private String result_code;
    private List<BindCardInfo> cardinfo;
    
	public String getResult_code() {
		return result_code;
	}
	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}
	public List<BindCardInfo> getCardinfo() {
		return cardinfo;
	}
	public void setCardinfo(List<BindCardInfo> cardinfo) {
		this.cardinfo = cardinfo;
	}
	
}
