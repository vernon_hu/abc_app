package com.abc.sz.app.view.drawer;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.forms.library.baseUtil.log.LogUtils;
import com.forms.library.baseUtil.view.ViewUtils;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * Created by hwt on 4/17/15.
 */
public class MenuItemView extends LinearLayout {
    @ViewInject(R.id.menuIcon) ImageView mItemIcon;
    @ViewInject(R.id.menuText) TextView mItemText;

    private Class<?> targetClass;
    private int mIndex;
    private int mResult;

    public MenuItemView(Context context) {
        super(context);
    }

    public MenuItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_menuitem, this);
        ViewUtils.inject(this, view);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.menuItemStyle);
        Drawable drawable = typedArray.getDrawable(R.styleable.menuItemStyle_itemIcon);
        if (drawable != null) {
            mItemIcon.setImageDrawable(drawable);
        }
        String text = typedArray.getString(R.styleable.menuItemStyle_itemText);
        if (!TextUtils.isEmpty(text)) {
            mItemText.setText(text);
        }
        mIndex = typedArray.getInteger(R.styleable.menuItemStyle_itemIndex,0);
        mResult = typedArray.getInteger(R.styleable.menuItemStyle_itemResult,-1);
        String classStr = typedArray.getString(R.styleable.menuItemStyle_itemTargetClass);
        if (!TextUtils.isEmpty(classStr)) {
            try {
                targetClass = Class.forName(classStr);
            } catch (ClassNotFoundException e) {
                LogUtils.d(e.getMessage(), e);
            }
        }
        typedArray.recycle();

        if(targetClass!=null) {
            setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(),targetClass);
                    if(mResult<0) {
                        getContext().startActivity(intent);
                    }
                }
            });
        }
    }

    public Class<?> getTargetClass() {
        return targetClass;
    }

    public void setTargetClass(Class<?> targetClass) {
        this.targetClass = targetClass;
    }

    public int getmIndex() {
        return mIndex;
    }

    public void setmIndex(int mIndex) {
        this.mIndex = mIndex;
    }


}
