package com.abc.sz.app.activity_phase2.lightpay;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.PayAction;
import com.abc.sz.app.action_phase2.LightpayAction;
import com.abc.sz.app.http.bean.pay.AESBean;
import com.abc.sz.app.view.CountDownButton;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.log.LogUtils;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.encrypt.AESUtils;
import com.forms.library.tools.encrypt.DESUtils;
import com.forms.view.toast.MyToast;

/**
 * Created by hwt on 4/21/15.
 */
@ContentView(R.layout.activity_light_bindcard)
public class LightBindCardActivity extends ABCActivity {

    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.userName) EditText userName;
//    @ViewInject(R.id.idType) TextView idType;
    @ViewInject(R.id.idNum) EditText idNum;
    @ViewInject(R.id.cardNum) EditText cardNum;
    @ViewInject(R.id.phoneNo) EditText phoneNo;
    @ViewInject(R.id.messageCode) EditText messageCode;
    @ViewInject(R.id.btnAuthCode) CountDownButton btnAuthCode;
    @ViewInject(R.id.agreementCheck) CheckBox agreementCheck;
    
    private LightpayAction lightpayAction;
    //支付请求
    private PayAction payAction;
    //加密因子
    private String seed = "";
    private String photonID;
//    private String[] certificateKey;
//    private String[] certificateValue;
//    private String certificate;
//    //系统跟踪号
//    private String follow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        
        init();
    }
    
    private void init() {
    	Bundle bundle = getIntent().getExtras();
    	if(bundle != null){
    		photonID = bundle.getString("photonID");
    	}
//    	certificateKey = getResources().getStringArray(R.array.certificate_key);
//    	certificateValue = getResources().getStringArray(R.array.certificate_value);
    	lightpayAction = (LightpayAction) controller.getAction(this, LightpayAction.class);
    	payAction = (PayAction) controller.getAction(this, PayAction.class);
        payAction.getAesKey().start();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_lightbindcard_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.bindCard){
        	if(agreementCheck.isChecked()){
        		 if (checknNewCard()) {
                 	if(TextUtils.isEmpty(messageCode.getText().toString())){
                 		FormsUtil.setErrorHtmlTxt(this, messageCode, R.string.input_authcode_isempty);
                 	}else{
                 		String cardNum = this.cardNum.getText().toString(); 
    		        	String phone = phoneNo.getText().toString();  
    		        	String checkCode = messageCode.getText().toString();
    		        	try {
    						cardNum = AESUtils.encryptMyBase64(seed, cardNum);
    					} catch (Exception e) {
    						e.printStackTrace();
    					}
    		        	lightpayAction.CreditCardCheckSMSAct(cardNum, phone, checkCode, photonID).start();
                 	}
        		 }
        	}else{
        		MyToast.showTEXT(LightBindCardActivity.this, "请同意并使用光子支付协议");
        	}
        }
        return super.onOptionsItemSelected(item);
    }
    
    @OnClick({R.id.idType, R.id.btnAuthCode})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.idType:
//            	DialogUtil.showToSelect(this, null, certificateValue, new OnClickListener() {
//					
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						FormsUtil.setTextViewTxt(idType, certificateValue[which]);
//						certificate = certificateKey[which];
//					}
//				});
            	break;
            case R.id.btnAuthCode:
            	if(checknNewCard()){
	            	String custName = userName.getText().toString();
	            	String idNumber = idNum.getText().toString();
	            	String cardNum = this.cardNum.getText().toString();
	            	String phone = phoneNo.getText().toString();
					try {
						cardNum = AESUtils.encryptMyBase64(seed, cardNum);
					} catch (Exception e) {
						e.printStackTrace();
					}
	            	lightpayAction.GZDJKBindCard(custName, idNumber, cardNum, phone).start();
            	}
            	break;
        }
        
    }
    
    /**
     * 检查输入项
     *
     * @return
     */
    private boolean checknNewCard() {
    	String name = this.userName.getText().toString();
    	String cardNum = this.cardNum.getText().toString();
    	String idNum = this.idNum.getText().toString();
    	String phoneNo = this.phoneNo.getText().toString();
    	if (TextUtils.isEmpty(name)) {
	         FormsUtil.setErrorHtmlTxt(this, this.userName, R.string.input_username_isempty);
	         return false;
	    }else if (TextUtils.isEmpty(idNum)) {
	    	FormsUtil.setErrorHtmlTxt(this, this.idNum, R.string.input_certNo_isempty);
            return false;
        } else if (TextUtils.isEmpty(cardNum)) {
            FormsUtil.setErrorHtmlTxt(this, this.cardNum, R.string.input_newCardNum_isEmpty);
            return false;
        } else if (cardNum.length() != 16) {
            FormsUtil.setErrorHtmlTxt(this, this.cardNum, R.string.input_nineteen_cardNum);
            return false;
        } else if (TextUtils.isEmpty(phoneNo)) {
	        FormsUtil.setErrorHtmlTxt(this, this.phoneNo, R.string.please_input_phone);
	        return false;
	    } else if (phoneNo.length() != 11) {
	        FormsUtil.setErrorHtmlTxt(this, this.phoneNo, R.string.input_eleven_phone);
	        return false;
	    } else if ("".equals(seed)) {
            payAction.getAesKey().start();
            return false;
        }
        return true;
    }
    
    /**
     * 查询加密因子成功
     * @param bean
     */
    public void querySuccess(AESBean bean) {
        String userId = getBaseApp().getLoginUser().getUserId();
        String desKey = "";
        if(userId.length() < 8){
        	desKey = userId + String.format("%1$0" + (8 - userId.length()) + "d", 0);
        }else{
        	desKey = userId;
        }
        if (bean != null) {
            try {
                seed = DESUtils.decryptBase64(desKey.getBytes(), desKey.getBytes(), bean.getKey());
                LogUtils.d("seed = " + seed);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
    
    /**
     * 身份验证发送成功
     * @param content
     */
    public void requestSuccess(String content) {
        btnAuthCode.timer.start();
//        follow = content;
    }
    
    /**
     * 绑卡成功
     */
    public void bindCardSuccess(){
    	finishForBack(RESULT_OK);
    }
}
