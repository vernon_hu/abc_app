package com.abc.sz.app.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.Advertisement;
import com.abc.sz.app.util.ImageViewUtil;
import com.forms.base.ABCWebViewActivity;
import com.forms.base.XDRImageLoader;
import com.forms.library.base.BaseActivity;

import java.util.List;

public class ImageAdapter extends BaseAdapter {

    private List<Advertisement> adImageList;
    private BaseActivity activity;
    private XDRImageLoader mImageLoader;

    public ImageAdapter(List<Advertisement> adImageList, BaseActivity activity,
                        XDRImageLoader mImageLoader) {
        this.adImageList = adImageList;
        this.activity = activity;
        this.mImageLoader = mImageLoader;
    }

    @Override
    public int getCount() {
        return adImageList.size();
    }

    @Override
    public Object getItem(int position) {
        return adImageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(activity).inflate(R.layout.layout_advertisement_item, null);
            viewHolder.ivAdImage = (ImageView) convertView.findViewById(R.id.ivAdImage);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final Advertisement adImage = adImageList.get(position);
        if (adImage != null) {
            mImageLoader.displayImage(adImage.getImgUrl(), viewHolder.ivAdImage, ImageViewUtil.getOption(), 1);
            viewHolder.ivAdImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    Bundle bundle = new Bundle();
                    bundle.putString(ABCWebViewActivity.TARGET_URL, adImage.getAttribute());
                    activity.callMe(ABCWebViewActivity.class, bundle);

                }
            });
        }
        return convertView;
    }

    static class ViewHolder {
        ImageView ivAdImage;
    }
}

