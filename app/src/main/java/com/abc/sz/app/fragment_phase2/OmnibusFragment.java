package com.abc.sz.app.fragment_phase2;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.OmnibusAction;
import com.abc.sz.app.adapter_phase2.OmnibusAdapter;
import com.abc.sz.app.bean_phase2.ImageAD;
import com.abc.sz.app.bean_phase2.Omnibus;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCFragment;
import com.forms.library.baseUtil.net.HttpTrans;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

/**
 * 达人精选
 *
 * @author hkj
 */
public class OmnibusFragment extends ABCFragment {

    @ViewInject(R.id.commodityView)
    RecyclerView recyclerView;
    @ViewInject(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @ViewInject(R.id.loadingView)
    LoadingView loadingView;

    private int pageNum = 0;
    private HttpTrans httpTrans;
    private OmnibusAction omnibusAction;
    private List<ImageAD> imageList = new ArrayList<>();
    private List<Omnibus> omnibusList = new ArrayList<>();
    private OmnibusAdapter omnibusAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_phase2_omnibus, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        
        imageList.add(new ImageAD("3", "", "", ""));

        omnibusAdapter = new OmnibusAdapter(baseActivity, baseActivity.getImageLoader(), imageList, omnibusList);
        recyclerView.setAdapter(omnibusAdapter);

        swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {

            @Override
            public void onRefresh() {
                request(true);
            }
        });

        omnibusAction = (OmnibusAction) getController().getAction(baseActivity, this, OmnibusAction.class);
        request(false);
    }

    private void request(boolean isRefresh) {
        httpTrans = controller.getTrans(baseActivity);
        httpTrans.addRequest(omnibusAction.loadingAdv());
        httpTrans.addRequest(omnibusAction.loadingOmnibusList(pageNum));
        if(isRefresh){
        	httpTrans.start(false);
        }else{
        	httpTrans.setLoadingView(loadingView);
            httpTrans.start();
        }
    }

    protected void initListener() {
//		recyclerView.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
//				// TODO 每日快报点击事件
//				JxBean jxBean = commodityList.get(position);
//				application.getCacheBean().putCache(jxBean);
//				baseActivity.callMe(KbInfoActivity.class);
//			}
//
//		});
    }

    /**
     * 广告请求成功回调
     */
    public void advSuccess(List<ImageAD> list) {
        swipeRefreshLayout.setRefreshing(false);
        if (list != null && list.size() > 0) {
            imageList.clear();
            imageList.addAll(list);
            omnibusAdapter.notifyItemChanged(0);
        }
    }

    /**
     * 达人精选成功回调
     *
     * @param list
     */
    public void omnibusSuccess(List<Omnibus> list) {
        swipeRefreshLayout.setRefreshing(false);
        if (list != null && list.size() > 0) {
            omnibusList.clear();
            omnibusList.addAll(list);
            omnibusAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 请求失败回调
     *
     * @param retCode
     */
    public void failed(RetCode retCode, String requestFrom) {
        swipeRefreshLayout.setRefreshing(false);
        if ("advSuccess".equals(retCode)) {

        } else if ("bulletinSuccess".equals(retCode)) {

        }
    }

}
