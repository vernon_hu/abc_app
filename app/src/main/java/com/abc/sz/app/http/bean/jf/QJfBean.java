package com.abc.sz.app.http.bean.jf;

/**
 * 缴费对象
 *
 * @author llc
 */
public class QJfBean {
    //单位编号
    private String companyNum;
    //缴款通知单编号
    private String noticeNum;

    public String getCompanyNum() {
        return companyNum;
    }

    public void setCompanyNum(String companyNum) {
        this.companyNum = companyNum;
    }

    public String getNoticeNum() {
        return noticeNum;
    }

    public void setNoticeNum(String noticeNum) {
        this.noticeNum = noticeNum;
    }


}
