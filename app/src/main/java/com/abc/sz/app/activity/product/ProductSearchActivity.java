package com.abc.sz.app.activity.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity_phase2.wdmap.WdSearchResultActivity;
import com.abc.sz.app.bean.SearchKeywords;
import com.abc.sz.app.bean.branch.HistoryTag;
import com.abc.sz.app.util.AutoCompleteFilterFormDbAdapter;
import com.abc.sz.app.util.AutoCompleteFilterObject;
import com.abc.sz.app.view.flowlayout.FlowLayout;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.db.sqlite.Selector;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

import java.util.List;
import java.util.TimeZone;

import hirondelle.date4j.DateTime;

/**
 * 商品搜索
 *
 * @author ftl
 */
@ContentView(R.layout.activity_product_search)
public class ProductSearchActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.flowLayout) FlowLayout flowLayout;
    @ViewInject(R.id.autoCompleteTextView) AutoCompleteTextView autoCompleteTextView;
    private List<SearchKeywords> searchKeyWordList;
    public final static String QUERYTEXT_TAG = "QUERYTEXT_TAG";
    private boolean isFromList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isFromList = bundle.getBoolean("isFromList");
        }
        //设置自动完成的数据源
        autoCompleteTextView.setAdapter(new AutoCompleteFilterFormDbAdapter(
                this,
                new SearchKeywords(),
                SearchKeywords.class,
                "keywordsName",
                "searchTime",
                R.layout.layout_product_search_auto
        ) {

            @Override
            public void viewHandler(int position, AutoCompleteFilterObject filterObject, View convertView) {
                TextView textView = ViewHolder.get(convertView, R.id.text1);
                FormsUtil.setTextViewTxts(textView, filterObject.getSearchWord());
            }
        });
        //选中提示项搜索
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                search();
            }
        });
        //键盘搜索按键搜索
        autoCompleteTextView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                    search();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        initViewData();
    }

    private void initViewData() {
        searchKeyWordList = getDBUtils().findAll(Selector.from(SearchKeywords.class).orderBy("searchTime").limit(30));
        if (searchKeyWordList != null) {
            flowLayout.removeAllViews();
            for (int i = 0; i < searchKeyWordList.size(); i++) {
                final TextView TextView = (TextView) LayoutInflater.from(this).inflate(R.layout.layout_history_tagview, null);
                ViewGroup.MarginLayoutParams layoutParams = new ViewGroup.MarginLayoutParams(
                        ViewGroup.MarginLayoutParams.WRAP_CONTENT,
                        ViewGroup.MarginLayoutParams.WRAP_CONTENT
                );
                layoutParams.topMargin = FormsUtil.dip2px(8);
                layoutParams.leftMargin = FormsUtil.dip2px(8);
                TextView.setLayoutParams(layoutParams);
                TextView.setText(searchKeyWordList.get(i).getKeywordsName());
                TextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextView textView = (android.widget.TextView) v;
                        if (!TextUtils.isEmpty(textView.getText())) {
                            Bundle bundle = new Bundle();
                            bundle.putString(ProductListActivity.SEARCH_KEY, TextView.getText().toString());
                            callMe(ProductListActivity.class, bundle);
                        } else {
                            Toast.makeText(ProductSearchActivity.this, "请输入查询条件", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                flowLayout.addView(TextView);
            }
        }
    }

    @OnClick(R.id.iv_search)
    public void onClick(View view) {
        search();
    }

    /**
     * 保存并传递搜索关键字
     */
    public void search() {
        String keyWords = autoCompleteTextView.getText().toString();
        if (!TextUtils.isEmpty(keyWords)) {
            SearchKeywords searchKeywords = new SearchKeywords();
            searchKeywords.setSearchTime(DateTime.now(TimeZone.getDefault()).format("YYYY-MM-DD hh:mm:ss"));
            searchKeywords.setKeywordsName(keyWords);
            getDBUtils().save(searchKeywords);

            Bundle bundle = new Bundle();
            bundle.putString(ProductListActivity.SEARCH_KEY, keyWords.trim());
            if (isFromList) {
                Intent intent = new Intent();
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
            } else {
                callMe(ProductListActivity.class, bundle);
            }
        } else {
            Toast.makeText(this, "请输入查询条件", Toast.LENGTH_SHORT).show();
        }
    }
}
