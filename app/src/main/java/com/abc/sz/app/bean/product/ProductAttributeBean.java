package com.abc.sz.app.bean.product;

import com.forms.library.base.BaseBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品属性
 *
 * @author llc
 */
public class ProductAttributeBean extends BaseBean {

    public ProductAttributeBean() {
        attributeList = new ArrayList<AttributeValueBean>();
    }

    //属性名称
    private String attributeName;
    //属性值
    private List<AttributeValueBean> attributeList;

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public List<AttributeValueBean> getAttributeList() {
        return attributeList;
    }

    public void setAttributeList(List<AttributeValueBean> attributeList) {
        this.attributeList = attributeList;
    }


}
