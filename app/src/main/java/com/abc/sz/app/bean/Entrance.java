package com.abc.sz.app.bean;

import com.forms.library.baseUtil.db.annotation.Column;
import com.forms.library.baseUtil.db.annotation.Id;
import com.forms.library.baseUtil.db.annotation.Table;

/**
 * 常用生活助手
 *
 * @author ftl
 */
@Table(name = "entrance")
public class Entrance {

    @Id(column = "id")
    private int id;
    /* Id */
    @Column(column = "entranceId")
    private int entranceId;
    /* 名称 */
    @Column(column = "entranceText")
    private String entranceText;
    /* 图标 */
    @Column(column = "entranceIcon")
    private String entranceIcon;

    public Entrance() {

    }

    public Entrance(int entranceId, String entranceText, String entranceIcon) {
        super();
        this.entranceId = entranceId;
        this.entranceText = entranceText;
        this.entranceIcon = entranceIcon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEntranceId() {
        return entranceId;
    }

    public void setEntranceId(int entranceId) {
        this.entranceId = entranceId;
    }

    public String getEntranceText() {
        return entranceText;
    }

    public void setEntranceText(String entranceText) {
        this.entranceText = entranceText;
    }

    public String getEntranceIcon() {
        return entranceIcon;
    }

    public void setEntranceIcon(String entranceIcon) {
        this.entranceIcon = entranceIcon;
    }
}
