package com.abc.sz.app.activity_phase2.creditcard;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * 分期理财计算器结果页面
 * 
 * @author hkj
 */
@ContentView(R.layout.activity_phase2_cfpresult)
public class CFPResultActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	@ViewInject(R.id.tvTotalSum)
	private TextView tvTotalSum;
	@ViewInject(R.id.tvTotalPoundage)
	private TextView tvTotalPoundage;
	@ViewInject(R.id.tvPeriods)
	private TextView tvPeriods;
	@ViewInject(R.id.tvEveryRefund)
	private TextView tvEveryRefund;
	@ViewInject(R.id.tvEveryPoundage)
	private TextView tvEveryPoundage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		
	}
	
}
