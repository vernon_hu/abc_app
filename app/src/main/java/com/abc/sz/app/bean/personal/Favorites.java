package com.abc.sz.app.bean.personal;

/**
 * Created by hwt on 14/11/12.
 */
public class Favorites {
    private String favouriteId; // 收藏ID
    private String productId;// 商品ID
    private String productName;// 商品名称
    private Double hisPrice;// 历史价格
    private Double price;// 价格
    private String description;// 商品描述
    private String smallPicture;// 商品小图 URL
    private String flag;// 有效标志,0:无效，1：为有效

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getHisPrice() {
        return hisPrice;
    }

    public void setHisPrice(Double hisPrice) {
        this.hisPrice = hisPrice;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSmallPicture() {
        return smallPicture;
    }

    public void setSmallPicture(String smallPicture) {
        this.smallPicture = smallPicture;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getFavouriteId() {
        return favouriteId;
    }

    public void setFavouriteId(String favouriteId) {
        this.favouriteId = favouriteId;
    }
}
