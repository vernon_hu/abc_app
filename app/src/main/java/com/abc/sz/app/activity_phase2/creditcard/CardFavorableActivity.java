package com.abc.sz.app.activity_phase2.creditcard;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;

import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * 信用卡优惠页面
 * 
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_cardfavorable)
public class CardFavorableActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
//	@ViewInject(R.id.llContent)
//	private LinearLayout llContent;
//	@ViewInject(R.id.gridView)
//	private GridView gridView;
	@ViewInject(R.id.webView)
	private WebView webView;

//	private List<Integer> resIds = new ArrayList<Integer>() {
//		{
//			add(R.drawable.icon_card_favorable_ms); add(R.drawable.icon_card_favorable_dy);
//			add(R.drawable.icon_card_favorable_ds); add(R.drawable.icon_card_favorable_wl);
//			add(R.drawable.icon_card_favorable_swly); add(R.drawable.icon_card_favorable_gwc);
//			add(R.drawable.icon_card_favorable_qtyh); 
//		}
//	};
//
//	private List<String> functions = new ArrayList<String>() {
//		{
//			add("美食"); add("电影"); add("读书"); add("玩乐");
//			add("商务/旅游"); add("购物"); add("其他优惠"); 
//		}
//	};
//
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}
//
	private void init() {
//		List<CellView.CellViewBean> cellList = new ArrayList<>();
//		for (int i = 0; i < functions.size(); i++) {
//			cellList.add(new CellView.CellViewBean(resIds.get(i), functions.get(i)));
//		}
//
//		BaseAdapter<CellViewBean> adapter = new BaseAdapter<CellView.CellViewBean>(this, cellList,
//				R.layout.layout_phase2_creditcard_item) {
//			@Override
//			public void viewHandler(final int position, CellView.CellViewBean cellViewBean, View convertView) {
//				CellView cellView = ViewHolder.get(convertView, R.id.cellView);
//				cellView.reset(cellViewBean, gridView.getMeasuredWidth() / gridView.getNumColumns());
//				cellView.setOnClickListener(new OnClickListener() {
//					
//					@Override
//					public void onClick(View v) {
//						callMe(CardFavorableListActivity.class);
//						switch (position) {
//						case 0:
//							break;
//						case 1:
//							break;
//						case 2:
//							break;
//						case 3:
//							break;
//						case 4:
//							break;
//						case 5:
//							break;
//						case 6:
//							break;
//						}	
//					}
//				});
//			}
//		};
//		gridView.setAdapter(adapter);
//		
//		llContent.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				callMe(CardFavorableSearchActivity.class);
//			}
//		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			if(webView != null && webView.canGoBack()){
				webView.goBack();
				return false;
			}
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		if(webView != null && webView.canGoBack()){
			webView.goBack();
		}else{
			super.onBackPressed();
		}
	}


}
