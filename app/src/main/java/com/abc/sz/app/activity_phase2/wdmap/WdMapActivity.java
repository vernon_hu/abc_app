package com.abc.sz.app.activity_phase2.wdmap;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.adapter_phase2.BranchBankListAdapter;
import com.abc.sz.app.bean.branch.BranchBank;
import com.abc.sz.app.bean.branch.BranchSearchRests;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;

/**
 * 网点地图
 *
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_wdmap)
public class WdMapActivity extends ABCActivity {

    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.mapView) MapView mMapView;
    private BranchBank branchBank;
    private BaiduMap mBaiduMap;
    private Marker mMarker;
    private InfoWindow mInfoWindow;
    private TextView mOverlayTitle;
    private TextView mOverlayContent;
    private boolean isShowOverlay = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BranchSearchRests searchRests = getIntent().getParcelableExtra(BranchBankListAdapter.BRANCH_DATA);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            if ("1".equals(searchRests.getTypeId())) {
                branchBank = searchRests.getBranchBank();
            } else if ("2".equals(searchRests.getTypeId())) {
                branchBank = searchRests.getSelfServiceBank();
            } else if ("2".equals(searchRests.getTypeId())) {
                branchBank = searchRests.getSelfServiceBank();
            }
            getSupportActionBar().setTitle(branchBank.getName());
        }
        mMapView.showZoomControls(false);
        mMapView.showScaleControl(true);
        mBaiduMap = mMapView.getMap();
        mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
        mBaiduMap.setMyLocationEnabled(true);
//        mBaiduMap.setTrafficEnabled(true);

        //定义Maker坐标点
        LatLng point = new LatLng(branchBank.getLatitude(), branchBank.getLongitude());
        //构建Marker图标
        BitmapDescriptor bitmap = BitmapDescriptorFactory.fromResource(R.drawable.icon_gcoding);
        //构建MarkerOption，用于在地图上添加Marker
        OverlayOptions options = new MarkerOptions()
                .position(point)
                .icon(bitmap)
                .zIndex(15);  //设置marker所在层级;
        //在地图上添加Marker，并显示
        mBaiduMap.addOverlay(options);

        mBaiduMap.setOnMarkerClickListener(new BaiduMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (isShowOverlay) {
                    isShowOverlay = false;
                    mBaiduMap.hideInfoWindow();
                } else {
                    isShowOverlay = true;
                    FormsUtil.setTextViewTxts(mOverlayTitle, branchBank.getName());
                    FormsUtil.setTextViewTxts(mOverlayContent, branchBank.getFullAddress());
                    mBaiduMap.showInfoWindow(mInfoWindow);
                }
                return false;
            }
        });

        MapStatusUpdate mapStatusUpdate = MapStatusUpdateFactory.newLatLng(point);
        mBaiduMap.animateMapStatus(mapStatusUpdate);

        //创建InfoWindow展示的view
        View view = LayoutInflater.from(this).inflate(R.layout.layout_mappoint_overlay, null);
        mOverlayTitle = (TextView) view.findViewById(R.id.overlayTitle);
        mOverlayContent = (TextView) view.findViewById(R.id.overlayContent);
        //创建InfoWindow , 传入 view， 地理坐标， y 轴偏移量
        mInfoWindow = new InfoWindow(view, point, -47);
    }


    @Override
    protected void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        mMapView.onResume();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        // 退出时销毁定位
//		mLocClient.stop();
        // 关闭定位图层
//		mBaiduMap.setMyLocationEnabled(false);
        mMapView.onDestroy();
        // 回收 bitmap 资源
        super.onDestroy();
    }


}
