package com.abc.sz.app.http.bean.loan;

import java.util.List;

/**
 * Created by hwt on 4/30/15.
 */
public class QDepositLoanInfo {
    private String customerNum;//private String 必选	   	     客户号
    private List<String> depositAccouts;//private String 条件必选	关联存款账号，多条数据（开立/新增存在关联时必选）
    private List<String> loanAccounts;//private String 条件必选	关联贷款账号，多条数据（新增贷款账号关联时必选）
    private String payBackType;//private String 条件必选	 1：返还至存贷通账户 2：按关联账户存款余额比例返还
    
    public QDepositLoanInfo(){}

    public QDepositLoanInfo(String customerNum, List<String> depositAccouts,
			List<String> loanAccounts, String payBackType) {
		super();
		this.customerNum = customerNum;
		this.depositAccouts = depositAccouts;
		this.loanAccounts = loanAccounts;
		this.payBackType = payBackType;
	}

	public String getCustomerNum() {
        return customerNum;
    }

    public void setCustomerNum(String customerNum) {
        this.customerNum = customerNum;
    }

    public List<String> getDepositAccouts() {
        return depositAccouts;
    }

    public void setDepositAccouts(List<String> depositAccouts) {
        this.depositAccouts = depositAccouts;
    }

    public List<String> getLoanAccounts() {
        return loanAccounts;
    }

    public void setLoanAccounts(List<String> loanAccounts) {
        this.loanAccounts = loanAccounts;
    }

    public String getPayBackType() {
        return payBackType;
    }

    public void setPayBackType(String payBackType) {
        this.payBackType = payBackType;
    }
}
