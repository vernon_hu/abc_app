package com.abc.sz.app.activity_phase2.creditcard;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.CreditcardAction;
import com.abc.sz.app.bean.cridetcard.CardShop;
import com.abc.sz.app.util.ImageViewUtil;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;

/**
 * 信用卡优惠信息页面
 * 
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_cardfavorableinfo)
public class CardFavorableInfoActivity extends ABCActivity {

	@ViewInject(R.id.appBar) Toolbar toolbar;
	@ViewInject(R.id.ivImgUrl) ImageView ivImgUrl;
	@ViewInject(R.id.tvShopName) TextView tvShopName;
	@ViewInject(R.id.tvDistance) TextView tvDistance;
	@ViewInject(R.id.tvArea) TextView tvArea;
	@ViewInject(R.id.tvShopType) TextView tvShopType;
	@ViewInject(R.id.tvShopAddress) TextView tvShopAddress;
	@ViewInject(R.id.tvPhone) TextView tvPhone;
	@ViewInject(R.id.tvFavorable) TextView tvFavorable;
	
	private CreditcardAction creditcardAction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}
	
	private void init() {
		creditcardAction = (CreditcardAction) controller.getAction(this, CreditcardAction.class);
		String shopID = cacheBean.getStringCache("shopID");
		creditcardAction.queryYOUShopDetail(shopID).start();
	}
	
	/**
     * 信用卡优惠商户详情查询成功回调
     */
    public void querySuccess(CardShop cardShop) {
        if (cardShop != null) {
        	imageLoader.displayImage(cardShop.getShopIconUrl(), ivImgUrl, ImageViewUtil.getOption());
			FormsUtil.setTextViewTxt(tvShopName, cardShop.getShopName());
			FormsUtil.setTextViewTxt(tvDistance, cardShop.getShopDistance());
			FormsUtil.setTextViewTxt(tvArea, cardShop.getShopAddress());
			FormsUtil.setTextViewTxt(tvShopAddress, cardShop.getShopAddress());
			FormsUtil.setTextViewTxt(tvPhone, cardShop.getShopPhone());
			FormsUtil.setTextViewTxt(tvFavorable, cardShop.getShopPreferentialContent());
        }
    }
	
}
