package com.abc.sz.app.activity.order;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.abc.ABC_SZ_APP.R;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.logger.Logger;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * 订单确认支付
 *
 * @author ftl
 */
@ContentView(R.layout.activity_order_affirm_pay)
public class OrderAffirmPayActivity extends ABCActivity {
	
    @ViewInject(R.id.loadingBar) ProgressBar loadingBar;
    @ViewInject(R.id.webView) WebView mWebView;
    
    private Pattern pattern;
    //是否回首页
    private boolean isToHomePage;

    @SuppressLint("SetJavaScriptEnabled")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Bundle bundle = getIntent().getExtras();
    	if(bundle != null){
			String paymentyURL = bundle.getString("paymentyURL");
			isToHomePage = bundle.getBoolean("gotoHomePage", false);
			loadingBar.setMax(100);
			pattern = Pattern.compile("xiaodarenbackmainvc", Pattern.CASE_INSENSITIVE);
			
			WebSettings settings = mWebView.getSettings();
	    	settings.setJavaScriptEnabled(true);//可以运行javaScript
	        settings.setUseWideViewPort(true);
	        settings.setLoadWithOverviewMode(true);
	        settings.setDomStorageEnabled(true);
	        settings.setSupportZoom(false);//是否支持缩放
	        settings.setBuiltInZoomControls(false);//是否显示缩放按钮
	        settings.setAppCacheEnabled(true);
	        settings.setGeolocationEnabled(true);
//	        mWebView.setInitialScale(100);
	        mWebView.setWebViewClient(new WebViewClient() {
	            /**
	             * onPageStarted中启动一个计时器,到达设置时间后利用handle发送消息给activity执行超时后的动作.
	             * @param view
	             * @param url
	             * @param favicon
	             */
	            @Override
	            public void onPageStarted(final WebView view, String url, Bitmap favicon) {
	                super.onPageStarted(view, url, favicon);
	                Logger.d("+++++++++++++++++onPageStarted+++++++++++++");
	            }

	            /**
	             * onPageFinished指页面加载完成,完成后取消计时器
	             * @param view
	             * @param url
	             */
	            @Override
	            public void onPageFinished(WebView view, String url) {
	                super.onPageFinished(view, url);
	                Logger.d("+++++++++++++++onPageFinished+++++++++++++");
	            }

	            @Override
	            public void onReceivedError(WebView view, int errorCode,
	                                        String description, String failingUrl) {
	            	Logger.d("+++++++++++++++++++++++++onReceivedError+++++++++++++++++++++++++");
	            	 // 打开错误，隐藏进度条，并置零
	                loadingBar.setVisibility(View.GONE);
	                loadingBar.setProgress(0);
	            }

	            @Override
	            public boolean shouldOverrideUrlLoading(WebView view, String url) {
	                Logger.d("++++++++++++++++++++++shouldOverrideUrlLoading++++++++++++++++++++++");
	                loadingBar.setVisibility(View.VISIBLE);// 再次请求网页，显示进度条
	                Matcher matcher = pattern.matcher(url);
		            if (matcher.find()) {
		            	gotoHomePage();
		            	return true;
		            }
	                return super.shouldOverrideUrlLoading(view, url);
	            }

				@Override
	            public void onReceivedSslError(WebView view,
	            		SslErrorHandler handler, SslError error) {
					Logger.d("+++++++++++++++++++++++++onReceivedSslError+++++++++++++++++++++++++");
	            	handler.proceed();
	            }

				@Override
				public void onLoadResource(WebView view, String url) {
					super.onLoadResource(view, url);
					
				}

	        });

	        mWebView.setWebChromeClient(new WebChromeClient() {
	            /**
	             * 页面加载进度
	             * @param view
	             * @param newProgress
	             */
	            @Override
	            public void onProgressChanged(WebView view, final int newProgress) {
	            	loadingBar.setProgress(newProgress);
	                if (100 == newProgress) {// 加载完成页面
	                    loadingBar.setVisibility(View.GONE);
	                    loadingBar.setProgress(0);
	                }
	            }

	            /**
	             *
	             * @param view
	             * @param url
	             * @param message
	             * @param result
	             * @return
	             */
	            @Override
	            public boolean onJsAlert(WebView view, String url, String message,
	                                     JsResult result) {
	                Logger.d("+++++++++++++++++++++++++onJsAlert+++++++++++++++++++++++++");
	                return super.onJsAlert(view, url, message, result);
	            }
	            
	        });


	        mWebView.setOnLongClickListener(new View.OnLongClickListener() {
	            /**
	             * 屏蔽掉长按事件 因为webview长按时将会调用系统的复制控件:
	             * @param v
	             * @return
	             */
	            @Override
	            public boolean onLongClick(View v) {
	                return true;
	            }
	        });
			if(paymentyURL != null && !"".equals(paymentyURL)){
				mWebView.loadUrl(paymentyURL);
			}
    	}
        
    }
    
    public void gotoHomePage(){
    	if(isToHomePage){
    		List<Activity> list = getActivityList();
	       	for(int i = 0; i < list.size(); i++){
	   			if(i != 0){
	   				list.get(i).finish();
	   			}
	   		}
    	}else{
    		finishForBack(RESULT_OK);
    	}
    }
    
    @Override
    public void onBackPressed() {
    	gotoHomePage();
    }
}
