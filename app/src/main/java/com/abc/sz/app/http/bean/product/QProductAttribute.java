package com.abc.sz.app.http.bean.product;

/**
 * 商品属性查询
 *
 * @author llc
 */
public class QProductAttribute {
    //商品类别id
    private String productId;
    //0私有  1共有
    private String type;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
