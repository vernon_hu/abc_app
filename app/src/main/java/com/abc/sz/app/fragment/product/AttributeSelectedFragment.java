package com.abc.sz.app.fragment.product;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.TextView;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity.product.ProductListActivity;
import com.abc.sz.app.adapter.product.AttributeExpandableAdapter;
import com.abc.sz.app.bean.product.AttributeValueBean;
import com.abc.sz.app.bean.product.TypeAttributeBean;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCFragment;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.widget.slidingmenu.SlidingMenu;
import com.forms.view.toast.MyToast;

/**
 * 筛选属性
 *
 * @author llc
 */
public class AttributeSelectedFragment extends ABCFragment {

    @ViewInject(R.id.lv_attribute) ExpandableListView lv_attribute;
    @ViewInject(R.id.loadingView) LoadingView loadingView;

    private SlidingMenu menu;
    //数据列表
    private List<TypeAttributeBean> typeAttributeList = new ArrayList<TypeAttributeBean>();
    //适配器
    private AttributeExpandableAdapter adapter;
    //查询属性
    private List<AttributeValueBean> queryAttributeList = new ArrayList<AttributeValueBean>();

    private ProductListActivity parentActivity;
    //是否选中筛选属性
    private boolean flag = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_attribute_select, container, false);
        return view;
    }

    @Override
    protected void initData() {
        // TODO Auto-generated method stub
        super.initData();

        parentActivity = (ProductListActivity) baseActivity;

        if (baseActivity instanceof ProductListActivity) {
            menu = ((ProductListActivity) baseActivity).getMenu();
        }
    }

    @Override
    protected void initView() {
        // TODO Auto-generated method stub
        super.initView();

        adapter = new AttributeExpandableAdapter(typeAttributeList);
        lv_attribute.setAdapter(adapter);
    }

    @Override
    protected void initListener() {
        // TODO Auto-generated method stub
        super.initListener();

        lv_attribute.setOnGroupExpandListener(new OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                // TODO Auto-generated method stub
                for (int i = 0; i < adapter.getGroupCount(); i++) {
                    if (i != groupPosition && lv_attribute.isGroupExpanded(i)) {
                        lv_attribute.collapseGroup(i);
                        return;
                    }
                }
            }
        });

        lv_attribute.setOnChildClickListener(new OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                flag = true;
                int position = groupPosition - parent.getFirstVisiblePosition();
                View view = parent.getChildAt(position);
                TextView tv_value = (TextView) view.findViewById(R.id.tv_value);
                tv_value.setText(((TextView) v.findViewById(R.id.tv_name)).getText());
                parent.collapseGroup(groupPosition);
                AttributeValueBean bean = queryAttributeList.get(groupPosition);
                bean.setValue(typeAttributeList.get(groupPosition).getList().get(childPosition).getKey());
                return false;
            }
        });
    }


    /**
     * 点击事件
     *
     * @param view
     */
    @OnClick(value = {R.id.bt_cancle, R.id.bt_sure})
    public void btnOnClick(View view) {
        switch (view.getId()) {
            case R.id.bt_sure:
                if (flag) {
                	parentActivity.setAttributeList(queryAttributeList);
                	parentActivity.onRefresh();
                } else {
                    MyToast.show(baseActivity, "请选择一项筛选值", MyToast.TEXT, Gravity.CENTER, Toast.LENGTH_SHORT);
                }
                break;
            case R.id.bt_cancle:
                break;
        }
        menu.showContent();
    }

    public List<TypeAttributeBean> getTypeAttributeList() {
        return typeAttributeList;
    }

    public void setTypeAttributeList(List<TypeAttributeBean> typeAttributeList) {
    	this.typeAttributeList.clear();
    	this.queryAttributeList.clear();
    	if(typeAttributeList != null){
	        this.typeAttributeList.addAll(typeAttributeList);
	        for (int i = 0; i < this.typeAttributeList.size(); i++) {
	            AttributeValueBean bean = new AttributeValueBean();
	            bean.setKey(typeAttributeList.get(i).getAttributeId());
	            queryAttributeList.add(bean);
	        }
	    	adapter = new AttributeExpandableAdapter(typeAttributeList);
	        lv_attribute.setAdapter(adapter);
    	}else{
    		loadingView.setVisibility(View.VISIBLE);
    		loadingView.noData(getString(R.string.loadingNoData));
    	}
    }

    public LoadingView getLoadingView() {
        return loadingView;
    }

}
