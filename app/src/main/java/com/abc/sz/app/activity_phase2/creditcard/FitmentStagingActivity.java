package com.abc.sz.app.activity_phase2.creditcard;

import java.util.ArrayList;
import java.util.List;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.CreditcardAction;
import com.abc.sz.app.adapter_phase2.ImageAdapter;
import com.abc.sz.app.bean_phase2.ImageAD;
import com.abc.sz.app.util.DialogUtil;
import com.abc.sz.app.view.CountDownButton;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.view.toast.MyToast;
import com.forms.view.viewflow.CircleFlowIndicator;
import com.forms.view.viewflow.ViewFlow;

/**
 * 家装分期页面
 * 
 * @author hkj
 */
@ContentView(R.layout.activity_phase2_fitmentstaging)
public class FitmentStagingActivity extends ABCActivity {

	@ViewInject(R.id.appBar) Toolbar toolbar;
	@ViewInject(R.id.viewFlow) ViewFlow viewFlow;
	@ViewInject(R.id.viewflowindic) CircleFlowIndicator viewflowindic;
	@ViewInject(R.id.tvArea) TextView tvArea;
	@ViewInject(R.id.etProportion) EditText etProportion;
	@ViewInject(R.id.etPhone) EditText etPhone;
	@ViewInject(R.id.etMessageCode) EditText etMessageCode;
	@ViewInject(R.id.btnAuthCode) CountDownButton btnAuthCode;
	@ViewInject(R.id.cbContact) CheckBox cbContact;
	
	private CreditcardAction creditcardAction;
	private String agreedToCall = "0";
	private String[] areaArray;
	private List<ImageAD> mImageADs = new ArrayList<>();
	private ImageAdapter imageAdapter;
	private final int NEXTIMAGE = 1;
	private int imageADPosition = 0;
	
	private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == NEXTIMAGE) {
                if (imageADPosition == mImageADs.size() - 1) {
                    imageADPosition = 0;
                } else {
                    imageADPosition += 1;
                    viewFlow.setSelection(imageADPosition);
                    handler.sendEmptyMessageDelayed(NEXTIMAGE, 3000);
                }
            }
        }
    };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		areaArray = getResources().getStringArray(R.array.area_key);
		creditcardAction = (CreditcardAction) controller.getAction(this, CreditcardAction.class);
		creditcardAction.loadingAdv("0").start();
		ViewGroup.LayoutParams params = viewFlow.getLayoutParams();
        params.height = FormsUtil.SCREEN_WIDTH / 4;
        viewFlow.setLayoutParams(params);
        viewFlow.setFlowIndicator(viewflowindic);
        mImageADs.add(new ImageAD("3", "", "", ""));
        imageAdapter = new ImageAdapter(this, getImageLoader(), mImageADs);
        viewFlow.setAdapter(imageAdapter, 0);
        viewFlow.setOnViewSwitchListener(new ViewFlow.ViewSwitchListener() {
             @Override
             public void onSwitched(View view, int position) {
                 imageADPosition = viewFlow.getSelectedItemPosition();
             }
        });
        if(mImageADs != null && mImageADs.size() > 1) {
            handler.sendEmptyMessageDelayed(NEXTIMAGE, 3000);
        }
		cbContact.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					agreedToCall = "1";
				}else{
					agreedToCall = "0";
				}
			}
		});
	}

	@OnClick({R.id.tvArea, R.id.btnAuthCode, R.id.btnNext})
	public void onClick(View view) {
		String phone = etPhone.getText().toString();
		switch (view.getId()) {
			case R.id.tvArea:
				DialogUtil.showToSelect(this, null, areaArray, new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						FormsUtil.setTextViewTxt(tvArea, areaArray[which]);
					}
				});
				break;
			case R.id.btnAuthCode:
				if (TextUtils.isEmpty(phone)) {
			         FormsUtil.setErrorHtmlTxt(this, etPhone, R.string.please_input_phone);
			    } else if (phone.length() != 11) {
			         FormsUtil.setErrorHtmlTxt(this, etPhone, R.string.input_eleven_phone);
			    } else {
			    	 creditcardAction.getHomeSmsCheck(phone).start();
			    }
				break;
			case R.id.btnNext:
				String area = tvArea.getText().toString();
				String proportion = etProportion.getText().toString();
				String checkCode = etMessageCode.getText().toString();
				if (TextUtils.isEmpty(area)) {
			         MyToast.showTEXT(this, "请选择区域");
			    } else if (TextUtils.isEmpty(proportion)) {
			         FormsUtil.setErrorHtmlTxt(this, etProportion, R.string.input_decorationArea_isempty);
			    } else if (TextUtils.isEmpty(phone)) {
			         FormsUtil.setErrorHtmlTxt(this, etPhone, R.string.please_input_phone);
			    } else if (phone.length() != 11) {
			         FormsUtil.setErrorHtmlTxt(this, etPhone, R.string.input_eleven_phone);
			    } else if(TextUtils.isEmpty(checkCode)){
			    	 FormsUtil.setErrorHtmlTxt(this, etMessageCode, R.string.please_input_authCode);
			    } else {
					creditcardAction.queryHouseDecorationLoanCommitment(area, proportion, phone, checkCode, agreedToCall).start();
			    }
				break;
		}
	}
	
	/**
     * 家装分期贷款额度查询成功回调
     */
    public void querySuccess(String result) {
    	String decorationArea = etProportion.getText().toString();
    	int area = Integer.parseInt(decorationArea);
    	double money = area * 5000/10000;
    	if(money > 50){
    		money = 50;
    	}
    	Bundle bundle = new Bundle();
    	bundle.putDouble("money", money);
    	callMe(CreditLineActivity.class, bundle);
//    	finish();
    }
    
    public void advSuccess(List<ImageAD> list){
    	if(list != null && list.size() > 0){
    		mImageADs.clear();
    		mImageADs.addAll(list);
    		imageAdapter.notifyDataSetChanged();
    	}
    }
    
    /**
     * 获取验证码请求成功
     * @param content
     */
    public void requestSuccess() {
        btnAuthCode.timer.start();
    }
	
}
