package com.abc.sz.app.activity.personal;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity.order.EntityOrderActivity;
import com.abc.sz.app.activity.order.MovieOrderActivity;
import com.abc.sz.app.activity.order.PrepaidOrderActivity;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;

/**
 * 我的订单
 *
 * @author ftl
 */
@ContentView(R.layout.activity_my_order)
public class MyOrderActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @OnClick(value = {R.id.ll_entity_order,
            R.id.ll_ticket_order,
            R.id.ll_movie,
            R.id.ll_prepaid_phone})
    public void onClick(View view) {
        Bundle bundle = new Bundle();
        switch (view.getId()) {
            case R.id.ll_entity_order:
                bundle.putInt("fromPage", 1);
                callMe(EntityOrderActivity.class, bundle);
                break;
            case R.id.ll_ticket_order:
                bundle.putInt("fromPage", 2);
                callMe(EntityOrderActivity.class, bundle);
                break;
            case R.id.ll_movie:
                callMe(MovieOrderActivity.class);
                break;
            case R.id.ll_prepaid_phone:
                callMe(PrepaidOrderActivity.class);
                break;
        }
    }

}
