package com.abc.sz.app.activity_phase2.depositloan;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.DepositloanAction;
import com.abc.sz.app.bean.loan.Account;
import com.abc.sz.app.bean.loan.DepositLoanInfo;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;
import com.forms.view.noscoller.MyListView;

/**
 * 关联贷款账户确认页面
 * 
 * @author hkj
 */
@ContentView(R.layout.activity_phase2_depositloanconfirm)
public class DepositLoanConfirmActivity extends ABCActivity {

	@ViewInject(R.id.appBar)
	private Toolbar toolbar;
	@ViewInject(R.id.tvLoanAccount)
	private TextView tvLoanAccount;
	@ViewInject(R.id.tvRepayAccount)
	private TextView tvRepayAccount;
	@ViewInject(R.id.mlvDetail)
	private MyListView mlvDeposit;
	@ViewInject(R.id.mlvDetail)
	private MyListView mlvLoan;
	@ViewInject(R.id.tvIncomeCycle)
	private TextView tvIncomeCycle;
	@ViewInject(R.id.tvIncomeType)
	private TextView tvIncomeType;
	@ViewInject(R.id.tvManagementExpense)
	private TextView tvManagementExpense;
	@ViewInject(R.id.loadingView)
	private LoadingView loadingView;
	
	private BaseAdapter<Account> depositAdapter;
	private BaseAdapter<Account> loanAdapter;
	private List<Account> depositList = new ArrayList<Account>();
	private List<Account> loanList = new ArrayList<Account>();
	private DepositloanAction depositloanAction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		depositloanAction = (DepositloanAction) controller.getAction(this, DepositloanAction.class);
		depositloanAction.queryCreateInfo("").setLoadingView(loadingView).start();
		depositAdapter = new BaseAdapter<Account>(this, depositList, R.layout.layout_phase2_account_item) {

			@Override
			public void viewHandler(final int position, Account t, View convertView) {
				TextView tvAccount = ViewHolder.get(convertView, R.id.tvAccount);
				if(t != null){
					FormsUtil.setTextViewTxt(tvAccount, t.getAccount());
				}
			}
		};
		loanAdapter = new BaseAdapter<Account>(this, loanList, R.layout.layout_phase2_account_item) {

			@Override
			public void viewHandler(final int position, Account t, View convertView) {
				TextView tvAccount = ViewHolder.get(convertView, R.id.tvAccount);
				if(t != null){
					FormsUtil.setTextViewTxt(tvAccount, t.getAccount());
				}
			}
		};
	}

	@OnClick({ R.id.btnConfirm })
	public void calculate(View v) {
		switch (v.getId()) {
		case R.id.btnConfirm:
			depositloanAction.affirmCreateComplete("").start();
			break;
		}
	}
	
	/**
	 * 开立信息反查成功回调
	 */
	public void querySuccess(DepositLoanInfo depositLoanInfo){
		if(depositLoanInfo != null){
			FormsUtil.setTextViewTxt(tvLoanAccount, depositLoanInfo.getLoanAccount());
			FormsUtil.setTextViewTxt(tvRepayAccount, depositLoanInfo.getDepositAccount());
			FormsUtil.setTextViewTxt(tvIncomeCycle, depositLoanInfo.getCycle());
			FormsUtil.setTextViewTxt(tvIncomeType, depositLoanInfo.getPayBackType());
			FormsUtil.setTextViewTxt(tvManagementExpense, depositLoanInfo.getManagerFee());
			depositList = depositLoanInfo.getDepositList();
			loanList = depositLoanInfo.getLoanList();
			depositAdapter.notifyDataSetChanged();
			loanAdapter.notifyDataSetChanged();
		}
	}
	
	/**
	 * 开立信息确认成功回调
	 */
	public void affirmSuccess(){
		
	}
	
}
