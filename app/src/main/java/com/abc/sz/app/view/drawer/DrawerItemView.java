package com.abc.sz.app.view.drawer;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.forms.library.baseUtil.logger.Logger;
import com.forms.library.baseUtil.view.ViewUtils;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * Created by hwt on 4/16/15.
 */
public class DrawerItemView extends LinearLayout {
    @ViewInject(R.id.itemIcon)
    ImageView mItemIcon;
    @ViewInject(R.id.itemText)
    TextView mItemText;
    @ViewInject(R.id.drawerItem)
    LinearLayout mDrawerItem;

    private boolean isClickedable = false;
    private boolean isSelected = false;
    private int mIndex;
    private Class<?> targetClass;
    private boolean isNeedLogin = false;

    public DrawerItemView(Context context) {
        super(context);
    }

    public DrawerItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_phase2_draweritem, this);
        ViewUtils.inject(this, view);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.menuItemStyle);
        Drawable drawable = typedArray.getDrawable(R.styleable.menuItemStyle_itemIcon);
        if (drawable != null) {
            mItemIcon.setImageDrawable(drawable);
        }
        String text = typedArray.getString(R.styleable.menuItemStyle_itemText);
        if (!TextUtils.isEmpty(text)) {
            mItemText.setText(text);
        }
        String classStr = typedArray.getString(R.styleable.menuItemStyle_itemTargetClass);
        if (!TextUtils.isEmpty(classStr)) {
            try {
                targetClass = Class.forName(classStr);
            } catch (ClassNotFoundException e) {
                Logger.e(e, e.getMessage());
            }
        }
        isNeedLogin = typedArray.getBoolean(R.styleable.menuItemStyle_isNeedLogin, false);
        typedArray.recycle();
    }

    public boolean isClickedable() {
        return isClickedable;
    }

    public void setIsClickedable(boolean isClickedable) {
        this.isClickedable = isClickedable;
    }

    @Override
    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public ImageView getmItemIcon() {
        return mItemIcon;
    }

    public void setmItemIcon(ImageView mItemIcon) {
        this.mItemIcon = mItemIcon;
    }

    public TextView getmItemText() {
        return mItemText;
    }

    public void setmItemText(TextView mItemText) {
        this.mItemText = mItemText;
    }

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
    }

    public Class<?> getTargetClass() {
        return targetClass;
    }

    public void setTargetClass(Class<?> targetClass) {
        this.targetClass = targetClass;
    }

    public boolean isNeedLogin() {
        return isNeedLogin;
    }

    public DrawerItemView setIsNeedLogin(boolean isNeedLogin) {
        this.isNeedLogin = isNeedLogin;
        return this;
    }
}
