package com.abc.sz.app.http.bean.product;

/**
 * 商品类别
 *
 * @author user
 */
public class QProductType {
    //类别等级
    private String Grade;
    //类别ID
    private String TypeId;

    public String getGrade() {
        return Grade;
    }

    public void setGrade(String grade) {
        Grade = grade;
    }

    public String getTypeId() {
        return TypeId;
    }

    public void setTypeId(String typeId) {
        TypeId = typeId;
    }


}
