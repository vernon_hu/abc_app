package com.abc.sz.app.bean_phase2;

/**
 * 达人精选对象
 *
 * @author hkj
 */
public class Omnibus {

    // 快报id
    private String DailyId;
    // 商品id
    private String ProductId;
    // 商品名称
    private String ProductName;
    // 历史价格
    private String HisPrice;
    // 商品价格
    private String ProductPrice;
    // 商品类别: 1、积分折扣，2特价活动
    private String ProductType;
    // 收藏数
    private String FavoriteNum;
    // 图片URL
    private String ProductPic;
    // 推荐原因
    private String ReasonContent;
    // 优惠条件
    private String Condition;
    // 商家名称
    private String SellerName;
    // 商品链接url
    private String Url;

    public String getDailyId() {
        return DailyId;
    }

    public void setDailyId(String dailyId) {
        DailyId = dailyId;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getHisPrice() {
        return HisPrice;
    }

    public void setHisPrice(String hisPrice) {
        HisPrice = hisPrice;
    }

    public String getProductPrice() {
        return ProductPrice;
    }

    public void setProductPrice(String productPrice) {
        ProductPrice = productPrice;
    }

    public String getProductType() {
        return ProductType;
    }

    public void setProductType(String productType) {
        ProductType = productType;
    }

    public String getFavoriteNum() {
        return FavoriteNum;
    }

    public void setFavoriteNum(String favoriteNum) {
        FavoriteNum = favoriteNum;
    }

    public String getProductPic() {
        return ProductPic;
    }

    public void setProductPic(String productPic) {
        ProductPic = productPic;
    }

    public String getReasonContent() {
        return ReasonContent;
    }

    public void setReasonContent(String reasonContent) {
        ReasonContent = reasonContent;
    }

    public String getCondition() {
        return Condition;
    }

    public void setCondition(String condition) {
        Condition = condition;
    }

    public String getSellerName() {
        return SellerName;
    }

    public void setSellerName(String sellerName) {
        SellerName = sellerName;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

}
