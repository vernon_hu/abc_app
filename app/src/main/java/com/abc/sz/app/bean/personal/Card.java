package com.abc.sz.app.bean.personal;

/**
 * Created by hwt on 14/11/11.
 * 卡片信息
 */
public class Card {
    private String cardName; //卡片名字
    private String cardNum;  //卡片数据
    private String logoUrl;  //卡片 logo
    private int logoResourceId;//

    public int getLogoResourceId() {
        return logoResourceId;
    }

    public void setLogoResourceId(int logoResourceId) {
        this.logoResourceId = logoResourceId;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }
}
