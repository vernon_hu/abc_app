package com.abc.sz.app.activity.personal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.UserAction;
import com.abc.sz.app.util.DialogUtil;
import com.forms.base.ABCActivity;
import com.forms.base.BaseConfig;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.baseUtil.view.annotation.event.OnFocusChange;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.encrypt.MD5;
import com.forms.view.crouton.Crouton;
import com.forms.view.crouton.Style;

/**
 * Created by hwt on 14/11/17.
 * 登录页
 */
@ContentView(R.layout.activity_login)
public class LoginActivity extends ABCActivity {
    @ViewInject(R.id.username) EditText username;
    @ViewInject(R.id.password) EditText password;
    @ViewInject(R.id.appBar) Toolbar toolbar;

    private UserAction userAction;
    private boolean loginTimeOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        userAction = (UserAction) controller.getAction(this, UserAction.class);

        initData();
        initListener();
    }

    public void initData() {
    	SharedPreferences sharePref = getSharedPreferences(BaseConfig.LOGIN_INFO, Context.MODE_PRIVATE);
    	String phone = sharePref.getString(BaseConfig.LOGIN_USER_PHONE, "");
    	if(!"".equals(phone)){
    		username.setText(phone);
    	}
    	
        loginTimeOut = getIntent().getBooleanExtra("loginTimeOut", false);
        if (loginTimeOut) {
        	if(getBaseApp().getLoginUser() != null){
        		 username.setText(getBaseApp().getLoginUser().getPhone());
                 username.setEnabled(false);
                 getBaseApp().logout();
        	}
        }
    }

    public void initListener() {
        if (loginTimeOut) {
            setOnFinishingListener(new OnFinishingListener() {

                @Override
                public boolean onFinishing() {
                    //getActivityManager().finishAllWithoutMain();
                    return false;
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_register_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.register) {
            callMe(RegisterActivity.class);
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(value = {R.id.loginBtn, R.id.forgetPswd})
    public void loginBtnClick(View view) {
        switch (view.getId()) {
            case R.id.loginBtn:
                if (checkInfo()) {
                        /*登录*/
                    userAction.login(username.getText().toString(),
                    		MD5.encrypt(MD5.encrypt(password.getText().toString()))).start();
                }
                break;
            case R.id.forgetPswd:
                callMe(ResetPasswordActivity.class);
                break;
        }
    }

    /**
     * 输入完成时检验输入内容
     *
     * @param v
     * @param hasFocus
     */
    @OnFocusChange(value = {R.id.username, R.id.password})
    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
            EditText textView = (EditText) v;
            if (TextUtils.isEmpty(textView.getText())) {
                FormsUtil.setErrorHtmlTxt(this, textView, R.string.input_username_isempty);
            }
        }
    }

    /**
     * 检查输入信息
     *
     * @return
     */
    private boolean checkInfo() {
        if (TextUtils.isEmpty(username.getText())) {
            FormsUtil.setErrorHtmlTxt(this, username, R.string.input_username_isempty);
            return false;
        } else if (password.getText().length() < 6) {
            FormsUtil.setErrorHtmlTxt(this, password, R.string.input_six_password);
            return false;
        } 
        return true;
    }

    /**
     * 登录成功
     */
    public void loginSuccess() {
        Crouton.showText(this, R.string.login_success_alert, Style.ALERT);
        setResult(RESULT_OK);
        finish();
    }

    /**
     * 登录失败
     *
     * @param retCode 失败返回错误信息
     */
    public void loginFailed(RetCode retCode) {
        DialogUtil.showWithNoTitile(this, retCode.getRetMsg());
    }

}
