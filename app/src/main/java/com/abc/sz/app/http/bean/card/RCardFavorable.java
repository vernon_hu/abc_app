package com.abc.sz.app.http.bean.card;

/**
 * 信用卡特优惠
 * 
 * @author ftl
 */
public class RCardFavorable {

    private String preferentialID;//You特权ID
    private String preferentialName;//You特权名称
    private String preferentialDate;//You特权有效期
    private String preferentialNumber;//You特权次数
    private String preferentialDetail;//YOU特权详情（活动内容）
    
	public String getPreferentialID() {
		return preferentialID;
	}
	public void setPreferentialID(String preferentialID) {
		this.preferentialID = preferentialID;
	}
	public String getPreferentialName() {
		return preferentialName;
	}
	public void setPreferentialName(String preferentialName) {
		this.preferentialName = preferentialName;
	}
	public String getPreferentialDate() {
		return preferentialDate;
	}
	public void setPreferentialDate(String preferentialDate) {
		this.preferentialDate = preferentialDate;
	}
	public String getPreferentialNumber() {
		return preferentialNumber;
	}
	public void setPreferentialNumber(String preferentialNumber) {
		this.preferentialNumber = preferentialNumber;
	}
	public String getPreferentialDetail() {
		return preferentialDetail;
	}
	public void setPreferentialDetail(String preferentialDetail) {
		this.preferentialDetail = preferentialDetail;
	}
	
    
}
