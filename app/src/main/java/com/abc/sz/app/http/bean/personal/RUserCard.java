package com.abc.sz.app.http.bean.personal;


/**
 * Created by hwt on 14/11/18.
 */
public class RUserCard {
    private String cardId;//卡id
    private String userId;//用户 ID
    private String cardNum;//卡号码
    private String cardType;//卡类型
    private String isDefault;//是否是默认卡,1表示是，0表示否
    private String state;//是否绑定状态，1表示绑定，0表示不绑定

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
