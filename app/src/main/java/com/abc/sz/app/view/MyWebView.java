package com.abc.sz.app.view;

import java.util.Random;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.forms.base.ABCActivity;
import com.forms.base.LoginUser;
import com.forms.library.baseUtil.logger.Logger;
import com.forms.library.tools.encrypt.AESUtils;
import com.forms.library.tools.encrypt.MD5;
/**
 * webview公共处理
 * @author ftl
 *
 */
public class MyWebView{
	
	private ABCActivity activity;
	private WebView mWebView;
	private ProgressBar loadingBar;
	
	public MyWebView(ABCActivity activity, WebView mWebView, ProgressBar loadingBar) {
		this.activity = activity;
		this.mWebView = mWebView;
		this.loadingBar = loadingBar;
	}

	@SuppressLint("SetJavaScriptEnabled")
	public void initWebViewConfig(){		
		WebSettings settings = mWebView.getSettings();
    	settings.setJavaScriptEnabled(true);//可以运行javaScript
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setDomStorageEnabled(true);
        settings.setSupportZoom(false);//是否支持缩放
        settings.setBuiltInZoomControls(false);//是否显示缩放按钮
        settings.setAppCacheEnabled(true);
        settings.setGeolocationEnabled(true);
//        mWebView.setInitialScale(100);
        mWebView.setWebViewClient(new WebViewClient() {
            /**
             * onPageStarted中启动一个计时器,到达设置时间后利用handle发送消息给activity执行超时后的动作.
             * @param view
             * @param url
             * @param favicon
             */
            @Override
            public void onPageStarted(final WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Logger.d("+++++++++++++++++onPageStarted+++++++++++++");
            }

            /**
             * onPageFinished指页面加载完成,完成后取消计时器
             * @param view
             * @param url
             */
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Logger.d("+++++++++++++++onPageFinished+++++++++++++");
            }

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
            	Logger.d("+++++++++++++++++++++++++onReceivedError+++++++++++++++++++++++++");
            	 // 打开错误，隐藏进度条，并置零
                loadingBar.setVisibility(View.GONE);
                loadingBar.setProgress(0);
            }

            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
	            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
		            Logger.d("+++++++++++++++++++++++++shouldInterceptRequest LOLLIPOP+++++++++++++++++++++++++");
		            Logger.d("URL = " + request.getUrl().toString());
	            }

            	return super.shouldInterceptRequest(view, request);
            }
            
            @SuppressWarnings("deprecation")
			@Override
            public WebResourceResponse shouldInterceptRequest(WebView view,
            		String url) {
	            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
		            Logger.d("+++++++++++++++++++++++++shouldInterceptRequest+++++++++++++++++++++++++");
		            Logger.d("URL = " + url);
	            }
            	return super.shouldInterceptRequest(view, url);
            }
            
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Logger.d("++++++++++++++++++++++shouldOverrideUrlLoading++++++++++++++++++++++");
                loadingBar.setVisibility(View.VISIBLE);// 再次请求网页，显示进度条
                return super.shouldOverrideUrlLoading(view, url);
            }

			@Override
            public void onReceivedSslError(WebView view,
            		SslErrorHandler handler, SslError error) {
				Logger.d("+++++++++++++++++++++++++onReceivedSslError+++++++++++++++++++++++++");
            	handler.proceed();
            }

			@Override
			public void onLoadResource(WebView view, String url) {
				super.onLoadResource(view, url);
				
			}

        });

        mWebView.setWebChromeClient(new WebChromeClient() {
            /**
             * 页面加载进度
             * @param view
             * @param newProgress
             */
            @Override
            public void onProgressChanged(WebView view, final int newProgress) {
            	loadingBar.setProgress(newProgress);
                if (100 == newProgress) {// 加载完成页面
                    loadingBar.setVisibility(View.GONE);
                    loadingBar.setProgress(0);
                }
            }

            /**
             *
             * @param view
             * @param url
             * @param message
             * @param result
             * @return
             */
            @Override
            public boolean onJsAlert(WebView view, String url, String message,
                                     JsResult result) {
                Logger.d("+++++++++++++++++++++++++onJsAlert+++++++++++++++++++++++++");
                return super.onJsAlert(view, url, message, result);
            }
            
        });


        mWebView.setOnLongClickListener(new View.OnLongClickListener() {
            /**
             * 屏蔽掉长按事件 因为webview长按时将会调用系统的复制控件:
             * @param v
             * @return
             */
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
	}
	
	public String getUserAgent(){
		String userAgent = "";
		LoginUser loginUser = activity.getBaseApp().getLoginUser();
		if(loginUser != null){
			String random = getRandom();
			String userId = loginUser.getUserId();
			String length =  userId.length() < 10 ? "0" + userId.length() : String.valueOf(userId.length());
			userAgent = AESUtils.toHex(random + length + userId + MD5.encrypt(random + length + userId)) + "-SZABC-Android";
		}
		return userAgent;
	}
	
	public String getRandom(){
		String result = "";
		for (int i = 0; i < 5; i++) {
			Random ran = new Random();
			result += Integer.toHexString(ran.nextInt(16));
		}
		return result;
	}

}
