package com.abc.sz.app.activity.order;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.OrderAction;
import com.abc.sz.app.activity.product.ProductInfoActivity;
import com.abc.sz.app.adapter.order.EntityOrderProductAdapter;
import com.abc.sz.app.bean.Order;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.util.ShoppingCarCount;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;
import com.forms.view.toast.MyToast;

/**
 * 订单支付列表
 *
 * @author ftl
 */
@ContentView(R.layout.activity_order_pay_list)
public class OrderPayListActivity extends ABCActivity {

    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.lv_order) ListView lv_order;
    @ViewInject(R.id.loadingView) LoadingView loadingView;

    private List<Order> list = new ArrayList<Order>();
    private BaseAdapter<Order> adapter;
    private OrderAction orderAction;
    private String address = "";
    private int index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        init();
    }

    @SuppressWarnings("unchecked")
    public void init() {
        orderAction = (OrderAction) controller.getAction(this, OrderAction.class);
        final Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            address = bundle.getString("address");
            list = (List<Order>) bundle.get("orderList");
        }
        lv_order.setAdapter(adapter = new BaseAdapter<Order>(
                this,
                list,
                R.layout.layout_entity_order_item
        ) {
            @Override
            public void viewHandler(final int position, Order t, View convertView) {
                final Order order = list.get(position);
                ListView lvProduct = ViewHolder.get(convertView, R.id.lv_order);
                final List<Product> productList = order.getProductList();
                EntityOrderProductAdapter adapter = new EntityOrderProductAdapter(productList, getImageLoader());
                lvProduct.setAdapter(adapter);
                TextView tvSellerName = ViewHolder.get(convertView, R.id.tv_sellerName);
                final TextView tvPrice = ViewHolder.get(convertView, R.id.tv_price);
                TextView tvOperate = ViewHolder.get(convertView, R.id.tv_operate);
                TextView tvState = ViewHolder.get(convertView, R.id.tv_state);

                FormsUtil.setTextViewTxt(tvSellerName, order.getSellerName(), null);
                tvPrice.setText(ShoppingCarCount.formatMoney(order.getAmount()));
                tvOperate.setVisibility(View.VISIBLE);
                tvState.setVisibility(View.GONE);
                tvOperate.setText(activity.getResources().getString(R.string.product_pay));
                tvOperate.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        index = position;
//                        orderAction.queryOrderRequest(order.getOrderId());
                        Bundle bundle = new Bundle();
                        bundle.putString("orderId", order.getOrderId());
                        callMe(ChoiceCardPayActivity.class, bundle);
                        /*orderAction.orderImmediatePay(order.getOrderId(), String.valueOf(order.getAmount()),
                                address, order.getProductList(), "1").start();*/
                    }
                });
                lvProduct.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                            long arg3) {
                        Bundle bundle = new Bundle();
                        bundle.putString(ProductInfoActivity.PRODUCT_ID, productList.get(position).getProductId());
                        callMe(ProductInfoActivity.class, bundle);
                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            list.remove(index);
//        	if(list.size() == 0){
//        		finishForBack(RESULT_OK);
//        	}else{
//        		adapter.notifyDataSetChanged();
//        	}
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * 订单支付成功
     */
    public void paySuccess(String content) {
        Bundle bundle = new Bundle();
        bundle.putString("paymentyURL", content);
        if (list.size() == 1) {
            bundle.putBoolean("gotoHomePage", true);
        }
        callMeForBack(OrderAffirmPayActivity.class, bundle, 1);
    }

    /**
     * 订单支付失败
     */
    public void payFailed(String message) {
        MyToast.showFAILED(this, message);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finishForBack(RESULT_OK);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finishForBack(RESULT_OK);
    }

}
