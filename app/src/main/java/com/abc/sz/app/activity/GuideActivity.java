package com.abc.sz.app.activity;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.adapter.MyPagerAdapter;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

@ContentView(R.layout.activity_guide)
public class GuideActivity extends ABCActivity {

    @ViewInject(R.id.vp_content) ViewPager vp_content;
    @ViewInject(R.id.rg_content) private RadioGroup rg_content;

    private List<View> views = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        for (int i = 0; i < 3; i++) {
            rg_content.addView(createRadioButton(i));
        }
        rg_content.check(0);
        vp_content.setOnPageChangeListener(new MyPageChangeListener());

        SharedPreferences sb = getPreferences(1);
        boolean isUse = sb.getBoolean("isUse", false);
        if (!isUse) {
            Editor editor = sb.edit();
            editor.putBoolean("isUse", true);
            editor.commit();
            View view1 = LayoutInflater.from(this).inflate(R.layout.layout_guide_view, null);
            View view2 = LayoutInflater.from(this).inflate(R.layout.layout_guide_view, null);
            View view3 = LayoutInflater.from(this).inflate(R.layout.layout_guide_view, null);

            ImageView imageView1 = (ImageView) view1.findViewById(R.id.iv_loading);
            ImageView imageView2 = (ImageView) view2.findViewById(R.id.iv_loading);
            ImageView imageView3 = (ImageView) view3.findViewById(R.id.iv_loading);
            ImageView imageView4 = (ImageView) view3.findViewById(R.id.iv_experience);
            imageView1.setImageResource(R.drawable.loading1);
            imageView2.setImageResource(R.drawable.loading2);
            imageView3.setImageResource(R.drawable.loading3);
            imageView4.setVisibility(View.VISIBLE);

            views.add(view1);
            views.add(view2);
            views.add(view3);
            vp_content.setAdapter(new MyPagerAdapter(views));

            imageView4.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    callMe(LoadingActivity.class);
                    finish();
                }
            });
        } else {
            callMe(LoadingActivity.class);
            finish();
        }
    }

    public RadioButton createRadioButton(int index) {
        RadioButton rb = new RadioButton(this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                32, LayoutParams.MATCH_PARENT);
        rb.setPadding(20, 0, 0, 0);
        rb.setId(index);
        rb.setClickable(false);
        rb.setLayoutParams(params);
        rb.setButtonDrawable(R.drawable.home_radio_selector);
        return rb;
    }

    public class MyPageChangeListener implements OnPageChangeListener {
        @Override
        public void onPageSelected(int index) {
            rg_content.check(index);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    }

}
