package com.abc.sz.app.bean.cridetcard;
/**
 * 车商优惠内容
 * @author ftl
 *
 */
public class CarShopPreferentialContent {

	private String title;
	private String content;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	
}
