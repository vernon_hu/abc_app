package com.abc.sz.app.adapter.order;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.ShoppingCar;
import com.abc.sz.app.bean.product.AttributeValueBean;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.util.ShoppingCarCount;
import com.forms.base.XDRImageLoader;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 购物车编辑列表适配
 *
 * @author ftl
 */
public class ShoppingCarEditAdapter extends BaseAdapter {

    private List<ShoppingCar> list;
    private Map<Integer, Boolean> map;
    private Handler handler;
    private XDRImageLoader mImageLoader;

    @SuppressLint("UseSparseArrays")
    public ShoppingCarEditAdapter(List<ShoppingCar> list, Handler handler,
                                  XDRImageLoader mImageLoader) {
        this.list = list;
        this.handler = handler;
        this.mImageLoader = mImageLoader;
        map = new HashMap<Integer, Boolean>();
        for (int i = 0; i < list.size(); i++) {
            map.put(i, false);
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public Map<Integer, Boolean> getMap() {
        return map;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ShoppingCar shoppingCar = list.get(position);
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_shopping_car_edit_item, null);
        }
        CheckBox cbChoose = ViewHolder.get(convertView, R.id.cb_choose);
        TextView tvProductName = ViewHolder.get(convertView, R.id.tv_productName);
        TextView tvProductAttribute = ViewHolder.get(convertView, R.id.tv_productAttribute);
        TextView tvProductPrice = ViewHolder.get(convertView, R.id.tv_productPrice);
        final TextView tvAmount = ViewHolder.get(convertView, R.id.tv_amount);
        Button btnReduce = ViewHolder.get(convertView, R.id.btn_reduce);
        Button btnAdd = ViewHolder.get(convertView, R.id.btn_add);

        cbChoose.setChecked(map.get(position));
        cbChoose.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (map.get(position)) {
                    map.put(position, false);
                } else {
                    map.put(position, true);
                }
                handler.sendEmptyMessage(0);
            }
        });
        if (shoppingCar != null) {
            ImageView ivProductImage = ViewHolder.get(convertView, R.id.iv_productImage);
            List<AttributeValueBean> attributeList = shoppingCar.getAttributeList();
            mImageLoader.displayImage(shoppingCar.getImageUrl(), ivProductImage, ImageViewUtil.getOption());
            FormsUtil.setTextViewTxt(tvProductName, shoppingCar.getProductName(), null);
            if(attributeList != null && attributeList.size() > 0){
            	StringBuffer sb = new StringBuffer();
            	for (int i = 0; i < attributeList.size(); i++) {
            		AttributeValueBean attributeValueBean = attributeList.get(i);
            		sb.append(attributeValueBean.getKey() + ":" + attributeValueBean.getValue() + ";");
				}
            	String attribute = sb.toString();
            	FormsUtil.setTextViewTxt(tvProductAttribute, attribute.substring(0, attribute.length() - 1));
            }
            FormsUtil.setTextViewTxt(tvAmount, shoppingCar.getAmount(), null);
            tvProductPrice.setText(ShoppingCarCount.formatMoney(shoppingCar.getProductPrice()));

            btnReduce.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    String amount = shoppingCar.getAmount();
                    if (amount != null) {
                        int number = Integer.parseInt(amount);
                        number--;
                        if (number < 1) {
                            number = 1;
                        }
                        tvAmount.setText("" + number);
                        shoppingCar.setAmount("" + number);
                    }
                }
            });

            btnAdd.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    String amount = shoppingCar.getAmount();
                    if (amount != null) {
                        int number = Integer.parseInt(amount);
                        number++;
                        tvAmount.setText("" + number);
                        shoppingCar.setAmount("" + number);
                    }
                }
            });
        }
        return convertView;
    }
}
