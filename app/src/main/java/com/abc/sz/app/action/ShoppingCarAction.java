package com.abc.sz.app.action;

import com.abc.sz.app.activity.order.ShoppingCarActivity;
import com.abc.sz.app.activity.order.ShoppingCarEditActivity;
import com.abc.sz.app.bean.ShoppingCar;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.order.QShoppingCarDelete;
import com.abc.sz.app.http.bean.order.QShoppingCarList;
import com.abc.sz.app.http.bean.order.ShoppingCarDelete;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.alibaba.fastjson.TypeReference;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;

import java.util.List;


/**
 * 购物车操作
 *
 * @author ftl
 */
public class ShoppingCarAction extends BaseAction {

    /**
     * 购物车列表查询
     *
     * @return
     */
    public Http queryShoppingCar() {
        return APPRequestPhase2.queryShoppingCar(new RequestBean(getAccessToken()),
                new ResponseNotify<List<ShoppingCar>>(new TypeReference<RespBean<List<ShoppingCar>>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<List<ShoppingCar>> response) {
                        ((ShoppingCarActivity) uiObject).querySuccess(retCode, response.getContent());
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        ((ShoppingCarActivity) uiObject).queryFailed(retCode);
                    }
                });
    }

    /**
     * 购物车更新
     *
     * @param list
     * @return
     */
    public Http updateShoppingCar(List<ShoppingCar> list) {
        QShoppingCarList qShoppingCarList = new QShoppingCarList(list);
        RequestBean<QShoppingCarList> params = new RequestBean<QShoppingCarList>(qShoppingCarList);
        params.setToken(getAccessToken());
        return APPRequestPhase2.updateShoppingCar(params,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        ((ShoppingCarEditActivity) uiObject).updateSuccess();
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        ((ShoppingCarEditActivity) uiObject).updateFailed(retCode);
                    }
                });
    }

    /**
     * 购物车商品删除
     *
     * @param list
     * @return
     */
    public Http deleteShoppingCar(List<ShoppingCarDelete> list) {
        QShoppingCarDelete qShoppingCarDelete = new QShoppingCarDelete(list);
        RequestBean<QShoppingCarDelete> params = new RequestBean<QShoppingCarDelete>(qShoppingCarDelete);
        params.setToken(getAccessToken());
        return APPRequestPhase2.deleteShoppingCar(params,
                new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
                    @Override
                    public void onResponse(RetCode retCode, RespBean<String> response) {
                        ((ShoppingCarEditActivity) uiObject).deleteSuccess();
                    }

                    @Override
                    public void onFailed(RetCode retCode) {
                        ((ShoppingCarEditActivity) uiObject).updateFailed(retCode);
                    }
                });
    }
}
