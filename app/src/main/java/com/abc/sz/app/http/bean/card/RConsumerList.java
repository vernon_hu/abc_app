package com.abc.sz.app.http.bean.card;

import java.util.List;

import com.abc.sz.app.bean.cridetcard.Consumer;

/**
 * 消费列表
 * 
 * @author ftl
 */
public class RConsumerList {

	private String cardID;// 信用卡id
	private String cardNumber;// 信用卡号
	private List<Consumer> consumerList;// 消费列表
	
	public String getCardID() {
		return cardID;
	}
	public void setCardID(String cardID) {
		this.cardID = cardID;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public List<Consumer> getConsumerList() {
		return consumerList;
	}
	public void setConsumerList(List<Consumer> consumerList) {
		this.consumerList = consumerList;
	}

}
