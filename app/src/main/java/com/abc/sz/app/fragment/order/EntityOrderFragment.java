package com.abc.sz.app.fragment.order;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.OrderAction;
import com.abc.sz.app.adapter.order.EntityOrderAdapter;
import com.abc.sz.app.bean.Order;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCFragment;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.net.ViewLoading.OnRetryListener;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.view.pullToRefresh.PullToRefreshBase;
import com.forms.view.pullToRefresh.PullToRefreshListView;
import com.forms.view.toast.MyToast;

import java.util.ArrayList;
import java.util.List;

/**
 * 实物订单
 *
 * @author ftl
 */
public class EntityOrderFragment extends ABCFragment implements PullToRefreshBase.OnLastItemVisibleListener {

    @ViewInject(R.id.lv_all_order)
    private PullToRefreshListView lv_all_order;
    @ViewInject(R.id.loadingView)
    private LoadingView loadingView;

    private List<Order> list = new ArrayList<>();
    private EntityOrderAdapter adapter;
    private OrderAction orderAction;
    private int pageNum = 0;
    private boolean isRefresh = false;
    private String state = "";
    private int fromPage = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order, container, false);
    }


    @Override
    protected void initData() {
        super.initData();
        state = getArguments().getString("state");
        fromPage = getArguments().getInt("fromPage");
        orderAction = (OrderAction) controller.getAction(baseActivity, this, OrderAction.class);
        lv_all_order.setOnLastItemVisibleListener(this);
        adapter = new EntityOrderAdapter(list, baseActivity);
        lv_all_order.setAdapter(adapter);

        queryOrderList(false);
        loadingView.retry(new OnRetryListener() {

            @Override
            public void retry() {
                onRefresh();
            }
        });
    }


    @Override
    protected void initView() {
        super.initView();
    }


    @Override
    protected void initListener() {
        super.initListener();
    }

    public void queryOrderList(boolean isPullUp) {
    	Http http = orderAction.queryOrderList(pageNum, String.valueOf(fromPage), state);
    	if(isPullUp){
    		http.start(false);
    	}else{
    		http.setLoadingView(loadingView).start();
    	}
    }

    @Override
    public void onLastItemVisible() {
        ++pageNum;
        queryOrderList(true);
    }

    public void onRefresh() {
        isRefresh = true;
        pageNum = 0;
        queryOrderList(false);
    }

    /**
     * 查询订单列表成功
     */
    public void querySuccess(RetCode retCode, List<Order> content) {
    	if (isRefresh) 
             list.clear();
        if (retCode != RetCode.noData && content != null && content.size() > 0) {
            list.addAll(content);
            adapter.notifyDataSetChanged();
        } else {
            if (list.size() > 0) {
                MyToast.showTEXT(baseActivity, getString(R.string.noMoreData));
            } else {
                loadingView.noData(getString(R.string.loadingNoData));
            }
        }
        isRefresh = false;
    }


    /**
     * 查询订单列表失败
     *
     * @param retCode
     */
    public void queryFailed(RetCode retCode) {
        if (list.size() > 0) {
            MyToast.showTEXT(baseActivity, retCode.getRetMsg());
        } else {
            loadingView.failed(retCode.getRetMsg());
        }
        isRefresh = false;
    }


}
