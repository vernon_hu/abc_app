package com.abc.sz.app.adapter.product;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import com.forms.base.ABCFragment;

import java.util.List;

public class ProductInfoAdapter extends FragmentPagerAdapter {

    private List<ABCFragment> fragments;

    public ProductInfoAdapter(FragmentActivity activity, List<ABCFragment> fragments) {
        super(activity.getSupportFragmentManager());
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return fragments.size();
    }

}
