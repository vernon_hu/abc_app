package com.abc.sz.app.activity_phase2.creditcard;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.CreditcardAction;
import com.abc.sz.app.bean.cridetcard.ConsumerDetail;
import com.abc.sz.app.util.DialogUtil;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;

/**
 * 消费分期申请页面
 * 
 * @author hkj
 */
@ContentView(R.layout.activity_phase2_consumeapply)
public class ConsumeApplyActivity extends ABCActivity {

	@ViewInject(R.id.appBar) Toolbar toolbar;
	@ViewInject(R.id.tvAccount) TextView tvAccount;
	@ViewInject(R.id.tvBillDate) TextView tvBillDate;
	@ViewInject(R.id.tvRefundDate) TextView tvRefundDate;
	@ViewInject(R.id.tvCurRefund) TextView tvCurRefund;
	@ViewInject(R.id.tvMinRefund) TextView tvMinRefund;
	@ViewInject(R.id.tvPeriods) TextView tvPeriods;
	
	private CreditcardAction creditcardAction;
	private String[] PERIODS_TYPES = { "有期", "无期", "6期" };
	private String cardNumber;
	private String consumerID;
	private String issueNumber;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		creditcardAction = (CreditcardAction) controller.getAction(this, CreditcardAction.class);
		consumerID = cacheBean.getStringCache("consumerID");
		creditcardAction.queryConsumerDetail(consumerID).start();
	}

	@OnClick({ R.id.tvPeriods, R.id.btnApply}) 
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tvPeriods:
			showListDlg();
			break;
		case R.id.btnApply:
			creditcardAction.applyConsumerInstallment(cardNumber, consumerID, issueNumber);
			break;
		}
	}
	
	private void showListDlg() {
		DialogUtil.showToSelect(this, "选择", PERIODS_TYPES, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				tvPeriods.setText(PERIODS_TYPES[which]);
				issueNumber = PERIODS_TYPES[which];
			}
			
		});
	}
	
	/**
     *消费分期详情查询成功回调
     */
    public void querySuccess(ConsumerDetail consumerDetail) {
    	if(consumerDetail != null){
    		cardNumber = consumerDetail.getCardNumber();
    		FormsUtil.setTextViewTxt(tvAccount, consumerDetail.getCardNumber());
			FormsUtil.setTextViewTxt(tvBillDate, consumerDetail.getBillOn());
			FormsUtil.setTextViewTxt(tvRefundDate, consumerDetail.getPaymentDueDate());
			FormsUtil.setTextViewTxt(tvCurRefund, consumerDetail.getRepaymentAmount());
			FormsUtil.setTextViewTxt(tvMinRefund, consumerDetail.getMinimumRepayment());
    	}
    }
    
    /**
     *申请消费分期成功回调
     */
    public void applySuccess(String result){
    	
    }
	
	
}
