package com.abc.sz.app.http.bean.personal;

/**
 * 重置用户密码
 *
 * @author ftl
 */
public class QResetPassword {
    private String phone;
    private String email;
    private String newPassWord;
    private String checkCode;

    public QResetPassword(String phone, String email, String newPassWord,
                          String checkCode) {
        super();
        this.phone = phone;
        this.email = email;
        this.newPassWord = newPassWord;
        this.checkCode = checkCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNewPassWord() {
        return newPassWord;
    }

    public void setNewPassWord(String newPassWord) {
        this.newPassWord = newPassWord;
    }

    public String getCheckCode() {
        return checkCode;
    }

    public void setCheckCode(String checkCode) {
        this.checkCode = checkCode;
    }
}
