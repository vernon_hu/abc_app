package com.abc.sz.app.fragment.order;

import com.abc.sz.app.bean.Order;
import com.forms.base.ABCActivity;
import com.forms.base.Worker;
import com.forms.base.WorkerData;
import com.forms.library.baseUtil.asynchttpclient.AsyncHttpClient;

/**
 * Created by monkey on 2015/8/26.
 */
public class QOrderStateWorker extends Worker {
    private Order order;
    private ABCActivity abcActivity;
    private AsyncHttpClient httpClient;

    public QOrderStateWorker(Order order, ABCActivity abcActivity) {
        this.order = order;
        this.abcActivity = abcActivity;

    }

    @Override
    public void doWork(WorkerData workerData) {
        super.doWork(workerData);
    }

    @Override
    public WorkerData handle(WorkerData workerData) {
        return null;
    }

    @Override
    public WorkerData failedHandle(WorkerData workerData) {
        return null;
    }
}
