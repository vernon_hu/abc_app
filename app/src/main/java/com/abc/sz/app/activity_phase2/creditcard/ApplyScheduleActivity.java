package com.abc.sz.app.activity_phase2.creditcard;

import java.io.Serializable;
import java.util.List;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action_phase2.CreditcardAction;
import com.abc.sz.app.bean.cridetcard.CardSchedule;
import com.abc.sz.app.util.DialogUtil;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;

/**
 * 办卡进度页面
 * 
 * @author hkj
 */
@ContentView(R.layout.activity_phase2_applyschedule)
public class ApplyScheduleActivity extends ABCActivity {

	@ViewInject(R.id.appBar) Toolbar toolbar;
	@ViewInject(R.id.tvCertType) TextView tvCertType;
	@ViewInject(R.id.etCertNo) EditText etCertNo;
	@ViewInject(R.id.etCustname) EditText etCustname;
	
	private CreditcardAction creditcardAction;
	private String[] certtypeKey;
    private String[] certtypeValue;
	private String carttype = "I";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	private void init() {
		certtypeKey = getResources().getStringArray(R.array.certtype_key);
		certtypeValue = getResources().getStringArray(R.array.certtype_value);
		creditcardAction = (CreditcardAction) controller.getAction(this, CreditcardAction.class);
	}

	@OnClick({ R.id.btnQuery, R.id.tvCertType })
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnQuery:
			String certno = etCertNo.getText().toString();
			String custname = etCustname.getText().toString();
			if (TextUtils.isEmpty(certno)) {
		         FormsUtil.setErrorHtmlTxt(this, etCertNo, R.string.input_username_isempty);
		    } else if (TextUtils.isEmpty(custname)) {
		         FormsUtil.setErrorHtmlTxt(this, etCustname, R.string.input_certNo_isempty);
		    } else {
		    	creditcardAction.querySchedule(carttype, certno, custname).start();
		    }
			break;
		case R.id.tvCertType:
			DialogUtil.showToSelect(this, null, certtypeValue, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					FormsUtil.setTextViewTxt(tvCertType, certtypeValue[which]);
					carttype = certtypeKey[which];
				}
				
			});
			break;
		}
	}
	
	/**
	 * 进度查询成功
	 * @param list
	 */
	public void querySuccess(List<CardSchedule> list){
		if(list != null){
			Bundle bundle = new Bundle();
			bundle.putSerializable("cardScheduleList", (Serializable)list);
			callMe(ApplyResultActivity.class, bundle);
		}
	}
}
