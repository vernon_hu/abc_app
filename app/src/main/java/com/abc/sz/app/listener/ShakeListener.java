package com.abc.sz.app.listener;


import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
/**
 * 摇一摇监听
 * @author ftl
 *
 */
public class ShakeListener implements SensorEventListener {
	
	
	//速度阀值
	private static final int SPEED_SHRESHOLD = 3000;
	//检测时间间隔
	private static final int UPTATE_INTERVAL_TIME = 100;
	
	private SensorManager sensorManager;
	//重力传感器
	private Sensor sensor;
	
	private float lastX,lastY,lastZ;
	//上次检测时间
	private long lastupdateTime;
	private Context context;
	
	private OnShakeListener onShakeListener;
	
	public ShakeListener(Context context){
		this.context =context;
		start();
	}

	public void start() {
		sensorManager = (SensorManager) context.getSystemService(context.SENSOR_SERVICE);
		if(sensorManager !=null)
			sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		if(sensor!=null)
			sensorManager.registerListener(this, sensor,SensorManager.SENSOR_DELAY_GAME);
	}
	
	public void stop()	{
		sensorManager.unregisterListener(this);
	}
	

	@Override
	public void onSensorChanged(SensorEvent event) {
		long timeInterval = System.currentTimeMillis()-lastupdateTime;
		if(timeInterval < UPTATE_INTERVAL_TIME)
			return ;
		lastupdateTime = System.currentTimeMillis();
		float x = event.values[0];
		float y = event.values[1];
		float z = event.values[2];
		
		float dx = x - lastX;
		float dy = y - lastY;
		float dz = z - lastZ;
		
		lastX = x;
		lastY = y;
		lastZ = z;
		
		double speed = Math.sqrt(dx*dx + dy*dy + dz*dz)/timeInterval*10000;
		//触发
		if(speed >= SPEED_SHRESHOLD){
			onShakeListener.onShake();
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}
	
	public interface OnShakeListener{
		public void onShake();
	}
	
	public OnShakeListener getOnShakeListener() {
		return onShakeListener;
	}

	public void setOnShakeListener(OnShakeListener onShakeListener) {
		this.onShakeListener = onShakeListener;
	}


}
