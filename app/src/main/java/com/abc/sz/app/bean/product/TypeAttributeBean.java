package com.abc.sz.app.bean.product;

import com.forms.library.base.BaseBean;

import java.util.List;

/**
 * 类别属性值
 *
 * @author llc
 */
public class TypeAttributeBean extends BaseBean {

    //属性id
    private String attributeId;
    //属性名
    private String attributeName;
    private List<AttributeValueBean> list;

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public List<AttributeValueBean> getList() {
        return list;
    }

    public void setList(List<AttributeValueBean> list) {
        this.list = list;
    }


}
