package com.abc.sz.app.bean.branch;


/**
 * 城市
 *
 * @author ftl
 */
public class City {

    /*城市id*/
    private String cityId;
    /*城市名称*/
    private String cityName;
    /*是否还有下一级  1：是  0：否*/
    private String isLast;

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getIsLast() {
        return isLast;
    }

    public void setIsLast(String isLast) {
        this.isLast = isLast;
    }

}
