package com.abc.sz.app.http.bean.lightpay;

public class QRemoveBindCard {

	private String operateType; //操作类型
    private String userId;		//光ID
    private String cardId;		//卡ID
    private String phone;		//卡绑定的手机号
    
    public QRemoveBindCard(){}
    
    public QRemoveBindCard(String operateType, String userId, String cardId) {
		super();
		this.operateType = operateType;
		this.userId = userId;
		this.cardId = cardId;
	}
    
	public QRemoveBindCard(String operateType, String userId, String cardId, String phone) {
		super();
		this.operateType = operateType;
		this.userId = userId;
		this.cardId = cardId;
		this.phone = phone;
	}
	
	public String getOperateType() {
		return operateType;
	}
	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
    
}
