package com.abc.sz.app.http.bean.personal;

/**
 * 完善用戶信息
 *
 * @author ftl
 */
public class QPerfectUserInfo {
    private String userNickName;
    private String gender;
    private String email;

    public QPerfectUserInfo(String userNickName, String gender, String email) {
        super();
        this.userNickName = userNickName;
        this.gender = gender;
        this.email = email;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
