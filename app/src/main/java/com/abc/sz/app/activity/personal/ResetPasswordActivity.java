package com.abc.sz.app.activity.personal;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.UserAction;
import com.abc.sz.app.util.InputVerifyUtil;
import com.abc.sz.app.view.CountDownButton;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.encrypt.MD5;
import com.forms.view.toast.MyToast;

/**
 * 重置密码
 *
 * @author ftl
 */
@ContentView(R.layout.activity_reset_password)
public class ResetPasswordActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.et_phone) EditText phone;
    @ViewInject(R.id.et_email) EditText email;
    @ViewInject(R.id.et_newPassword) EditText newPassword;
    @ViewInject(R.id.et_rePassword) EditText rePassword;
    @ViewInject(R.id.et_authCode) EditText authCode;
    @ViewInject(R.id.btn_authCode) CountDownButton btn_authCode;

    private UserAction userAction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        userAction = (UserAction) controller.getAction(this, UserAction.class);
    }

    @OnClick(value = {R.id.btn_confirm, R.id.btn_authCode})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_confirm:
                if (check()) {
                    userAction.resetPassword(phone.getText().toString(), email.getText().toString(),
                    		MD5.encrypt(MD5.encrypt(newPassword.getText().toString())),
                            authCode.getText().toString()).start();
                }
                break;
            case R.id.btn_authCode:
                if (TextUtils.isEmpty(phone.getText())) {
                    FormsUtil.setErrorHtmlTxt(this, phone, R.string.input_username_isempty);
                } else {
                    userAction.getResetPasswordCode(phone.getText().toString()).start();
                }
                break;
        }

    }

    private boolean check() {
        String pswd = newPassword.getText().toString();
        if (TextUtils.isEmpty(phone.getText())) {
            FormsUtil.setErrorHtmlTxt(this, phone, R.string.please_input_phone);
            return false;
        } else if (phone.getText().toString().length() != 11) {
            FormsUtil.setErrorHtmlTxt(this, phone, R.string.input_eleven_phone);
            return false;
        } else if (TextUtils.isEmpty(email.getText())) {
            FormsUtil.setErrorHtmlTxt(this, email, R.string.please_input_email);
            return false;
        } else if (!InputVerifyUtil.isEmail(email.getText().toString())) {
            FormsUtil.setErrorHtmlTxt(this, email, R.string.input_email_iserror);
            return false;
        } else if (pswd.length() < 6) {
            FormsUtil.setErrorHtmlTxt(this, newPassword, R.string.input_six_password);
            return false;
        } else if (!InputVerifyUtil.checkSecurity(newPassword.getText().toString())) {
            FormsUtil.setErrorHtmlTxt(this, newPassword, R.string.input_password_easy);
            return false;
        } else if (TextUtils.isEmpty(rePassword.getText())) {
            FormsUtil.setErrorHtmlTxt(this, rePassword, R.string.input_rePassword_isempty);
            return false;
        } else if (!pswd.equals(rePassword.getText().toString())) {
            FormsUtil.setErrorHtmlTxt(this, rePassword, R.string.input_passwdnoEqual_isempty);
            return false;
        } else if (TextUtils.isEmpty(authCode.getText())) {
        	FormsUtil.setErrorHtmlTxt(this, authCode, R.string.input_authcode_isempty);
            return false;
        }
        return true;
    }

    public void requestSuccess() {
        MyToast.show(this, "请求发送成功!", MyToast.TEXT,
                Gravity.CENTER, Toast.LENGTH_SHORT);
        btn_authCode.timer.start();
    }

}
