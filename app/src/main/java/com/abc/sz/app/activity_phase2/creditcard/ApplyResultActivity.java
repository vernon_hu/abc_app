package com.abc.sz.app.activity_phase2.creditcard;

import java.util.List;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.cridetcard.CardSchedule;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.base.BaseAdapter;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

/**
 * 办卡进度结果页面
 * 
 * @author ftl
 */
@ContentView(R.layout.activity_phase2_applyresult)
public class ApplyResultActivity extends ABCActivity {

	@ViewInject(R.id.appBar) Toolbar toolbar;
	@ViewInject(R.id.loadingView) LoadingView loadingView;
	@ViewInject(R.id.listView) ListView listView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		init();
	}

	@SuppressWarnings("unchecked")
	private void init() {
		Bundle bundle = getIntent().getExtras();
		if(bundle != null){
			List<CardSchedule> list = (List<CardSchedule>)bundle.get("cardScheduleList");
			if(list != null && list.size() > 0){
				loadingView.loaded();
				BaseAdapter<CardSchedule> adapter = new BaseAdapter<CardSchedule>(this, list, R.layout.layout_phase2_applyresult_item) {
	
					@Override
					public void viewHandler(int position, CardSchedule t, View convertView) {
						TextView tvProductName = ViewHolder.get(convertView, R.id.tvProductName);
						TextView tvApplyDate = ViewHolder.get(convertView, R.id.tvApplyDate);
						TextView tvApplyChannel = ViewHolder.get(convertView, R.id.tvApplyChannel);
						TextView tvApplyType = ViewHolder.get(convertView, R.id.tvApplyType);
						TextView tvApplyStatus = ViewHolder.get(convertView, R.id.tvApplyStatus);
						TextView tvMailcode = ViewHolder.get(convertView, R.id.tvMailcode);
						if(t != null){
							FormsUtil.setTextViewTxts(tvProductName, getString(R.string.productName), t.getCpmc() == null ? "" : t.getCpmc());
							FormsUtil.setTextViewTxts(tvApplyDate, getString(R.string.applyDate), t.getSqrq() == null ? "" : t.getSqrq());
							FormsUtil.setTextViewTxts(tvApplyChannel, getString(R.string.applyChannel), t.getSqqd() == null ? "" :t.getSqqd());
							FormsUtil.setTextViewTxts(tvApplyType, getString(R.string.applyType), t.getSqlx() == null ? "" : t.getSqlx());
							FormsUtil.setTextViewTxts(tvApplyStatus, getString(R.string.applyStatus), t.getApplyStatus() == null ? "" : t.getApplyStatus());
							FormsUtil.setTextViewTxts(tvMailcode, getString(R.string.mailcode), t.getMailcode() == null ? "" : t.getMailcode());
						}
						
					}
				};
				listView.setAdapter(adapter);
			}else{
				loadingView.noData(getString(R.string.loadingNoData));
			}
		}
	}

}
