package com.abc.sz.app.bean.loan;

/**
 * Created by hwt on 4/29/15.
 */
public class Account {
    private String cardId;//	卡 ID
    private String account;//	卡号
    private String amount;//	贷款金额
    private boolean isSelect;// 是否选中

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

	public boolean isSelect() {
		return isSelect;
	}

	public void setSelect(boolean isSelect) {
		this.isSelect = isSelect;
	}
    
}
