package com.abc.sz.app.fragment.product;

import java.util.List;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.ProductInfoAction;
import com.abc.sz.app.activity.product.ProductInfoActivity;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.product.QProductInfo;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCFragment;
import com.forms.base.XDRImageLoader;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * 商品详情
 *
 * @author llc
 */
public class ProductContentInfoFragment extends ABCFragment {

    private ProductInfoAction action;
    // 商品id
    private String productId;

    private XDRImageLoader imageLoader;
//    private MyEvaluationListView ll_content;
    @ViewInject(R.id.sv_content) ScrollView sv_content;
    @ViewInject(R.id.loadingView) LoadingView loadingView;
    @ViewInject(R.id.ll_content) LinearLayout ll_content;

//    private List<ImageView> list = new ArrayList<ImageView>();
//    private ProductImageListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_product_info_content, container, false);
        return view;
    }
    

	@Override
    protected void initData() {
        // TODO 发起请求
        super.initData();
        // 获得请求对象
//        ll_content = (MyEvaluationListView) view.findViewById(R.id.ll_content);
//        adapter = new ProductImageListAdapter(list);
//        ll_content.setAdapter(adapter);

        action = controller.getTargetAction(getABCActivity(), this, ProductInfoAction.class);
        productId = ((ProductInfoActivity) baseActivity).getProductId();

        imageLoader = baseActivity.getImageLoader();

        // 设置请求商品id
        QProductInfo productInfo = new QProductInfo();
        productInfo.setProductId(productId);
        RequestBean<QProductInfo> params = new RequestBean<QProductInfo>(productInfo);
        action.loadingInfoPicture(params).setLoadingView(loadingView).start();
    }

    @Override
    protected void initListener() {
        // TODO 滑动监听
        super.initListener();

//        ll_content.setOnScrollListener(new OnScrollListener() {
//
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//                // TODO Auto-generated method stub
//
//            }
//
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem,
//                                 int visibleItemCount, int totalItemCount) {
//                // TODO Auto-generated method stub
//                if (firstVisibleItem == 0)
//                    ProductContentFragment.isListScrolledTop = true;
//                else
//                    ProductContentFragment.isListScrolledTop = false;
//            }
//        });
        
        sv_content.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_MOVE){
					int scrollY = v.getScrollY();
					
					if (scrollY == 0)
	                    ProductContentFragment.isListScrolledTop = true;
	                else
	                    ProductContentFragment.isListScrolledTop = false;
				}
				return false;
			}
		});
    }

    /**
     * 商品图片请求回调
     *
     * @param list
     */
    public void onSuccess(List<String> list) {
    	if(list != null && list.size() > 0){
//    		this.list.clear();
	        for (int i = 0; i < list.size(); i++) {
	            ImageView iv = new ImageView(baseActivity);
	            iv.setScaleType(ScaleType.FIT_XY);
	            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 300);
                layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
                layoutParams.topMargin = 10;
                layoutParams.leftMargin = 25;
                layoutParams.rightMargin = 25;
                layoutParams.bottomMargin = 10;
                iv.setLayoutParams(layoutParams);
	            imageLoader.displayImage(list.get(i), iv, ImageViewUtil.getOption(), 1);
	            ll_content.addView(iv);
//	            this.list.add(iv);
	        }
    	}
//        adapter.notifyDataSetChanged();
    }
    
    /**
     * 请求失败回调
     *
     * @param retCode
     */
    public void failed(RetCode retCode, String requestFrom) {
        if ("loadingProductInfo".equals(requestFrom)) {

        } else if ("advSuccess".equals(requestFrom)) {

        }
    }
}
