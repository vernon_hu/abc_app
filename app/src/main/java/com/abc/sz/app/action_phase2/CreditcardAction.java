package com.abc.sz.app.action_phase2;

import java.util.List;

import android.content.Context;

import com.abc.sz.app.action.BaseAction;
import com.abc.sz.app.activity_phase2.creditcard.ApplyScheduleActivity;
import com.abc.sz.app.activity_phase2.creditcard.BillStagingShakeActivity;
import com.abc.sz.app.activity_phase2.creditcard.CarShopInfoActivity;
import com.abc.sz.app.activity_phase2.creditcard.CarStagingActivity;
import com.abc.sz.app.activity_phase2.creditcard.CarStagingListActivity;
import com.abc.sz.app.activity_phase2.creditcard.CardFavorableInfoActivity;
import com.abc.sz.app.activity_phase2.creditcard.CardFavorableListActivity;
import com.abc.sz.app.activity_phase2.creditcard.ConsumeApplyActivity;
import com.abc.sz.app.activity_phase2.creditcard.ConsumeStagingActivity;
import com.abc.sz.app.activity_phase2.creditcard.FitmentStagingActivity;
import com.abc.sz.app.activity_phase2.creditcard.MyPrerogativeActivity;
import com.abc.sz.app.activity_phase2.creditcard.PrerogativeInfoActivity;
import com.abc.sz.app.activity_phase2.wdmap.WdListActivity;
import com.abc.sz.app.activity_phase2.wdmap.WdListActivityCopy;
import com.abc.sz.app.bean.cridetcard.BillInstallments;
import com.abc.sz.app.bean.cridetcard.CarShop;
import com.abc.sz.app.bean.cridetcard.CardFavorable;
import com.abc.sz.app.bean.cridetcard.CardSchedule;
import com.abc.sz.app.bean.cridetcard.CardShop;
import com.abc.sz.app.bean.cridetcard.ConsumerDetail;
import com.abc.sz.app.bean.cridetcard.ConsumerList;
import com.abc.sz.app.bean.cridetcard.DistrictAndBrand;
import com.abc.sz.app.bean_phase2.ImageAD;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.card.QApplyConsumer;
import com.abc.sz.app.http.bean.card.QCarLoanCommitment;
import com.abc.sz.app.http.bean.card.QCarShopList;
import com.abc.sz.app.http.bean.card.QCardFavorable;
import com.abc.sz.app.http.bean.card.QCardSchedule;
import com.abc.sz.app.http.bean.card.QHouseLoanCommitment;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.alibaba.fastjson.TypeReference;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.view.toast.MyToast;

/**
 * 信用卡Action
 *
 * @author ftl
 */
public class CreditcardAction extends BaseAction {

    // 信用卡进度查询
    public Http querySchedule(String certtyp, String certno, String custname) {
    	QCardSchedule qCardSchedule = new QCardSchedule(certtyp, certno, custname, "0000");
        RequestBean<QCardSchedule> params = new RequestBean<QCardSchedule>(getAccessToken(), qCardSchedule);
        return APPRequestPhase2.XYKQuerySchedule(params, new ResponseNotify<List<CardSchedule>>(
                new TypeReference<RespBean<List<CardSchedule>>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<List<CardSchedule>> response) {
            	((ApplyScheduleActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
            	MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }
    
    // 信用卡特惠权查询
    public Http queryPreferentialRight(String idCardNum, String cardNum) {
    	QCardFavorable qCardFavorable = new QCardFavorable(idCardNum, cardNum);
        RequestBean<QCardFavorable> params = new RequestBean<QCardFavorable>(getAccessToken(), qCardFavorable);
        return APPRequestPhase2.XYKQueryPreferentialRight(params, new ResponseNotify<List<CardFavorable>>(
                new TypeReference<RespBean<List<CardFavorable>>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<List<CardFavorable>> response) {
                 ((MyPrerogativeActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 信用卡特惠权详情
    public Http queryPreferentialRightDetail(String preferentialID) {
        return APPRequestPhase2.XYKPreferentialRightDetail(getAccessToken(), preferentialID, new ResponseNotify<CardFavorable>(
                new TypeReference<RespBean<CardFavorable>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<CardFavorable> response) {
            		((PrerogativeInfoActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 信用卡优惠商户列表
    public Http queryYOUShopList(String gps, String searchText) {
        return APPRequestPhase2.XYKYOUShopList(getAccessToken(), gps, searchText, new ResponseNotify<List<CardShop>>(
                new TypeReference<RespBean<List<CardShop>>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<List<CardShop>> response) {
            		((CardFavorableListActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 信用卡优惠商户详情
    public Http queryYOUShopDetail(String shopID) {
        return APPRequestPhase2.XYKYOUShopDetail(getAccessToken(), shopID, new ResponseNotify<CardShop>(
                new TypeReference<RespBean<CardShop>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<CardShop> response) {
            		((CardFavorableInfoActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 车商查询
    public Http queryCarShopList(String pageNumber, String brand, String area, String carShop) {
    	QCarShopList qCarShopList = new QCarShopList(pageNumber, brand, area, carShop);
    	RequestBean<QCarShopList> params = new RequestBean<QCarShopList>(getAccessToken(), qCarShopList);
        return APPRequestPhase2.XYKCarShopList(params, new ResponseNotify<List<CarShop>>(
                new TypeReference<RespBean<List<CarShop>>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<List<CarShop>> response) {
            		((CarStagingListActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 车商详情
    public Http queryCarShopDetail(String carShopID) {
        return APPRequestPhase2.XYKCarShopDetail(getAccessToken(), carShopID, new ResponseNotify<CarShop>(
                new TypeReference<RespBean<CarShop>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<CarShop> response) {
            	((CarShopInfoActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 汽车分期获取短信验证
    public Http getCarSmsCheck(String phone) {
        return APPRequestPhase2.XYKCarSmsCheck(phone, new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	((CarStagingActivity) uiObject).requestSuccess();
            }

            @Override
            public void onFailed(RetCode retCode) {
            	MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }
    
    // 家装分期获取短信验证
    public Http getHomeSmsCheck(String phone) {
        return APPRequestPhase2.XYKHomeSmsCheck(phone, new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	((FitmentStagingActivity) uiObject).requestSuccess();
            }

            @Override
            public void onFailed(RetCode retCode) {
            	MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }
    
    // 汽车分期贷款额度查询
    public Http queryCarLoanCommitment(String brand, String carPrice, String phone,
			String checkCode, String agreedToCall) {
    	QCarLoanCommitment qCarLoanCommitment = new QCarLoanCommitment(brand, carPrice, phone, checkCode, agreedToCall);
    	RequestBean<QCarLoanCommitment> params = new RequestBean<QCarLoanCommitment>(getAccessToken(), qCarLoanCommitment);
        return APPRequestPhase2.XYKCarLoanCommitment(params, new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	((CarStagingActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
            	MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }
    
    // 家装分期贷款额度查询
    public Http queryHouseDecorationLoanCommitment(String district, String area, String phone,
			String checkCode, String agreedToCall) {
    	QHouseLoanCommitment qHouseLoanCommitment = new QHouseLoanCommitment(district, area, phone, checkCode, agreedToCall);
    	RequestBean<QHouseLoanCommitment> params = new RequestBean<QHouseLoanCommitment>(getAccessToken(), qHouseLoanCommitment);
        return APPRequestPhase2.XYKHouseDecorationLoanCommitment(params, new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	((FitmentStagingActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
            	MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }
    
    // 查询是否已进行账单分期
    public Http queryBillInstallments(String cardNumber) {
        return APPRequestPhase2.XYKQueryBillInstallments(getAccessToken(), cardNumber, new ResponseNotify<BillInstallments>(
                new TypeReference<RespBean<BillInstallments>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<BillInstallments> response) {
            	
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 账单分期摇一摇抽奖
    public Http queryBillInstallmentsLottery(String cardNumber) {
        return APPRequestPhase2.XYKBillInstallmentsLottery(getAccessToken(), cardNumber, new ResponseNotify<BillInstallments>(
                new TypeReference<RespBean<BillInstallments>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<BillInstallments> response) {
            	((BillStagingShakeActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
            	((BillStagingShakeActivity) uiObject).queryFail(retCode);
            }
        });
    }
    
    // 消费分期
    public Http queryConsumerInstallment() {
        return APPRequestPhase2.XYKConsumerInstallment(getAccessToken(), new ResponseNotify<List<ConsumerList>>(
                new TypeReference<RespBean<List<ConsumerList>>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<List<ConsumerList>> response) {
            	((ConsumeStagingActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 消费分期详情
    public Http queryConsumerDetail(String consumerID) {
        return APPRequestPhase2.XYKConsumerDetail(getAccessToken(), consumerID, new ResponseNotify<ConsumerDetail>(
                new TypeReference<RespBean<ConsumerDetail>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<ConsumerDetail> response) {
            	((ConsumeApplyActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 申请消费分期
    public Http applyConsumerInstallment(String cardNumber, String consumerID, String issueNumber) {
    	QApplyConsumer qApplyConsumer = new QApplyConsumer(cardNumber, consumerID, issueNumber);
    	RequestBean<QApplyConsumer> params = new RequestBean<QApplyConsumer>(getAccessToken(), qApplyConsumer);
        return APPRequestPhase2.XYKApplyConsumerInstallment(params, new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	((ConsumeApplyActivity) uiObject).applySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 现金分期
    public Http queryCashInstallment(String cardNumber) {
        return APPRequestPhase2.XYKCashInstallment(getAccessToken(), cardNumber, new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 车商区域和品牌
    public Http queryDistrictAndBrandList() {
        return APPRequestPhase2.DistrictAndBrandListAct(getAccessToken(), new ResponseNotify<DistrictAndBrand>(
                new TypeReference<RespBean<DistrictAndBrand>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<DistrictAndBrand> response) {
            	if(uiObject instanceof CarStagingListActivity){
            		((CarStagingListActivity) uiObject).districtAndBrandSuccess(response.getContent());
            	}else if(uiObject instanceof WdListActivity){
            		((WdListActivityCopy) uiObject).districtAndBrandSuccess(response.getContent());
            	}else if(uiObject instanceof CarStagingActivity){
            		((CarStagingActivity) uiObject).districtAndBrandSuccess(response.getContent());
            	}
            	
            }

            @Override
            public void onFailed(RetCode retCode) {
            	MyToast.showFAILED((Context) uiObject, retCode.getRetMsg());
            }
        });
    }
    
    // 广告请求
    public Http loadingAdv(String type) {
        return APPRequestPhase2.advQuery(type, new ResponseNotify<List<ImageAD>>(
                new TypeReference<RespBean<List<ImageAD>>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<List<ImageAD>> response) {
            	if(uiObject instanceof CarStagingActivity){
            		((CarStagingListActivity) uiObject).advSuccess(response.getContent());
            	}else if(uiObject instanceof FitmentStagingActivity){
            		((FitmentStagingActivity) uiObject).advSuccess(response.getContent());
            	}
                 
            }

            @Override
            public void onFailed(RetCode retCode) {
               
            }
        });
    }

}
