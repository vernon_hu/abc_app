package com.abc.sz.app.fragment_phase2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity.personal.MyInfoActivity;
import com.abc.sz.app.view.ShareUtil;
import com.abc.sz.app.view.drawer.DrawerItemView;
import com.abc.sz.app.view.drawer.MenuItemGroup;
import com.abc.sz.app.view.drawer.UserInfoView;
import com.forms.base.ABCActivity;
import com.forms.base.ABCFragment;
import com.forms.base.BaseConfig;
import com.forms.base.LoginUser;
import com.forms.library.baseUtil.log.LogUtils;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * Created by hwt on 4/14/15.
 */
public class MainDrawerFragment extends ABCFragment {
	
    @ViewInject(R.id.userInfoView) UserInfoView mUserInfoView;
    @ViewInject(R.id.qrCode) DrawerItemView mQrCode;
    @ViewInject(R.id.payCode) DrawerItemView mPayCode;
    @ViewInject(R.id.lightPay) DrawerItemView mLightPay;
    @ViewInject(R.id.myFavorites) DrawerItemView mMyFavorites;
    @ViewInject(R.id.myOrder) DrawerItemView mMyOrder;
    @ViewInject(R.id.payment) DrawerItemView mPayment;
    @ViewInject(R.id.setting) DrawerItemView mSetting;
    @ViewInject(R.id.share) DrawerItemView mShare;

    private MenuItemGroup mMenuItemGroup;

    /**
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maindrawer, container, false);
        return view;
    }

    /**
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mUserInfoView.ToUserInfo(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getABCActivity().isLogin()) {
                    getABCActivity().callMeForBack(MyInfoActivity.class, ABCActivity.LOGINOUT_RESULTCODE);
                }
            }
        }).setOnSignListener(new UserInfoView.OnSignListener() {
            @Override
            public void onSign(View v, long point) {
                LogUtils.d("----onSign----");
            }
        });
        mMenuItemGroup = new MenuItemGroup().setBackground(R.drawable.select_phase2_cellview_bg);
        mMenuItemGroup.addMenuItemView(mQrCode);
        mMenuItemGroup.addMenuItemView(mPayCode);
        mMenuItemGroup.addMenuItemView(mLightPay);
        mMenuItemGroup.addMenuItemView(mMyFavorites);
        mMenuItemGroup.addMenuItemView(mMyOrder);
        mMenuItemGroup.addMenuItemView(mPayment);
        mMenuItemGroup.addMenuItemView(mSetting);
        mMenuItemGroup.addMenuItemView(mShare);
//        TextView divideLine = (TextView) mShare.findViewById(R.id.divideLine);
//        divideLine.setVisibility(View.GONE);
        mMenuItemGroup.setItemClickListener(new MenuItemGroup.ItemClickListener() {
            @Override
            public void itemClick(View view, int position) {
                DrawerItemView drawerItemView = (DrawerItemView) view;
                if(drawerItemView.getTargetClass() == null){
                	 //分享
                	 if (getABCActivity().isLogin()) {
                		 ShareUtil shareUtil = new ShareUtil(getABCActivity(), view, getString(R.string.app_name), BaseConfig.APP_DOWNLOAD_URL);
                		 shareUtil.showShareWindow();
                	 }
                }else{
	                if (drawerItemView.isNeedLogin()) {
	                    if (getABCActivity().isLogin()) {
	                        startActivity(new Intent(getActivity(), drawerItemView.getTargetClass()));
	                    }
	                } else {
	                    startActivity(new Intent(getActivity(), drawerItemView.getTargetClass()));
	                }
                }
            }
        });

    }
    
    @Override
    public void onResume() {
    	super.onResume();
    	mUserInfoView.initUserInfo(getABCActivity().getBaseApp().getLoginUser());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ABCActivity.LOGIN_RESULTCODE && resultCode == Activity.RESULT_OK) {
            if (getABCActivity().isLogin()) {
                LoginUser loginUser = getABCActivity().getBaseApp().getLoginUser();
                mUserInfoView.initUserInfo(loginUser);
            }
        } else if (requestCode == ABCActivity.LOGINOUT_RESULTCODE && resultCode == Activity.RESULT_OK) {
            mUserInfoView.initUserInfo(null);
        }
    }
}
