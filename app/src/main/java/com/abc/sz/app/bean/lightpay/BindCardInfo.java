package com.abc.sz.app.bean.lightpay;

import java.io.Serializable;

/**
 * Created by hwt on 4/29/15.
 */
public class BindCardInfo implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String card_sequence;//	卡序列号
    private String hidden_card;  //	卡号
    
	public String getCard_sequence() {
		return card_sequence;
	}
	public void setCard_sequence(String card_sequence) {
		this.card_sequence = card_sequence;
	}
	public String getHidden_card() {
		return hidden_card;
	}
	public void setHidden_card(String hidden_card) {
		this.hidden_card = hidden_card;
	}
}
