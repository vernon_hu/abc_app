package com.abc.sz.app.activity.product;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.adapter.product.ProductPictureAdapter;
import com.abc.sz.app.util.ImageViewUtil;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.view.photoview.PhotoView;

import java.util.ArrayList;
import java.util.List;

/**
 * 点击查看商品大图
 *
 * @author llc
 */
@ContentView(R.layout.activity_photo_view)
public class PhotoActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    public static String URL_LIST = "URL_LIST";

    @ViewInject(R.id.iv_imageLeft)
    private ImageView iv_imageLeft;

    @ViewInject(R.id.iv_imageRight)
    private ImageView iv_imageRight;

    @ViewInject(R.id.vp_content)
    private ViewPager vp_content;

    //图片列表
    private ArrayList<String> pictureList;

    private List<View> listView = new ArrayList<View>();

    private ProductPictureAdapter apdater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initData();
        initView();
        initListener();
    }


    private void initData() {
        // TODO Auto-generated method stub


        pictureList = getIntent().getStringArrayListExtra(URL_LIST);

        for (int i = 0; i < pictureList.size(); i++) {
            PhotoView view = new PhotoView(this);
            imageLoader.displayImage(pictureList.get(i), view, ImageViewUtil.getOption());
            listView.add(view);
        }

        apdater = new ProductPictureAdapter(listView);
        vp_content.setAdapter(apdater);
    }

    private void initView() {
        // TODO Auto-generated method stub

    }

    private void initListener() {
        // TODO Auto-generated method stub
        vp_content.setOnPageChangeListener(new OnPageChangeListener() {
            boolean isScrolling = false;
            boolean right = false;
            boolean left = false;
            int lastValue = -1;
            boolean startAnimation = false;

            @Override
            public void onPageScrollStateChanged(int arg0) {
                if (arg0 == 1) {
                    isScrolling = true;
                } else {
                    isScrolling = false;
                }
                if (arg0 == 2) {
                    right = left = startAnimation = false;
                }

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                System.out.println("arg1:" + arg1 + "   arg2:" + arg2);
                if (isScrolling) {
                    if (lastValue > arg2) {
                        // 递减，向右侧滑动
                        right = true;
                        left = false;
                        if (!startAnimation) {
                            ((PhotoView) listView.get(vp_content.getCurrentItem())).setZoomable(true);
                            startAnimation = true;
                        }
                    } else if (lastValue < arg2) {
                        // 递增，向左侧滑动
                        right = false;
                        left = true;
                        if (!startAnimation) {
                            ((PhotoView) listView.get(vp_content.getCurrentItem())).setZoomable(true);
                            startAnimation = true;
                        }
                    } else if (lastValue == arg2) {
                        right = left = false;
                    }
                }
                lastValue = arg2;
            }

            @Override
            public void onPageSelected(int arg0) {
                lastValue = -1;
            }
        });
    }


}
