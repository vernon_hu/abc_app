package com.abc.sz.app.action_phase2;

import java.util.List;

import com.abc.sz.app.action.BaseAction;
import com.abc.sz.app.activity_phase2.depositloan.DepositLoanActivity;
import com.abc.sz.app.activity_phase2.depositloan.DepositLoanConfirmActivity;
import com.abc.sz.app.activity_phase2.depositloan.DepositLoanIncomeDetailActivity;
import com.abc.sz.app.activity_phase2.depositloan.DepositLoanIncomeQueryActivity;
import com.abc.sz.app.activity_phase2.depositloan.DepositLoanOpenActivity;
import com.abc.sz.app.activity_phase2.depositloan.DepositLoanVerifyCodeActivity;
import com.abc.sz.app.activity_phase2.depositloan.DepositRelateLoanActivity;
import com.abc.sz.app.activity_phase2.depositloan.DepositRelateRepayActivity;
import com.abc.sz.app.bean.loan.Account;
import com.abc.sz.app.bean.loan.DepositLoanInfo;
import com.abc.sz.app.bean_phase2.DLIncome;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.loan.QAccountDetail;
import com.abc.sz.app.http.bean.loan.QDepositLoanInfo;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.alibaba.fastjson.TypeReference;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;

/**
 * 存贷通Action
 *
 * @author ftl
 */
public class DepositloanAction extends BaseAction{

	// 查询是否符合存贷通开立条件
    public Http queryCheckUserCondition() {
        return APPRequestPhase2.CDTCheckUserCondition(getAccessToken(), new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	((DepositLoanActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 查询贷款账号列表
    public Http queryLoanAccountList(String customerNum) {
        return APPRequestPhase2.CDTLoanAccountList(getAccessToken(), customerNum, new ResponseNotify<List<Account>>(
                new TypeReference<RespBean<List<Account>>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<List<Account>> response) {
            	if(uiObject instanceof DepositLoanOpenActivity){
            		((DepositLoanOpenActivity) uiObject).querySuccess(response.getContent());
            	}else if(uiObject instanceof DepositLoanIncomeQueryActivity){
            		((DepositLoanIncomeQueryActivity) uiObject).querySuccess(response.getContent());
            	}
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 选择贷款账号开立存贷通
    public Http selectCreateAccount(String account) {
        return APPRequestPhase2.CDTCreateAccount(getAccessToken(), account, new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	((DepositLoanOpenActivity) uiObject).createSuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 还款账号交易密码校验
    public Http verifyPasswd(String customer, String passwd) {
        return APPRequestPhase2.CDTVerifyPasswd(getAccessToken(), customer, passwd,
        		new ResponseNotify<String>(new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	((DepositRelateRepayActivity) uiObject).verifyPasswdSuccess();
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 还款账号验证码校验
    public Http checkVerifyCode(String verifyCode) {
        return APPRequestPhase2.CDTCheckVerifyCode(getAccessToken(), verifyCode, new ResponseNotify<String>(
        		new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	((DepositLoanVerifyCodeActivity) uiObject).checkSuccess();
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 查询存款账号列表
    public Http queryDepositAccountList(String customerNum) {
        return APPRequestPhase2.CDTDepositAccountList(getAccessToken(), customerNum, new ResponseNotify<List<Account>>(
        		new TypeReference<RespBean<List<Account>>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<List<Account>> response) {
            	((DepositRelateRepayActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 查询其他贷款账号
    public Http queryOtherLoanAccountList(String customerNum) {
        return APPRequestPhase2.CDTOtherLoanAccountList(getAccessToken(), customerNum, new ResponseNotify<List<Account>>(
        		new TypeReference<RespBean<List<Account>>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<List<Account>> response) {
            	((DepositRelateLoanActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 账号关联信息提交
    public Http postCreateInfo(String customerNum, List<String> depositAccouts,
			List<String> loanAccounts, String payBackType) {
    	QDepositLoanInfo qDepositLoanInfo = new QDepositLoanInfo(customerNum, depositAccouts, loanAccounts, payBackType);
        RequestBean<QDepositLoanInfo> params = new RequestBean<QDepositLoanInfo>(getAccessToken(), qDepositLoanInfo);
        return APPRequestPhase2.CDTPostCreateInfo(params, new ResponseNotify<String>(
                new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	((DepositRelateLoanActivity) uiObject).postSuccess();
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 开立信息反查
    public Http queryCreateInfo(String customerNum) {
        return APPRequestPhase2.CDTQueryCreateInfo(getAccessToken(), customerNum, new ResponseNotify<DepositLoanInfo>(
        		new TypeReference<RespBean<DepositLoanInfo>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<DepositLoanInfo> response) {
            	((DepositLoanConfirmActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 开立信息确认
    public Http affirmCreateComplete(String customerNum) {
        return APPRequestPhase2.CDTCreateComplete(getAccessToken(), customerNum, new ResponseNotify<String>(
        		new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	((DepositLoanConfirmActivity) uiObject).affirmSuccess();
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 账户信息查询
    public Http queryAccount(String customerNum) {
        return APPRequestPhase2.CDTQueryAccount(getAccessToken(), customerNum, new ResponseNotify<DepositLoanInfo>(
        		new TypeReference<RespBean<DepositLoanInfo>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<DepositLoanInfo> response) {
            	
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 存贷通收益返还明细查询
    public Http queryAccountDetail(String accountNum, String startTime, String endTime) {
    	QAccountDetail qAccountDetail = new QAccountDetail(accountNum, startTime, endTime);
    	RequestBean<QAccountDetail> params = new RequestBean<QAccountDetail>(getAccessToken(), qAccountDetail);
        return APPRequestPhase2.CDTQueryAccountDetail(params, new ResponseNotify<DLIncome>(
        		new TypeReference<RespBean<DLIncome>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<DLIncome> response) {
            	((DepositLoanIncomeDetailActivity) uiObject).querySuccess(response.getContent());
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
    
    // 贷通计算器利率查询
    public Http queryCalculator() {
        return APPRequestPhase2.CDTCalculator(getAccessToken(), new ResponseNotify<String>(
        		new TypeReference<RespBean<String>>() {
                }) {
            @Override
            public void onResponse(RetCode retCode, RespBean<String> response) {
            	
            }

            @Override
            public void onFailed(RetCode retCode) {
                
            }
        });
    }
}
