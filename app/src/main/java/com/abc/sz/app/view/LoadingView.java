package com.abc.sz.app.view;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.forms.library.baseUtil.net.ViewLoading;
import com.forms.library.baseUtil.view.ViewUtils;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.tools.FormsUtil;

/**
 * Created by hwt on 14/11/20.
 */
public class LoadingView extends LinearLayout implements ViewLoading {
    @ViewInject(R.id.flloadingLayout) LinearLayout flloadingLayout;
    @ViewInject(R.id.pbLoading) LinearLayout pbLoading;
    @ViewInject(R.id.llTryAgain) LinearLayout llTryAgain;
    @ViewInject(R.id.tvFailedTips) TextView tvFailedTips;
    @ViewInject(R.id.llNoData) LinearLayout llNoData;
    @ViewInject(R.id.tvNodataText) TextView tvNodataText;
    @ViewInject(R.id.loadingImage) ImageView loadingImage;


    public final static int FAILED = -1;
    public final static int SUCCESS = 0;
    public final static int LOADING = 1;
    public final static int NODATA = 2;
    private int loadState = -1;
    private OnRetryListener mOnRetryListener;

    public LoadingView(Context context) {
        super(context);
    }

    public LoadingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isInEditMode()) return;
        View view = LayoutInflater.from(context).inflate(R.layout.layout_loadingview, this);
        ViewUtils.inject(this, view);
        loadingImage.setImageResource(R.drawable.xdr_run_animation);
        AnimationDrawable drawable = (AnimationDrawable) loadingImage.getDrawable();
        drawable.start();
    }

    public TextView getTvFailedTips() {
        return tvFailedTips;
    }

    /**
     * 设置失败时的提示文字
     *
     * @param failedTipsTxt
     */
    public void setTvFailedTips(String failedTipsTxt) {
        if (!TextUtils.isEmpty(failedTipsTxt)) {
            this.tvFailedTips.setText(failedTipsTxt);
        } else {
            this.tvFailedTips.setText(getContext().getString(R.string.defaultFailedTipsTxt));
        }
    }

    public int getLoadState() {
        return loadState;
    }

    public void setGone() {
        llNoData.setVisibility(GONE);
        pbLoading.setVisibility(GONE);
        llTryAgain.setVisibility(GONE);
        setVisibility(GONE);
    }

    @Override
    public void loading(int progress) {
        setGone();
        loadState = LOADING;
        setVisibility(VISIBLE);
        pbLoading.setVisibility(VISIBLE);
        llTryAgain.setVisibility(GONE);
    }

    @Override
    public void noData(String noDataMsg) {
        setGone();
        loadState = NODATA;
        llNoData.setVisibility(VISIBLE);
        pbLoading.setVisibility(GONE);
        llTryAgain.setVisibility(GONE);
        setVisibility(VISIBLE);
        FormsUtil.setTextViewTxt(tvNodataText, noDataMsg, null);
    }

    @Override
    public void failed(String failedMsg) {
        setGone();
        setTvFailedTips(failedMsg);
        loadState = FAILED;
        setVisibility(VISIBLE);
        pbLoading.setVisibility(GONE);
        llTryAgain.setVisibility(VISIBLE);
        if (mOnRetryListener != null) {
            flloadingLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*重试启动后重置点击事件，防止重复点击*/
                    mOnRetryListener.retry();
                    flloadingLayout.setOnClickListener(null);
                }
            });
        }
    }

    public void failed(String failedMsg, OnClickListener onClickListener) {
        setGone();
        setTvFailedTips(failedMsg);
        loadState = FAILED;
        setVisibility(VISIBLE);
        pbLoading.setVisibility(GONE);
        llTryAgain.setVisibility(VISIBLE);
        if (onClickListener != null) {
            flloadingLayout.setOnClickListener(onClickListener);
        }
    }

    @Override
    public void loaded() {
        setGone();
        loadState = SUCCESS;
        setVisibility(GONE);
    }

    @Override
    public void canceled() {

    }

    @Override
    public boolean isLoading() {
        return loadState == LOADING;
    }

    @Override
    public void retry(OnRetryListener onRetryListener) {
        mOnRetryListener = onRetryListener;
    }

    @Override
    public boolean isHasRetry() {
        return mOnRetryListener != null;
    }


}
