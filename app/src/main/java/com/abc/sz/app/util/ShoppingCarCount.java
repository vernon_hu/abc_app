package com.abc.sz.app.util;

import com.abc.sz.app.bean.ShoppingCar;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * 购物车计算
 *
 * @author ftl
 */
public class ShoppingCarCount {

    public static NumberFormat decimalFormat;


    /**
     * 计算总金额
     *
     * @param list
     * @return
     */
    public static String countTotalMoney(List<ShoppingCar> list) {
        double totalMoney = 0;
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ShoppingCar shoppingCar = list.get(i);
                String productPrice = shoppingCar.getProductPrice();
                String amount = shoppingCar.getAmount();
                if (productPrice != null && amount != null) {
                    totalMoney += Double.parseDouble(productPrice) * Double.parseDouble(amount);
                }
            }
        }
        return formatMoney(totalMoney);
    }

    /**
     * 格式化金额保留两位小数
     *
     * @param money
     * @return
     */
    public static String formatMoney(Object money) {
        if (money != null && !"".equals(money)) {
            if (decimalFormat == null) {
                decimalFormat = DecimalFormat.getCurrencyInstance(Locale.CHINA);
            }
            decimalFormat.setMaximumFractionDigits(2);
            //是string类型
            if (money instanceof String) {
                return decimalFormat.format(Double.parseDouble((String) money));
            } else {
                return decimalFormat.format(money);
            }
        } else {
            return "";
        }
    }

}
