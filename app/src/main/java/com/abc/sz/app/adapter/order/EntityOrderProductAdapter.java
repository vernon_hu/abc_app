package com.abc.sz.app.adapter.order;

import java.util.List;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.bean.product.AttributeValueBean;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.util.ShoppingCarCount;
import com.forms.base.XDRImageLoader;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

/**
 * 实物订单商品列表适配
 *
 * @author ftl
 */
public class EntityOrderProductAdapter extends BaseAdapter {

    private List<Product> list;
    private XDRImageLoader mImageLoader;

    public EntityOrderProductAdapter(List<Product> list, XDRImageLoader mImageLoader) {
        this.list = list;
        this.mImageLoader = mImageLoader;
    }

    public void setList(List<Product> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Product product = list.get(position);
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_entity_order_product_item, null);
        }
        TextView tvProductName = ViewHolder.get(convertView, R.id.tv_productName);
        TextView tvProductPrice = ViewHolder.get(convertView, R.id.tv_productPrice);
        TextView tvAttribute = ViewHolder.get(convertView, R.id.tv_attribute);
        TextView tvAmount = ViewHolder.get(convertView, R.id.tv_amount);

        if (product != null) {
            ImageView ivProductImage = ViewHolder.get(convertView, R.id.iv_productImage);
            mImageLoader.displayImage(product.getImageUrl(), ivProductImage, ImageViewUtil.getOption());
            FormsUtil.setTextViewTxt(tvProductName, product.getProductName(), null);
            FormsUtil.setTextViewTxt(tvAmount, product.getNum(), null);
            tvProductPrice.setText(ShoppingCarCount.formatMoney(product.getProductPrice()));
            List<AttributeValueBean> attributeList = product.getAttributeList();
            if (attributeList != null && attributeList.size() > 0) {
            	StringBuffer sb = new StringBuffer();
            	for (int i = 0; i < attributeList.size(); i++) {
            		AttributeValueBean attributeValueBean = attributeList.get(i);
            		sb.append(attributeValueBean.getKey() + ":" + attributeValueBean.getValue() + ";");
				}
            	String attribute = sb.toString();
            	FormsUtil.setTextViewTxt(tvAttribute, attribute.substring(0, attribute.length() - 1));
            }
        }
        return convertView;
    }

}
