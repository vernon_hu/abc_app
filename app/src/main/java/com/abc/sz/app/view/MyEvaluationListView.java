package com.abc.sz.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ListView;
import com.abc.sz.app.fragment.product.ProductContentFragment;


/**
 * 属性列表ListView
 * 解决滑动冲突
 *
 * @author llc
 */
public class MyEvaluationListView extends ListView {

    private int downY;
    private int currY;

    public MyEvaluationListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        System.out.println("lv onInterceptTouchEvent: " + event.getAction());
        downY = (int) event.getY();
        if (ProductContentFragment.isListScrolledTop && currY > 0) {
            getParent().getParent().getParent().getParent().requestDisallowInterceptTouchEvent(false);
        } else {
            getParent().getParent().getParent().getParent().requestDisallowInterceptTouchEvent(true);
        }
        return super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        System.out.println("lv onTouchEvent: " + event.getAction());
        if (getChildCount() != 0) {
            if (getChildCount() * getChildAt(0).getHeight() <= getHeight()) {
                ProductContentFragment.isListScrolledTop = true;
                return false;
            }
        } else {
            ProductContentFragment.isListScrolledTop = true;
            return false;
        }
        currY = (int) event.getY() - downY;
        System.out.println("currY:" + currY);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (currY <= 0) {
                    ProductContentFragment.isListScrolledTop = false;
                }
                break;
            case MotionEvent.ACTION_MOVE:

                if (currY < 0) {
                    getParent().requestDisallowInterceptTouchEvent(true);
                } else {
                    if (ProductContentFragment.isTop) {
                        ProductContentFragment.isListScrolledTop = true;
                    }
                    getParent().requestDisallowInterceptTouchEvent(false);
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                break;
        }
        return super.onTouchEvent(event);
    }

    public int getCurrY() {
        return currY;
    }


}
