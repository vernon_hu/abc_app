package com.abc.sz.app.activity.personal;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.UserAction;
import com.abc.sz.app.adapter.person.ConsumeAdapter;
import com.abc.sz.app.http.bean.personal.RConsume;
import com.abc.sz.app.view.DateDialog;
import com.abc.sz.app.view.LoadingView;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.log.LogUtils;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.net.ViewLoading.OnRetryListener;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.view.pullToRefresh.PullToRefreshBase;
import com.forms.view.pullToRefresh.PullToRefreshListView;
import com.forms.view.toast.MyToast;

import hirondelle.date4j.DateTime;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by hwt on 14/11/12.
 * 我的消费
 */
@ContentView(R.layout.activity_consume)
public class ConsumeActivity extends ABCActivity {
    @ViewInject(R.id.dateSelected) private LinearLayout dateSelected;
    @ViewInject(R.id.consumeListview) PullToRefreshListView consumeListview;
    @ViewInject(R.id.tv_beginDate) TextView tv_beginDate;
    @ViewInject(R.id.tv_endDate) TextView tv_endDate;
    @ViewInject(R.id.loadingView) LoadingView loadingView;
    @ViewInject(R.id.appBar) Toolbar toolbar;

    private boolean isHiden = false;
    private boolean isRefresh = false;  //触发刷新
    int tempFirst = 0;
    private ConsumeAdapter consumeAdapter;
    private List<RConsume> consumeList = new ArrayList<RConsume>();
    private NumberFormat format;
    private DateTime startDate;
    private DateTime endDate;
    private UserAction userAction;
    private Integer pageNum = 0;
    private DateDialog dateDialog;
    boolean isReload = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initData();
        initView();
        initListener();
    }

    private void initData() {
        format = DecimalFormat.getCurrencyInstance(Locale.CHINA);
        format.setMaximumFractionDigits(2);

        endDate = DateTime.now(TimeZone.getDefault());
        startDate = endDate.minus(0, 1, 0, 0, 0, 0, 0, DateTime.DayOverflow.LastDay);
        setDateStr(tv_endDate, endDate);
        setDateStr(tv_beginDate, startDate);

        userAction = (UserAction) controller.getAction(this, UserAction.class);
        userAction.queryConsumes(pageNum, tv_beginDate.getText().toString(),
                tv_endDate.getText().toString()).setLoadingView(loadingView).start();
        loadingView.retry(new OnRetryListener() {
			
			@Override
			public void retry() {
				 userAction.queryConsumes(pageNum, tv_beginDate.getText().toString(),
			                tv_endDate.getText().toString()).setLoadingView(loadingView).start();
			}
		});
    }

    private void initView() {
        consumeAdapter = new ConsumeAdapter(consumeList);
        consumeListview.setAdapter(consumeAdapter);


        consumeListview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {

            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                isRefresh = true;
                String startDateStr = startDate.format("YYYY-MM-DD");
                String endDateStr = endDate.format("YYYY-MM-DD");
                userAction.queryConsumes(++pageNum, startDateStr, endDateStr).start(false);
            }
        });

    }


    private void initListener() {


        consumeListview.setOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                System.out.println("scrollState:" + scrollState);
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub
                if (firstVisibleItem > tempFirst + 1 &&
                        (firstVisibleItem + visibleItemCount - 1 < totalItemCount) &&
                        firstVisibleItem > 1) {
                    if (!isHiden && !isRefresh) {
                        ObjectAnimator.ofFloat(dateSelected, "translationY", 0, -FormsUtil.dip2px(80)).setDuration(300).start();
                        isHiden = true;
                    }
                    tempFirst = firstVisibleItem;
                } else if ((tempFirst > 2 && firstVisibleItem < tempFirst - 1 &&
                        firstVisibleItem > 1) || firstVisibleItem == 0) {
                    if (isHiden && !isRefresh) {
                        ObjectAnimator.ofFloat(dateSelected, "translationY", -FormsUtil.dip2px(80), 0).setDuration(300).start();
                        isHiden = false;
                    }
                    tempFirst = firstVisibleItem;
                }
            }
        });

    }

    @OnClick(value = {R.id.bt_query, R.id.tv_beginDate, R.id.tv_endDate})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_query:
                if (check()) {
                    isReload = true;
                    userAction.queryConsumes(0,
                            tv_beginDate.getText().toString(),
                            tv_endDate.getText().toString()).start();
                }
                break;
            case R.id.tv_beginDate:
                dateDialog = new DateDialog(this, startDate, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startDate = dateDialog.getDateTime();
                        dateDialog.dismiss();
                        setDateStr(tv_beginDate, startDate);
                    }
                });
                dateDialog.show();
                break;
            case R.id.tv_endDate:
                dateDialog = new DateDialog(this, endDate, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        endDate = dateDialog.getDateTime();
                        dateDialog.dismiss();
                        setDateStr(tv_endDate, endDate);
                    }
                });
                dateDialog.show();
                break;
        }
    }

    private boolean check() {
        if (startDate.gt(endDate)) {
            MyToast.showTEXT(this, getString(R.string.input_startd_gt_endd));
            return false;
        }
        return true;
    }

    /**
     * 查询 完成
     *
     * @param retCode
     * @param content
     */
    public void queryFinish(RetCode retCode, List<RConsume> content) {
        isRefresh = false;
        consumeListview.onRefreshComplete();
        if (retCode != RetCode.noData &&
                content != null &&
                content.size() > 0) {
            if (isReload) {
                consumeList.clear();
                isReload = false;
            }
            consumeList.addAll(content);
            consumeAdapter.notifyDataSetChanged();
        } else {
            if (consumeList.size() > 0) {
                MyToast.showTEXT(this, getString(R.string.noMoreData));
            } else {
                LogUtils.d("*********dateSelected*****nodata************");
                ObjectAnimator.ofFloat(dateSelected, "translationY", -FormsUtil.dip2px(80), 0).setDuration(300).start();
                isHiden = false;
                loadingView.noData(getString(R.string.loadingNoData));
            }
        }
    }

    /**
     * 查询失败
     *
     * @param retCode
     */
    public void queryFailed(RetCode retCode) {
        isRefresh = false;
        consumeListview.onRefreshComplete();
        if (consumeList.size() > 0) {
            MyToast.showTEXT(this, retCode.getRetMsg());
        } else {
            LogUtils.d("*********dateSelected*****failed************");
            ObjectAnimator.ofFloat(dateSelected, "translationY", -FormsUtil.dip2px(80), 0).setDuration(300).start();
            isHiden = false;
            loadingView.failed(retCode.getRetMsg());

        }
    }

    private void setDateStr(TextView textView, DateTime dateTime) {
        String dateTimeStr = dateTime.format("YYYY-MM-DD");
        FormsUtil.setTextViewTxt(textView, dateTimeStr);
    }
}
