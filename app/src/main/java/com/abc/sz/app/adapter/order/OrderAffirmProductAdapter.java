package com.abc.sz.app.adapter.order;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity.product.ProductInfoActivity;
import com.abc.sz.app.bean.Discount;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.util.ShoppingCarCount;
import com.forms.base.XDRImageLoader;
import com.forms.library.base.BaseActivity;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 订单确认商品列表适配
 *
 * @author ftl
 */
public class OrderAffirmProductAdapter extends BaseAdapter {

    private List<Product> list;
    private Handler handler;
    private String orderId;
    private Map<String, String> map;
    private XDRImageLoader mImageLoader;
    private BaseActivity activity;

    public OrderAffirmProductAdapter(List<Product> list, Handler handler, String orderId,
                                     XDRImageLoader mImageLoader, BaseActivity activity) {
        this.list = list;
        this.handler = handler;
        this.orderId = orderId;
        this.mImageLoader = mImageLoader;
        this.activity = activity;
        map = new HashMap<String, String>();
        for (int i = 0; i < list.size(); i++) {
            map.put(list.get(i).getProductId(), list.get(i).getProductPrice());
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Product product = list.get(position);
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_order_affirm_product_item, null);
        }
        LinearLayout llProduct = ViewHolder.get(convertView, R.id.ll_product);
        TextView tvProductName = ViewHolder.get(convertView, R.id.tv_productName);
        TextView tv_productPrice = ViewHolder.get(convertView, R.id.tv_productPrice);
        TextView tvAmount = ViewHolder.get(convertView, R.id.tv_amount);
        CheckBox cbChoose = ViewHolder.get(convertView, R.id.cb_choose);
        TextView tvReason = ViewHolder.get(convertView, R.id.tv_reson);

        if (product != null) {
            ImageView ivProductImage = ViewHolder.get(convertView, R.id.iv_productImage);
            mImageLoader.displayImage(product.getImageUrl(), ivProductImage, ImageViewUtil.getOption());
            FormsUtil.setTextViewTxt(tvProductName, product.getProductName(), null);
            FormsUtil.setTextViewTxt(tvAmount, product.getNum(), null);
            tv_productPrice.setText(ShoppingCarCount.formatMoney(product.getProductPrice()));
            List<Discount> discountList = product.getList();
            if (discountList != null && discountList.get(0) != null) {
                final Discount discount = discountList.get(0);
                final String reason = discount.getReason();
                final String discountPrice = discount.getDiscountPrice();
                if (reason != null && "".equals(reason) && discountPrice != null &&
                        "".equals(discountPrice)) {
                    cbChoose.setVisibility(View.VISIBLE);
                    tvReason.setVisibility(View.VISIBLE);
                    FormsUtil.setTextViewTxt(tvReason, reason, null);
                    cbChoose.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            //选中优惠信息
                            if (isChecked) {
                                product.setProductPrice(discountPrice);
                            } else {
                                product.setProductPrice(map.get(product.getProductId()));
                            }
                            notifyDataSetChanged();
                            Message message = new Message();
                            message.obj = orderId;
                            handler.sendMessage(message);
                        }
                    });

                } else {
                    cbChoose.setVisibility(View.INVISIBLE);
                    tvReason.setVisibility(View.INVISIBLE);
                }
            }
            llProduct.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString(ProductInfoActivity.PRODUCT_ID, list.get(position).getProductId());
                    activity.callMe(ProductInfoActivity.class, bundle);
                }
            });
        }
        return convertView;
    }

}
