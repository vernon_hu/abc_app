package com.abc.sz.app.http.bean.product;


/**
 * 属性值对象
 *
 * @author ftl
 */
public class QAttributeValueBean {

    private String key;
    private String value;

    public QAttributeValueBean(String key, String value) {
        super();
        this.key = key;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

}
