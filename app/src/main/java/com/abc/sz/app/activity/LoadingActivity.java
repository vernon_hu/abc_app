package com.abc.sz.app.activity;

import android.os.AsyncTask;
import android.os.Bundle;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity_phase2.FreeWIFIActivity;
import com.abc.sz.app.activity_phase2.WebViewActivity;
import com.abc.sz.app.activity_phase2.lightpay.LockActivity;
import com.abc.sz.app.activity_phase2.wdmap.WdListActivity;
import com.abc.sz.app.util.FormsDBUtils;
import com.abc.sz.app.view.plusitemview.PlusItemBean;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.db.sqlite.Selector;

import java.util.ArrayList;
import java.util.List;

public class LoadingActivity extends ABCActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        initData();
    }


    public void initData() {
        FormsDBUtils dbUtils = getDBUtils();
        if (!dbUtils.tableIsExist(PlusItemBean.class)) {
            /*表不存在则创建表并初始化数据 */
            dbUtils.createTableIfNotExist(PlusItemBean.class);
            List<PlusItemBean> plusItemBeans = new ArrayList<>();
            PlusItemBean plusItemBean = new PlusItemBean().setIconResId(R.drawable.jrzs_lightpay)
                    .setIndex_(0).setPriority(10).setIsChecked(true).setText(getString(R.string.lightPay))
                    .setTargetClass(LockActivity.class.getName());
            plusItemBeans.add(plusItemBean);
            plusItemBean = new PlusItemBean().setIconResId(R.drawable.jrzs_wifi)
                    .setIndex_(1).setPriority(10).setIsChecked(true).setText(getString(R.string.freeWIFI))
                    .setTargetClass(FreeWIFIActivity.class.getName());
            plusItemBeans.add(plusItemBean);
            plusItemBean = new PlusItemBean().setIconResId(R.drawable.jrzs_wdmap)
                    .setIndex_(2).setPriority(10).setIsChecked(true).setText(getString(R.string.wdMap))
                    .setTargetClass(WdListActivity.class.getName());
            plusItemBeans.add(plusItemBean);
            plusItemBean = new PlusItemBean().setIconResId(R.drawable.jrzs_price)
                    .setIndex_(3).setPriority(10).setIsChecked(true).setText(getString(R.string.price))
                    .setTargetClass(WebViewActivity.class.getName());
            plusItemBeans.add(plusItemBean);
            plusItemBean = new PlusItemBean().setIconResId(R.drawable.jrzs_cal)
                    .setIndex_(4).setPriority(10).setIsChecked(true).setText(getString(R.string.calculator))
                    .setTargetClass(WebViewActivity.class.getName());
            plusItemBeans.add(plusItemBean);
            plusItemBean = new PlusItemBean().setIconResId(R.drawable.jrzs_jfy)
                    .setIndex_(5).setPriority(10).setIsChecked(true).setText(getString(R.string.broadband))
                    .setTargetClass(WebViewActivity.class.getName());
            plusItemBeans.add(plusItemBean);
            plusItemBean = new PlusItemBean().setIconResId(R.drawable.jrzs_phonepay)
                    .setIndex_(6).setPriority(10).setIsChecked(true).setText(getString(R.string.phonePay))
                    .setTargetClass(WebViewActivity.class.getName());
            plusItemBeans.add(plusItemBean);
            plusItemBean = new PlusItemBean().setIconResId(R.drawable.jrzs_phoneflow)
                    .setIndex_(7).setPriority(10).setIsChecked(true).setText(getString(R.string.phoneFlow))
                    .setTargetClass(WebViewActivity.class.getName());
            plusItemBeans.add(plusItemBean);
            plusItemBean = new PlusItemBean().setIconResId(R.drawable.jrzs_trafficabc)
                    .setIndex_(8).setPriority(10).setIsChecked(true).setText(getString(R.string.trafficabc))
                    .setTargetClass(WebViewActivity.class.getName());
            plusItemBeans.add(plusItemBean);
            dbUtils.saveBindingIdAll(plusItemBeans);
        } else {
            List<PlusItemBean> plusItemBeans = getDBUtils().findAll(Selector.from(PlusItemBean.class));
            for (int i = 0; i < plusItemBeans.size(); i++) {
                PlusItemBean plusItemBean = plusItemBeans.get(i);
                if (plusItemBean.getIndex_() == 0) {
                    plusItemBean.setIconResId(R.drawable.jrzs_lightpay);
                } else if (plusItemBean.getIndex_() == 1) {
                    plusItemBean.setIconResId(R.drawable.jrzs_wifi);
                } else if (plusItemBean.getIndex_() == 2) {
                    plusItemBean.setIconResId(R.drawable.jrzs_wdmap);
                } else if (plusItemBean.getIndex_() == 3) {
                    plusItemBean.setIconResId(R.drawable.jrzs_price);
                } else if (plusItemBean.getIndex_() == 4) {
                    plusItemBean.setIconResId(R.drawable.jrzs_cal);
                } else if (plusItemBean.getIndex_() == 5) {
                    plusItemBean.setIconResId(R.drawable.jrzs_jfy);
                } else if (plusItemBean.getIndex_() == 6) {
                    plusItemBean.setIconResId(R.drawable.jrzs_phonepay);
                } else if (plusItemBean.getIndex_() == 7) {
                    plusItemBean.setIconResId(R.drawable.jrzs_phoneflow);
                } else if (plusItemBean.getIndex_() == 8) {
                    plusItemBean.setIconResId(R.drawable.jrzs_trafficabc);
                }
            }
            dbUtils.updateAll(plusItemBeans, "iconResId");
        }

        new AsyncTask<Object, Integer, Integer>() {
            @Override
            protected Integer doInBackground(Object... params) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return 0;
            }

            @Override
            protected void onPostExecute(Integer integer) {
                callMe(com.abc.sz.app.activity_phase2.MainActivity.class);
                finish();
            }
        }.execute();
    }
}
