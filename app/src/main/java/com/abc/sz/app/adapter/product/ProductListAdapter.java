package com.abc.sz.app.adapter.product;

import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity.product.ProductInfoActivity;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.util.ShoppingCarCount;
import com.forms.base.XDRImageLoader;
import com.forms.library.base.BaseActivity;
import com.forms.library.tools.ViewHolder;

import java.util.List;

public class ProductListAdapter extends BaseAdapter {
    private List<Product> mList;
    private XDRImageLoader imageLoader;

    public ProductListAdapter(XDRImageLoader imageLoader, List<Product> list) {
        mList = list;
        this.imageLoader = imageLoader;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final Product bean = mList.get(position);

        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_product_item, null);
        }
        //图片
        ImageView ivCommodityImage = ViewHolder.get(convertView, R.id.iv_picture);
        //积分折扣
        TextView tv_jfzk = ViewHolder.get(convertView, R.id.tv_jfzk);
        //特价活动
        TextView tv_tjhd = ViewHolder.get(convertView, R.id.tv_tjhd);
        //名称
        TextView tvCommodityName = ViewHolder.get(convertView, R.id.tv_mrkb_commodityName);
        //现价
        TextView tvPrice = ViewHolder.get(convertView, R.id.tv_mrkb_price);
        //历史价格
        TextView tvHisPrice = ViewHolder.get(convertView, R.id.tv_mrkb_hisPrice);
        //收藏数量
        tvHisPrice.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        imageLoader.displayImage(bean.getImageUrl(), ivCommodityImage, ImageViewUtil.getOption());

        if (bean.getList() != null) {
            if ("1".equals(bean.getList().get(0).getPreferentialType())) {
                tv_jfzk.setVisibility(View.VISIBLE);
                tv_tjhd.setVisibility(View.GONE);
            } else if ("2".equals(bean.getList().get(0).getPreferentialType())) {
                tv_jfzk.setVisibility(View.GONE);
                tv_tjhd.setVisibility(View.VISIBLE);
            }
        } else {
            tv_jfzk.setVisibility(View.GONE);
            tv_tjhd.setVisibility(View.GONE);
        }

        tvCommodityName.setText(bean.getProductName());
        tvHisPrice.setText(ShoppingCarCount.formatMoney(bean.getHisPrice()));
        tvPrice.setText(ShoppingCarCount.formatMoney(bean.getProductPrice()));

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String productId = bean.getProductId();
                Bundle bundle = new Bundle();
                bundle.putString(ProductInfoActivity.PRODUCT_ID, productId);
                ((BaseActivity) v.getContext()).callMe(ProductInfoActivity.class, bundle);
            }
        });

        return convertView;
    }

    public void setmList(List<Product> mList) {
        this.mList = mList;
    }

    public List<Product> getmList() {
        return mList;
    }


}
