package com.abc.sz.app.activity.order;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.OrderAction;
import com.abc.sz.app.adapter.order.ProductEvaluateAdapter;
import com.abc.sz.app.bean.Evaluate;
import com.abc.sz.app.bean.Order;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.util.DialogUtil;
import com.forms.base.ABCActivity;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.view.toast.MyToast;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品评价
 *
 * @author ftl
 */
@ContentView(R.layout.activity_product_evaluate)
public class ProductEvaluateActivity extends ABCActivity {
    @ViewInject(R.id.appBar) Toolbar toolbar;
    @ViewInject(R.id.lv_evaluate)
    private ListView lv_evaluate;

    private ProductEvaluateAdapter adapter;
    private List<Product> list = new ArrayList<Product>();
    private List<Evaluate> evaluateList = new ArrayList<Evaluate>();
    private Order order;
    private OrderAction orderAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        orderAction = (OrderAction) controller.getAction(this, OrderAction.class);
        initData();
    }

    /**
     * 初始化数据
     */
    private void initData() {

        order = getCacheBean().getCache(Order.class);
        if (order != null) {
            list = order.getProductList();
        }
        adapter = new ProductEvaluateAdapter(list, evaluateList, this, imageLoader);
        lv_evaluate.setAdapter(adapter);
    }


    @OnClick(R.id.btn_evaluate)
    public void btn_evaluate(View view) {
        if (order != null) {
            boolean flag = true;
            for (int i = 0; i < evaluateList.size(); i++) {
                String starNum = evaluateList.get(i).getStarNum();
                if ("0".equals(starNum)) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                orderAction.evaluateOrder(order.getOrderId(), evaluateList).start();
            } else {
                MyToast.show(this, "请给每个商品评分", MyToast.TEXT, Gravity.CENTER, Toast.LENGTH_SHORT);
            }
        }
    }

    /**
     * 评价成功
     */
    public void evaluateSuccess() {
        finishForBack(RESULT_OK);
        MyToast.show(this, "评价成功!", MyToast.TEXT, Gravity.CENTER, Toast.LENGTH_SHORT);
    }

    /**
     * 评价失败
     *
     * @param retCode
     */
    public void evaluateFailed(RetCode retCode) {
        DialogUtil.showWithNoTitile(this, retCode.getRetMsg());
    }
}
