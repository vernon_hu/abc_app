package com.abc.sz.app.bean.cridetcard;

import java.util.List;

/**
 * 车商
 * 
 * @author ftl
 */
public class CarShop {

    private String carShopID;//车商id
    private String carShopName;//车商店名
    private String carShopAddress;//车商地址
    private String carShopIconUrl;//车商icon url
    private List<CarShopPreferentialContent> carShopPreferentialContent;//车商优惠内容
    private String carShopPhone;//车商联系电话
    private String carShopPoundage;//手续费
    private String carShopDetailInformation;//车商详细信息
    
	public String getCarShopID() {
		return carShopID;
	}
	public void setCarShopID(String carShopID) {
		this.carShopID = carShopID;
	}
	public String getCarShopName() {
		return carShopName;
	}
	public void setCarShopName(String carShopName) {
		this.carShopName = carShopName;
	}
	public String getCarShopAddress() {
		return carShopAddress;
	}
	public void setCarShopAddress(String carShopAddress) {
		this.carShopAddress = carShopAddress;
	}
	public String getCarShopIconUrl() {
		return carShopIconUrl;
	}
	public void setCarShopIconUrl(String carShopIconUrl) {
		this.carShopIconUrl = carShopIconUrl;
	}
	public List<CarShopPreferentialContent> getCarShopPreferentialContent() {
		return carShopPreferentialContent;
	}
	public void setCarShopPreferentialContent(List<CarShopPreferentialContent> carShopPreferentialContent) {
		this.carShopPreferentialContent = carShopPreferentialContent;
	}
	public String getCarShopPhone() {
		return carShopPhone;
	}
	public void setCarShopPhone(String carShopPhone) {
		this.carShopPhone = carShopPhone;
	}
	public String getCarShopPoundage() {
		return carShopPoundage;
	}
	public void setCarShopPoundage(String carShopPoundage) {
		this.carShopPoundage = carShopPoundage;
	}
	public String getCarShopDetailInformation() {
		return carShopDetailInformation;
	}
	public void setCarShopDetailInformation(String carShopDetailInformation) {
		this.carShopDetailInformation = carShopDetailInformation;
	}
    
}
