package com.abc.sz.app.util;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.forms.library.baseUtil.db.DbUtils;
import com.forms.library.baseUtil.db.sqlite.DbModelSelector;
import com.forms.library.baseUtil.db.sqlite.Selector;
import com.forms.library.baseUtil.db.sqlite.SqlInfo;
import com.forms.library.baseUtil.db.sqlite.WhereBuilder;
import com.forms.library.baseUtil.db.table.DbModel;
import com.forms.library.baseUtil.exception.DbException;
import com.forms.library.baseUtil.log.LogUtils;

import java.util.List;

/**
 * Created by huwentao on 2014/5/20.
 */
public class FormsDBUtils {
    private static FormsDBUtils formsDBUtils = null;
    private DbUtils dbUtils;

    private FormsDBUtils(Context context, DbUtils.DbUpgradeListener upgradeListener) {
        dbUtils = DbUtils.create(context, FormsConfig.dbName, FormsConfig.dbVersion, upgradeListener);
    }

    public static FormsDBUtils getInstance(Context context, DbUtils.DbUpgradeListener upgradeListener) {
        if (formsDBUtils == null) {
            formsDBUtils = new FormsDBUtils(context, upgradeListener);
        }
        return formsDBUtils;
    }


    public DbUtils configDebug(boolean debug) {
        return dbUtils.configDebug(debug);
    }


    public DbUtils configAllowTransaction(boolean allowTransaction) {
        return dbUtils.configAllowTransaction(allowTransaction);
    }


    public SQLiteDatabase getDatabase() {
        return dbUtils.getDatabase();
    }


    public DbUtils.DaoConfig getDaoConfig() {
        return dbUtils.getDaoConfig();
    }


    public void saveOrUpdate(Object entity) {
        try {
            dbUtils.saveOrUpdate(entity);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public void replace(Object entity) {
        try {
            dbUtils.replace(entity);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }

    public void save(Object entity) {
        try {
            dbUtils.save(entity);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public boolean saveBindingId(Object entity) {
        try {
            return dbUtils.saveBindingId(entity);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
        return false;
    }


    public void saveBindingIdAll(List<?> entities) {
        try {
            dbUtils.saveBindingIdAll(entities);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public void deleteById(Class<?> entityType, Object idValue) {
        try {
            dbUtils.deleteById(entityType, idValue);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public void delete(Object entity) {
        try {
            dbUtils.delete(entity);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public void delete(Class<?> entityType, WhereBuilder whereBuilder) {
        try {
            dbUtils.delete(entityType, whereBuilder);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public void deleteAll(List<?> entities) {
        try {
            dbUtils.deleteAll(entities);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public void deleteAll(Class<?> entityType) {
        try {
            dbUtils.deleteAll(entityType);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public void update(Object entity, String... updateColumnNames) {
        try {
            dbUtils.update(entity, updateColumnNames);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public void update(Object entity, WhereBuilder whereBuilder, String... updateColumnNames) {
        try {
            dbUtils.update(entity, whereBuilder, updateColumnNames);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public void updateAll(List<?> entities, String... updateColumnNames) {
        try {
            dbUtils.updateAll(entities, updateColumnNames);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public void updateAll(List<?> entities, WhereBuilder whereBuilder, String... updateColumnNames) {
        try {
            dbUtils.updateAll(entities, whereBuilder, updateColumnNames);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public <T> T findById(Class<T> entityType, Object idValue) {
        try {
            return dbUtils.findById(entityType, idValue);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
        return null;
    }


    public <T> T findFirst(Selector selector) {
        try {
            return dbUtils.findFirst(selector);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
        return null;
    }


    public <T> T findFirst(Class<T> entityType) {
        try {
            return dbUtils.findFirst(entityType);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
        return null;
    }


    public <T> List<T> findAll(Selector selector) {
        try {
            return dbUtils.findAll(selector);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
        return null;
    }


    public <T> List<T> findAll(Class<T> entityType) {
        try {
            return dbUtils.findAll(entityType);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
        return null;
    }


    public DbModel findDbModelFirst(SqlInfo sqlInfo) {
        try {
            return dbUtils.findDbModelFirst(sqlInfo);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
        return null;
    }


    public DbModel findDbModelFirst(DbModelSelector selector) {
        try {
            return dbUtils.findDbModelFirst(selector);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
        return null;
    }


    public List<DbModel> findDbModelAll(SqlInfo sqlInfo) {
        try {
            return dbUtils.findDbModelAll(sqlInfo);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
        return null;
    }


    public List<DbModel> findDbModelAll(DbModelSelector selector) {
        try {
            return dbUtils.findDbModelAll(selector);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
        return null;
    }


    public long count(Selector selector) {
        try {
            return dbUtils.count(selector);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
        return -1;
    }


    public long count(Class<?> entityType) {
        try {
            return dbUtils.count(entityType);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
        return -1;
    }


    public void createTableIfNotExist(Class<?> entityType) {
        try {
            dbUtils.createTableIfNotExist(entityType);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public boolean tableIsExist(Class<?> entityType) {
        try {
            return dbUtils.tableIsExist(entityType);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
        return false;
    }


    public void dropDb() {
        try {
            dbUtils.dropDb();
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public void dropTable(Class<?> entityType) {
        try {
            dbUtils.dropTable(entityType);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public void close() {
        dbUtils.close();
    }


    public void execNonQuery(SqlInfo sqlInfo) {
        try {
            dbUtils.execNonQuery(sqlInfo);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public void execNonQuery(String sql) {
        try {
            dbUtils.execNonQuery(sql);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
    }


    public Cursor execQuery(SqlInfo sqlInfo) {
        try {
            return dbUtils.execQuery(sqlInfo);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
        return null;
    }


    public Cursor execQuery(String sql) {
        try {
            return dbUtils.execQuery(sql);
        } catch (DbException e) {
            LogUtils.d(e.getMessage(), e);
        }
        return null;
    }
}
