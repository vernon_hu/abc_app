package com.abc.sz.app.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.activity.product.ProductInfoActivity;
import com.abc.sz.app.activity.product.ProductListActivity;
import com.abc.sz.app.bean.Product;
import com.abc.sz.app.bean.product.ProductListBean;
import com.abc.sz.app.util.ImageViewUtil;
import com.abc.sz.app.util.ShoppingCarCount;
import com.forms.base.XDRImageLoader;
import com.forms.library.base.BaseActivity;
import com.forms.library.tools.ViewHolder;

import java.util.List;

/**
 * 掌上商城商品列表适配
 *
 * @author user
 */
public class CommodityZsscAdapter extends BaseAdapter {

    private List<ProductListBean> commodityList;
    private XDRImageLoader imageLoader;

    public CommodityZsscAdapter(XDRImageLoader imageLoader, List<ProductListBean> commodityList) {
        this.commodityList = commodityList;
        this.imageLoader = imageLoader;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return commodityList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return commodityList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ProductListBean bean = commodityList.get(position);
        final List<Product> list = bean.getTypeList();
        final Context context = parent.getContext();
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.layout_zssc_item, null);
        }
        //左侧商品
        LinearLayout ll_left_item = ViewHolder.get(convertView, R.id.ll_left_item);
        //右侧商品
        LinearLayout ll_right_item = ViewHolder.get(convertView, R.id.ll_right_item);
        //更多
        TextView tv_more = ViewHolder.get(convertView, R.id.tv_more);
        //类别
        TextView tv_type_name = ViewHolder.get(convertView, R.id.tv_type_name);

        //左侧商品图片
        ImageView iv_zssc_commodity_image1 = ViewHolder.get(convertView, R.id.iv_zssc_commodity_image1);
        //左侧商品名称
        TextView tv_left_name = ViewHolder.get(convertView, R.id.tv_left_name);
        //左侧商品描述
        TextView tv_left_description = ViewHolder.get(convertView, R.id.tv_left_description);
        //左侧商品价格
        TextView tv_left_price = ViewHolder.get(convertView, R.id.tv_left_price);

        //右侧商品图片
        ImageView iv_zssc_commodity_image2 = ViewHolder.get(convertView, R.id.iv_zssc_commodity_image2);
        //右侧商品名称
        TextView tv_right_name = ViewHolder.get(convertView, R.id.tv_right_name);
        //右侧商品描述
        TextView tv_right_description = ViewHolder.get(convertView, R.id.tv_right_description);
        //右侧商品价格
        TextView tv_right_price = ViewHolder.get(convertView, R.id.tv_right_price);


        tv_type_name.setText(bean.getTypeName());
        if (list.size() > 1) {
            imageLoader.displayImage(list.get(0).getImageUrl(), iv_zssc_commodity_image1, ImageViewUtil.getOption());
            tv_left_name.setText(list.get(0).getProductName());
            tv_left_description.setText(list.get(0).getDescription());
            tv_left_price.setText(ShoppingCarCount.formatMoney(list.get(0).getProductPrice()));

            imageLoader.displayImage(list.get(1).getImageUrl(), iv_zssc_commodity_image2, ImageViewUtil.getOption());
            tv_right_name.setText(list.get(1).getProductName());
            tv_right_description.setText(list.get(1).getDescription());
            tv_right_price.setText(ShoppingCarCount.formatMoney(list.get(1).getProductPrice()));

            ll_right_item.setVisibility(View.VISIBLE);
        } else if (list.size() > 0) {
            imageLoader.displayImage(list.get(0).getImageUrl(), iv_zssc_commodity_image1, ImageViewUtil.getOption());
            tv_left_name.setText(list.get(0).getProductName());
            tv_left_description.setText(list.get(0).getDescription());
            tv_left_price.setText(ShoppingCarCount.formatMoney(list.get(0).getProductPrice()));

            ll_right_item.setVisibility(View.INVISIBLE);
        }


        ll_left_item.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO 左侧Item点击事件
                String productId = list.get(0).getProductId();
                Bundle bundle = new Bundle();
                bundle.putString(ProductInfoActivity.PRODUCT_ID, productId);
                ((BaseActivity) context).callMe(ProductInfoActivity.class, bundle);
            }
        });
        ll_right_item.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO 右侧Item点击事件
                String productId = list.get(1).getProductId();
                Bundle bundle = new Bundle();
                bundle.putString(ProductInfoActivity.PRODUCT_ID, productId);
                ((BaseActivity) context).callMe(ProductInfoActivity.class, bundle);
            }
        });

        tv_more.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String typeId = bean.getTypeId();
                Bundle bundle = new Bundle();
                bundle.putString(ProductListActivity.TYPE_ID, typeId);
                ((BaseActivity) context).callMe(ProductListActivity.class, bundle);
            }
        });

        return convertView;
    }

    public void setCommodityList(List<ProductListBean> commodityList) {
        this.commodityList = commodityList;
    }


}
