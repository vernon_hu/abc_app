package com.abc.sz.app.activity.order;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.abc.ABC_SZ_APP.R;
import com.abc.sz.app.action.OrderAction;
import com.abc.sz.app.action.UserAction;
import com.abc.sz.app.activity.personal.BindNewCardActivity;
import com.abc.sz.app.http.bean.personal.RUserCard;
import com.abc.sz.app.util.DialogUtil;
import com.forms.base.ABCActivity;
import com.forms.base.BaseAdapter;
import com.forms.library.baseUtil.net.Controller;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.HttpQueue;
import com.forms.library.baseUtil.net.RetCode;
import com.forms.library.baseUtil.view.annotation.ContentView;
import com.forms.library.baseUtil.view.annotation.ViewInject;
import com.forms.library.baseUtil.view.annotation.event.OnClick;
import com.forms.library.tools.FormsUtil;
import com.forms.library.tools.ViewHolder;

import java.util.ArrayList;
import java.util.List;

@ContentView(R.layout.activity_choice_card_pay)
public class ChoiceCardPayActivity extends ABCActivity {
    @ViewInject(R.id.cardNum) TextView cardNum;
    @ViewInject(R.id.appBar) Toolbar toolbar;

    private AlertDialog choiceCardDialog;
    private BaseAdapter<RUserCard> dialogAdapter;
    private List<RUserCard> mRUserCards = new ArrayList<>();
    private String orderId;
    private int choicePosition = 0;
    private UserAction userAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        userAction = (UserAction) getController().getAction(this, UserAction.class);
        orderId = getIntent().getStringExtra("orderId");

        dialogAdapter = new BaseAdapter<RUserCard>(
                this, mRUserCards, R.layout.layout_phase2_district_brand_item) {
            @Override
            public void viewHandler(int position, RUserCard rUserCard, View convertView) {
                TextView textView = ViewHolder.get(convertView, R.id.tvContent);
                FormsUtil.setTextViewTxt(textView, rUserCard.getCardNum());
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        mRUserCards.clear();
        userAction.queryBindCardList().start(true);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (choiceCardDialog != null && choiceCardDialog.isShowing()) {
            choiceCardDialog.dismiss();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_choice_card_pay, menu);
        return true;
    }

    /**
     * Card info query success
     *
     * @param retCode reture Code
     * @param content User's Card Info
     */
    public void loadedCardList(RetCode retCode, List<RUserCard> content) {
        if (retCode != RetCode.noData && content.size() > 0) {
            mRUserCards.addAll(content);
            dialogAdapter.notifyDataSetChanged();
            choiceCardDialog = getChoiceCardDialog();
        } else {
            callMe(BindNewCardActivity.class);
            finish();
        }
    }

    /**
     * 查询卡列表失败
     *
     * @param retCode
     */
    public void loadedCardListFailed(RetCode retCode) {
        DialogUtil.showWithOneBtn(this, getString(R.string.reTryLoading),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
    }

    /**
     * 创建卡选择列表
     *
     * @return
     */
    public AlertDialog getChoiceCardDialog() {
        return new AlertDialog.Builder(this)
                .setTitle(getString(R.string.choiceCardNum))
                .setAdapter(dialogAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        RUserCard rUserCard = mRUserCards.get(which);
                        FormsUtil.setTextViewTxt(cardNum, rUserCard.getCardNum());
                        choicePosition = which;
                        dialog.dismiss();
                    }
                }).create();
    }

    @OnClick(R.id.cardNum)
    public void choiceCardNum(View view) {
        if (choiceCardDialog != null && dialogAdapter.getCount() > 0) {
            if (!choiceCardDialog.isShowing()) {
                choiceCardDialog.show();
            }
        }
    }

    @OnClick(R.id.choicePayCard)
    public void choicePayCard(View view) {

    }


}
