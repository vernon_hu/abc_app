package com.abc.sz.app.view.plusitemview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.abc.ABC_SZ_APP.R;
import com.forms.library.baseUtil.view.ViewUtils;
import com.forms.library.baseUtil.view.annotation.ViewInject;

/**
 * Created by hwt on 4/27/15.
 */
public class PlusItemView extends LinearLayout {
    @ViewInject(R.id.itemIcon) ImageView mItemIcon;
    @ViewInject(R.id.itemText) TextView mItemText;
    @ViewInject(R.id.itemChecked) CheckBox mItemChecked;

    private PlusItemBean plusItemBean;

    public PlusItemView(Context context) {
        super(context);
    }

    public PlusItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_plusitemview, this);
        ViewUtils.inject(this, view);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.menuItemStyle);
        Drawable drawable = typedArray.getDrawable(R.styleable.menuItemStyle_itemIcon);
        if (drawable != null) {
            mItemIcon.setImageDrawable(drawable);
        }
        String text = typedArray.getString(R.styleable.menuItemStyle_itemText);
        if (!TextUtils.isEmpty(text)) {
            mItemText.setText(text);
        }
        boolean flag = typedArray.getBoolean(R.styleable.menuItemStyle_itemChecked, false);
        mItemChecked.setChecked(flag);
        typedArray.recycle();
    }

    public ImageView getmItemIcon() {
        return mItemIcon;
    }

    public void setItemIcon(int iconResId) {
        if (iconResId > 0) {
            mItemIcon.setImageResource(iconResId);
        }
    }

    public TextView getItemText() {
        return mItemText;
    }

    public void setItemText(String itemText) {
        if (!TextUtils.isEmpty(itemText)) {
            mItemText.setText(itemText);
        }
    }

    public CheckBox getItemChecked() {
        return mItemChecked;
    }

    public void setItemChecked(boolean itemChecked) {
        mItemChecked.setChecked(itemChecked);
    }

    public PlusItemBean getPlusItemBean() {
        return plusItemBean;
    }

    public void setPlusItemBean(PlusItemBean plusItemBean) {
        this.plusItemBean = plusItemBean;
        if (plusItemBean != null) {
            setItemIcon(plusItemBean.getIconResId());
            setItemText(plusItemBean.getText());
            setItemChecked(plusItemBean.isChecked());
        }
    }
}
