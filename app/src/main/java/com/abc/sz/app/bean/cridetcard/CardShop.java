package com.abc.sz.app.bean.cridetcard;

/**
 * 信用卡优惠商户
 * 
 * @author ftl
 */
public class CardShop {

    private String shopID;//商户id
    private String shopName;//商户店名
    private String shopAddress;//商户地址
    private String shopIconUrl;//商户icon url
    private String shopDistance;//商户距离
    private String shopPhone;//商户联系电话
    private String shopPreferentialContent;//优惠内容
    
	public String getShopID() {
		return shopID;
	}
	public void setShopID(String shopID) {
		this.shopID = shopID;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	public String getShopIconUrl() {
		return shopIconUrl;
	}
	public void setShopIconUrl(String shopIconUrl) {
		this.shopIconUrl = shopIconUrl;
	}
	public String getShopDistance() {
		return shopDistance;
	}
	public void setShopDistance(String shopDistance) {
		this.shopDistance = shopDistance;
	}
	public String getShopPhone() {
		return shopPhone;
	}
	public void setShopPhone(String shopPhone) {
		this.shopPhone = shopPhone;
	}
	public String getShopPreferentialContent() {
		return shopPreferentialContent;
	}
	public void setShopPreferentialContent(String shopPreferentialContent) {
		this.shopPreferentialContent = shopPreferentialContent;
	}
    
}
