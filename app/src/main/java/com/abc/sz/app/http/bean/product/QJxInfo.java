package com.abc.sz.app.http.bean.product;

/**
 * 精选详情查询
 *
 * @author llc
 */
public class QJxInfo {
    //商品ID
    private String dailyId;

    public String getDailyId() {
        return dailyId;
    }

    public void setDailyId(String dailyId) {
        this.dailyId = dailyId;
    }


}
