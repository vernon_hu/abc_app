package com.abc.sz.app.bean;

import java.io.Serializable;

/**
 * 网点地图
 * @author ftl
 *
 */
public class WdMap implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String branchName;//网点名称
	private String address;	  //地址
	private String phone;	  //电话
	private String distance;  //距离
	private String lon; 	  //经度
	private String lat;       //纬度
	
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	
}
