package com.abc.sz.app.listener;

import android.view.View;

/**
 * Created by hwt on 14/11/12.
 */
public abstract class ListClickListener implements View.OnClickListener {
    private int position;

    public ListClickListener(int position) {
        this.position = position;
    }

    @Override
    public void onClick(View v) {
        listClick(v, position);
    }

    public abstract void listClick(View v, int position);
}
