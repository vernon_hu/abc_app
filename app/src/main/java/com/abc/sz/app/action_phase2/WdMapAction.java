package com.abc.sz.app.action_phase2;

import java.util.List;

import com.abc.sz.app.action.BaseAction;
import com.abc.sz.app.activity_phase2.wdmap.WdListActivity;
import com.abc.sz.app.activity_phase2.wdmap.WdListActivityCopy;
import com.abc.sz.app.bean.WdMap;
import com.abc.sz.app.http.bean.RequestBean;
import com.abc.sz.app.http.bean.wdmap.QWdMap;
import com.abc.sz.app.http.request.APPRequestPhase2;
import com.alibaba.fastjson.TypeReference;
import com.forms.library.baseUtil.net.Http;
import com.forms.library.baseUtil.net.RespBean;
import com.forms.library.baseUtil.net.ResponseNotify;
import com.forms.library.baseUtil.net.RetCode;

/**
 * 网点Action
 *
 * @author ftl
 */
public class WdMapAction extends BaseAction {
	
	 // 网点查询
    public Http queryNetPointList(String custname, String area, String distance, String lon,
			String lat, String pageNum) {
    	QWdMap qWdMap = new QWdMap(custname, area, distance, lon, lat, pageNum);
    	RequestBean<QWdMap> params = new RequestBean<QWdMap>(qWdMap);
        return APPRequestPhase2.NetPointQueryListAct(params, new ResponseNotify<List<WdMap>>(
        		new TypeReference<RespBean<List<WdMap>>>() {
                }) {
			
			@Override
			public void onResponse(RetCode retCode, RespBean<List<WdMap>> response) {
				((WdListActivityCopy) uiObject).querySuccess(response.getContent());
			}
			
			@Override
			public void onFailed(RetCode retCode) {
				
			}
		});
    }

}
